import React, {Component} from 'react';
import '@blueprintjs/core/dist/blueprint.css';

class AdminNavBar extends Component {
    render() {
        return (
            <nav className="pt-navbar pt-dark">
                <div className="pt-navbar-group pt-align-left">
                    <div className="pt-navbar-heading">Dashboard</div>
                    <div className="pt-navbar-heading">Logs</div>
                </div>
            </nav>
        );
    }
}

export default AdminNavBar;