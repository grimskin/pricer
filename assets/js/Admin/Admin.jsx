import React, {Component} from 'react';
import AdminNavBar from './AdminNavBar';
import axios from 'axios';
import '@blueprintjs/core/dist/blueprint.css';

class Admin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tasks: []
        };
    }

    componentDidMount() {
        axios.get(`/api/be/dashboard`)
            .then(res => {
                const tasks = res.data.tasks;
                this.setState({ 'tasks': tasks });
            });
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <div className="layout">
                        <AdminNavBar/>
                    </div>
                </div>

                <br />

                <div id="admin-dashboard">
                    <div className="pt-card pt-elevation-1">
                        <h5>Tasks</h5>
                        {this.state.tasks.map(entry => {
                            if (entry.count>0)
                                return <p key={entry.alias}>
                                    <a href={entry.url}>{entry.caption} {entry.count}</a>
                                </p>
                            }
                        )}
                    </div>

                    <div className="pt-card pt-elevation-1">
                        <h5>Placeholder for second card</h5>
                        <p>Lorem Ipsum, bitches</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Admin;