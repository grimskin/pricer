import React, { Component } from 'react';
import '@blueprintjs/core/dist/blueprint.css';
import LogList from "./LogList";

class App extends Component {
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <div className="layout">
                        <nav className="pt-navbar pt-dark">
                            <div className="pt-navbar-group pt-align-left">
                                <div className="pt-navbar-heading">Logs</div>
                            </div>
                        </nav>
                    </div>
                </div>
                <LogList />
            </div>
        );
    }
}

export default App;