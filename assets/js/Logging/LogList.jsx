import React, { Component } from 'react';
import axios from 'axios';

class LogList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            entries: []
        };
    }

    componentDidMount() {
        axios.get(`/api/logs/`)
            .then(res => {
                const entries = res.data.logs;
                this.setState({ 'entries': entries });
            });
    }

    render() {
        return (
            <div className="LogList">
                <table className="pt-table .pr-bordered">
                    <thead>
                    <tr>
                        <th>channel</th>
                        <th>level</th>
                        <th>date</th>
                        <th>message</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.entries.map(entry =>
                        <tr key={entry.id}>
                            <td>{entry.channel}</td>
                            <td>{entry.level}</td>
                            <td>{entry.createdAt}</td>
                            <td>{entry.message}</td>
                        </tr>
                    )}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default LogList;