<?php

namespace Tests\DataCollectingBundle\Document;

use DataCollectingBundle\Document\ShopCategory;
use PHPUnit\Framework\TestCase;

class ShopCategoryTest extends TestCase
{
    /**
     * @test
     * @dataProvider levelCalculationProvider
     * @param string $url
     * @param int    $expectedLevel
     */
    public function shouldProperlyCalculateLevel($url, $expectedLevel)
    {
        $document = new ShopCategory();
        $document->setRelativePageUrl($url);
        $this->assertEquals($expectedLevel, $document->getLevel());
    }

    /**
     * @return array
     */
    public function levelCalculationProvider()
    {
        return [
            [ '/', 0 ],
            [ 'category', 1 ],
            [ '/category', 1 ],
            [ '/category/', 1 ],
            [ '/two/level/', 2 ],
            [ '/two/level//', 2 ],
        ];
    }
}
