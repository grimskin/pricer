<?php

namespace Tests\ShopParsingBundle\Service;

use ShopParsingBundle\Parsers\ProductExtractor;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Tests\ShopParsingBundle\PageFixtures\MoreleNet\iPhone5sSale;
use Tests\ShopParsingBundle\PageFixtures\MoreleNet\SamsungGalaxy;

class MoreleProductExtractorTest extends KernelTestCase
{
    /**
     * @var ProductExtractor
     */
    private $service;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        static::bootKernel();

        $this->service = static::$kernel->getContainer()->get('morele_net.product_extractor');
    }

    public function testExtraction()
    {
        $html = SamsungGalaxy::getHtml();
        $crawler = new Crawler($html);

        $this->service->setCrawler($crawler);

        $this->assertEquals('Samsung Galaxy S8 Orchid Grey (SM-G950F)', $this->service->getName());
        $this->assertEquals('339800', $this->service->getPrice());
        $this->assertEquals('PLN', $this->service->getCurrency());
        $this->assertEquals(0, $this->service->getOriginalPrice());
    }

    public function testExtractionWithDiscount()
    {
        $html = iPhone5sSale::getHtml();
        $crawler = new Crawler($html);

        $this->service->setCrawler($crawler);

        $this->assertEquals('Apple iPhone 5S 16GB EU Srebrny (ME433LP/A)', $this->service->getName());
        $this->assertEquals('144099', $this->service->getPrice());
        $this->assertEquals('158481', $this->service->getOriginalPrice());
        $this->assertEquals('PLN', $this->service->getCurrency());
    }
}
