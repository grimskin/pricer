<?php

namespace Tests\ShopParsingBundle\PageFixtures\MoreleNet;

class SamsungGalaxy
{
    /**
     * @return string
     */
    public static function getUrl()
    {
        return 'https://www.morele.net/smartfon-samsung-galaxy-s8-orchid-grey-sm-g950f-976330/';
    }

    /**
     * @return \DateTime
     */
    public static function getFetchDate(): \DateTime
    {
        return new \DateTime('16.09.2017 15:53 CEST');
    }

    /**
     * @return string
     */
    public static function getHtml()
    {
        $html = <<<'HTML'

<!DOCTYPE html>
<html lang="pl-PL">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, minimum-scale=1 user-scalable = no">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)... - Szukasz tego produktu? Sprawdź - najczęściej to właśnie Morele.net ma najniższą cenę w Polsce.">
<meta name="keywords" content="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F), Samsung, Smartfon">
<title>Samsung Galaxy S8 Orchid Grey (SM-G950F) w Morele.net</title>
<meta property="og:title" content="Samsung Galaxy S8 Orchid Grey (SM-G950F) w Morele.net" />
<meta property="og:description" content="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)... - Szukasz tego produktu? Sprawdź - najczęściej to właśnie Morele.net ma najniższą cenę w Polsce." />
<meta property="og:url" content="https://www.morele.net/smartfon-samsung-galaxy-s8-orchid-grey-sm-g950f-976330/" />
<meta property="og:image" content="" />
<meta property="og:site_name" content="www.morele.net" />
<meta name="google-site-verification" content="niS37rd1jAIoJBt_prZY3qmVzXjQZ62iFNyeBx8pQ0I" />
<link rel="canonical" href="https://www.morele.net/smartfon-samsung-galaxy-s8-orchid-grey-sm-g950f-976330/"> <link rel="dns-prefetch" href="\images.morele.net">
<link rel="dns-prefetch" href="\fonts.googleapis.com">
<link rel="dns-prefetch" href="\cdnjs.cloudflare.com">
<link rel="dns-prefetch" href="\gravatar.com">
<link rel="dns-prefetch" href="\www.googletagservices.com">
<link rel="dns-prefetch" href="\partner.googleadservices.com">
<link rel="dns-prefetch" href="\tpc.googlesyndication.com">
<link rel="dns-prefetch" href="\securepubads.g.doubleclick.net">
<link rel="dns-prefetch" href="\www.youtube.com">
<link rel="dns-prefetch" href="\pagead2.googlesyndication.com">
<link rel="dns-prefetch" href="\www.google.com">
<link rel="dns-prefetch" href="\static.doubleclick.net">
<link rel="dns-prefetch" href="\googleads.g.doubleclick.net">
<link rel="dns-prefetch" href="\scontent-frt3-1.xx.fbcdn.net">
<link rel="dns-prefetch" href="\static.criteo.net">
<link rel="prefetch" href="/static/css/product.css?version=4.9.10">
<link rel="prefetch" href="/static/js/product.app.js?version=4.9.10">
<link rel="prefetch" href="/static/css/category.css?version=4.9.10">
<link rel="prefetch" href="/static/js/category.app.js?version=4.9.10">
<link rel="prefetch" href="/static/js/search.app.js?version=4.9.10">
<link rel="icon" type="image/x-icon" href="/static/favicon-morele.ico?version=4.9.10" />
<link rel="stylesheet" type="text/css" href="/static/css/product.css?version=4.9.10">
</head><body id="catalog_product" data-controller="catalog" data-page="product" data-shop="morele" data-frontend-id="1" data-frontend-name="morele" data-language="pl" data-currency="zł" data-apri-url="https://apri.morele.net/" data-apri-tokken="0" data-images-url="https://images.morele.net/" data-environment="prod" itemscope itemtype="http://schema.org/Product">
<div class="notify-cont">
</div>
<div class="wrapper">
<div class="menu-overlay"></div>
<header id="header" data-role="header">
<div class="h-first-row hidden-xs hidden-sm">
<div class="container-fluid">
<div class="row">
<div class="col-md-6">
<nav class="h-sites-menu">
<ul>
<li class="active"><a href="https://www.morele.net/" title="Morele.net"><i class="icon-morele-home" aria-hidden="true"></i></a></li>
 <li class="frontend-hulahop"><a href="https://www.hulahop.pl/" title="Hulahop.pl">hulahop.pl</a></li>
<li class="frontend-amfora"><a href="https://www.amfora.pl/" title="Amfora.pl">amfora.pl</a></li>
<li class="frontend-pupilo"><a href="https://www.pupilo.pl/" title="Pupilo.pl">pupilo.pl</a></li>
</ul>
</nav>
</div>
<div class="col-md-6 text-right">
<nav class="h-help-menu">
<ul>
<li><a href="/info/netpunkt/">Punkty odbioru osobistego</a></li>
<li><a href="/pomoc/">Pomoc</a></li>
<li><a href="/index/faq_wybor/">Kontakt</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div>
<div class="h-second-row">
<div class="container-fluid">
<div class="h-second-cont">
<a id="btn_hamb" href="#" class="btn-hamb"><span></span></a>
<div class="row">
<div class="col-wr logo-wr logo-wr-morele ">
<div class="logo">
<a href="/">
<img width="220" height="40" src="/static/img/shop/img-morele-logo.png" alt="Logo" />
</a>
</div>
</div>
<div class="col-wr search-content-wr">
<div class="search-content">
<form action="/search/search2" method="post" name="searchform" id="searchform">
<input type="hidden" name="search_type" value="product" />
<div class="search-content-form">
<div class="search-content-input">
<input type="text" name="search" data-lang-product1="produkt" data-lang-product234="produkty" data-lang-productOthers="produktów" data-lang-error="Wystąpił błąd podczas wyszukiwania, proszę spróbować później" data-lang-category-title="Przejdź do kategorii:" data-lang-autocomplete-search="Wyszukiwarka morele.net" class="autocompl-input" placeholder="Szukany produkt..." />
<div class="search-content-category">
<input id="searchNarrowType" name="catalog_type" type="hidden" value="catdep" />
<div class="m-dropdown m-dropdown-serch">
<input id="searchNarrowData" type="hidden" name="elem_id" value="0" class="dropdown-input" />
<button class="btn btn-searchCategory btn-block dropdown-button" type="button">
<span class="default">
wszystkie działy </span>
</button>
<div class="dropdown-content">
<div class="list">
<ul>
<li data-value="0" class="dropdown-active">wszystkie działy</li>
 <li data-value="1">Komputery</li>
<li data-value="2">RTV</li>
<li data-value="3">Fotografia i kamery</li>
<li data-value="4">AGD</li>
<li data-value="5">Telefony</li>
<li data-value="6">Biuro</li>
<li data-value="7">Strefa Gracza</li>
<li data-value="8">Usługi</li>
<li data-value="38">Laptopy</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="search-content-button">
<button type="submit" class="btn btn-primary btnSearch">
<i class="icon-morele-search"></i>
</button>
</div>
</div>
</form> </div>
</div>
<div class="col-wr panel-wr">
<div class="panel" id="panel">
<div class="search-content-button-icon">
<div class="search-content-button-icon-mobile" id="search_content_button_mobile">
<i class="icon-morele-search"></i>
</div>
</div>
<div class="userAccount">
<div class="inner">
<a href="/login" class="link">
<div class="icon">
<i class="icon-morele-user"></i>
</div>
<div class="display-table allWidth hidden-xs hidden-sm">
<div class="text-wr">
<div class="text hidden-sm">Zaloguj się</div>
<div class="text hidden-sm">Załóż konto</div>
</div>
</div>
</a>
</div>
</div>
<div class="basket">
<div class="inner">
<a href="/koszyk/" class="link">
<div class="icon">
<i class="icon-morele-cart"></i>
<div style="display:none;" class="totalCount">0</div>
 </div>
<div class="text-wr">
<div class="text hidden-sm">
<div class="totalPrice">0,00 zł</div>
</div>
</div>
</a>
</div>
</div>
</div>
</div>
</div>
</div>
<nav class="primary-menu" data-parent-daley="300,0" data-min-width="1025">
<ul class="pr-menu-cat">
<li class="pr-menu-subcat-h no-link">
Kategorie </li>
<li class="pr-menu-item   has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/"><span class="helper-before"></span>Laptopy<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Laptopy
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/">Notebooki, laptopy, ultrabooki </a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Notebooki, laptopy, ultrabooki
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,30183O978877/1/">Laptopy</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,30183O978878/1/">Ultrabooki</a>
</li>
 <li>
<a href="/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,30183O978880/1/">Laptopy 2w1</a>
</li>
<li>
<a href="/laptopy/laptopy/laptopy-wyprzedazowe-i-poleasingowe-497/">Laptopy wyprzedażowe i poleasingowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/">Podzespoły do laptopów</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Podzespoły do laptopów
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/dyski-ssd-518/">Dyski SSD</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/dyski-2-5-i-mniejsze-155/">Dyski 2.5&#039;&#039; i mniejsze</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/napedy-optyczne-28/">Napędy optyczne</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/pamieci-do-laptopow-117/">Pamięci do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/podzespoly-do-notebookow-719/">Podzespoły i części serwisowe</a>
</li>
 </ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/laptopy/">Laptopy - zastosowanie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Laptopy - zastosowanie
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098457/1/">Laptopy biznesowe</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098455/1/">Laptopy dla domu</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098458/1/">Laptopy ultramobilne</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098456/1/">Laptopy multimedialne</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098459/1/">Laptopy dla graczy</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098461/1/">Laptopy premium</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098460/1/">Mobilne stacje robocze</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/przenoszenie-i-ochrona/">Przenoszenie i ochrona</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Przenoszenie i ochrona
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/przenoszenie-i-ochrona/torby-do-laptopow-32/">Torby do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/przenoszenie-i-ochrona/etui-do-laptopow-628/">Etui do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/przenoszenie-i-ochrona/plecaki-na-laptopy-593/">Plecaki do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/przenoszenie-i-ochrona/linki-zabezpieczajace-715/">Linki zabezpieczające</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/laptopy/">Laptopy - popularni producenci</a>
 <a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Laptopy - popularni producenci
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,511,,,,,/1/">Laptopy Lenovo</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,248,,,,,/1/">Laptopy Dell</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,92,,,,,/1/">Laptopy HP</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,25,,,,,/1/">Laptopy Asus</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,496,,,,,/1/">Laptopy Apple</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,151,,,,,/1/">Laptopy MSI</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,75,,,,,/1/">Laptopy Fujitsu</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,218,,,,,/1/">Laptopy Toshiba</a>
</li>
</ul>
</li>
 <li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/laptopy/">Laptopy - rozmiar</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Laptopy - rozmiar
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,18749O890445.1017253.477015/1/">Laptopy z ekranem 10.1&#039;&#039; - 12.5&#039;&#039;</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,18749O314031.888064.313129/1/">Laptopy z ekranem 13.3&#039;&#039; - 14.1&#039;&#039;</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,18749O314226/1/">Laptopy z ekranem 15.6&#039;&#039;</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,/1/">Laptopy z ekranem 17.3&#039;&#039;</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/">Akcesoria do laptopów</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
 </div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Akcesoria do laptopów
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/baterie-do-laptopow-516/">Baterie do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/dyski-zewnetrzne-207/">Dyski zewnętrzne</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/lampki-usb-697/">Lampki USB</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/podstawki-chlodzace-636/">Podstawki chłodzące</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/przetwornice-222/">Przetwornice</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/rysiki-645/">Rysiki</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/stacje-dokujace-i-replikatory-portow-725/">Stacje dokujące i replikatory portów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/wentylatory-usb-698/">Wentylatory USB</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/zasilacze-do-laptopow-531/">Zasilacze do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/akcesoria-poleasingowe-1473/">Akcesoria poleasingowe</a>
</li>
 <li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/pozostale-akcesoria-do-laptopow-63/">Pozostałe akcesoria do laptopów</a>
</li>
</ul>
</li>
</ul>
</div>
</div>
</li>
<li class="pr-menu-item menu-komputery  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/komputery/"><span class="helper-before"></span>Komputery<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Komputery
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/komputery-pc/">Komputery PC</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Komputery PC
</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/">Komputery dla graczy</a>
</li>
 <li>
<a href="/komputery/komputery-pc/komputery-do-domu-i-biura-19/">Komputery do domu i biura</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-all-in-one-40/">Komputery All-In-One</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-mini-pc-i-nuc-714/">Komputery Mini PC i NUC</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-wyprzedazowe-i-poleasingowe-493/">Komputery wyprzedażowe i poleasingowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/serwery/">Serwery</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Serwery
</a>
</li>
<li>
<a href="/komputery/serwery/serwery-plikow-190/">Serwery Plików</a>
</li>
<li>
<a href="/komputery/serwery/dyski-do-serwerow-147/">Dyski do serwerów</a>
</li>
<li>
<a href="/komputery/serwery/szafy-rack-142/">Szafy Rack</a>
</li>
<li>
<a href="/komputery/serwery/ups-57/">UPS</a>
</li>
 </ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/sieci/">Sieci</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Sieci
</a>
</li>
<li>
<a href="/komputery/sieci/routery-48/">Routery</a>
</li>
<li>
<a href="/komputery/sieci/kable-teleinformatyczne-140/">Kable teleinformatyczne</a>
</li>
<li>
<a href="/komputery/sieci/karty-sieciowe-wi-fi-477/">Karty sieciowe Wi-Fi</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/podzespoly-komputerowe/">Podzespoły komputerowe</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Podzespoły komputerowe
</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/karty-graficzne-12/">Karty graficzne</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/procesory-45/">Procesory</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/plyty-glowne-42/">Płyty główne</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/pamieci-ram-38/">Pamięci RAM</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/obudowy-33/">Obudowy</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/zasilacze-61/">Zasilacze</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/chlodzenie-komputerowe/">Chłodzenie komputerowe</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/sluchawki-glosniki-mikrofony/">Słuchawki, głośniki, mikrofony</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Słuchawki, głośniki, mikrofony
</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/glosniki-komputerowe-6/">Głośniki komputerowe</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/sluchawki-nauszne-780/">Słuchawki nauszne</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/sluchawki-douszne-i-dokanalowe-457/">Słuchawki douszne i dokanałowe</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/sluchawki-bezprzewodowe-458/">Słuchawki bezprzewodowe</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/sluchawki-z-mikrofonem-728/">Słuchawki z mikrofonem</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/sluchawki-dla-graczy-466/">Słuchawki dla graczy</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/monitory-i-akcesoria/monitory-komputerowe-523/">Monitory komputerowe</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Monitory komputerowe
</a>
</li>
<li>
 <a href="/komputery/monitory-i-akcesoria/monitory-led-lcd-523/,,,,,,,,,,30812O1044561/1/">Do domu i biura</a>
</li>
<li>
<a href="/komputery/monitory-i-akcesoria/monitory-led-lcd-523/,,,,,,,,,,30812O1044560/1/">Dla graczy</a>
</li>
<li>
<a href="/komputery/monitory-i-akcesoria/monitory-led-lcd-523/,,,,,,,,,,30812O1044563/1/">Dotykowe i wielkoformatowe (LFD)</a>
</li>
<li>
<a href="/komputery/monitory-i-akcesoria/monitory-led-lcd-523/,,,,,,,,,,30812O1044562/1/">Profesjonalne</a>
</li>
<li>
<a href="https://www.morele.net/komputery/monitory-i-akcesoria/monitory-poleasingowe-494/">Poleasingowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/klawiatury-i-myszki/">Klawiatury i myszki</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Klawiatury i myszki
</a>
</li>
<li>
<a href="/komputery/klawiatury-i-myszki/klawiatury-komputerowe-18/">Klawiatury komputerowe</a>
</li>
<li>
<a href="/komputery/klawiatury-i-myszki/myszy-komputerowe-464/">Myszy komputerowe</a>
</li>
</ul>
</li>
 <li class="">
<div class="pr-menu-inner">
<a href="/komputery/dyski-i-nosniki-danych/">Dyski i nośniki danych</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Dyski i nośniki danych
</a>
</li>
<li>
<a href="/komputery/dyski-i-nosniki-danych/dyski-ssd-518/">Dyski SSD</a>
</li>
<li>
<a href="/komputery/dyski-i-nosniki-danych/dyski-twarde-3-5-4/">Dyski twarde 3,5&#039;&#039;</a>
</li>
<li>
<a href="/komputery/dyski-i-nosniki-danych/dyski-zewnetrzne-207/">Dyski zewnętrzne</a>
</li>
<li>
<a href="/komputery/dyski-i-nosniki-danych/pendrive-8/">Pendrive</a>
</li>
<li>
<a href="/komputery/dyski-i-nosniki-danych/karty-pamieci-13/">Karty pamięci</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/akcesoria-zasilanie-i-kamerki/">Akcesoria, zasilanie i kamerki</a>
</div>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
 <li class="">
<div class="pr-menu-inner">
<a href="/komputery/oprogramowanie/">Oprogramowanie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Oprogramowanie
</a>
</li>
<li>
<a href="/komputery/oprogramowanie/systemy-operacyjne-196/">Systemy operacyjne</a>
</li>
<li>
<a href="/komputery/oprogramowanie/microsoft-office-198/">Microsoft Office</a>
</li>
<li>
<a href="/komputery/oprogramowanie/programy-biurowe-2983/">Programy biurowe</a>
</li>
<li>
<a href="/komputery/oprogramowanie/bezpieczenstwo-197/">Bezpieczeństwo</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/drukowanie-i-skanery/">Drukowanie i skanery</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Drukowanie i skanery
</a>
 </li>
<li>
<a href="/komputery/drukowanie-i-skanery/drukarki-atramentowe-269/">Drukarki atramentowe</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/drukarki-laserowe-279/">Drukarki laserowe</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/urzadzenia-wielofunkcyjne-atramentowe-298/">Urządzenia wielofunkcyjne atramentowe</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/urzadzenia-wielofunkcyjne-laserowe-296/">Urządzenia wielofunkcyjne laserowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/kable-i-adaptery/">Kable i adaptery</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Kable i adaptery
</a>
</li>
<li>
<a href="/komputery/kable-i-adaptery/kable-komputerowe-i-przejsciowki-193/">Kable komputerowe i przejściówki</a>
</li>
<li>
<a href="/komputery/monitory-i-akcesoria/kable-i-konwertery-av-194/">Kable i konwertery AV</a>
</li>
<li>
<a href="/komputery/kable-i-adaptery/adaptery-usb-654/">Adaptery USB</a>
</li>
<li>
 <a href="/komputery/kable-i-adaptery/kable-zasilajace-736/">Kable zasilające</a>
</li>
</ul>
</li>
</ul>
</div>
<div class="menu-sis menu-komputery hidden-xs hidden-sm">
<div class="label">Sklepy producentów:</div>
<div class="content">
<div class="left">
<div class="item">
<a href="/iiyama/" class="logo-iiyama">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/msi/" class="logo-msi">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/sklep/microsoft/main/" class="logo-microsoft">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/oki/" class="logo-oki">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/thrustmaster/" class="logo-thrustmaster">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/razer/" class="logo-razer">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/ADATA/" class="logo-adata">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
 </div>
</div>
<div class="right">
<div class="item">
<a href="/komputery/podzespoly-komputerowe/procesory-45/?konfigurator=konfigurator-zestawu-komputerowego#configurator" class="logo-configurator">
<span class="img-color"></span>
</a>
</div>
</div>
<div class="clearBoth"></div>
</div>
<div class="clearBoth"></div>
</div>
</div>
</li>
<li class="pr-menu-item menu-rtv  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/rtv/"><span class="helper-before"></span>RTV<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> RTV
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/telewizory/telewizory-412/">Telewizory</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Telewizory
</a>
</li>
<li>
<a href="/rtv/telewizory/telewizory-412/,,,,,,,,,,23217O724442/1/">Telewizory FullHD</a>
</li>
<li>
<a href="/rtv/telewizory/telewizory-412/,,,,,,,,,,23217O761261/1/">Telewizory 4K (Ultra HD)</a>
</li>
 <li>
<a href="/rtv/telewizory/telewizory-412/,,,,,,,,,,23156O719102/1/">Telewizory 3D</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/dvd-i-blu-ray/">DVD i BLU-RAY</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> DVD i BLU-RAY
</a>
</li>
<li>
<a href="/rtv/dvd-blu-ray/odtwarzacze-dvd-106/">Odtwarzacze DVD</a>
</li>
<li>
<a href="/rtv/dvd-blu-ray/odtwarzacze-blu-ray-462/">Odtwarzacze BLU-RAY</a>
</li>
<li>
<a href="/rtv/dvd-blu-ray/przenosne-odtwarzacze-492/">Przenośne odtwarzacze</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/tunery-tv/">Tunery TV</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
 <a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Tunery TV
</a>
</li>
<li>
<a href="/rtv/tunery-tv/tunery-tv-541/">Tunery TV (DVB-T)</a>
</li>
<li>
<a href="/rtv/tunery-tv/tunery-tv-satelitarnej-560/">Tunery TV satelitarnej</a>
</li>
<li>
<a href="/rtv/tunery-tv/anteny-rtv-215/">Anteny RTV</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/audio-i-hifi/">Audio i HiFi</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Audio i HiFi
</a>
</li>
<li>
<a href="/rtv/audio-hifi/wieze-111/">Wieże</a>
</li>
<li>
<a href="/rtv/audio-hifi/radioodtwarzacze-121/">Radioodtwarzacze</a>
</li>
<li>
<a href="/rtv/audio-hifi/kina-domowe-448/">Kina domowe</a>
</li>
<li>
<a href="/rtv/audio-hifi/soundbary-613/">Soundbary</a>
 </li>
<li>
<a href="/rtv/audio-hifi/wzmacniacze-audio-588/">Wzmacniacze audio</a>
</li>
<li>
<a href="/rtv/audio-hifi/amplitunery-167/">Amplitunery</a>
</li>
<li>
<a href="/rtv/audio-hifi/kolumny-i-glosniki-122/">Kolumny i głośniki</a>
</li>
<li>
<a href="/rtv/audio-hifi/subwoofery-611/">Subwoofery</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/mp3-i-mp4/">Odtwarzacze MP3, MP4, dyktafony</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Odtwarzacze MP3, MP4, dyktafony
</a>
</li>
<li>
<a href="/rtv/mp3-mp4/odtwarzacze-mp3-36/">Odtwarzacze MP3</a>
</li>
<li>
<a href="/rtv/mp3-mp4/odtwarzacze-mp4-184/">Odtwarzacze MP4</a>
</li>
<li>
<a href="/rtv/mp3-mp4/dyktafony-449/">Dyktafony</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
 <li class="">
<div class="pr-menu-inner">
<a href="/rtv/projektory-i-akcesoria/">Projektory i akcesoria</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Projektory i akcesoria
</a>
</li>
<li>
<a href="/rtv/projektory-i-akcesoria/projektory-46/">Projektory</a>
</li>
<li>
<a href="/rtv/projektory-i-akcesoria/uchwyty-do-projektorow-658/">Uchwyty do projektorów</a>
</li>
<li>
<a href="/rtv/projektory-i-akcesoria/ekrany-projekcyjne-276/">Ekrany projekcyjne</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/sluchawki/">Słuchawki</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Słuchawki
</a>
</li>
<li>
 <a href="/rtv/sluchawki/sluchawki-bezprzewodowe-458/">Słuchawki bezprzewodowe</a>
</li>
<li>
<a href="/rtv/sluchawki/sluchawki-douszne-i-dokanalowe-457/">Słuchawki douszne i dokanałowe</a>
</li>
<li>
<a href="/rtv/sluchawki/sluchawki-nauszne-780/">Słuchawki nauszne</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/akcesoria-rtv/">Akcesoria RTV</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Akcesoria RTV
</a>
</li>
<li>
<a href="/rtv/akcesoria-rtv/wieszaki-i-uchwyty-rtv-163/">Wieszaki i uchwyty RTV</a>
</li>
<li>
<a href="/rtv/akcesoria-rtv/listwy-zasilajace-22/">Listwy zasilające</a>
</li>
<li>
<a href="/rtv/akcesoria-rtv/kable-av-194/">Kable AV</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/car-audio/">Car audio</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Car audio
</a>
</li>
<li>
<a href="/rtv/car-audio/radia-samochodowe-59/">Radia samochodowe</a>
</li>
<li>
<a href="/rtv/car-audio/glosniki-samochodowe-120/">Głośniki samochodowe</a>
</li>
<li>
<a href="/rtv/car-audio/subwoofery-samochodowe-454/">Subwoofery samochodowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/nawigacja-gps-i-cb-radia/">Nawigacja GPS i CB radia</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Nawigacja GPS i CB radia
</a>
</li>
<li>
<a href="/rtv/nawigacja-gps-cb-radia/nawigacja-gps-139/">Nawigacja GPS</a>
</li>
<li>
<a href="/rtv/nawigacja-gps-cb-radia/cb-radia-544/">CB Radia</a>
 </li>
<li>
<a href="/rtv/nawigacja-gps-cb-radia/cb-anteny-545/">CB Anteny</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/monitoring/">Monitoring</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Monitoring
</a>
</li>
<li>
<a href="/rtv/monitoring/kamery-ip-750/">Kamery IP</a>
</li>
<li>
<a href="/rtv/monitoring/kamery-przemyslowe-i-akcesoria-221/">Kamery przemysłowe i akcesoria</a>
</li>
<li>
<a href="/rtv/monitoring/rejestratory-obrazu-183/">Rejestratory obrazu</a>
</li>
</ul>
</li>
</ul>
</div>
<div class="menu-sis menu-rtv hidden-xs hidden-sm">
<div class="label">Sklepy producentów:</div>
<div class="content">
<div class="left">
<div class="item">
<a href="/ADATA/" class="logo-adata">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
</div>
</div>
<div class="clearBoth"></div>
</div>
</div>
</li>
<li class="pr-menu-item menu-agd   has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/agd/"><span class="helper-before"></span>AGD<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> AGD
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/agd/agd-do-zabudowy/">AGD do zabudowy</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> AGD do zabudowy
</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/lodowki-z-zamrazalnikiem-u-dolu-zabudowa-261/">Lodówki z zamrażalnikiem u dołu (do zabudowy)</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/zmywarki-do-zabudowy-90/">Zmywarki do zabudowy</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/zestaw-piekarnik-z-plyta-gazowa-248/">Zestawy piekarnik z płytą gazową</a>
</li>
 <li>
<a href="/agd/agd-do-zabudowy/piekarniki-101/">Piekarniki do zabudowy</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/plyty-ceramiczne-244/">Płyty ceramiczne</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/okapy-kominowe-265/">Okapy kominowe</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/ekspresy-do-kawy-zabudowa-231/">Ekspresy do zabudowy</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/kuchenki-mikrofalowe-do-zabudowy-188/">Kuchenki mikrofalowe do zabudowy</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/agd/agd-wolnostojace/">AGD wolnostojące</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> AGD wolnostojące
</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/lodowki-jednodrzwiowe-235/">Lodówki jednodrzwiowe</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/lodowki-z-zamrazalnikiem-u-dolu-237/">Lodówki z zamrażalnikiem u dołu</a>
</li>
<li>
 <a href="/agd/agd-wolnostojace/lodowki-side-by-side-238/">Lodówki Side by Side</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/zmywarki-wolnostojace-91/">Zmywarki wolnostojące</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/pralki-ladowane-od-frontu-233/">Pralki ładowane od frontu</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/kuchnie-gazowo-elektryczne-240/">Kuchnie gazowo-elektryczne</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/kuchnie-z-plyta-ceramiczna-241/">Kuchnie z płytą ceramiczną</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/plyty-wolnostojace-242/">Płyty wolnostojące</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/agd/kuchnia-i-gotowanie/">Kuchnia i gotowanie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Kuchnia i gotowanie
</a>
</li>
<li>
<a href="/agd/kuchnia-i-gotowanie/ekspresy-cisnieniowe-277/">Ekspresy ciśnieniowe</a>
</li>
<li>
 <a href="/agd/kuchnia-i-gotowanie/garnki-patelnie-128/">Garnki i patelnie</a>
</li>
<li>
<a href="/agd/kuchnia-i-gotowanie/czajniki-85/">Czajniki</a>
</li>
<li>
<a href="/agd/kuchnia-i-gotowanie/kuchenki-mikrofalowe-86/">Kuchenki mikrofalowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/agd/zdrowie-i-uroda/">Zdrowie i uroda</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Zdrowie i uroda
</a>
</li>
<li>
<a href="/agd/uroda-i-higiena/golarki-84/">Golarki</a>
</li>
<li>
<a href="/agd/uroda-i-higiena/szczoteczki-elektryczne-112/">Szczoteczki elektryczne</a>
</li>
<li>
<a href="/agd/uroda-i-higiena/maszynki-do-wlosow-97/">Maszynki do włosów</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/agd/sprzatanie-prasowanie-i-szycie/">Sprzątanie, prasowanie, szycie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Sprzątanie, prasowanie, szycie
</a>
</li>
<li>
<a href="/agd/sprzatanie-prasowanie-i-szycie/zelazka-94/">Żelazka</a>
</li>
<li>
<a href="/agd/sprzatanie-prasowanie-i-szycie/odkurzacze-271/">Odkurzacze</a>
</li>
<li>
<a href="/agd/sprzatanie-prasowanie-i-szycie/myjki-cisnieniowe-170/">Myjki ciśnieniowe</a>
</li>
<li>
<a href="/agd/sprzatanie-prasowanie-i-szycie/generatory-pary-274/">Generatory pary</a>
</li>
<li>
<a href="/agd/sprzatanie-prasowanie-i-szycie/odkurzacze-271/?konfigurator=konfigurator-zestawow-do-sprzatania#configurator">Dobierz zestaw, który ułatwia sprzątanie</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/agd/nawilzacze-i-wentylatory/">Nawilżacze i wentylatory</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/agd/zlewy-baterie/">Zlewy, baterie</a>
</div>
</li>
 <li class="">
<div class="pr-menu-inner">
<a href="/agd/oswietlenie/">Oświetlenie</a>
</div>
</li>
</ul>
</div>
<div class="menu-sis menu-agd  hidden-xs hidden-sm">
<div class="label">Sklepy producentów:</div>
<div class="content">
<div class="item">
<a href="/miele/" class="logo-miele">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
</div>
<div class="clearBoth"></div>
</div>
</div>
</li>
<li class="pr-menu-item menu-phones  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/telefony/"><span class="helper-before"></span>Telefony i tablety<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Telefony i tablety
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/telefony/telefony-smartfony-krotkofalowki/smartfony-280">Smartfony</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Smartfony
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/smartfony-280/,,,,,,,,,,31881O1104912/1/">iOS</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/smartfony-280/,,,,,,,,,,31881O1104911/1/">Android</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/smartfony-280/,,,,,,,,,,31881O1104913/1/">Windows</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/telefony/telefony-smartfony-krotkofalowki/telefony-komorkowe-64">Telefony komórkowe</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Telefony komórkowe
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/telefony-komorkowe-64/,,,,,,,,,,29773O945498/1/">Dla aktywnych</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/telefony-komorkowe-64/,,,,,,,,,,29773O942721/1/">Dla seniora</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/telefony/telefony-stacjonarne">Telefony stacjonarne</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Telefony stacjonarne
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-stacjonarne/telefony-przewodowe-282/">Telefony przewodowe</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-stacjonarne/telefony-bezprzewodowe-281/">Telefony bezprzewodowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/telefony/faxy">Faksy</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Faksy
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/faksy/faksy-na-papier-zwykly-308/">Faksy na papier zwykły</a>
</li>
 <li>
<a href="https://www.morele.net/telefony/faksy/faksy-na-papier-termiczny-307/">Faksy na papier termiczny</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/krotkofalowki-158/">Krótkofalówki</a>
</div>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/tablety-i-czytniki-ebookow/tablety-528">Tablety</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Tablety
</a>
</li>
<li>
<a href="/telefony/tablety/tablety-528/,,,,,,,,,,28297O916565.988281.1035980.1152626/1/">iOS</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety-i-czytniki-ebookow/tablety-528/,,,,,,,,,,28297O978488.1024088.968829.1099825.995112.905213.1070772/1/">Android</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety-i-czytniki-ebookow/tablety-528/,,,,,,,,,,28297O1041887.1029605.966795.969817.918973.1070772/1/">Windows</a>
</li>
</ul>
</li>
 <li class=" sb-normal ">
<div class="pr-menu-inner">
<a href="https://www.morele.net/komputery/klawiatury-i-myszki/tablety-graficzne-54/">Tablety graficzne</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Tablety graficzne
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/czytniki-e-book-542/">Czytniki e-book</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/tablety/">Akcesoria do tabletów</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Akcesoria do tabletów
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/etui-do-tabletow-632/">Etui do tabletów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/etui-z-klawiatura-667/">Etui z klawiaturą</a>
</li>
 <li>
<a href="https://www.morele.net/telefony/tablety/folie-ochronne-do-tabletow-706/">Folie ochronne do tabletów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/stojaki-do-tabletow-675/">Stojaki do tabletów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/uchwyty-do-tabletow-644/">Uchwyty do tabletów</a>
</li>
<li>
<a href="https://www.morele.net/komputery/tablety-i-czytniki-e-bookow/pozostale-akcesoria-do-tabletow-498/">Pozostałe akcesoria do tabletów</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/akcesoria-gsm/">Dźwięk i komunikacja</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Dźwięk i komunikacja
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/glosniki-przenosne-677/">Głośniki przenośne</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/sluchawki-bezprzewodowe-458/">Słuchawki bezprzewodowe</a>
</li>
<li>
<a href="https://www.morele.net/komputery/sluchawki-glosniki-i-mp3/sluchawki-douszne-i-dokanalowe-457/">Słuchawki douszne i dokanałowe</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/zestawy-glosnomowiace-gsm-534/">Zestawy głośnomówiące GSM</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/zestawy-sluchawkowe-gsm-288/">Zestawy słuchawkowe GSM</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/akcesoria-gsm/">Ochrona</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Ochrona
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/etui-i-pokrowce-do-telefonow-450/">Etui i pokrowce do telefonów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/folie-i-szkla-ochronne-do-telefonow-540/">Folie i szkła ochronne do telefonów</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/akcesoria-gsm/">Zasilanie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Zasilanie
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/baterie-do-telefonow-gsm-514/">Baterie do telefonów GSM</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/ladowarki-samochodowe-659/">Ładowarki samochodowe</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/ladowarki-sieciowe-643/">Ładowarki sieciowe</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/powerbanki-584/">Powerbanki</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/telefony/akcesoria-gsm">Pozostałe akcesoria</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Pozostałe akcesoria
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/gogle-vr-1436/">Gogle VR</a>
</li>
 <li>
<a href="https://www.morele.net/komputery/kable-i-adaptery/kable-usb-699/">Kable USB</a>
</li>
<li>
<a href="https://www.morele.net/komputery/dyski-i-nosniki-danych/karty-pamieci-microsd-626/">Karty pamięci microSD</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/rysiki-645/">Rysiki</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/selfie-stick-1341/">Selfie stick</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/uchwyty-do-telefonow-536/">Uchwyty do telefonów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/pozostale-akcesoria-gsm-731/">Pozostałe akcesoria GSM</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/">Smartwatche, wearables, sport</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Smartwatche, wearables, sport
</a>
</li>
<li>
<a href="/biuro/zegarki/zegarki-sportowe-1524/">Zegarki sportowe</a>
</li>
<li>
 <a href="https://www.morele.net/telefony/smartwatche-wearables-sport/zegarki-smartwatch-732/">Zegarki smartwatch</a>
</li>
<li>
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/smartbandy-1291/">Smartbandy</a>
</li>
<li>
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/akcesoria-do-smartwatchow-776/">Akcesoria do smartwatchów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/pulsometry-i-krokomierze-733/">Pulsometry i krokomierze</a>
</li>
<li>
<a href="/telefony/smartwatche-wearables-sport/deskorolki-elektryczne-1343/">Deskorolki elektryczne</a>
</li>
<li>
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/akcesoria-sportowe-1011/">Akcesoria sportowe</a>
</li>
</ul>
</li>
</ul>
</div>
</div>
</li>
<li class="pr-menu-item menu-foto  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/"><span class="helper-before"></span>Foto i kamery<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Foto i kamery
</a>
</div>
<ul class="pr-menu-sb-cat-item">
 <li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/aparaty-cyfrowe/">Aparaty cyfrowe</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Aparaty cyfrowe
</a>
</li>
<li>
<a href="/fotografia-i-kamery/aparaty-cyfrowe/aparaty-kompaktowe-2/">Aparaty kompaktowe</a>
</li>
<li>
<a href="/fotografia-i-kamery/aparaty-cyfrowe/bezlusterkowce-586/">Bezlusterkowce</a>
</li>
<li>
<a href="/fotografia-i-kamery/aparaty-cyfrowe/lustrzanki-114/">Lustrzanki</a>
</li>
<li>
<a href="/fotografia-i-kamery/aparaty-cyfrowe/aparaty-kompaktowe-2/,,,,,,,,,,31328O1078008/1/">Aparaty natychmiastowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/sprzet-fotograficzny/">Sprzęt fotograficzny</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
 <i class="fa fa-angle-left" aria-hidden="true"></i> Sprzęt fotograficzny
</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/statywy-408/">Statywy</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/glowice-do-statywow-505/">Głowice do statywów</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/monopody-455/">Monopody</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/obiektywy-285/">Obiektywy</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/filtry-foto-video-478/">Filtry foto-video</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/konwertery-foto-526/">Konwertery foto</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/lampy-blyskowe-286/">Lampy błyskowe</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/kamery/">Kamery</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Kamery
</a>
</li>
<li>
<a href="/fotografia-i-kamery/kamery/kamery-cyfrowe-113/">Kamery cyfrowe</a>
</li>
<li>
<a href="/fotografia-i-kamery/kamery/kamery-sportowe-748/">Kamery sportowe</a>
</li>
<li>
<a href="/fotografia-i-kamery/kamery/kamery-profesjonalne-642/">Kamery profesjonalne</a>
</li>
<li>
<a href="/fotografia-i-kamery/kamery/kamery-samochodowe-652/">Kamery samochodowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/zasilanie/">Zasilanie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Zasilanie
</a>
</li>
<li>
<a href="/fotografia-i-kamery/zasilanie/akumulatory-do-aparatow-i-kamer-287/">Akumulatory do aparatów i kamer</a>
</li>
<li>
<a href="/fotografia-i-kamery/zasilanie/ladowarki-do-aparatow-i-kamer-488/">Ładowarki do aparatów i kamer</a>
</li>
<li>
<a href="/fotografia-i-kamery/zasilanie/battery-gripy-472/">Battery gripy</a>
</li>
 </ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/">Torby, futerały, obudowy</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Torby, futerały, obudowy
</a>
</li>
<li>
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/torby-foto-video-444/">Torby foto-video</a>
</li>
<li>
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/futeraly-foto-66/">Futerały foto</a>
</li>
<li>
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/plecaki-foto-447/">Plecaki foto</a>
</li>
<li>
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/obudowy-podwodne-503/">Obudowy podwodne</a>
</li>
<li>
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/walizki-foto-616/">Walizki foto</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/">Akcesoria fotograficzne</a>
 <a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Akcesoria fotograficzne
</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/akcesoria-do-kamer-sportowych-1010/">Akcesoria do kamer sportowych</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/albumy-fotograficzne-565/">Albumy fotograficzne</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/cyfrowe-ramki-foto-187/">Cyfrowe ramki foto</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/czytniki-kart-pamieci-65/">Czytniki kart pamięci</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/dekielki-zaslepki-504/">Dekielki, zaślepki</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/karty-pamieci-13/">Karty pamięci</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/muszle-oczne-i-wizjery-637/">Muszle oczne i wizjery</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/oslony-przeciwsloneczne-i-tulipany-622/">Osłony przeciwsłoneczne i tulipany</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/piloty-i-wezyki-spustowe-627/">Piloty i wężyki spustowe</a>
</li>
 </ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/wyposazenie-studia/">Wyposażenie studia</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Wyposażenie studia
</a>
</li>
<li>
<a href="/fotografia-i-kamery/wyposazenie-studia/akcesoria-studyjne-647/">Akcesoria studyjne</a>
</li>
<li>
<a href="/fotografia-i-kamery/wyposazenie-studia/blendy-646/">Blendy</a>
</li>
<li>
<a href="/fotografia-i-kamery/wyposazenie-studia/lampy-pierscieniowe-603/">Lampy pierścieniowe</a>
</li>
<li>
<a href="/fotografia-i-kamery/wyposazenie-studia/lampy-studyjne-602/">Lampy studyjne</a>
</li>
<li>
<a href="/fotografia-i-kamery/wyposazenie-studia/zestawy-studyjne-653/">Zestawy studyjne</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/obserwacja/">Obserwacja</a>
 <a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Obserwacja
</a>
</li>
<li>
<a href="/fotografia-i-kamery/obserwacja/dalmierze-171/">Dalmierze</a>
</li>
<li>
<a href="/fotografia-i-kamery/obserwacja/lornetki-165/">Lornetki</a>
</li>
<li>
<a href="/fotografia-i-kamery/obserwacja/lunety-615/">Lunety</a>
</li>
<li>
<a href="/fotografia-i-kamery/obserwacja/mikroskopy-1293/">Mikroskopy</a>
</li>
<li>
<a href="/fotografia-i-kamery/obserwacja/teleskopy-577/">Teleskopy</a>
</li>
</ul>
</li>
<li class=" sb-normal ">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/modele-zdalnie-sterowane/drony-769/">Drony</a>
</div>
</li>
<li class=" sb-normal ">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/modele-zdalnie-sterowane/akcesoria-do-dronow-770/">Akcesoria do dronów</a>
</div>
</li>
<li class="">
 <div class="pr-menu-inner">
<a href="/komputery/drukowanie-i-skanery/">Drukowanie i skanery</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Drukowanie i skanery
</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/drukarki-fotograficzne-283/">Drukarki fotograficzne</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/skanery-filmow-i-negatywow-581/">Skanery filmów i negatywów</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/papier-fotograficzny-441/">Papier fotograficzny</a>
</li>
</ul>
</li>
</ul>
</div>
<div class="menu-sis menu-foto hidden-xs hidden-sm">
<div class="label">Sklepy producentów:</div>
<div class="content">
<div class="item">
<a href="/gopro/" class="logo-gopro">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
</div>
<div class="clearBoth"></div>
</div>
</div>
</li>
<li class="pr-menu-item menu-gamer  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/strefa-gracza/"><span class="helper-before"></span>Strefa Gracza<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Strefa Gracza
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/strefa-gracza/konsole/">Konsole</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Konsole
</a>
</li>
<li>
<a href="/strefa-gracza/konsole/konsole-115/0,0,,,,,,,,,2114O792360/1">Playstation 4</a>
</li>
<li>
<a href="/strefa-gracza/konsole/konsole-115/0,0,,,,,,,,,2114O968583/1">Xbox One</a>
</li>
<li>
<a href="/strefa-gracza/konsole/konsole-115/0,0,,,,,,,,,2114O231870/1">Xbox 360</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/">Komputery dla graczy</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Komputery dla graczy
</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/,1999.00,,,,676,,p,0,,/1/">SKY seria G1000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/1999.00,2999.00,,,,676,,p,0,,/1/">SKY seria G2000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/3000.00,3999.00,,,,676,,p,0,,/1/">SKY seria G3000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/4000.00,4998.00,,,,676,,p,0,,/1/">SKY seria G4000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/4999.00,5999.00,,,,676,,p,0,,/1/">SKY seria G5000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/6000.00,6999.00,,,,676,,p,0,,/1/">SKY seria G6000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/7000.00,7999.00,,,,676,,p,0,,/1/">SKY seria G7000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/8000.00,8999.00,,,,676,,p,0,,/1/">SKY seria G8000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/9000.00,,,,,676,,p,0,,/1/">SKY seria G9000</a>
</li>
</ul>
</li>
</ul>
 <ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/strefa-gracza/gry-konsole-i-pc/">Gry</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Gry
</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-pc-wersje-pudelkowe-29/">Gry PC (wersje pudełkowe)</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-pc-wersje-cyfrowe-1296/">Gry PC (wersje cyfrowe)</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-xbox-one-734/">Gry Xbox One</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-playstation-4-729/">Gry Playstation 4</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-xbox-360-254/">Gry Xbox 360</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-playstation-3-257/">Gry Playstation 3</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/abonamenty-psn-i-xbox-411/">Abonamenty PSN i Xbox</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/strefa-gracza/sprzet-dla-graczy/">Sprzęt dla graczy</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Sprzęt dla graczy
</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/klawiatury-dla-graczy-465/">Klawiatury dla graczy</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/myszy-dla-graczy-27/">Myszy dla graczy</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/podkladki-dla-graczy-467/">Podkładki dla graczy</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/sluchawki-dla-graczy-466/">Słuchawki dla graczy</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/gamepady-10/">Gamepady</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/kierownice-116/">Kierownice</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/joysticki-709/">Joysticki</a>
</li>
<li>
<a href="/biuro/meble-biurowe/fotele-dla-graczy-747/">Fotele dla graczy</a>
</li>
<li>
 <a href="/strefa-gracza/sprzet-dla-graczy/odziez-gamingowa-564/">Odzież gamingowa</a>
</li>
<li class="sb-bold">
<a href="/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098459/1/">Laptopy dla graczy</a>
</li>
<li class="sb-bold">
<a href="/komputery/monitory-i-akcesoria/monitory-led-lcd-523/,,,,,,,,,,30812O1044560/1/">Monitory dla graczy</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/strefa-gracza/konsole-akcesoria/">Akcesoria do konsol</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Akcesoria do konsol
</a>
</li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/kontrolery-ruchu-do-konsol-771/">Kontrolery ruchu do konsol</a>
</li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/gamepady-10/,,,,,,,,,,29078O905688.972535.902048.905723.944304.1019375.905677/1/">Gamepady do konsol</a>
</li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/kierownice-116/">Kierownice</a>
</li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/nakladki-na-kontrolery-569/">Nakładki na kontrolery</a>
</li>
 <li>
<a href="/strefa-gracza/konsole-akcesoria/ladowarki-baterie-zasilacze-571/">Ładowarki, baterie, zasilacze</a>
</li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/akcesoria-do-konsol-74/">Akcesoria, konwertery, kable</a>
</li>
</ul>
</li>
</ul>
</div>
<div class="menu-sis menu-gamer hidden-xs hidden-sm">
<div class="label">Sklepy producentów:</div>
<div class="content">
<div class="item">
<a href="/thrustmaster/" class="logo-thrustmaster">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/razer/" class="logo-razer">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
</div>
<div class="clearBoth"></div>
</div>
</div>
</li>
<li class="pr-menu-item menu-all  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/biuro/"><span class="helper-before"></span>Biuro<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Biuro
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/akcesoria-biurowe/">Akcesoria biurowe</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/artykuly-pakowe/">Artykuły pakowe</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/artykuly-papiernicze/">Artykuły papiernicze</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/artykuly-pismiennicze/">Artykuły piśmiennicze</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/artykuly-szkolne/">Artykuły szkolne</a>
</div>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/chemia-gospodarcza-i-art-higieniczne/">Chemia gospodarcza i art. higieniczne</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/biuro/edukacja/">Edukacja</a>
</div>
</li>
 <li class="">
<div class="pr-menu-inner">
<a href="/biuro/meble-biurowe/">Meble biurowe</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/organizacja-dokumentow/">Organizacja dokumentów</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/prezentacja-wizualna/">Prezentacja wizualna</a>
</div>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/sejfy-i-kasetki/">Sejfy i kasetki</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/urzadzenia-biurowe/">Urządzenia biurowe</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/zegarki/">Zegarki</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/urzadzenia-biurowe/zestawy-biurowe-737/">Zestawy biurowe</a>
</div>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
</ul>
</div>
 </div>
</li>
</ul>
</nav> </div>
</div>
</header>
<div class="search-content mobile" id="search_content_mobile">
<form action="/search/search2" method="post" name="searchform" id="searchform">
<input type="hidden" name="search_type" value="product" />
<div class="search-content-form">
<div class="search-content-input">
<input type="text" name="search" data-lang-product1="produkt" data-lang-product234="produkty" data-lang-productOthers="produktów" data-lang-error="Wystąpił błąd podczas wyszukiwania, proszę spróbować później" data-lang-category-title="Przejdź do kategorii:" data-lang-autocomplete-search="Wyszukiwarka morele.net" class="autocompl-input" placeholder="Szukany produkt..." />
<div class="search-content-category">
<input id="searchNarrowType" name="catalog_type" type="hidden" value="catdep" />
<div class="m-dropdown m-dropdown-serch">
<input id="searchNarrowData" type="hidden" name="elem_id" value="0" class="dropdown-input" />
<button class="btn btn-searchCategory btn-block dropdown-button" type="button">
<span class="default">
wszystkie działy </span>
</button>
<div class="dropdown-content">
<div class="list">
<ul>
<li data-value="0" class="dropdown-active">wszystkie działy</li>
<li data-value="1">Komputery</li>
<li data-value="2">RTV</li>
<li data-value="3">Fotografia i kamery</li>
<li data-value="4">AGD</li>
<li data-value="5">Telefony</li>
<li data-value="6">Biuro</li>
<li data-value="7">Strefa Gracza</li>
<li data-value="8">Usługi</li>
<li data-value="38">Laptopy</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="search-content-button">
<button type="submit" class="btn btn-primary btnSearch">
<i class="icon-morele-search"></i>
</button>
</div>
</div>
</form></div> <section class="page-content">
<section id="content" data-role="content">
<div class="container-fluid">
<div class="breadcrumbs-wrap">
<ol id="breadcrumbs" class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList"><li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/" itemprop="item"><span itemprop="name">Morele.net</span></a><meta itemprop="position" content="1" /></li><li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/telefony/" itemprop="item"><span itemprop="name">Telefony</span></a><meta itemprop="position" content="2" /></li><li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/telefony/telefony-smartfony-krotkofalowki/" itemprop="item"><span itemprop="name">Telefony, smartfony, krótkofalówki</span></a><meta itemprop="position" content="3" /></li><li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="/telefony/telefony-smartfony-krotkofalowki/smartfony-280/" itemprop="item"><span itemprop="name">Smartfony</span></a><meta itemprop="position" content="4" /></li><li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)</span><meta itemprop="position" content="5" /></li></ol>
</div>
</div>
<div class="product-data apri-data" data-product-id="976330" data-user-id="" data-user-logged="notLogged" data-reviews-page="1" data-reviews-sort="by_date" data-reviews-rate="0" data-tech-page="1" data-product-active="false" data-variants-products="false">
<div class="container-fluid">
<a class="goback-mobile" href="/telefony/telefony-smartfony-krotkofalowki/smartfony-280/" class="title"><i class="fa fa-chevron-left" aria-hidden="true"></i>Smartfony</a>
<div class="section-compare hidden-xs">
<h4>Porównaj produkty:</h4>
<div id="section_compare_mask"></div>
<div class="section-compare-inner" id="section_compare">
<div class="section-compare-items" id="category_compare_items">
<div class="section-compare-item" id="box_1">
<a class="icon-remove"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
</div>
<div class="section-compare-item" id="box_2">
<a class="icon-remove"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
</div>
<div class="section-compare-item" id="box_3">
<a class="icon-remove"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
</div>
<div class="section-compare-item" id="box_4">
<a class="icon-remove"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
</div>
<div class="section-compare-item" id="box_5">
<a class="icon-remove"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
</div>
</div>
<div class="section-compare-button">
<a href="#" id="button_collate" class="btn btn-sm btn-primary">Porównaj</a>
</div>
</div>
</div>
<div class="product-top">
<div class="row">
<div class="col-xs-12 col-md-3 col-md-push-9 hidden-sm">
<div class="row">
<div class="col-xs-6 col-xs-push-6 col-sm-push-0 col-sm-12 text-right">
<div class="product-brandcert">
<a href="/info/brand/191/" id="open_brand_cer_info" data-title="Informacje o certyfikacie">Autoryzowany Partner Samsung</a>
<div class="hidden" id="brand_cer_detail">
<div class="brand-cer-detail">
<div class="cer-name">Autoryzowany Partner Samsung</div>
<div class="cer-logo">
<img data-src="https://images.morele.net/brand/191.gif" alt="Autoryzowany Partner Samsung" />
</div>
<div class="cer-desc">Dzięki temu certyfikatowi klient może mieć pewność, że kupując u nas produkty firmy Samsung kupuje sprzęt oryginalny z legalnej dystrybucji, posiadający gwarancję na terenie RP. Certyfikat Autoryzowany Partner Samsung informuje naszych klientów o tym, że pracownicy Morele.net dysponują odpowiednią wiedzą techniczną na temat oferowanych produktów Samsung.</div>
<div class="cer-image">
<a href="https://images.morele.net/brcert/full/191_2.jpg" class="open-cer-image" data-title="Autoryzowany Partner Samsung">
<img data-src="https://images.morele.net/brcert/i48/191_2.jpg" alt="Autoryzowany Partner Samsung" />
</a>
</div>
</div>
</div>
</div>
</div>
<div class="col-xs-6 col-xs-pull-6 col-sm-pull-0 col-sm-12 text-xs-left text-right">
<div class="product-id">ID produktu:
976330
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-md-9 col-md-pull-3" itemprop="name">
<h1 class="page-title">Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)</h1>
<div class="title-fname">Pamięć wewnętrzna: 64 GB Szary 2960 x 1440 Klasa premium Phablet</div>
<div class="row hidden-xs">
<div class="col-sm-6">
<div class="panel-over-image">
<div class="checkbox compi">
<label>
<input id="compare" type="checkbox" data-cat-id="280" name="compare_976330" data-product-id="976330" />
<span class="input"></span>
do porównania </label>
</div>
<div class="product-fb">
<div class="fb-like" data-href="/smartfon-samsung-galaxy-s8-orchid-grey-sm-g950f-976330/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
</div>
</div>
</div>
<div class="col-sm-6 hidden-xs hidden-md hidden-lg text-right">
<div class="product-brandcert">
<a href="/info/brand/191/" id="open_brand_cer_info" data-title="Informacje o certyfikacie">Autoryzowany Partner Samsung</a>
</div>
<div class="product-id">ID produktu:
976330
</div>
</div>
</div>
</div>
</div>
<div class="row hidden-sm hidden-md hidden-lg">
<div class="col-xs-6">
<div class="product-rating scroll-to-reviews">
<div class="rating-container rate-disabled">
<div class="not-avalible"><span>Oceń jako pierwszy</span></div>
</div>
</div>
</div>
<div class="col-xs-6 text-right">
<div class="product-buy">
Kupiły: <b class="get-buy-info blue" data-product-id="976330" data-days="90">22 osoby</b>
<i class="icon-morele-question-mark-in-dark-circle" data-tooltip="Ten produkt w ciągu ostatnich 90 dni kupiły 22 osoby"></i>
</div>
</div>
</div>
</div>
<div class="product-middle">
<div class="row">
<div class="col-sm-8 col-md-6 block-gallery">
<div class="product-gallery">
<div class="big-image" id="light_gallery" data-product-image="https://images.morele.net/i500/976330_0_i500.jpg">
<img width="500" height="500" src="https://images.morele.net/i500/976330_0_i500.jpg" alt="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)" title="SM-G950FZVAXEO" />
<a data-link="0" data-src="https://images.morele.net/full/976330_0_f.jpg" data-index="1" title="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)"></a>
<a data-link="1" data-src="https://images.morele.net/full/976330_1_f.jpg" data-index="2" title="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)"></a>
<a data-link="2" data-src="https://images.morele.net/full/976330_2_f.jpg" data-index="3" title="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)"></a>
<a data-link="3" data-src="https://images.morele.net/full/976330_3_f.jpg" data-index="4" title="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)"></a>
<a data-link="4" data-src="https://images.morele.net/full/976330_4_f.jpg" data-index="5" title="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)"></a>
<a data-link="5" data-src="https://images.morele.net/full/976330_5_f.jpg" data-index="6" title="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)"></a>
<a data-link="6" data-src="https://images.morele.net/full/976330_6_f.jpg" data-index="7" title="Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)"></a>
</div>
<div class="thumbs-images-wr">
<div class="thumbs-images owl-carousel">
<div class="thumbs-image">
<a href="https://images.morele.net/i500/976330_0_i500.jpg" class="owl-lazy" data-index="1" data-src="https://images.morele.net/i80/976330_0_i80.jpg"></a>
</div>
<div class="thumbs-image">
<a href="https://images.morele.net/i500/976330_1_i500.jpg" class="owl-lazy" data-index="2" data-src="https://images.morele.net/i80/976330_1_i80.jpg"></a>
</div>
<div class="thumbs-image">
<a href="https://images.morele.net/i500/976330_2_i500.jpg" class="owl-lazy" data-index="3" data-src="https://images.morele.net/i80/976330_2_i80.jpg"></a>
</div>
<div class="thumbs-image">
<a href="https://images.morele.net/i500/976330_3_i500.jpg" class="owl-lazy" data-index="4" data-src="https://images.morele.net/i80/976330_3_i80.jpg"></a>
</div>
<div class="thumbs-image">
<a href="https://images.morele.net/i500/976330_4_i500.jpg" class="owl-lazy" data-index="5" data-src="https://images.morele.net/i80/976330_4_i80.jpg"></a>
</div>
<div class="thumbs-image">
<a href="https://images.morele.net/i500/976330_5_i500.jpg" class="owl-lazy" data-index="6" data-src="https://images.morele.net/i80/976330_5_i80.jpg"></a>
</div>
<div class="thumbs-image">
<a href="https://images.morele.net/i500/976330_6_i500.jpg" class="owl-lazy" data-index="7" data-src="https://images.morele.net/i80/976330_6_i80.jpg"></a>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-3 col-md-push-3 block-offers" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
<div class="sidebar-block add-to-basket-section">
<div class="price">
<div class="price-new" itemprop="priceCurrency" content="PLN">
<div class="text">Cena:</div>
<div class="value" id="product_price_brutto" itemprop="price" content="3398.00">3398,00 zł</div>
</div>
</div>
<div class="add-to-basket">
 <button class="btn btn-block btn-default btn-lg disabled">
Produkt niedostępny </button>
<link itemprop="availability" href="http://schema.org/OutOfStock" />
</div>
<div class="product-avialable">
<a href="/notify/976330/">Powiadom mnie kiedy produkt będzie dostępny</a>
</div>
</div>
<div class="ekp-box pd-box">
<div class="ekp-box-text">﻿Jeśli nie jesteś pewny,<br /> czy trafisz z prezentem,<br /> podaruj kartę<br /> podarunkową!</div>
<div class="text-right">
<a href="/kartypodarunkowe/">Zobacz kartę ></a>
</div>
</div>
</div>
<div class="col-xs-12 col-md-3 col-md-pull-3 block-options">
<div class="row">
<div class="col-sm-4 col-md-12">
<div class="product-guardian">
<div class="name"><b>Opiekun produktu</b> Marcin Płatek</div>
<div class="links">
<span>Zadaj pytanie:</span>
<a href="/info/napisz_zapytanie/42/976330/">handlowe</a>
<a id="brandtech" href="#tech_question" data-title="Pomoc techniczna producenta">techniczne</a>
<div id="brandtech_content" class="hidden">
<div class="brandtech">
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">Telefony:</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">0 801 172 678</div>
<div class="info-item">0 801 672 786</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">www:</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">
<a href="http://www.samsung.com/pl/info/contactus.html" title="Do pomocy technicznej">Link do pomocy technicznej</a>
</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">Opis:</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">&lt;p&gt;Samsung Polska: &lt;a href=&quot;http://www.samsung.com/pl/support/erms1/&quot; title=&quot;Wyślij zapytanie E-mail&quot;&gt;Wyślij zapytanie E-mail&lt;/a&gt;&lt;/p&gt;</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-4 col-md-12">
<b class="features-title">Podstawowe cechy:</b>
<ul class="features">
<li>
4G LTE:
<b>
Tak </b>
</li>
<li>
Dual SIM:
<b>
Nie </b>
</li>
<li>
Maksymalna pojemność karty:
<b>
Do 256 GB </b>
</li>
<li>
Obsługa kart pamięci:
<b>
Karta microSDXC </b>
</li>
<li>
Pamięć wewnętrzna:
<b>
64 GB  </b>
</li>
</ul>
</div>
<div class="col-sm-4 col-md-12 product-options">
<div class="product-buy hidden-xs">
Kupiły: <b class="get-buy-info blue" data-product-id="976330" data-days="90">22 osoby</b>
<i class="icon-morele-question-mark-in-dark-circle" data-tooltip="Ten produkt w ciągu ostatnich 90 dni kupiły 22 osoby"></i>
</div>
<div class="product-rating scroll-to-reviews hidden-xs">
<div class="rating-container rate-disabled">
<div class="not-avalible"><span>Oceń jako pierwszy</span></div>
</div>
</div>
<div class="product-fb hidden-sm hidden-md hidden-lg">
<div class="fb-like" data-href="/smartfon-samsung-galaxy-s8-orchid-grey-sm-g950f-976330/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
</div>
<div class="similar-products">
<a rel="nofollow" href="/telefony/telefony-smartfony-krotkofalowki/smartfony-280/,,,,,,,,,,30657O896605,30067O973057,6299O780189,30347O1024691,28065O1134869/1/">Zobacz podobne produkty</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="section-block recommended-products hidden">
<div class="section-title">Polecane produkty z tej kategorii</div>
<div class="owl-prod owl-carousel owl-preloader owl-recommended" data-category-id="280" data-lang-no-rate="Oceń jako pierwszy">
<div class="morele-preloader ">
<div class='loader3'><div><div><div><div></div></div></div></div></div>
<div class="loader-logo">
<svg data-name="morele" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156.25 161.49">
<title>Morele</title>
<path class="cls-1" d="M16.38,124.77H51.3c0.14-2.44.35-4.53,0.37-6.62,0.14-14.65.17-29.3,0.42-44A20.09,20.09,0,0,1,53.74,67a12.73,12.73,0,0,1,13.85-7.52c5.56,1,9.15,5.47,9.2,12,0.12,15.82.25,31.65-.06,47.47-0.1,4.93,1.46,6.23,6.19,6,8-.38,16-0.41,24,0,5,0.26,6.09-1.58,6-6.24-0.3-12.82-.21-25.65,0-38.47a46.45,46.45,0,0,1,1.38-11.82,12.2,12.2,0,0,1,13.91-8.76c5.68,0.91,9.65,5.57,9.72,12.14,0.15,15.65.07,31.31,0.08,47,0,1.79,0,3.58,0,5.94h23.56c-16.1,23.73-37.24,37-64.54,39.18C63.33,166.52,36.79,153.54,16.38,124.77Z" transform="translate(-5.21 -2.68)" />
<path class="cls-1" d="M109.37,46.81C102.77,34.92,92.16,31.79,80,32,68.07,32.28,58,36.78,49.39,47.18V34.27H22.18C40,13.57,61.25,3.22,87,2.7c27.21-.55,49.72,9.7,68.16,30.83C134.23,30.31,124.21,32.17,109.37,46.81Z" transform="translate(-5.21 -2.68)" />
<path class="cls-1" d="M15.52,45.45v76C1.8,104.69,1.76,62.48,15.52,45.45Z" transform="translate(-5.21 -2.68)" />
</svg>
</div>
</div>
</div>
</div>
</div>
<div class="m-tabs" data-feature-related="true">
<div class="m-tabs-h hidden-xs">
<div class="container-fluid">
 <div class="m-tabs-m">
<div class="item active" data-tabs-menu-id="2">Opis</div>
<div class="item" data-tabs-menu-id="3">Opinie <span class="review-count">()</span></div>
<div class="item" data-tabs-menu-id="4">Pytania i odpowiedzi <span class="tech-count">(0)</span></div>
<div class="item" data-tabs-menu-id="9">Produkty podobne</div>
<div class="item" data-tabs-menu-id="6">Zakup na raty</div>
<div class="item" data-tabs-menu-id="7">Gwarancja i zwroty</div>
</div>
</div>
</div>
<div class="m-tabs-b">
<div class="container-fluid">
<div class="m-tabs-cont">
<div class="item active" data-tabs-content-id="2">
<div class="m-tabs-ma" data-tabs-menu-id="2">Opis</div>
<div class="m-tabs-cont-in">
<h2 class="tabs-title">Opis produktu</h2>
<div class="bLazyContainer">
<div class="top-desc hidden-xs" id="all_description">
<div class="wc-desc description">
</div>
<div class="product-desc description" itemprop="description">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><p><span style="color: #ffffff;"><span style="font-size: 1pt;">Samsung Galaxy S8 Orchidea Grey</span>&nbsp;</span></p>
<p><iframe src="" width="1170" height="600" frameborder="0" allowfullscreen="allowfullscreen" class="lazy-desc" data-src="https://www.youtube.com/embed/S_ce01IOMK8"></iframe></p>
<p><iframe src="" width="1170" height="18100" frameborder="0" scrolling="no" class="lazy-desc" data-src="https://content.morele.net/Samsung/Galaxy%20s8/Galaxy%20s8/Standard/index.html"></iframe></p></body></html>
</div>
</div>
<div class="top-desc mobile-desc hidden-sm hidden-md hidden-lg" id="mobile_description">
<div class="wc-desc description">
</div>
<div class="product-desc description" itemprop="description">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html><body><p><span style="color: #ffffff;"><span style="font-size: 1pt;">Samsung Galaxy S8 Orchidea Grey</span>&nbsp;</span></p>
<p><iframe src="" width="1170" height="600" frameborder="0" allowfullscreen="allowfullscreen" class="lazy-desc" data-src="https://www.youtube.com/embed/S_ce01IOMK8"></iframe></p>
<p><iframe src="" width="1170" height="18100" frameborder="0" scrolling="no" class="lazy-desc" data-src="https://content.morele.net/Samsung/Galaxy%20s8/Galaxy%20s8/Standard/index.html"></iframe></p></body></html>
</div>
</div>
</div>
<div class="product-all-desc hidden-sm hidden-md hidden-lg">
<noindex><a href="/show-description/976330/" rel="nofollow" class="btn btn-primary btn-block">Pokaż pełny opis</a></noindex>
</div>
<div class="technical-specification">
<h2>Specyfikacja techniczna</h2>
<h3>Produkt</h3>
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
Producent </div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">
<a href="/kategorie/producenci/samsung-191/" target="_blank">Samsung</a>
</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
Kod producenta </div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">SM-G950FZVAXEO</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
EAN </div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">8806088717159</div>
</div>
</div>
</div>
<h3>Informacje podstawowe</h3>
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="29135">
Zastosowanie<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Klasa premium</div>
<div class="info-item">Phablet</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28057">
Kolor<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Szary</div>
</div>
</div>
</div>
<h3>Techniczne</h3>
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28059">
Procesor<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Exynos 8895</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
Pamięć RAM
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">4 GB</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="6298">
Pamięć wewnętrzna<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">64 GB</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
 <a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="6299">
Obsługa kart pamięci<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Karta microSDXC</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="30031">
Maksymalna pojemność karty<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Do 256 GB</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="30067">
Dual SIM<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
 </div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Nie</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="6296">
UMTS<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Tak</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
4G LTE
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Tak</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28068">
 Transmisja danych<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">HSPA</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="6300">
Złącza<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">USB typ C</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="30032">
Łączność bezprzewodowa<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">WiFi</div>
 <div class="info-item">NFC</div>
<div class="info-item">Bluetooth v5.0</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28061">
Sterowanie<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Boczny klawisz regulacji głośności</div>
<div class="info-item">Ekran dotykowy</div>
<div class="info-item">MultiTouch</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
Dodatkowe cechy
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Certyfikat IP68</div>
<div class="info-item">Corning Gorilla Glass 5</div>
<div class="info-item">Zakrzywiony ekran Edge</div>
</div>
</div>
</div>
<h3>Wyświetlacz</h3>
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28063">
Rodzaj wyświetlacza<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Super AMOLED</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="24640">
Przekątna ekranu [cal]<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
 <div class="info-item">5.8</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28064">
Rozdzielczość<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">2960 x 1440</div>
</div>
</div>
</div>
<h3>Oprogramowanie</h3>
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
Platforma
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Android</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28065">
System operacyjny<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Android 7.0</div>
</div>
</div>
</div>
<h3>Multimedia</h3>
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="21022">
Matryca aparatu głównego [MPix]<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">12</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
Matryca aparatu przedniego [MPix]
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">8</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="6313">
Radio<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Nie</div>
</div>
</div>
</div>
<h3>Funkcje</h3>
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="21023">
Moduł GPS<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Tak</div>
</div>
</div>
 <div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="21024">
Rodzaj nawigacji<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">
<a href="#" class="f-popup" data-raty-to="fvalue" data-fvalue-id="903823">
A-GPS<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="info-item">Beidou</div>
<div class="info-item">Galileo</div>
<div class="info-item">Glonass</div>
<div class="info-item">
<a href="#" class="f-popup" data-raty-to="fvalue" data-fvalue-id="794852">
GPS<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
 </div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28071">
Dodatkowe funkcje<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Akcelerometr</div>
<div class="info-item">Barometr</div>
<div class="info-item">Bezprzewodowe ładowanie</div>
<div class="info-item">Czujnik Halla</div>
<div class="info-item">Czujnik linii papilarnych</div>
<div class="info-item">Czujnik światła</div>
<div class="info-item">Czujnik zbliżeniowy</div>
<div class="info-item">Funkcja głośnomówiąca</div>
<div class="info-item">
<a href="#" class="f-popup" data-raty-to="fvalue" data-fvalue-id="897167">
Geotagging<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="info-item">Kompas cyfrowy</div>
<div class="info-item">Magnetometr</div>
<div class="info-item">Pulsometr</div>
<div class="info-item">Skaner tęczówki</div>
<div class="info-item">Żyroskop</div>
</div>
</div>
</div>
<h3>Bateria</h3>
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28072">
Akumulator<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Li-Ion</div>
<div class="info-item">Zintegrowany</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28073">
Pojemność [mAh]<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">3000</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28074">
Maksymalny czas czuwania [h]<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Brak danych</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="6328">
Maksymalny czas rozmów [h]<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">20</div>
</div>
</div>
</div>
<h3>Fizyczne</h3>
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="6291">
Wysokość [mm]<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">148.9</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
 <a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="6292">
Szerokość [mm]<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">68.1</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="6293">
Głębokość [mm]<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">8</div>
</div>
</div>
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28058">
Waga [g]<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
 <div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">155</div>
</div>
</div>
</div>
<h3>Załączone wyposażenie</h3>
<div class="table-info">
<div class="row table-info-item">
<div class="table-info-inner name col-xs-6 col-sm-5">
<a href="#" class="f-popup" data-raty-to="fhead" data-fhead-id="28075">
Wyposażenie<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader hidden" aria-hidden="true"></i>
</a>
</div>
<div class="table-info-inner col-xs-6 col-sm-7">
<div class="info-item">Adapter USB (C-A)</div>
<div class="info-item">Adapter USB (C-B)</div>
<div class="info-item">Instrukcja obsługi</div>
<div class="info-item">Kabel USB</div>
<div class="info-item">Ładowarka</div>
<div class="info-item">Narzędzie do wyciągania karty SIM</div>
 <div class="info-item">Słuchawki</div>
</div>
</div>
</div>
</div>
<div class="product-desc-print"><a rel="nofollow" href="/drukuj-opis-produktu/976330/">Drukuj opis</a></div>
<div class="product-desc-error"><b>Błąd w opisie? <a rel="nofollow" href="/zglos-blad/976330/">Zgłoś!</a></b></div>
</div>
</div>
<div class="item" data-tabs-content-id="3" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
<div class="m-tabs-ma" data-tabs-menu-id="3">Opinie <span class="review-count">()</span></div>
<div class="m-tabs-cont-in">
<div class="reviews-wrap">
<h2 class="tabs-title">Opinie, recenzje i testy dla produktu:</h2>
<div class="tabs-product-name">Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)</div>
<div class="product-reviews-rate" id="product_reviews_rate">
<div class="row">
<div class="rate-wrap-container col-sm-8 col-md-6">
<div class="rate-wrap">
<div class="row">
<div class="col-sm-6">
<div class="rate-left">
<div class="rate-stars"></div>
<div class="rate-rating">
Średnia ocena: <b class="value" itemprop="ratingValue"></b>
</div>
<div class="rate-quantity">
Opinie: <b class="value" itemprop="reviewCount"></b>
</div>
<div class="rate-good">
<span class="value"></span>% osób poleca ten produkt </div>
</div>
</div>
<div class="col-sm-6">
<div class="rate-info"></div>
</div>
</div>
</div>
</div>
<div class="add-rate-button col-sm-4 col-md-5 col-md-offset-1">
<div class="add-rate">
<div class="label">Posiadasz ten produkt? Oceń by pomóc innym klientom w podjęciu właściwego wyboru</div>
<div class="button">
<button type="button" class="btn btn-primary add-review" data-toggle="#ad_rate_form" data-toggle-duration="300">Oceń produkt</button>
</div>
</div>
</div>
</div>
</div>
<div class="add-review-form hidden" id="ad_rate_form">
<div class="title">Dodaj opinie</div>
<form id="review_form">
<div class="row">
<div class="col-sm-6">
<div class="form-item-validate required checkIllegalCharacters">
<label for="form_up_nick">Twój podpis:</label>
<div class="form-item-validate-input">
<input type="text" name="author_name" class="form-control" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="form-item-validate required checkEmail checkIllegalCharacters">
<label for="author_email">Twój e-mail:</label>
<div class="form-item-validate-input">
<input type="text" name="author_email" maxlength="40" class="form-control author-email" value="" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6 form-item">
<label for="review_rate">Ocena:</label>
<div class="m-dropdown m-dropdown-b">
<div class="form-item-validate required">
<div class="form-item-validate-dropdown">
<input type="hidden" name="review_rate" class="dropdown-input" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
 <button class="btn btn-dropdown btn-block btn-offer dropdown-button" type="button">
<span class="default">Wybierz</span>
<span class="icon-select"></span>
</button>
<div class="dropdown-content">
<div class="list">
<ul>
<li data-value="" class="dropdown-active">Wybierz</li>
<li data-value="5">5 - Rewelacja</li>
<li data-value="4">4 - Dobry</li>
<li data-value="3">3 - Zadowalający</li>
<li data-value="2">2 - Średni</li>
<li data-value="1">1 - Słaby</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-6 line-margin-xs">
<label for="review_owntime">Produkt posiadam:</label>
<div class="m-dropdown m-dropdown-b">
<div class="form-item-validate required">
<div class="form-item-validate-dropdown">
<input type="hidden" name="review_owntime" class="dropdown-input" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
<button class="btn btn-dropdown btn-block btn-offer dropdown-button" type="button">
<span class="default">Wybierz</span>
<span class="icon-select"></span>
</button>
<div class="dropdown-content">
<div class="list">
<ul>
<li data-value="" class="dropdown-active">Wybierz</li>
 <li data-value="1">Od dziś</li>
<li data-value="2">Od kilku dni</li>
<li data-value="3">1-3 tygodnie</li>
<li data-value="4">1-2 miesiące</li>
<li data-value="5">3-6 miesięcy</li>
<li data-value="6">Ponad pół roku</li>
<li data-value="7">Ponad rok</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="form-item form-item-validate required checkIllegalCharacters">
<label for="review_comment">Krótki komentarz:</label>
<div class="form-item-validate-input">
<textarea class="form-control" rows="5" name="review_comment"></textarea>
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
<div class="row form-item">
<div class="col-sm-6">
<div class="form-item form-item-validate checkIllegalCharacters">
<label for="review_good">Mocne strony:</label>
<div class="form-item-validate-input">
<textarea class="form-control" rows="5" name="review_good"></textarea>
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="form-item form-item-validate checkIllegalCharacters">
<label for="review_bad">Słabe strony:</label>
<div class="form-item-validate-input">
<textarea class="form-control" rows="5" name="review_bad"></textarea>
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
</div>
<div class="form-item text-right">
<button type="submit" class="btn btn-primary">Dodaj recenzję</button>
</div>
</form>
</div>
<div class="reviews-list-wrap">
<div class="reviews-list-title">Opinie klientów</div>
<div class="rev-filters">
<div class="row">
<div class="col-sm-6">
<div class="m-dropdown m-dropdown-bs m-dropdown-filter">
<div class="form-item-validate">
<div class="form-item-validate-dropdown">
<input type="hidden" name="review_filter_rate" class="dropdown-input" value="0" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
<button class="btn btn-dropdown btn-block btn-offer dropdown-button" type="button">
<span class="default">
Wszystkie oceny </span>
<span class="icon-select"></span>
</button>
<div class="dropdown-content">
 <div class="list">
<ul>
<li data-value="0" class="dropdown-active">
Wszystkie oceny </li>
<li data-value="1">
1 gwiazdka
</li>
<li data-value="2">
2 gwiazdki
</li>
<li data-value="3">
3 gwiazdki
</li>
<li data-value="4">
4 gwiazdki
</li>
<li data-value="5">
5 gwiazdek
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="m-dropdown m-dropdown-bs m-dropdown-sort">
 <div class="form-item-validate">
<div class="form-item-validate-dropdown">
<input type="hidden" name="review_filter_rate" class="dropdown-input" value="by_date" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
<button class="btn btn-dropdown btn-block btn-offer dropdown-button" type="button">
<span class="default">Najnowsze</span>
<span class="icon-select"></span>
</button>
<div class="dropdown-content">
<div class="list">
<ul>
<li data-value="by_date" class="dropdown-active">Najnowsze</li>
<li data-value="by_helpful">Najbardziej pomocne</li>
<li data-value="by_rating">Ocena: od najwyższej</li>
<li data-value="by_rating_asc">Ocena: od najniższej</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="clearBoth"></div>
<div class="reviews-list" id="reviews_list">
</div>
<div class="pagination-wrapper" id="reviews_pagination" data-page="1"><ul class="pagination dynamic"></ul></div>
<div class="morele-preloader ">
<div class='loader3'><div><div><div><div></div></div></div></div></div>
<div class="loader-logo">
<svg data-name="morele" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156.25 161.49">
<title>Morele</title>
<path class="cls-1" d="M16.38,124.77H51.3c0.14-2.44.35-4.53,0.37-6.62,0.14-14.65.17-29.3,0.42-44A20.09,20.09,0,0,1,53.74,67a12.73,12.73,0,0,1,13.85-7.52c5.56,1,9.15,5.47,9.2,12,0.12,15.82.25,31.65-.06,47.47-0.1,4.93,1.46,6.23,6.19,6,8-.38,16-0.41,24,0,5,0.26,6.09-1.58,6-6.24-0.3-12.82-.21-25.65,0-38.47a46.45,46.45,0,0,1,1.38-11.82,12.2,12.2,0,0,1,13.91-8.76c5.68,0.91,9.65,5.57,9.72,12.14,0.15,15.65.07,31.31,0.08,47,0,1.79,0,3.58,0,5.94h23.56c-16.1,23.73-37.24,37-64.54,39.18C63.33,166.52,36.79,153.54,16.38,124.77Z" transform="translate(-5.21 -2.68)" />
 <path class="cls-1" d="M109.37,46.81C102.77,34.92,92.16,31.79,80,32,68.07,32.28,58,36.78,49.39,47.18V34.27H22.18C40,13.57,61.25,3.22,87,2.7c27.21-.55,49.72,9.7,68.16,30.83C134.23,30.31,124.21,32.17,109.37,46.81Z" transform="translate(-5.21 -2.68)" />
<path class="cls-1" d="M15.52,45.45v76C1.8,104.69,1.76,62.48,15.52,45.45Z" transform="translate(-5.21 -2.68)" />
</svg>
</div>
</div>
</div>
<div id="add_revies_answer_form" class="hidden">
<div class="review-answer-form-wrap hidden">
<form class="review-answers-form" action="">
<div class="row">
<div class="col-sm-6 form-item-validate required checkIllegalCharacters">
<label for="form_up_nick">Twój podpis:</label>
<div class="form-item-validate-input">
<input type="text" name="author_name" class="form-control" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
<div class="col-sm-6">
<div class="form-item-validate required checkEmail checkIllegalCharacters">
<label for="author_email">Twój e-mail:</label>
<div class="form-item-validate-input">
<input type="text" name="author_email" maxlength="40" class="form-control author-email" value="" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
</div>
<div class="row line-margin">
<div class="col-xs-12">
<div class="form-item-validate required checkIllegalCharacters">
<label for="comment">Wpisz swój komentarz do opinii:</label>
<div class="form-item-validate-input">
<textarea name="comment" class="form-control comment" rows="5"></textarea>
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
</div>
<div class="row text-right">
<div class="col-xs-12">
<button type="submit" class="btn btn-primary" id="add_new_comment">Dodaj komentarz</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="item" data-tabs-content-id="4">
<div class="m-tabs-ma" data-tabs-menu-id="4">Pytania i odpowiedzi <span class="tech-count">(0)</span></div>
<div class="m-tabs-cont-in">
<div class="questions-answers">
<h2 class="tabs-title">Pytania i odpowiedzi dotyczące produktu:</h2>
<div class="tabs-product-name">Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)</div>
<div class="button-add-question">
<button class="btn btn-large btn-primary add-question" data-toggle="#questions_answers_form_wrap" data-toggle-duration="300">Zadaj pytanie</button>
</div>
<div class="questions-answers-form-wrap hidden" id="questions_answers_form_wrap">
<div class="title">Napisz zapytanie techniczne</div>
<form class="questions-form" id="questions_form" action="">
<div class="row">
<div class="col-sm-6">
<div class="form-item-validate required checkIllegalCharacters">
<label for="form_up_nick">Twój podpis:</label>
<div class="form-item-validate-input">
<input type="text" name="author_name" class="form-control" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="form-item-validate required checkEmail checkIllegalCharacters">
<label for="author_email">Twój e-mail:</label>
<div class="form-item-validate-input">
<input type="text" name="author_email" maxlength="40" class="form-control author-email" value="" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
 </div>
</div>
<div class="row line-margin">
<div class="col-sm-12">
<div class="form-item-validate required checkIllegalCharacters">
<label for="tech_title">Tytuł:</label>
<div class="form-item-validate-input">
<input type="text" name="tech_title" class="form-control question-title" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
</div>
<div class="row line-margin">
<div class="col-xs-12">
<div class="form-item-validate required checkIllegalCharacters">
<label for="comment">Treść:</label>
<div class="form-item-validate-input">
<textarea name="tech_body" class="form-control comment" rows="5"></textarea>
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
</div>
<div class="row text-right">
<div class="col-xs-12">
<button type="submit" class="btn btn-primary" id="add_new_comment">Dodaj pytanie</button>
</div>
</div>
</form>
</div>
<div class="questions-answers-wrapper">
<div class="questions-answers-list" id="questions_answers_list">
</div>
<div class="pagination-wrapper" id="tech_pagination" data-page="1"><ul class="pagination dynamic"></ul></div>
<div class="morele-preloader ">
<div class='loader3'><div><div><div><div></div></div></div></div></div>
<div class="loader-logo">
<svg data-name="morele" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156.25 161.49">
<title>Morele</title>
<path class="cls-1" d="M16.38,124.77H51.3c0.14-2.44.35-4.53,0.37-6.62,0.14-14.65.17-29.3,0.42-44A20.09,20.09,0,0,1,53.74,67a12.73,12.73,0,0,1,13.85-7.52c5.56,1,9.15,5.47,9.2,12,0.12,15.82.25,31.65-.06,47.47-0.1,4.93,1.46,6.23,6.19,6,8-.38,16-0.41,24,0,5,0.26,6.09-1.58,6-6.24-0.3-12.82-.21-25.65,0-38.47a46.45,46.45,0,0,1,1.38-11.82,12.2,12.2,0,0,1,13.91-8.76c5.68,0.91,9.65,5.57,9.72,12.14,0.15,15.65.07,31.31,0.08,47,0,1.79,0,3.58,0,5.94h23.56c-16.1,23.73-37.24,37-64.54,39.18C63.33,166.52,36.79,153.54,16.38,124.77Z" transform="translate(-5.21 -2.68)" />
 <path class="cls-1" d="M109.37,46.81C102.77,34.92,92.16,31.79,80,32,68.07,32.28,58,36.78,49.39,47.18V34.27H22.18C40,13.57,61.25,3.22,87,2.7c27.21-.55,49.72,9.7,68.16,30.83C134.23,30.31,124.21,32.17,109.37,46.81Z" transform="translate(-5.21 -2.68)" />
<path class="cls-1" d="M15.52,45.45v76C1.8,104.69,1.76,62.48,15.52,45.45Z" transform="translate(-5.21 -2.68)" />
</svg>
</div>
</div>
</div>
<div id="answer_form" class="hidden">
<div class="answer-form-wrap hidden">
<form class="answers-form" action="">
<div class="row">
<div class="col-sm-6">
<div class="form-item-validate required">
<label for="form_up_nick">Twój podpis:</label>
<div class="form-item-validate-input">
<input type="text" name="author_name" class="form-control" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="form-item-validate required checkEmail">
<label for="author_email">Twój e-mail:</label>
<div class="form-item-validate-input">
<input type="text" name="author_email" maxlength="40" class="form-control author-email" value="" />
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
</div>
<div class="row line-margin">
<div class="col-xs-12">
<div class="form-item-validate required">
<label for="comment">Napisz odpowiedź:</label>
<div class="form-item-validate-input">
<textarea name="comment" class="form-control comment" rows="5"></textarea>
<i class="fa fa-check validate-valid" aria-hidden="true"></i>
<i class="fa fa-times validate-unvalid" aria-hidden="true"></i>
</div>
</div>
</div>
</div>
<div class="row text-right">
<div class="col-xs-12">
<button type="submit" class="btn btn-primary" id="add_new_comment">Dodaj odpowiedź</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="item" data-tabs-content-id="9">
<div class="m-tabs-ma" data-tabs-menu-id="9">Produkty podobne</div>
<div class="m-tabs-cont-in">
<div class="section-block feature-related-product">
<div class="owl-prod owl-carousel owl-preloader" id="feature_related" data-lang-no-rate="Oceń jako pierwszy" data-products-url-without-filters="/api/feature_related/telefony/telefony-smartfony-krotkofalowki/smartfony-280" data-products-url-with-filters="/telefony/telefony-smartfony-krotkofalowki/smartfony-280/,,,,,,,,,,30657O896605,30067O973057,6299O780189,30347O1024691,28065O1134869/1/" data-product-id="976330">
<div class="morele-preloader ">
<div class='loader3'><div><div><div><div></div></div></div></div></div>
<div class="loader-logo">
<svg data-name="morele" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156.25 161.49">
<title>Morele</title>
<path class="cls-1" d="M16.38,124.77H51.3c0.14-2.44.35-4.53,0.37-6.62,0.14-14.65.17-29.3,0.42-44A20.09,20.09,0,0,1,53.74,67a12.73,12.73,0,0,1,13.85-7.52c5.56,1,9.15,5.47,9.2,12,0.12,15.82.25,31.65-.06,47.47-0.1,4.93,1.46,6.23,6.19,6,8-.38,16-0.41,24,0,5,0.26,6.09-1.58,6-6.24-0.3-12.82-.21-25.65,0-38.47a46.45,46.45,0,0,1,1.38-11.82,12.2,12.2,0,0,1,13.91-8.76c5.68,0.91,9.65,5.57,9.72,12.14,0.15,15.65.07,31.31,0.08,47,0,1.79,0,3.58,0,5.94h23.56c-16.1,23.73-37.24,37-64.54,39.18C63.33,166.52,36.79,153.54,16.38,124.77Z" transform="translate(-5.21 -2.68)" />
<path class="cls-1" d="M109.37,46.81C102.77,34.92,92.16,31.79,80,32,68.07,32.28,58,36.78,49.39,47.18V34.27H22.18C40,13.57,61.25,3.22,87,2.7c27.21-.55,49.72,9.7,68.16,30.83C134.23,30.31,124.21,32.17,109.37,46.81Z" transform="translate(-5.21 -2.68)" />
<path class="cls-1" d="M15.52,45.45v76C1.8,104.69,1.76,62.48,15.52,45.45Z" transform="translate(-5.21 -2.68)" />
</svg>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="item" data-tabs-content-id="6">
<div class="m-tabs-ma" data-tabs-menu-id="6">Zakup na raty</div>
<div class="m-tabs-cont-in installmentLeasing">
<div class="installment">
<h2 class="tabs-title">Raty w morele.net - niskie koszty, minimum formalności</h2>
<div class="row">
<div class="col-sm-6 col-md-5">
<h3><b>Oblicz wysokość raty:</b></h3>
<div class="installment-calcilators">
<div>
<a href="https://wniosek.eraty.pl/symulator/oblicz/numerSklepu/15543/wariantSklepu/1/typProduktu/0/wartoscTowarow/3398.00" target="_blank">
<img class="lazy" data-src="/static/img/shop/img-eraty.png" data-alt="Santander Consumer Bank S.A" />
</a>
</div>
</div>
</div>
<div class="col-sm-6 col-md-7">
<h3><b>Dowiedz się wiecej:</b></h3>
<ul class="list-with-links">
<li><a href="/pokaz_pomoc/22/" target="_blank">Jak kupić towar na raty w Santander Consumer Bank S.A?</a></li>
<li><a href="/pokaz_pomoc/114/" target="_blank">Jakie są koszty kredytu ratalnego?</a></li>
</ul>
</div>
</div>
</div>
<div class="leasing">
<h2>Leasing w morele.net - niskie koszty, minimum formalności</h2>
<ul class="list-with-links">
<li><a target="_blank" href="/pokaz_pomoc/106/">Przebieg transakcji</a></li>
<li><a target="_blank" href="/pokaz_pomoc/262/">Koszt Leasingu</a></li>
</ul>
</div>
</div>
</div>
<div class="item" data-tabs-content-id="7">
<div class="m-tabs-ma" data-tabs-menu-id="7">Gwarancja i zwroty</div>
<div class="m-tabs-cont-in warranty">
<h2 class="tabs-title">Warunki gwarancji produktu:</h2>
<div class="tabs-product-name">Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)</div>
<div class="warranty-table display-table allWidth reset-table-xs">
<div class="warranty-item table-row">
<div class="table-col v-align-top label">Długość gwarancji:</div>
<div class="table-col v-align-top value">24 miesiące</div>
</div>
<div class="warranty-item table-row">
<div class="table-col v-align-top label">Typ gwarancji:</div>
<div class="table-col v-align-top value">Producenta</div>
</div>
</div>
<div class="warranty-info">
<h3>Informacje o gwarancji producenta</h3>
<div class="warranty-table display-table allWidth reset-table-xs">
<div class="warranty-item table-row">
<div class="table-col v-align-top label">Telefony:</div>
<div class="table-col v-align-top value">
<div>0 801 172 678</div>
<div>0 801 672 786</div>
</div>
</div>
<div class="warranty-item table-row">
<div class="table-col v-align-top label">www:</div>
<div class="table-col v-align-top value">
<a href="http://www.samsung.com/pl/info/contactus.html">Link do pomocy technicznej</a>
</div>
</div>
<div class="warranty-item table-row">
<div class="table-col v-align-top label">Opis:</div>
<div class="table-col v-align-top value"><p>Samsung Polska: <a href="http://www.samsung.com/pl/support/erms1/" title="Wyślij zapytanie E-mail">Wyślij zapytanie E-mail</a></p></div>
</div>
</div>
</div>
<div class="cs-tabs warranty-tabs-wr">
<div class="cs-tab tab-active">
<div class="cs-tab-head">
Przedłuż gwarancję i ochroń swój sprzęt!
</div>
<div class="cs-tab-body">
<div class="display-table cs-tab-product">
<div class="table-row">
<div class="table-col cs-tab-product-img">
<a href="/dodatkowy-1-rok-gwarancji-181065/" title=" Dodatkowy 1 rok gwarancji">
<img class="lazy" data-src="https://images.morele.net/i256/181065_3_i256.png" data-alt=" Dodatkowy 1 rok gwarancji" />
</a>
 </div>
<div class="table-col cs-tab-product-name">
<a href="/dodatkowy-1-rok-gwarancji-181065/" title=" Dodatkowy 1 rok gwarancji">
Dodatkowy 1 rok gwarancji
</a>
</div>
<div class="table-col cs-tab-product-discount hidden-xs hidden-sm">
10% taniej </div>
<div class="table-col cs-tab-product-price">
<div class="discount hidden-md hidden-lg">10% taniej</div>
<div class="prices">
<div class="old-price">340,00 zł</div>
<div class="new-price">306,00 zł</div>
</div>
</div>
<div class="table-col cs-tab-product-button">
<label>
<input type="radio" name="productid_976330_gwarancja" class="add-to-link add-warranty cs-class-type_warranty-groupId_2463-csId_181065" data-text-add="Dodaj do koszyka" data-text-remove="Usuń produkt" data-group-id="2463" data-cs-id="181065" data-product-id="976330" data-cs-type="warranty" />
<span class="btn btn-sm btn-red add-to-basket">
Dodaj do koszyka </span>
</label>
<div class="add-to-basket-info">
<i class="fa fa-check" aria-hidden="true"></i> Produkt został dodany do koszyka wraz z produktem głównym </div>
</div>
</div>
</div>
<div class="display-table cs-tab-product">
<div class="table-row">
<div class="table-col cs-tab-product-img">
<a href="/dodatkowe-2-lata-gwarancji-181048/" title=" Dodatkowe 2 lata gwarancji">
<img class="lazy" data-src="https://images.morele.net/i256/181048_3_i256.png" data-alt=" Dodatkowe 2 lata gwarancji" />
</a>
</div>
<div class="table-col cs-tab-product-name">
<a href="/dodatkowe-2-lata-gwarancji-181048/" title=" Dodatkowe 2 lata gwarancji">
Dodatkowe 2 lata gwarancji
</a>
</div>
<div class="table-col cs-tab-product-discount hidden-xs hidden-sm">
10% taniej </div>
<div class="table-col cs-tab-product-price">
<div class="discount hidden-md hidden-lg">10% taniej</div>
<div class="prices">
<div class="old-price">510,00 zł</div>
<div class="new-price">459,00 zł</div>
</div>
</div>
<div class="table-col cs-tab-product-button">
<label>
<input type="radio" name="productid_976330_gwarancja" class="add-to-link add-warranty cs-class-type_warranty-groupId_2463-csId_181048" data-text-add="Dodaj do koszyka" data-text-remove="Usuń produkt" data-group-id="2463" data-cs-id="181048" data-product-id="976330" data-cs-type="warranty" />
<span class="btn btn-sm btn-red add-to-basket">
Dodaj do koszyka </span>
</label>
<div class="add-to-basket-info">
<i class="fa fa-check" aria-hidden="true"></i> Produkt został dodany do koszyka wraz z produktem głównym </div>
</div>
</div>
</div>
<div class="display-table cs-tab-product">
<div class="table-row">
<div class="table-col cs-tab-product-img">
<a href="/dodatkowe-3-lata-gwarancji-181031/" title=" Dodatkowe 3 lata gwarancji">
<img class="lazy" data-src="https://images.morele.net/i256/181031_3_i256.png" data-alt=" Dodatkowe 3 lata gwarancji" />
</a>
</div>
<div class="table-col cs-tab-product-name">
<a href="/dodatkowe-3-lata-gwarancji-181031/" title=" Dodatkowe 3 lata gwarancji">
Dodatkowe 3 lata gwarancji
</a>
</div>
<div class="table-col cs-tab-product-discount hidden-xs hidden-sm">
10% taniej </div>
<div class="table-col cs-tab-product-price">
<div class="discount hidden-md hidden-lg">10% taniej</div>
<div class="prices">
 <div class="old-price">680,00 zł</div>
<div class="new-price">612,00 zł</div>
</div>
</div>
<div class="table-col cs-tab-product-button">
<label>
<input type="radio" name="productid_976330_gwarancja" class="add-to-link add-warranty cs-class-type_warranty-groupId_2463-csId_181031" data-text-add="Dodaj do koszyka" data-text-remove="Usuń produkt" data-group-id="2463" data-cs-id="181031" data-product-id="976330" data-cs-type="warranty" />
<span class="btn btn-sm btn-red add-to-basket">
Dodaj do koszyka </span>
</label>
<div class="add-to-basket-info">
<i class="fa fa-check" aria-hidden="true"></i> Produkt został dodany do koszyka wraz z produktem głównym </div>
</div>
</div>
</div>
<div class="display-table cs-tab-product">
<div class="table-row">
<div class="table-col cs-tab-product-img">
<a href="/ochrona-przed-przypadkowymi-uszkodzeniami-1-rok-od-zakupu-636502/" title=" Ochrona przed przypadkowymi uszkodzeniami (1 rok od zakupu)">
<img class="lazy" data-src="https://images.morele.net/i256/636502_2_i256.png" data-alt=" Ochrona przed przypadkowymi uszkodzeniami (1 rok od zakupu)" />
</a>
</div>
<div class="table-col cs-tab-product-name">
<a href="/ochrona-przed-przypadkowymi-uszkodzeniami-1-rok-od-zakupu-636502/" title=" Ochrona przed przypadkowymi uszkodzeniami (1 rok od zakupu)">
Ochrona przed przypadkowymi uszkodzeniami (1 rok od zakupu)
</a>
</div>
<div class="table-col cs-tab-product-discount hidden-xs hidden-sm">
</div>
<div class="table-col cs-tab-product-price">
<div class="new-price">1189,00 zł</div>
</div>
<div class="table-col cs-tab-product-button">
<label>
<input type="radio" name="productid_976330_gwarancja" class="add-to-link add-warranty cs-class-type_warranty-groupId_2305-csId_636502" data-text-add="Dodaj do koszyka" data-text-remove="Usuń produkt" data-group-id="2305" data-cs-id="636502" data-product-id="976330" data-cs-type="warranty" />
<span class="btn btn-sm btn-red add-to-basket">
Dodaj do koszyka </span>
</label>
<div class="add-to-basket-info">
<i class="fa fa-check" aria-hidden="true"></i> Produkt został dodany do koszyka wraz z produktem głównym </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="container-fluid">
<div class="product-seo">
<div class="product-seo-inner box-hidden">
Zobacz również:
<a href="/cd-r-50-cake-700mb-52x-60912/">
Extreme CD-R/50/Cake 700MB 52x</a>, <a href="/tusz-t08704-c13t08704010-gloss-optimizer-212712/">
Epson tusz T08704 (C13T08704010) Gloss Optimizer</a>, <a href="/adapter-usb-na-port-centronics-36-pin-au0003c-411012/">
LogiLink Adapter USB na port Centronics 36-pin AU0003C</a>, <a href="/toslink-toslink-0-75m-czarny-991222500000-628312/">
Hama Toslink - Toslink 0.75m Czarny (991222500000)</a>, <a href="/rozgaleznik-1x-2x-c13-1-8m-16653-721212/">
InLine rozgałęźnik 1x - 2x C13, 1,8m (16653)</a>, <a href="/zarowka-led-e14-4w-35w-320lm-2700k-biala-ciepla-4770070873779-740112/">
Acme Żarówka LED E14, 4W (35W), 320lm, 2700K, biała ciepła (4770070873779)</a>, <a href="/dvd-r-4-7gb-16x-photo-25-sztuk-2761205-741312/">
PrimeOn DVD-R 4.7GB 16x Photo, 25 sztuk (2761205)</a>, <a href="/malaga-30x40-58512-760612/">
Hama Malaga 30x40 (58512)</a>, <a href="/usluga-serwisowa-1y-pw-nbd-ml370-g6-fc-svc-u2va2pe-768112/">
Hewlett-Packard Usługa serwisowa 1y PW Nbd ML370 G6 FC SVC (U2VA2PE)</a>, <a href="/hdmi-hdmi-20m-czarny-cl-75-778912/">
Savio HDMI - HDMI 20m Czarny (CL-75)</a>, <a href="/gaming-mauspad-speed-2-m-3pconmkbrb5-0001-902312/">
Cougar Gaming Mauspad Speed 2 M (3PCONMKBRB5.0001)</a>, <a href="/stand-4x-usb-137205-929412/">
Hama Stand 4x USB (137205)</a>, <a href="/500mm-preset-f-8-0-f1111511101-931312/">
Samyang 500mm Preset f/8.0  (F1111511101)</a>, <a href="/patchcord-cat-7-s-ftp-rozowy-30m-sftp730pi-936312/">
MicroConnect Patchcord CAT 7, S/FTP, różowy, 30m (SFTP730PI)</a>, <a href="/1-8m-kluczyk-lk-k-01-1000912/">
Gembird 1.8m, kluczyk (LK-K-01)</a>, <a href="/wsuwka-greengo-touch-xxxl-i9300-galaxy-s3-granatowa-gsm010544-1031112/">
GreenGo Wsuwka GreenGo Touch XXXL (i9300 Galaxy S3) granatowa - GSM010544</a>, <a href="/toner-black-p-5030dn-6030dn-p-5035i-6035i-mfp-4436010015-1130012/">
Triumph-Adler Toner black P-5030DN/6030DN, P-5035i/6035i MFP (4436010015)</a>, <a href="/clint-sp-freya-psu-black-1224512/">
Clint CLINT-SP-FREYA-PSU-BLACK</a>, <a href="/nakladka-ornament-do-samsung-galaxy-a3-2017-rozowo-zlota-gsm028716-1354812/">
NoName Nakładka Ornament do Samsung Galaxy A3 2017 różowo złota (GSM028716)</a>  </div>
</div>
</div>
<div id="warrantyDialogTemplate" class="hidden">
<div class="warranty-popup">
<div class="w-content">
<div class="ws-title ws-title-tablet">Wybierz gwarancję na swój produkt:</div>
<div class="display-table warranty-table">
<div class="table-row heading">
<div class="table-col ws-table-labels" data-column="warranty_labels">
<div class="ws-title ws-title-desctop">Wybierz gwarancję na swój produkt:</div>
<div class="ws-row-heading">Co zyskujesz od dnia zakupu:</div>
</div>
<div class="table-col wc-head ws-standard" data-column="warranty_standart">
<div class="ws-name">Gwarancja<br /> producenta</div>
</div>
<div class="table-col wc-head ws-extended-box hidden" data-box="#warranty_extended_box" data-column="warranty_extended">
<div class="ws-name">Gwarancja<br /> przedłużona</div>
</div>
<div class="table-col wc-head ws-extended hidden" data-box="#warranty_premium_box" data-column="warranty_premium">
<div class="ws-name">Pełna<br /> ochrona</div>
</div>
</div>
<div class="table-row warranty-body warranty-body-bg">
<div class="table-col" data-column="warranty_labels">
Bezpłatna naprawa w razie przypadkowego uszkodzenia </div>
<div class="table-col" data-column="warranty_standart">
<i class="fa fa-times" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_extended">
<i class="fa fa-times" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_premium">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
</div>
<div class="table-row warranty-body">
<div class="table-col" data-column="warranty_labels">
Bezpłatna naprawa w razie przepięcia sieci energetycznej </div>
<div class="table-col" data-column="warranty_standart">
<i class="fa fa-times" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_extended">
<i class="fa fa-times" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_premium">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
</div>
<div class="table-row warranty-body warranty-body-bg">
<div class="table-col" data-column="warranty_labels">
Nowy sprzęt w przypadku kradzieży lub rabunku </div>
<div class="table-col" data-column="warranty_standart">
<i class="fa fa-times" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_extended">
<i class="fa fa-times" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_premium">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
</div>
<div class="table-row warranty-body warranty-body-title">
<div class="table-col" data-column="warranty_labels">
<div class="ws-row-heading">Po <span id="warranty_perion_info">2</span> latach od zakupu:</div>
 </div>
<div class="table-col" data-column="warranty_standart"></div>
<div class="table-col hidden" data-column="warranty_extended"></div>
<div class="table-col hidden" data-column="warranty_premium"></div>
</div>
<div class="table-row warranty-body warranty-body-bg">
<div class="table-col" data-column="warranty_labels">
Profesjonalny serwis w razie awarii nawet do 5 lat </div>
<div class="table-col" data-column="warranty_standart">
<i class="fa fa-times" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_extended">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_premium">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
</div>
<div class="table-row warranty-body">
<div class="table-col" data-column="warranty_labels">
Wymiana na nowy, gdy naprawa jest niemożliwa </div>
<div class="table-col" data-column="warranty_standart">
<i class="fa fa-times" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_extended">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_premium">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
</div>
<div class="table-row warranty-body warranty-body-bg">
<div class="table-col" data-column="warranty_labels">
Wygodne zgłaszanie awarii - telefonicznie lub online </div>
<div class="table-col" data-column="warranty_standart">
<i class="fa fa-times" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_extended">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_premium">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
</div>
<div class="table-row warranty-body warranty-border-bottom">
<div class="table-col" data-column="warranty_labels">
Brak dodatkowych kosztów i limitu napraw </div>
<div class="table-col" data-column="warranty_standart">
<i class="fa fa-times" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_extended">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
<div class="table-col hidden" data-column="warranty_premium">
<i class="fa fa-check" aria-hidden="true"></i>
</div>
</div>
<div class="table-row warranty-footer">
<div class="table-col" data-column="warranty_labels">
<div class="w-product">
<div class="w-name"></div>
<div class="w-img"></div>
</div>
</div>
<div class="table-col" data-column="warranty_standart">
<div class="warranty-choose standard">
<div class="form-item">
<div class="radio radio-lg">
 <label>
<input type="radio" name="warranty" checked="checked" />
<span class="input"></span>
<span class="info">Standardowa gwarancja producenta</span>
</label>
</div>
</div>
</div>
</div>
<div class="table-col hidden" data-column="warranty_extended">
<div class="warranty-choose">
<div class="form-item">
<div class="radio radio-lg">
<label>
<input type="radio" name="warranty" data-warranty-id="181065" data-warranty-column="warranty_extended" data-months="12" />
<span class="input"></span>
<span class="info"><span class="info-name">Przedłuż do <b><em data-months="12"></em> lat</b></span></span>
</label>
</div>
</div>
<div class="form-item">
<div class="radio radio-lg">
<label>
<input type="radio" name="warranty" data-warranty-id="181048" data-warranty-column="warranty_extended" data-months="24" />
<span class="input"></span>
<span class="info"><span class="info-name">Przedłuż do <b><em data-months="24"></em> lat</b></span></span>
</label>
</div>
</div>
<div class="form-item">
<div class="radio radio-lg">
<label>
<input type="radio" name="warranty" data-warranty-id="181031" data-warranty-column="warranty_extended" data-months="36" />
<span class="input"></span>
<span class="info"><span class="info-name">Przedłuż do <b><em data-months="36"></em> lat</b></span></span>
</label>
</div>
</div>
</div>
</div>
<div class="table-col hidden" data-column="warranty_premium">
<div class="warranty-choose">
<div class="form-item">
<div class="radio radio-lg">
<label>
<input type="radio" name="warranty" data-warranty-id="602818" data-warranty-column="warranty_premium" data-months="12" />
<span class="input"></span>
<span class="info"><span class="info-name">Przedłuż do <b><em data-months="12"></em> lat</b></span></span>
</label>
</div>
</div>
<div class="form-item">
<div class="radio radio-lg">
<label>
<input type="radio" name="warranty" data-warranty-id="602819" data-warranty-column="warranty_premium" data-months="24" />
<span class="input"></span>
<span class="info"><span class="info-name">Przedłuż do <b><em data-months="24"></em> lat</b></span></span>
</label>
</div>
</div>
 <div class="form-item">
<div class="radio radio-lg">
<label>
<input type="radio" name="warranty" data-warranty-id="602820" data-warranty-column="warranty_premium" data-months="36" />
<span class="input"></span>
<span class="info"><span class="info-name">Przedłuż do <b><em data-months="36"></em> lat</b></span></span>
</label>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="warranty-hide">
<div class="checkbox">
<label>
<input type="checkbox" name="warranty_hide" class="not-show-popup" />
<span class="input"></span>
<span class="info">nie pokazuj tego okna</span>
</label>
</div>
</div>
</div>
<div id="warranty_extended_box" class="warranty-desc-box hidden">
<a href="#" class="btn btn-primary btn-sm btn-close-desc close-icon"><i class="fa fa-times" aria-hidden="true"></i></a>
<h2></h2>
<div class="desc"></div>
<a href="#" class="btn btn-primary btn-sm btn-close-desc">Powrót</a>
</div>
<div id="warranty_premium_box" class="warranty-desc-box hidden">
<a href="#" class="btn btn-primary btn-close-desc close-icon"><i class="fa fa-times" aria-hidden="true"></i></a>
<h2></h2>
<div class="desc"></div>
<a href="#" class="btn btn-primary btn-close-desc">Powrót</a>
</div>
</div>
</div> <div id="featureDescriptionsDialog" class="hidden">
<div class="fd-body"></div>
<div class="fb-footer">
<span class="featureRate-label">Czy ta informacja była dla Ciebie przydatna ?</span>
<a href="1" class="featureRate rateUp">
<span>
<i class="fa fa-thumbs-up fa-icon-like" aria-hidden="true"></i>
<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader" aria-hidden="true"></i>
</span>
tak </a>
<a href="-1" class="featureRate rateDown">
<span>
<i class="fa fa-thumbs-down fa-icon-like" aria-hidden="true"></i>
<i class="fa fa-circle-o-notch fa-spin fa-fw fa-icon-loader" aria-hidden="true"></i>
</span>
nie </a>
</div>
</div> <div class="hidden" id="buy_info_table">
<div class="buy-info-table-wr">
<div class="display-table base-table-style reset-table-xs buy-info-table">
<div class="table-row heading hidden-xs">
<div class="table-col col-xs-6">Użytkownik</div>
<div class="table-col col-xs-6">Data zakupu</div>
</div>
</div>
</div>
</div> </div>
<div class="container-fluid">
<div class="section-block lastviewed-products">
<div class="section-title">Ostatnio oglądane</div>
<div class="owl-prod owl-carousel owl-preloader owl-lastviewed" data-lang-no-rate="Oceń jako pierwszy">
<div class="morele-preloader ">
<div class='loader3'><div><div><div><div></div></div></div></div></div>
<div class="loader-logo">
<svg data-name="morele" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156.25 161.49">
<title>Morele</title>
<path class="cls-1" d="M16.38,124.77H51.3c0.14-2.44.35-4.53,0.37-6.62,0.14-14.65.17-29.3,0.42-44A20.09,20.09,0,0,1,53.74,67a12.73,12.73,0,0,1,13.85-7.52c5.56,1,9.15,5.47,9.2,12,0.12,15.82.25,31.65-.06,47.47-0.1,4.93,1.46,6.23,6.19,6,8-.38,16-0.41,24,0,5,0.26,6.09-1.58,6-6.24-0.3-12.82-.21-25.65,0-38.47a46.45,46.45,0,0,1,1.38-11.82,12.2,12.2,0,0,1,13.91-8.76c5.68,0.91,9.65,5.57,9.72,12.14,0.15,15.65.07,31.31,0.08,47,0,1.79,0,3.58,0,5.94h23.56c-16.1,23.73-37.24,37-64.54,39.18C63.33,166.52,36.79,153.54,16.38,124.77Z" transform="translate(-5.21 -2.68)" />
<path class="cls-1" d="M109.37,46.81C102.77,34.92,92.16,31.79,80,32,68.07,32.28,58,36.78,49.39,47.18V34.27H22.18C40,13.57,61.25,3.22,87,2.7c27.21-.55,49.72,9.7,68.16,30.83C134.23,30.31,124.21,32.17,109.37,46.81Z" transform="translate(-5.21 -2.68)" />
<path class="cls-1" d="M15.52,45.45v76C1.8,104.69,1.76,62.48,15.52,45.45Z" transform="translate(-5.21 -2.68)" />
</svg>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="page-overlay"></div>
<div class="page-overlay-click"></div>
</section>
<footer id="footer" data-role="footer">
<div class="pageToTop mobile">
<a href="#" data-scroll-to="#top" data-duration="1000" class="scrollPage">Powrót na górę strony <i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</div>
<div class="footer-top-content">
<div class="container-fluid">
<div class="contact">
<div class="row">
<div class="col-sm-7 col-md-9">
<div class="row">
<div class="col-md-6">
<div class="section newsletter">
<div class="title">Bądź na bieżąco!</div>
<div class="text">Zapisz się do newslettera i nie przegap żadnej promocji</div>
<div class="content">
<form action="/newsletter/subscribe/" method="post" onsubmit="if(!isValidFooter()){return false;}" novalidate="">
<div class="newsletter-form">
<div class="newsletter-input">
<span class="error" id="emailError1" style="float:right; width: 25%; display: inline-block;line-height: normal !important;"></span>
<input type="email" autocapitalize="off" autocorrect="off" name="email" id="email" size="30" placeholder="Twój adres e-mail..." style="" tabindex="0" />
</div>
<div class="newsletter-button">
 <input type="submit" class="btn btn-primary btn-to-left" name="submit" value="Zapisz się" style="float:right" id="footerFormSend" tabindex="0" />
</div>
<div class="clearBoth"></div>
</div>
</form>
</div>
</div>
</div>
<div class="col-md-5 col-md-offset-1">
<div class="section social-media">
<div class="title">Social media</div>
<div class="text">Znajdziesz nas na:</div>
<div class="content">
<div class="social-media-widget">
<a href="https://www.facebook.com/morele.net/" target="_blank" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
<a href="https://www.youtube.com/user/MoreleTV" target="_blank" class="youtube"><i class="fa fa-youtube" aria-hidden="true"></i></a>
<a href="https://www.instagram.com/morele_net/" target="_blank" class="instagram"><i class="icon-morele-instagram" aria-hidden="true"></i></a>
<a href="https://twitter.com/morele_net" target="_blank" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
<a href="https://plus.google.com/+MoreleNetsklep/posts" target="_blank" class="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-5 col-md-3">
<div class="section contact-info">
<div class="title">Masz pytania?</div>
<div class="text">Biuro obsługi klienta czynne od 8:00 do 21:00 w dni robocze</div>
<div class="content">
<p class="phone-number">12 418 40 26</p>
<p class="text">dla tel. komórkowych</p>
<p class="phone-number">801 000 861</p>
<p class="text">dla tel. stacjonarnych</p>
</div>
</div>
</div>
</div>
</div>
<div class="menus">
<div class="row">
<div class="col-md-6 menu-row">
<div class="menu-row-content">
<div class="row hidden-xs hidden-md hidden-lg" data-toggle='.menuRow1' data-close-on-resize="true">
<div class="col-sm-6">
<div class="title">Twoje konto</div>
</div>
<div class="col-sm-6">
<div class="title">
Informacje prawne <div class="title-toggle">
<span>
<i class="fa fa-plus" aria-hidden="true"></i>
 <i class="fa fa-minus" aria-hidden="true"></i>
</span>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6 menu-col">
<nav class="menu">
<div class="title hidden-sm" data-toggle='.menuItem1' data-close-on-resize="true">
Twoje konto <div class="title-toggle">
<span>
<i class="fa fa-plus" aria-hidden="true"></i>
<i class="fa fa-minus" aria-hidden="true"></i>
</span>
</div>
</div>
<ul class="menuItem1 menuRow1">
<li><a href="/profil/zamowienia-w-trakcie-realizacji/">Zamówienia w realizacji</a></li>
<li><a href="/feedback/list/pending/">Ostatnie zapytania</a></li>
<li><a href="/profil/produkty-bez-recenzji/">Produkty do oceny</a></li>
<li><a href="/rma_list/pending/">Zgłoszenia serwisowe</a></li>
<li><a href="/inventory/list/">Zapisane koszyki</a></li>
<li><a href="/pokaz_pomoc/16/">Modyfikacja / anulowanie zamówienia</a></li>
</ul>
</nav>
</div>
<div class="col-sm-6 menu-col">
<nav class="menu">
<div class="title hidden-sm" data-toggle='.menuItem2' data-close-on-resize="true">
Informacje prawne <div class="title-toggle">
<span>
<i class="fa fa-plus" aria-hidden="true"></i>
<i class="fa fa-minus" aria-hidden="true"></i>
</span>
</div>
</div>
<ul class="menuItem2 menuRow1">
<li><a href="/pokaz_pomoc/134/">Regulamin sklepu</a></li>
<li>
<a href="http://download.morele.net/polityka_pryw_i_cookies_dla_Morelenet.pdf" target="_blank">Polityka prywatności i cookies</a></li>
<li><a href="/info/bezpiecznie/">Bezpieczeństwo danych osobowych</a></li>
<li><a href="/pokaz_pomoc/81/">Koszty gospodarowania odpadami</a></li>
<li><a href="/pokaz_pomoc/140/">Materiały do pobrania</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div>
<div class="col-md-6 menu-row">
<div class="menu-row-content">
<div class="row hidden-xs hidden-md hidden-lg" data-toggle='.menuRow2' data-close-on-resize="true">
<div class="col-sm-6">
 <div class="title">Pomocne linki</div>
</div>
<div class="col-sm-6">
<div class="title">
Morele.net <div class="title-toggle">
<span>
<i class="fa fa-plus" aria-hidden="true"></i>
<i class="fa fa-minus" aria-hidden="true"></i>
</span>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6 menu-col">
<nav class="menu">
<div class="title hidden-sm" data-toggle='.menuItem3' data-close-on-resize="true">
Pomocne linki <div class="title-toggle">
<span>
<i class="fa fa-plus" aria-hidden="true"></i>
<i class="fa fa-minus" aria-hidden="true"></i>
</span>
</div>
</div>
<ul class="menuItem3 menuRow2">
<li><a href="/pokaz_pomoc/22/">Jak kupić na raty?</a></li>
<li><a href="/pomoc_grupa/1/">Reklamacje, zwroty, serwis</a></li>
<li><a href="/pomoc_grupa/5/">Częste pytania</a></li>
<li><a href="/info/netpunkt/">Punkty odbioru osobistego</a></li>
<li><a href="/pomoc_grupa/12/">Tax Free, Foreign Shipping</a></li>
<li><a href="/pomoc_grupa/13/">Dodatkowe gwarancje</a></li>
<li><a href="/pomoc_grupa/14/">Leasing</a></li>
<li><a href="/kategorie/">Wszystkie kategorie</a></li>
<li><a href="/kategorie/producenci/">Producenci</a></li>
</ul>
</nav>
</div>
<div class="col-sm-6 menu-col">
<nav class="menu">
<div class="title hidden-sm" data-toggle='.menuItem4' data-close-on-resize="true">
Morele.net
<div class="title-toggle">
<span>
<i class="fa fa-plus" aria-hidden="true"></i>
<i class="fa fa-minus" aria-hidden="true"></i>
</span>
</div>
</div>
<ul class="menuItem4 menuRow2">
<li><a href="/index/faq_wybor/">Kontakt</a></li>
<li><a href="/info/onas/">O nas</a></li>
<li><a href="/info/grupa_morele/">Grupa Morele.net</a></li>
<li><a href="/info/kariera/">Kariera</a></li>
<li><a href="/info/dane_firmy/">Dane firmy</a></li>
<li><a href="/info/referencje/">Nasze referencje</a></li>
<li><a href="/info/autoryzacje/">Autoryzacje producentów</a></li>
<li><a href="http://images.morele.net/tablica_promocyjna_kkc.pdf">Projekty
UE</a></li>
<li><a href="/biznes/">Współpraca handlowa</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="pageToTop desktop">
<a href="#" data-scroll-to="#top" data-duration="1000" class="scrollPage">Powrót na górę strony <i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</div>
</div>
<div class="force-to-desktop text-center hidden">
<span data-type="desktop" id="force_to_desktop">Przejdź na pełną wersję strony <i class="fa fa-chevron-right" aria-hidden="true"></i></span>
</div>
<div id="civil_code">
<div class="container-fluid">
<p>Informacje znajdujące się na stronach internetowych sklepu www.morele.net są jedynie zaproszeniem do zawarcia
umowy w rozumieniu Kodeksu Cywilnego.</p>
<p>
Salon firmowy:
<a style="margin-left:5px;" href="/sklep/microsoft/main/">Microsoft</a>
<a style="margin-left:5px;" href="/msi/">MSI</a>
<a style="margin-left:5px;" href="/iiyama/">iiyama</a>
<a style="margin-left:5px;" href="/SIS_Intel/">Intel</a>
<a style="margin-left:5px;" href="/thrustmaster/">Thrustmaster</a>
<a style="margin-left:5px;" href="/razer/">Razer</a>
<a style="margin-left:5px;" href="/gopro/">GoPro</a>
<a style="margin-left:5px;" href="/miele/">Miele</a>
<a style="margin-left:5px;" href="/ADATA/">ADATA</a>
</p>
<p>Odbierz osobiście zamówione produkty w miastach:
Białystok, Bydgoszcz, Częstochowa, Gdańsk, Gdynia, Gorzów Wielkopolski, Katowice, Kielce, Kraków, Lublin, Łódź, Olsztyn,
Opole, Poznań, Radom, Rzeszów, Sosnowiec, Szczecin, Toruń, Warszawa-Białołęka, Warszawa-Ursynów, Wrocław.
</p>
<br>
<p>Sprawdź także sklepy obsługiwane przez system Morele.net</p>
<p><a href="https://www.budujesz.pl/" target="_blank">budujesz.pl</a> - jeden z największych sklepów z artykułami budowlanymi i narzędziami</p>
<p><a href="https://www.amfora.pl/" target="_blank">amfora.pl</a> - drogeria internetowa</p>
<p><a href="https://www.hulahop.pl/" target="_blank">hulahop.pl</a> - sklep dziecięcy</p>
<p><a href="https://www.pupilo.pl/" target="_blank">pupilo.pl</a> - sklep zoologiczny</p>
<p><a href="https://www.trenujesz.pl/" target="_blank">trenujesz.pl</a> - sklep sportowy</p>
</div>
</div>
</footer>
<div id="cookieBoxWrapper" class="hidden">
<div class="cookieWrap">
<span>
Strona korzysta z plików cookies w celu realizacji usług i zgodnie z Polityką Plików Cookies. Możesz określić warunki przechowywania lub dostępu do plików cookies w Twojej przeglądarce. <div>
<button class="btn btn-primary accept">Zgadzam się</button>
</div>
</span>
</div>
</div>
<div class="hidden">
<div class="morele-page-preloader">
<div class='loader3'><div><div><div><div></div></div></div></div></div>
<div class="loader-logo">
<svg data-name="morele" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156.25 161.49">
<title>Morele</title>
<path class="cls-1" d="M16.38,124.77H51.3c0.14-2.44.35-4.53,0.37-6.62,0.14-14.65.17-29.3,0.42-44A20.09,20.09,0,0,1,53.74,67a12.73,12.73,0,0,1,13.85-7.52c5.56,1,9.15,5.47,9.2,12,0.12,15.82.25,31.65-.06,47.47-0.1,4.93,1.46,6.23,6.19,6,8-.38,16-0.41,24,0,5,0.26,6.09-1.58,6-6.24-0.3-12.82-.21-25.65,0-38.47a46.45,46.45,0,0,1,1.38-11.82,12.2,12.2,0,0,1,13.91-8.76c5.68,0.91,9.65,5.57,9.72,12.14,0.15,15.65.07,31.31,0.08,47,0,1.79,0,3.58,0,5.94h23.56c-16.1,23.73-37.24,37-64.54,39.18C63.33,166.52,36.79,153.54,16.38,124.77Z" transform="translate(-5.21 -2.68)" />
<path class="cls-1" d="M109.37,46.81C102.77,34.92,92.16,31.79,80,32,68.07,32.28,58,36.78,49.39,47.18V34.27H22.18C40,13.57,61.25,3.22,87,2.7c27.21-.55,49.72,9.7,68.16,30.83C134.23,30.31,124.21,32.17,109.37,46.81Z" transform="translate(-5.21 -2.68)" />
<path class="cls-1" d="M15.52,45.45v76C1.8,104.69,1.76,62.48,15.52,45.45Z" transform="translate(-5.21 -2.68)" />
</svg>
</div>
</div>
</div> <div class="button-to-top-fixed">
<button type="button" class="scrollPage" id="back_to_top" data-scroll-to="#top" data-duration="1000"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
</div>
</div>
<nav class="primary-menu" data-parent-daley="300,0" data-min-width="1025">
<ul class="pr-menu-cat">
<li class="pr-menu-subcat-h no-link">
Kategorie </li>
<li class="pr-menu-item   has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/"><span class="helper-before"></span>Laptopy<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Laptopy
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/">Notebooki, laptopy, ultrabooki </a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Notebooki, laptopy, ultrabooki
</a>
</li>
<li>
 <a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,30183O978877/1/">Laptopy</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,30183O978878/1/">Ultrabooki</a>
</li>
<li>
<a href="/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,30183O978880/1/">Laptopy 2w1</a>
</li>
<li>
<a href="/laptopy/laptopy/laptopy-wyprzedazowe-i-poleasingowe-497/">Laptopy wyprzedażowe i poleasingowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/">Podzespoły do laptopów</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Podzespoły do laptopów
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/dyski-ssd-518/">Dyski SSD</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/dyski-2-5-i-mniejsze-155/">Dyski 2.5&#039;&#039; i mniejsze</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/napedy-optyczne-28/">Napędy optyczne</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/pamieci-do-laptopow-117/">Pamięci do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/podzespoly-do-laptopow/podzespoly-do-notebookow-719/">Podzespoły i części serwisowe</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/laptopy/">Laptopy - zastosowanie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Laptopy - zastosowanie
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098457/1/">Laptopy biznesowe</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098455/1/">Laptopy dla domu</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098458/1/">Laptopy ultramobilne</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098456/1/">Laptopy multimedialne</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098459/1/">Laptopy dla graczy</a>
 </li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098461/1/">Laptopy premium</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098460/1/">Mobilne stacje robocze</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/przenoszenie-i-ochrona/">Przenoszenie i ochrona</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Przenoszenie i ochrona
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/przenoszenie-i-ochrona/torby-do-laptopow-32/">Torby do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/przenoszenie-i-ochrona/etui-do-laptopow-628/">Etui do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/przenoszenie-i-ochrona/plecaki-na-laptopy-593/">Plecaki do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/przenoszenie-i-ochrona/linki-zabezpieczajace-715/">Linki zabezpieczające</a>
</li>
</ul>
</li>
</ul>
 <ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/laptopy/">Laptopy - popularni producenci</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Laptopy - popularni producenci
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,511,,,,,/1/">Laptopy Lenovo</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,248,,,,,/1/">Laptopy Dell</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,92,,,,,/1/">Laptopy HP</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,25,,,,,/1/">Laptopy Asus</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,496,,,,,/1/">Laptopy Apple</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,151,,,,,/1/">Laptopy MSI</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,75,,,,,/1/">Laptopy Fujitsu</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,218,,,,,/1/">Laptopy Toshiba</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/laptopy/">Laptopy - rozmiar</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Laptopy - rozmiar
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,18749O890445.1017253.477015/1/">Laptopy z ekranem 10.1&#039;&#039; - 12.5&#039;&#039;</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,18749O314031.888064.313129/1/">Laptopy z ekranem 13.3&#039;&#039; - 14.1&#039;&#039;</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,18749O314226/1/">Laptopy z ekranem 15.6&#039;&#039;</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,/1/">Laptopy z ekranem 17.3&#039;&#039;</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
 <div class="pr-menu-inner">
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/">Akcesoria do laptopów</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Akcesoria do laptopów
</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/baterie-do-laptopow-516/">Baterie do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/dyski-zewnetrzne-207/">Dyski zewnętrzne</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/lampki-usb-697/">Lampki USB</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/podstawki-chlodzace-636/">Podstawki chłodzące</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/przetwornice-222/">Przetwornice</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/rysiki-645/">Rysiki</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/stacje-dokujace-i-replikatory-portow-725/">Stacje dokujące i replikatory portów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/wentylatory-usb-698/">Wentylatory USB</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/zasilacze-do-laptopow-531/">Zasilacze do laptopów</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/akcesoria-poleasingowe-1473/">Akcesoria poleasingowe</a>
</li>
<li>
<a href="https://www.morele.net/laptopy/akcesoria-do-laptopow/pozostale-akcesoria-do-laptopow-63/">Pozostałe akcesoria do laptopów</a>
</li>
</ul>
</li>
</ul>
</div>
</div>
</li>
<li class="pr-menu-item menu-komputery  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/komputery/"><span class="helper-before"></span>Komputery<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Komputery
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/komputery-pc/">Komputery PC</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
 <i class="fa fa-angle-left" aria-hidden="true"></i> Komputery PC
</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/">Komputery dla graczy</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-do-domu-i-biura-19/">Komputery do domu i biura</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-all-in-one-40/">Komputery All-In-One</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-mini-pc-i-nuc-714/">Komputery Mini PC i NUC</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-wyprzedazowe-i-poleasingowe-493/">Komputery wyprzedażowe i poleasingowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/serwery/">Serwery</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Serwery
</a>
</li>
<li>
<a href="/komputery/serwery/serwery-plikow-190/">Serwery Plików</a>
</li>
<li>
<a href="/komputery/serwery/dyski-do-serwerow-147/">Dyski do serwerów</a>
</li>
 <li>
<a href="/komputery/serwery/szafy-rack-142/">Szafy Rack</a>
</li>
<li>
<a href="/komputery/serwery/ups-57/">UPS</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/sieci/">Sieci</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Sieci
</a>
</li>
<li>
<a href="/komputery/sieci/routery-48/">Routery</a>
</li>
<li>
<a href="/komputery/sieci/kable-teleinformatyczne-140/">Kable teleinformatyczne</a>
</li>
<li>
<a href="/komputery/sieci/karty-sieciowe-wi-fi-477/">Karty sieciowe Wi-Fi</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/podzespoly-komputerowe/">Podzespoły komputerowe</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Podzespoły komputerowe
</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/karty-graficzne-12/">Karty graficzne</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/procesory-45/">Procesory</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/plyty-glowne-42/">Płyty główne</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/pamieci-ram-38/">Pamięci RAM</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/obudowy-33/">Obudowy</a>
</li>
<li>
<a href="/komputery/podzespoly-komputerowe/zasilacze-61/">Zasilacze</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/chlodzenie-komputerowe/">Chłodzenie komputerowe</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/sluchawki-glosniki-mikrofony/">Słuchawki, głośniki, mikrofony</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Słuchawki, głośniki, mikrofony
</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/glosniki-komputerowe-6/">Głośniki komputerowe</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/sluchawki-nauszne-780/">Słuchawki nauszne</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/sluchawki-douszne-i-dokanalowe-457/">Słuchawki douszne i dokanałowe</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/sluchawki-bezprzewodowe-458/">Słuchawki bezprzewodowe</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/sluchawki-z-mikrofonem-728/">Słuchawki z mikrofonem</a>
</li>
<li>
<a href="/komputery/sluchawki-i-glosniki/sluchawki-dla-graczy-466/">Słuchawki dla graczy</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/monitory-i-akcesoria/monitory-komputerowe-523/">Monitory komputerowe</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
 <ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Monitory komputerowe
</a>
</li>
<li>
<a href="/komputery/monitory-i-akcesoria/monitory-led-lcd-523/,,,,,,,,,,30812O1044561/1/">Do domu i biura</a>
</li>
<li>
<a href="/komputery/monitory-i-akcesoria/monitory-led-lcd-523/,,,,,,,,,,30812O1044560/1/">Dla graczy</a>
</li>
<li>
<a href="/komputery/monitory-i-akcesoria/monitory-led-lcd-523/,,,,,,,,,,30812O1044563/1/">Dotykowe i wielkoformatowe (LFD)</a>
</li>
<li>
<a href="/komputery/monitory-i-akcesoria/monitory-led-lcd-523/,,,,,,,,,,30812O1044562/1/">Profesjonalne</a>
</li>
<li>
<a href="https://www.morele.net/komputery/monitory-i-akcesoria/monitory-poleasingowe-494/">Poleasingowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/klawiatury-i-myszki/">Klawiatury i myszki</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Klawiatury i myszki
</a>
</li>
<li>
<a href="/komputery/klawiatury-i-myszki/klawiatury-komputerowe-18/">Klawiatury komputerowe</a>
 </li>
<li>
<a href="/komputery/klawiatury-i-myszki/myszy-komputerowe-464/">Myszy komputerowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/dyski-i-nosniki-danych/">Dyski i nośniki danych</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Dyski i nośniki danych
</a>
</li>
<li>
<a href="/komputery/dyski-i-nosniki-danych/dyski-ssd-518/">Dyski SSD</a>
</li>
<li>
<a href="/komputery/dyski-i-nosniki-danych/dyski-twarde-3-5-4/">Dyski twarde 3,5&#039;&#039;</a>
</li>
<li>
<a href="/komputery/dyski-i-nosniki-danych/dyski-zewnetrzne-207/">Dyski zewnętrzne</a>
</li>
<li>
<a href="/komputery/dyski-i-nosniki-danych/pendrive-8/">Pendrive</a>
</li>
<li>
<a href="/komputery/dyski-i-nosniki-danych/karty-pamieci-13/">Karty pamięci</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/akcesoria-zasilanie-i-kamerki/">Akcesoria, zasilanie i kamerki</a>
</div>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/oprogramowanie/">Oprogramowanie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Oprogramowanie
</a>
</li>
<li>
<a href="/komputery/oprogramowanie/systemy-operacyjne-196/">Systemy operacyjne</a>
</li>
<li>
<a href="/komputery/oprogramowanie/microsoft-office-198/">Microsoft Office</a>
</li>
<li>
<a href="/komputery/oprogramowanie/programy-biurowe-2983/">Programy biurowe</a>
</li>
<li>
<a href="/komputery/oprogramowanie/bezpieczenstwo-197/">Bezpieczeństwo</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/drukowanie-i-skanery/">Drukowanie i skanery</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
 </div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Drukowanie i skanery
</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/drukarki-atramentowe-269/">Drukarki atramentowe</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/drukarki-laserowe-279/">Drukarki laserowe</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/urzadzenia-wielofunkcyjne-atramentowe-298/">Urządzenia wielofunkcyjne atramentowe</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/urzadzenia-wielofunkcyjne-laserowe-296/">Urządzenia wielofunkcyjne laserowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/kable-i-adaptery/">Kable i adaptery</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Kable i adaptery
</a>
</li>
<li>
<a href="/komputery/kable-i-adaptery/kable-komputerowe-i-przejsciowki-193/">Kable komputerowe i przejściówki</a>
</li>
<li>
 <a href="/komputery/monitory-i-akcesoria/kable-i-konwertery-av-194/">Kable i konwertery AV</a>
</li>
<li>
<a href="/komputery/kable-i-adaptery/adaptery-usb-654/">Adaptery USB</a>
</li>
<li>
<a href="/komputery/kable-i-adaptery/kable-zasilajace-736/">Kable zasilające</a>
</li>
</ul>
</li>
</ul>
</div>
<div class="menu-sis menu-komputery hidden-xs hidden-sm">
<div class="label">Sklepy producentów:</div>
<div class="content">
<div class="left">
<div class="item">
<a href="/iiyama/" class="logo-iiyama">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/msi/" class="logo-msi">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/sklep/microsoft/main/" class="logo-microsoft">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/oki/" class="logo-oki">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/thrustmaster/" class="logo-thrustmaster">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/razer/" class="logo-razer">
 <span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/ADATA/" class="logo-adata">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
</div>
<div class="right">
<div class="item">
<a href="/komputery/podzespoly-komputerowe/procesory-45/?konfigurator=konfigurator-zestawu-komputerowego#configurator" class="logo-configurator">
<span class="img-color"></span>
</a>
</div>
</div>
<div class="clearBoth"></div>
</div>
<div class="clearBoth"></div>
</div>
</div>
</li>
<li class="pr-menu-item menu-rtv  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/rtv/"><span class="helper-before"></span>RTV<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> RTV
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/telewizory/telewizory-412/">Telewizory</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Telewizory
</a>
</li>
 <li>
<a href="/rtv/telewizory/telewizory-412/,,,,,,,,,,23217O724442/1/">Telewizory FullHD</a>
</li>
<li>
<a href="/rtv/telewizory/telewizory-412/,,,,,,,,,,23217O761261/1/">Telewizory 4K (Ultra HD)</a>
</li>
<li>
<a href="/rtv/telewizory/telewizory-412/,,,,,,,,,,23156O719102/1/">Telewizory 3D</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/dvd-i-blu-ray/">DVD i BLU-RAY</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> DVD i BLU-RAY
</a>
</li>
<li>
<a href="/rtv/dvd-blu-ray/odtwarzacze-dvd-106/">Odtwarzacze DVD</a>
</li>
<li>
<a href="/rtv/dvd-blu-ray/odtwarzacze-blu-ray-462/">Odtwarzacze BLU-RAY</a>
</li>
<li>
<a href="/rtv/dvd-blu-ray/przenosne-odtwarzacze-492/">Przenośne odtwarzacze</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/tunery-tv/">Tunery TV</a>
 <a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Tunery TV
</a>
</li>
<li>
<a href="/rtv/tunery-tv/tunery-tv-541/">Tunery TV (DVB-T)</a>
</li>
<li>
<a href="/rtv/tunery-tv/tunery-tv-satelitarnej-560/">Tunery TV satelitarnej</a>
</li>
<li>
<a href="/rtv/tunery-tv/anteny-rtv-215/">Anteny RTV</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/audio-i-hifi/">Audio i HiFi</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Audio i HiFi
</a>
</li>
<li>
<a href="/rtv/audio-hifi/wieze-111/">Wieże</a>
</li>
<li>
<a href="/rtv/audio-hifi/radioodtwarzacze-121/">Radioodtwarzacze</a>
 </li>
<li>
<a href="/rtv/audio-hifi/kina-domowe-448/">Kina domowe</a>
</li>
<li>
<a href="/rtv/audio-hifi/soundbary-613/">Soundbary</a>
</li>
<li>
<a href="/rtv/audio-hifi/wzmacniacze-audio-588/">Wzmacniacze audio</a>
</li>
<li>
<a href="/rtv/audio-hifi/amplitunery-167/">Amplitunery</a>
</li>
<li>
<a href="/rtv/audio-hifi/kolumny-i-glosniki-122/">Kolumny i głośniki</a>
</li>
<li>
<a href="/rtv/audio-hifi/subwoofery-611/">Subwoofery</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/mp3-i-mp4/">Odtwarzacze MP3, MP4, dyktafony</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Odtwarzacze MP3, MP4, dyktafony
</a>
</li>
<li>
<a href="/rtv/mp3-mp4/odtwarzacze-mp3-36/">Odtwarzacze MP3</a>
</li>
<li>
<a href="/rtv/mp3-mp4/odtwarzacze-mp4-184/">Odtwarzacze MP4</a>
</li>
 <li>
<a href="/rtv/mp3-mp4/dyktafony-449/">Dyktafony</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/projektory-i-akcesoria/">Projektory i akcesoria</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Projektory i akcesoria
</a>
</li>
<li>
<a href="/rtv/projektory-i-akcesoria/projektory-46/">Projektory</a>
</li>
<li>
<a href="/rtv/projektory-i-akcesoria/uchwyty-do-projektorow-658/">Uchwyty do projektorów</a>
</li>
<li>
<a href="/rtv/projektory-i-akcesoria/ekrany-projekcyjne-276/">Ekrany projekcyjne</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/sluchawki/">Słuchawki</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
 <ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Słuchawki
</a>
</li>
<li>
<a href="/rtv/sluchawki/sluchawki-bezprzewodowe-458/">Słuchawki bezprzewodowe</a>
</li>
<li>
<a href="/rtv/sluchawki/sluchawki-douszne-i-dokanalowe-457/">Słuchawki douszne i dokanałowe</a>
</li>
<li>
<a href="/rtv/sluchawki/sluchawki-nauszne-780/">Słuchawki nauszne</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/akcesoria-rtv/">Akcesoria RTV</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Akcesoria RTV
</a>
</li>
<li>
<a href="/rtv/akcesoria-rtv/wieszaki-i-uchwyty-rtv-163/">Wieszaki i uchwyty RTV</a>
</li>
<li>
<a href="/rtv/akcesoria-rtv/listwy-zasilajace-22/">Listwy zasilające</a>
</li>
<li>
<a href="/rtv/akcesoria-rtv/kable-av-194/">Kable AV</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/car-audio/">Car audio</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Car audio
</a>
</li>
<li>
<a href="/rtv/car-audio/radia-samochodowe-59/">Radia samochodowe</a>
</li>
<li>
<a href="/rtv/car-audio/glosniki-samochodowe-120/">Głośniki samochodowe</a>
</li>
<li>
<a href="/rtv/car-audio/subwoofery-samochodowe-454/">Subwoofery samochodowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/nawigacja-gps-i-cb-radia/">Nawigacja GPS i CB radia</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Nawigacja GPS i CB radia
</a>
</li>
<li>
<a href="/rtv/nawigacja-gps-cb-radia/nawigacja-gps-139/">Nawigacja GPS</a>
</li>
<li>
<a href="/rtv/nawigacja-gps-cb-radia/cb-radia-544/">CB Radia</a>
</li>
<li>
<a href="/rtv/nawigacja-gps-cb-radia/cb-anteny-545/">CB Anteny</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/rtv/monitoring/">Monitoring</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Monitoring
</a>
</li>
<li>
<a href="/rtv/monitoring/kamery-ip-750/">Kamery IP</a>
</li>
<li>
<a href="/rtv/monitoring/kamery-przemyslowe-i-akcesoria-221/">Kamery przemysłowe i akcesoria</a>
</li>
<li>
<a href="/rtv/monitoring/rejestratory-obrazu-183/">Rejestratory obrazu</a>
</li>
</ul>
</li>
</ul>
</div>
 <div class="menu-sis menu-rtv hidden-xs hidden-sm">
<div class="label">Sklepy producentów:</div>
<div class="content">
<div class="left">
<div class="item">
<a href="/ADATA/" class="logo-adata">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
</div>
</div>
<div class="clearBoth"></div>
</div>
</div>
</li>
<li class="pr-menu-item menu-agd   has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/agd/"><span class="helper-before"></span>AGD<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> AGD
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/agd/agd-do-zabudowy/">AGD do zabudowy</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> AGD do zabudowy
</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/lodowki-z-zamrazalnikiem-u-dolu-zabudowa-261/">Lodówki z zamrażalnikiem u dołu (do zabudowy)</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/zmywarki-do-zabudowy-90/">Zmywarki do zabudowy</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/zestaw-piekarnik-z-plyta-gazowa-248/">Zestawy piekarnik z płytą gazową</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/piekarniki-101/">Piekarniki do zabudowy</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/plyty-ceramiczne-244/">Płyty ceramiczne</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/okapy-kominowe-265/">Okapy kominowe</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/ekspresy-do-kawy-zabudowa-231/">Ekspresy do zabudowy</a>
</li>
<li>
<a href="/agd/agd-do-zabudowy/kuchenki-mikrofalowe-do-zabudowy-188/">Kuchenki mikrofalowe do zabudowy</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/agd/agd-wolnostojace/">AGD wolnostojące</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> AGD wolnostojące
</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/lodowki-jednodrzwiowe-235/">Lodówki jednodrzwiowe</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/lodowki-z-zamrazalnikiem-u-dolu-237/">Lodówki z zamrażalnikiem u dołu</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/lodowki-side-by-side-238/">Lodówki Side by Side</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/zmywarki-wolnostojace-91/">Zmywarki wolnostojące</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/pralki-ladowane-od-frontu-233/">Pralki ładowane od frontu</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/kuchnie-gazowo-elektryczne-240/">Kuchnie gazowo-elektryczne</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/kuchnie-z-plyta-ceramiczna-241/">Kuchnie z płytą ceramiczną</a>
</li>
<li>
<a href="/agd/agd-wolnostojace/plyty-wolnostojace-242/">Płyty wolnostojące</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/agd/kuchnia-i-gotowanie/">Kuchnia i gotowanie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Kuchnia i gotowanie
</a>
</li>
<li>
<a href="/agd/kuchnia-i-gotowanie/ekspresy-cisnieniowe-277/">Ekspresy ciśnieniowe</a>
 </li>
<li>
<a href="/agd/kuchnia-i-gotowanie/garnki-patelnie-128/">Garnki i patelnie</a>
</li>
<li>
<a href="/agd/kuchnia-i-gotowanie/czajniki-85/">Czajniki</a>
</li>
<li>
<a href="/agd/kuchnia-i-gotowanie/kuchenki-mikrofalowe-86/">Kuchenki mikrofalowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/agd/zdrowie-i-uroda/">Zdrowie i uroda</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Zdrowie i uroda
</a>
</li>
<li>
<a href="/agd/uroda-i-higiena/golarki-84/">Golarki</a>
</li>
<li>
<a href="/agd/uroda-i-higiena/szczoteczki-elektryczne-112/">Szczoteczki elektryczne</a>
</li>
<li>
<a href="/agd/uroda-i-higiena/maszynki-do-wlosow-97/">Maszynki do włosów</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
 <li class="">
<div class="pr-menu-inner">
<a href="/agd/sprzatanie-prasowanie-i-szycie/">Sprzątanie, prasowanie, szycie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Sprzątanie, prasowanie, szycie
</a>
</li>
<li>
<a href="/agd/sprzatanie-prasowanie-i-szycie/zelazka-94/">Żelazka</a>
</li>
<li>
<a href="/agd/sprzatanie-prasowanie-i-szycie/odkurzacze-271/">Odkurzacze</a>
</li>
<li>
<a href="/agd/sprzatanie-prasowanie-i-szycie/myjki-cisnieniowe-170/">Myjki ciśnieniowe</a>
</li>
<li>
<a href="/agd/sprzatanie-prasowanie-i-szycie/generatory-pary-274/">Generatory pary</a>
</li>
<li>
<a href="/agd/sprzatanie-prasowanie-i-szycie/odkurzacze-271/?konfigurator=konfigurator-zestawow-do-sprzatania#configurator">Dobierz zestaw, który ułatwia sprzątanie</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/agd/nawilzacze-i-wentylatory/">Nawilżacze i wentylatory</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/agd/zlewy-baterie/">Zlewy, baterie</a>
 </div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/agd/oswietlenie/">Oświetlenie</a>
</div>
</li>
</ul>
</div>
<div class="menu-sis menu-agd  hidden-xs hidden-sm">
<div class="label">Sklepy producentów:</div>
<div class="content">
<div class="item">
<a href="/miele/" class="logo-miele">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
</div>
<div class="clearBoth"></div>
</div>
</div>
</li>
<li class="pr-menu-item menu-phones  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/telefony/"><span class="helper-before"></span>Telefony i tablety<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Telefony i tablety
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/telefony/telefony-smartfony-krotkofalowki/smartfony-280">Smartfony</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
 </a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Smartfony
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/smartfony-280/,,,,,,,,,,31881O1104912/1/">iOS</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/smartfony-280/,,,,,,,,,,31881O1104911/1/">Android</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/smartfony-280/,,,,,,,,,,31881O1104913/1/">Windows</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/telefony/telefony-smartfony-krotkofalowki/telefony-komorkowe-64">Telefony komórkowe</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Telefony komórkowe
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/telefony-komorkowe-64/,,,,,,,,,,29773O945498/1/">Dla aktywnych</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/telefony-komorkowe-64/,,,,,,,,,,29773O942721/1/">Dla seniora</a>
 </li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/telefony/telefony-stacjonarne">Telefony stacjonarne</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Telefony stacjonarne
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-stacjonarne/telefony-przewodowe-282/">Telefony przewodowe</a>
</li>
<li>
<a href="https://www.morele.net/telefony/telefony-stacjonarne/telefony-bezprzewodowe-281/">Telefony bezprzewodowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/telefony/faxy">Faksy</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Faksy
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/faksy/faksy-na-papier-zwykly-308/">Faksy na papier zwykły</a>
</li>
<li>
<a href="https://www.morele.net/telefony/faksy/faksy-na-papier-termiczny-307/">Faksy na papier termiczny</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/telefony-smartfony-krotkofalowki/krotkofalowki-158/">Krótkofalówki</a>
</div>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/tablety-i-czytniki-ebookow/tablety-528">Tablety</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Tablety
</a>
</li>
<li>
<a href="/telefony/tablety/tablety-528/,,,,,,,,,,28297O916565.988281.1035980.1152626/1/">iOS</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety-i-czytniki-ebookow/tablety-528/,,,,,,,,,,28297O978488.1024088.968829.1099825.995112.905213.1070772/1/">Android</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety-i-czytniki-ebookow/tablety-528/,,,,,,,,,,28297O1041887.1029605.966795.969817.918973.1070772/1/">Windows</a>
</li>
 </ul>
</li>
<li class=" sb-normal ">
<div class="pr-menu-inner">
<a href="https://www.morele.net/komputery/klawiatury-i-myszki/tablety-graficzne-54/">Tablety graficzne</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Tablety graficzne
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/czytniki-e-book-542/">Czytniki e-book</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/tablety/">Akcesoria do tabletów</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Akcesoria do tabletów
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/etui-do-tabletow-632/">Etui do tabletów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/etui-z-klawiatura-667/">Etui z klawiaturą</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/folie-ochronne-do-tabletow-706/">Folie ochronne do tabletów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/stojaki-do-tabletow-675/">Stojaki do tabletów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/uchwyty-do-tabletow-644/">Uchwyty do tabletów</a>
</li>
<li>
<a href="https://www.morele.net/komputery/tablety-i-czytniki-e-bookow/pozostale-akcesoria-do-tabletow-498/">Pozostałe akcesoria do tabletów</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/akcesoria-gsm/">Dźwięk i komunikacja</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Dźwięk i komunikacja
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/glosniki-przenosne-677/">Głośniki przenośne</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/sluchawki-bezprzewodowe-458/">Słuchawki bezprzewodowe</a>
</li>
 <li>
<a href="https://www.morele.net/komputery/sluchawki-glosniki-i-mp3/sluchawki-douszne-i-dokanalowe-457/">Słuchawki douszne i dokanałowe</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/zestawy-glosnomowiace-gsm-534/">Zestawy głośnomówiące GSM</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/zestawy-sluchawkowe-gsm-288/">Zestawy słuchawkowe GSM</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/akcesoria-gsm/">Ochrona</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Ochrona
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/etui-i-pokrowce-do-telefonow-450/">Etui i pokrowce do telefonów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/folie-i-szkla-ochronne-do-telefonow-540/">Folie i szkła ochronne do telefonów</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/akcesoria-gsm/">Zasilanie</a>
 <a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Zasilanie
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/baterie-do-telefonow-gsm-514/">Baterie do telefonów GSM</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/ladowarki-samochodowe-659/">Ładowarki samochodowe</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/ladowarki-sieciowe-643/">Ładowarki sieciowe</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/powerbanki-584/">Powerbanki</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/telefony/akcesoria-gsm">Pozostałe akcesoria</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Pozostałe akcesoria
</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/gogle-vr-1436/">Gogle VR</a>
</li>
<li>
<a href="https://www.morele.net/komputery/kable-i-adaptery/kable-usb-699/">Kable USB</a>
</li>
<li>
<a href="https://www.morele.net/komputery/dyski-i-nosniki-danych/karty-pamieci-microsd-626/">Karty pamięci microSD</a>
</li>
<li>
<a href="https://www.morele.net/telefony/tablety/rysiki-645/">Rysiki</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/selfie-stick-1341/">Selfie stick</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/uchwyty-do-telefonow-536/">Uchwyty do telefonów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/akcesoria-gsm/pozostale-akcesoria-gsm-731/">Pozostałe akcesoria GSM</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/">Smartwatche, wearables, sport</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Smartwatche, wearables, sport
</a>
</li>
<li>
<a href="/biuro/zegarki/zegarki-sportowe-1524/">Zegarki sportowe</a>
</li>
<li>
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/zegarki-smartwatch-732/">Zegarki smartwatch</a>
</li>
<li>
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/smartbandy-1291/">Smartbandy</a>
</li>
<li>
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/akcesoria-do-smartwatchow-776/">Akcesoria do smartwatchów</a>
</li>
<li>
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/pulsometry-i-krokomierze-733/">Pulsometry i krokomierze</a>
</li>
<li>
<a href="/telefony/smartwatche-wearables-sport/deskorolki-elektryczne-1343/">Deskorolki elektryczne</a>
</li>
<li>
<a href="https://www.morele.net/telefony/smartwatche-wearables-sport/akcesoria-sportowe-1011/">Akcesoria sportowe</a>
</li>
</ul>
</li>
</ul>
</div>
 </div>
</li>
<li class="pr-menu-item menu-foto  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/"><span class="helper-before"></span>Foto i kamery<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Foto i kamery
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/aparaty-cyfrowe/">Aparaty cyfrowe</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Aparaty cyfrowe
</a>
</li>
<li>
<a href="/fotografia-i-kamery/aparaty-cyfrowe/aparaty-kompaktowe-2/">Aparaty kompaktowe</a>
</li>
<li>
<a href="/fotografia-i-kamery/aparaty-cyfrowe/bezlusterkowce-586/">Bezlusterkowce</a>
</li>
<li>
<a href="/fotografia-i-kamery/aparaty-cyfrowe/lustrzanki-114/">Lustrzanki</a>
</li>
<li>
<a href="/fotografia-i-kamery/aparaty-cyfrowe/aparaty-kompaktowe-2/,,,,,,,,,,31328O1078008/1/">Aparaty natychmiastowe</a>
</li>
 </ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/sprzet-fotograficzny/">Sprzęt fotograficzny</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Sprzęt fotograficzny
</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/statywy-408/">Statywy</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/glowice-do-statywow-505/">Głowice do statywów</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/monopody-455/">Monopody</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/obiektywy-285/">Obiektywy</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/filtry-foto-video-478/">Filtry foto-video</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/konwertery-foto-526/">Konwertery foto</a>
</li>
<li>
<a href="/fotografia-i-kamery/sprzet-fotograficzny/lampy-blyskowe-286/">Lampy błyskowe</a>
</li>
</ul>
</li>
 </ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/kamery/">Kamery</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Kamery
</a>
</li>
<li>
<a href="/fotografia-i-kamery/kamery/kamery-cyfrowe-113/">Kamery cyfrowe</a>
</li>
<li>
<a href="/fotografia-i-kamery/kamery/kamery-sportowe-748/">Kamery sportowe</a>
</li>
<li>
<a href="/fotografia-i-kamery/kamery/kamery-profesjonalne-642/">Kamery profesjonalne</a>
</li>
<li>
<a href="/fotografia-i-kamery/kamery/kamery-samochodowe-652/">Kamery samochodowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/zasilanie/">Zasilanie</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
 <a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Zasilanie
</a>
</li>
<li>
<a href="/fotografia-i-kamery/zasilanie/akumulatory-do-aparatow-i-kamer-287/">Akumulatory do aparatów i kamer</a>
</li>
<li>
<a href="/fotografia-i-kamery/zasilanie/ladowarki-do-aparatow-i-kamer-488/">Ładowarki do aparatów i kamer</a>
</li>
<li>
<a href="/fotografia-i-kamery/zasilanie/battery-gripy-472/">Battery gripy</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/">Torby, futerały, obudowy</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Torby, futerały, obudowy
</a>
</li>
<li>
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/torby-foto-video-444/">Torby foto-video</a>
</li>
<li>
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/futeraly-foto-66/">Futerały foto</a>
</li>
<li>
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/plecaki-foto-447/">Plecaki foto</a>
</li>
<li>
 <a href="/fotografia-i-kamery/torby-futeraly-obudowy/obudowy-podwodne-503/">Obudowy podwodne</a>
</li>
<li>
<a href="/fotografia-i-kamery/torby-futeraly-obudowy/walizki-foto-616/">Walizki foto</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/">Akcesoria fotograficzne</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Akcesoria fotograficzne
</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/akcesoria-do-kamer-sportowych-1010/">Akcesoria do kamer sportowych</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/albumy-fotograficzne-565/">Albumy fotograficzne</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/cyfrowe-ramki-foto-187/">Cyfrowe ramki foto</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/czytniki-kart-pamieci-65/">Czytniki kart pamięci</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/dekielki-zaslepki-504/">Dekielki, zaślepki</a>
</li>
 <li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/karty-pamieci-13/">Karty pamięci</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/muszle-oczne-i-wizjery-637/">Muszle oczne i wizjery</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/oslony-przeciwsloneczne-i-tulipany-622/">Osłony przeciwsłoneczne i tulipany</a>
</li>
<li>
<a href="/fotografia-i-kamery/akcesoria-fotograficzne/piloty-i-wezyki-spustowe-627/">Piloty i wężyki spustowe</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/wyposazenie-studia/">Wyposażenie studia</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Wyposażenie studia
</a>
</li>
<li>
<a href="/fotografia-i-kamery/wyposazenie-studia/akcesoria-studyjne-647/">Akcesoria studyjne</a>
</li>
<li>
<a href="/fotografia-i-kamery/wyposazenie-studia/blendy-646/">Blendy</a>
</li>
<li>
<a href="/fotografia-i-kamery/wyposazenie-studia/lampy-pierscieniowe-603/">Lampy pierścieniowe</a>
</li>
<li>
 <a href="/fotografia-i-kamery/wyposazenie-studia/lampy-studyjne-602/">Lampy studyjne</a>
</li>
<li>
<a href="/fotografia-i-kamery/wyposazenie-studia/zestawy-studyjne-653/">Zestawy studyjne</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/obserwacja/">Obserwacja</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Obserwacja
</a>
</li>
<li>
<a href="/fotografia-i-kamery/obserwacja/dalmierze-171/">Dalmierze</a>
</li>
<li>
<a href="/fotografia-i-kamery/obserwacja/lornetki-165/">Lornetki</a>
</li>
<li>
<a href="/fotografia-i-kamery/obserwacja/lunety-615/">Lunety</a>
</li>
<li>
<a href="/fotografia-i-kamery/obserwacja/mikroskopy-1293/">Mikroskopy</a>
</li>
<li>
<a href="/fotografia-i-kamery/obserwacja/teleskopy-577/">Teleskopy</a>
</li>
</ul>
</li>
 <li class=" sb-normal ">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/modele-zdalnie-sterowane/drony-769/">Drony</a>
</div>
</li>
<li class=" sb-normal ">
<div class="pr-menu-inner">
<a href="/fotografia-i-kamery/modele-zdalnie-sterowane/akcesoria-do-dronow-770/">Akcesoria do dronów</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/drukowanie-i-skanery/">Drukowanie i skanery</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Drukowanie i skanery
</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/drukarki-fotograficzne-283/">Drukarki fotograficzne</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/skanery-filmow-i-negatywow-581/">Skanery filmów i negatywów</a>
</li>
<li>
<a href="/komputery/drukowanie-i-skanery/papier-fotograficzny-441/">Papier fotograficzny</a>
</li>
</ul>
</li>
</ul>
</div>
 <div class="menu-sis menu-foto hidden-xs hidden-sm">
<div class="label">Sklepy producentów:</div>
<div class="content">
<div class="item">
<a href="/gopro/" class="logo-gopro">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
</div>
<div class="clearBoth"></div>
</div>
</div>
</li>
<li class="pr-menu-item menu-gamer  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/strefa-gracza/"><span class="helper-before"></span>Strefa Gracza<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Strefa Gracza
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/strefa-gracza/konsole/">Konsole</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Konsole
</a>
</li>
<li>
<a href="/strefa-gracza/konsole/konsole-115/0,0,,,,,,,,,2114O792360/1">Playstation 4</a>
</li>
<li>
 <a href="/strefa-gracza/konsole/konsole-115/0,0,,,,,,,,,2114O968583/1">Xbox One</a>
</li>
<li>
<a href="/strefa-gracza/konsole/konsole-115/0,0,,,,,,,,,2114O231870/1">Xbox 360</a>
</li>
</ul>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/">Komputery dla graczy</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Komputery dla graczy
</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/,1999.00,,,,676,,p,0,,/1/">SKY seria G1000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/1999.00,2999.00,,,,676,,p,0,,/1/">SKY seria G2000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/3000.00,3999.00,,,,676,,p,0,,/1/">SKY seria G3000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/4000.00,4998.00,,,,676,,p,0,,/1/">SKY seria G4000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/4999.00,5999.00,,,,676,,p,0,,/1/">SKY seria G5000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/6000.00,6999.00,,,,676,,p,0,,/1/">SKY seria G6000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/7000.00,7999.00,,,,676,,p,0,,/1/">SKY seria G7000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/8000.00,8999.00,,,,676,,p,0,,/1/">SKY seria G8000</a>
</li>
<li>
<a href="/komputery/komputery-pc/komputery-dla-graczy-672/9000.00,,,,,676,,p,0,,/1/">SKY seria G9000</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/strefa-gracza/gry-konsole-i-pc/">Gry</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Gry
</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-pc-wersje-pudelkowe-29/">Gry PC (wersje pudełkowe)</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-pc-wersje-cyfrowe-1296/">Gry PC (wersje cyfrowe)</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-xbox-one-734/">Gry Xbox One</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-playstation-4-729/">Gry Playstation 4</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-xbox-360-254/">Gry Xbox 360</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/gry-playstation-3-257/">Gry Playstation 3</a>
</li>
<li>
<a href="/strefa-gracza/gry-konsole-i-pc/abonamenty-psn-i-xbox-411/">Abonamenty PSN i Xbox</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/strefa-gracza/sprzet-dla-graczy/">Sprzęt dla graczy</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Sprzęt dla graczy
</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/klawiatury-dla-graczy-465/">Klawiatury dla graczy</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/myszy-dla-graczy-27/">Myszy dla graczy</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/podkladki-dla-graczy-467/">Podkładki dla graczy</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/sluchawki-dla-graczy-466/">Słuchawki dla graczy</a>
 </li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/gamepady-10/">Gamepady</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/kierownice-116/">Kierownice</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/joysticki-709/">Joysticki</a>
</li>
<li>
<a href="/biuro/meble-biurowe/fotele-dla-graczy-747/">Fotele dla graczy</a>
</li>
<li>
<a href="/strefa-gracza/sprzet-dla-graczy/odziez-gamingowa-564/">Odzież gamingowa</a>
</li>
<li class="sb-bold">
<a href="/laptopy/notebooki-laptopy-ultrabooki-31/,,,,,,,,,,31463O1098459/1/">Laptopy dla graczy</a>
</li>
<li class="sb-bold">
<a href="/komputery/monitory-i-akcesoria/monitory-led-lcd-523/,,,,,,,,,,30812O1044560/1/">Monitory dla graczy</a>
</li>
</ul>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/strefa-gracza/konsole-akcesoria/">Akcesoria do konsol</a>
<a href="#" class="pr-sbm-open">
<i class="fa fa-angle-right" aria-hidden="true"></i>
</a>
</div>
<ul class="childs">
<li class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Akcesoria do konsol
</a>
 </li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/kontrolery-ruchu-do-konsol-771/">Kontrolery ruchu do konsol</a>
</li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/gamepady-10/,,,,,,,,,,29078O905688.972535.902048.905723.944304.1019375.905677/1/">Gamepady do konsol</a>
</li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/kierownice-116/">Kierownice</a>
</li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/nakladki-na-kontrolery-569/">Nakładki na kontrolery</a>
</li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/ladowarki-baterie-zasilacze-571/">Ładowarki, baterie, zasilacze</a>
</li>
<li>
<a href="/strefa-gracza/konsole-akcesoria/akcesoria-do-konsol-74/">Akcesoria, konwertery, kable</a>
</li>
</ul>
</li>
</ul>
</div>
<div class="menu-sis menu-gamer hidden-xs hidden-sm">
<div class="label">Sklepy producentów:</div>
<div class="content">
<div class="item">
<a href="/thrustmaster/" class="logo-thrustmaster">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
<div class="item">
<a href="/razer/" class="logo-razer">
<span class="img-first"></span>
<span class="img-second"></span>
</a>
</div>
</div>
<div class="clearBoth"></div>
 </div>
</div>
</li>
<li class="pr-menu-item menu-all  has-subitems" data-page-overlay-dalay="0,0" data-min-width="1025" data-overlay-duration="200">
<div class="pr-menu-inner">
<a href="/biuro/"><span class="helper-before"></span>Biuro<span class="helper-after"></span></a>
<a href="#" class="pr-sbm-open"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div>
<div class="pr-menu-sb-cat">
<div class="row">
<div class="pr-menu-subcat-h">
<a href="#" class="pr-sbm-close">
<i class="fa fa-angle-left" aria-hidden="true"></i> Biuro
</a>
</div>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/akcesoria-biurowe/">Akcesoria biurowe</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/artykuly-pakowe/">Artykuły pakowe</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/artykuly-papiernicze/">Artykuły papiernicze</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/artykuly-pismiennicze/">Artykuły piśmiennicze</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/artykuly-szkolne/">Artykuły szkolne</a>
</div>
</li>
 </ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/chemia-gospodarcza-i-art-higieniczne/">Chemia gospodarcza i art. higieniczne</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="https://www.morele.net/biuro/edukacja/">Edukacja</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/meble-biurowe/">Meble biurowe</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/organizacja-dokumentow/">Organizacja dokumentów</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/prezentacja-wizualna/">Prezentacja wizualna</a>
</div>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/sejfy-i-kasetki/">Sejfy i kasetki</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
 <a href="/biuro/urzadzenia-biurowe/">Urządzenia biurowe</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/zegarki/">Zegarki</a>
</div>
</li>
<li class="">
<div class="pr-menu-inner">
<a href="/biuro/urzadzenia-biurowe/zestawy-biurowe-737/">Zestawy biurowe</a>
</div>
</li>
</ul>
<ul class="pr-menu-sb-cat-item">
</ul>
</div>
</div>
</li>
</ul>
</nav> <div class="fixed-elements">
<div id="productGallery">
<div class="sliderHolder hidden" data-elem="sliderHolder">
<div class="slider" data-elem="slider" data-options="initShow:true; resetScrollDuration:1;" data-show="autoAlpha:1; display:block" data-hide="autoAlpha:0; display:none">
<div class="controlHolder">
<div class="" data-elem="zoomOut" data-on="autoAlpha:1; cursor: pointer;" data-off="autoAlpha:0.5; cursor:default"> <i class="fa fa-search-minus" aria-hidden="true"></i></div>
<div class="" data-elem="zoomIn" data-on="autoAlpha:1; cursor: pointer;" data-off="autoAlpha:0.5; cursor:default"> <i class="fa fa-search-plus" aria-hidden="true"></i></div>
<div class="" data-elem="prev" data-on="autoAlpha:1; cursor: pointer;" data-off="autoAlpha:0.5; cursor:default"> <i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i></div>
<div class="" data-elem="next" data-on="autoAlpha:1; cursor: pointer;" data-off="autoAlpha:0.5; cursor:default"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> </div>
<div class="" data-elem="close"><i class="fa fa-times-circle-o" aria-hidden="true"></i></div>
</div>
<div class="sliderBg"></div>
<div class="slides" data-elem="slides" data-options="maxZoom: 3; resizeDuration:-1; adjustHeight:0"></div>
<div class="gsThumbsHolder" data-elem="thumbsHolder">
<div class="gsThumbs blackBgAlpha60" data-elem="thumbs" data-options="initShow:true; space:5" data-show="bottom:0px;" data-hide="bottom:-100%;"></div>
</div>
<div class="photoslider-big-nav prev" data-elem="prev" data-on="autoAlpha:1; cursor: pointer;" data-off="autoAlpha:0.5; cursor:default"></div>
<div class="photoslider-big-nav next" data-elem="next" data-on="autoAlpha:1; cursor: pointer;" data-off="autoAlpha:0.5; cursor:default"></div>
<ul data-elem="items">
<li>
<a href="https://images.morele.net/full/976330_0_f.jpg">
<img itemprop="image" data-src="https://images.morele.net/i80/976330_0_i80.jpg" />
</a>
</li>
<li>
<a href="https://images.morele.net/full/976330_1_f.jpg">
<img itemprop="image" data-src="https://images.morele.net/i80/976330_1_i80.jpg" />
</a>
</li>
<li>
<a href="https://images.morele.net/full/976330_2_f.jpg">
<img itemprop="image" data-src="https://images.morele.net/i80/976330_2_i80.jpg" />
</a>
</li>
<li>
<a href="https://images.morele.net/full/976330_3_f.jpg">
<img itemprop="image" data-src="https://images.morele.net/i80/976330_3_i80.jpg" />
</a>
</li>
<li>
<a href="https://images.morele.net/full/976330_4_f.jpg">
<img itemprop="image" data-src="https://images.morele.net/i80/976330_4_i80.jpg" />
</a>
</li>
<li>
<a href="https://images.morele.net/full/976330_5_f.jpg">
<img itemprop="image" data-src="https://images.morele.net/i80/976330_5_i80.jpg" />
</a>
</li>
<li>
<a href="https://images.morele.net/full/976330_6_f.jpg">
<img itemprop="image" data-src="https://images.morele.net/i80/976330_6_i80.jpg" />
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<script data-cfasync='false' type="text/javascript">
        defOwned = {"1":"Od dzi\u015b","2":"Od kilku dni","3":"1-3 tygodnie","4":"1-2 miesi\u0105ce","5":"3-6 miesi\u0119cy","6":"Ponad p\u00f3\u0142 roku","7":"Ponad rok"};
        window.promotionText = {"1":"Rekomendacja eksperta:","3":"Producent poleca:"};
    </script>
<script data-cfasync='false' id="translates">
        var pageTrans = {
            warranty: {
                toBasket: 'Przejdź do koszyka',
                mc: 'm-c'
            },
            tech: {
                empty: 'Jeszcze nie ma żadnego zapytania',
                questionLabel: 'Pytanie:',
                answerLabel: 'Odpowiedź:',
                answerButton: 'Znasz odpowiedź? Odpisz!'
            },
            review: {
                empty: 'Oceń jako pierwszy',
                listPlusy: 'Plusy:',
                listMinusy: 'Minusy:',
                listComment: 'Komentarze do opinii:',
                listButtonAnswer: 'Odpowiedź',
                listButtonAuthor: 'Autor:',
                listButtonRate: 'Ocena:',
                listButtonTerm: 'Posiadam ten produkt:',
                listButtonQustion: 'Czy opinia jest dla ciebie pomocna?',
                listButtonAnswerYes: 'tak',
                listButtonAnswerNo: 'nie',
                listRevRate: 'Ocena recenzji:',
                listRevCount: 'Ilość ocen:',
                ratePositive: 'pozytywnych',
                showAnswers: 'Pokaż odpowiedzi',
                hideAnswers: 'Ukryj odpowiedzi',
                modeeration: 'Zgłoś nadużycie',
                modeerationTooltip: 'Tylko zalogowani użytkownicy mogą zgłaszać nadużycie!'
            },
            order: {
                orderVerify: 'Zakup zweryfikowany',
                orderVerifyTooltipReview: 'Użytkownik wystawiający opinię zrealizował zakup tego produktu w sklepie morele.net.',
                orderVerifyTooltipCommentReview: 'Użytkownik udzielający odpowiedzi zrealizował zakup tego produktu w sklepie grupy morele.net.',
                orderVerifyTooltipTech: 'Użytkownik zadający pytanie zrealizował zakup tego produktu w sklepie grupy morele.net.',
                orderVerifyTooltipCommentTech: 'Użytkownik odpowiadający na pytanie zrealizował zakup tego produktu w sklepie grupy morele.net.',
            },
            delivery: {
                date: 'Dostawa lub odbiór na'
            },
            outlet: {
                warranty_period: 'Gwarancja',
                warranty_type: 'Typ gwarancji',
                order_button: 'Kup w outlecie'
            },
            notify: {
                error: {
                    base: 'Wystąpił nieoczekiwany błąd',
                    rate: 'Już oceniłeś tę recenzję',
                    notlogged: 'Musisz się zalogować, żeby ocenić tę recenzję',
                    validation: 'Proszę wypełnić wszystkie wymagane pola formularza'
                },
                success: {
                    tech: {
                        isLogged: 'Pytanie zostało dodane',
                        notLogged: 'Pytanie zostało dodane i jest w trakcie moderacji',
                        hasAccount: 'Na ten mail jest założone konto w naszym sklepie. Prosimy o zalogowanie się przed wysłaniem zapytania.'
                    },
                    review: {
                        isLogged: 'Opinia została dodana',
                        notLogged: 'Opinia została dodana i jest w trakcie moderacji',
                        hasAccount: 'Na ten mail jest założone konto w naszym sklepie. Prosimy o zalogowanie się przed wysłaniem opinii.'
                    },
                    rate: 'Ocena do recenzji jest dodana',
                    moderation: 'Nadużycie zostało zgłoszone',
                    answer: {
                        tech: {
                            isLogged: 'Odpowiedź została dodana',
                            notLogged: 'Odpowiedź została dodana i jest w trakcie moderacji'
                        },
                        review: {
                            isLogged: 'Komentarz został dodany',
                            notLogged: 'Komentarz został dodany i jest w trakcie moderacji'
                        }
                    }
                }
            }
        }
    </script>
<script data-cfasync="true" src="/static/js/product.app.js?version=4.9.10"></script>
<div id="fb-root"></div>
<script data-cfasync="fasle">
    window.warrantyProduction = true
    window.warrantyPopupPage = 'product';
    window.warrantyPopupCatId = '280';
</script>
<div id="fb-root"></div>
<script async="true" data-cfasync="false">(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.async=true;
            js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

<script type='text/javascript' data-cfasync="false">
        var _paq2 = window._paq2 || [];
        (function () {
            var u = "https://ga.getresponse.com/",
                    d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0],
                    traits;

            traits = {
                xsid: '' || document.cookie.replace(/(?:(?:^|.*;\s*)gaVisitorId\s*\=\s*([^;]*).*$)|^.*$/, "$1") || '',
                email: document.cookie.replace(/(?:(?:^|.*;\s*)gaVisitorEmail\s*\=\s*([^;]*).*$)|^.*$/, "$1") || document.cookie.replace(/(?:(?:^|.*;\s*)visitorId\s*\=\s*([^;]*).*$)|^.*$/, "$1") || '',
                save: function() {
                    this.setCookie('gaVisitorEmail', this.email);
                    this.setCookie('gaVisitorId', this.xsid);
                    _paq2.push(["setUserId", this.toString()]);
                    _paq2.push(["trackPageView"]);
                },
                toString: function() {
                    return JSON.stringify({'email':this.email, 'xsid': this.xsid});
                },
                setCookie: function (param, value) {
                    var date = new Date();
                    date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
                    document.cookie = param + '=' + value + '; expires=' + date.toGMTString() + '; path=/';
                }
            };

            _paq2.push(["setCustomVariable", 1, "grid", "pA10FMkQ=", "visit"]);
            _paq2.push(["enableLinkTracking", true]);
            _paq2.push(["setTrackerUrl", u + 'index.php?ver=2']);
            traits.save();

            g.type="text/javascript"; g.async=true; g.defer=true; g.src=u+"js/piwik.js"; s.parentNode.insertBefore(g,s);

            window.gaSetUserId = function (email) {
                if (email) {
                    _paq2.push(["setCustomUrl", window.location.href]);
                    traits.email = email;
                    traits.save();
                }
            };

            window.gaPush = function () {
                _paq2.push(['setCustomUrl', window.location.href]);
                _paq2.push(['trackPageView']);
            };

        }());
            </script>


<noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-FGJG" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
<script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "Organization",
          "name" : "Morele.net Sp. z o.o.",
          "url": "http://www.morele.net",
          "logo": "http://www.morele.net/static/img/shop/img-morele-logo.png",
          "contactPoint" : [{
            "@type" : "ContactPoint",
            "telephone" : "+48 12 418 40 26",
            "contactType" : "customer support"
          }],
          "sameAs" : [
            "https://www.facebook.com/morele.net/",
            "https://www.youtube.com/user/MoreleTV",
            "https://plus.google.com/+MoreleNetsklep"
          ]
        }
        </script>
<script data-cfasync="false" type="text/javascript">
        window.onload = function(){
                            (function(w,d,s,l,i){
                    w[l]=w[l]||[];
                    w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});
                    var f=d.getElementsByTagName(s)[0],
                            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';
                    j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;
                    f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-FGJG');
                        
            
            
            
        
                
        
        
            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = 'https://static.criteo.net/js/ld/ld.js';
                ga.onload = function(){
                    window.criteo_q = window.criteo_q || [];
                    window.criteo_q.push(
                            {event: "setAccount", account: 15285},
                            {event: "setCustomerId", id: ""},
                            {event: "setSiteType", type: "d"},
                            {event: "viewItem", item: 976330}
                    );
                };
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

                
        
                                }
    </script>

<script async data-cfasync="false" type="text/javascript">
                window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var o=e[n]={exports:{}};t[n][0].call(o.exports,function(e){var o=t[n][1][e];return r(o||e)},o,o.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(t,e,n){function r(t){try{s.console&&console.log(t)}catch(e){}}var o,i=t("ee"),a=t(15),s={};try{o=localStorage.getItem("__nr_flags").split(","),console&&"function"==typeof console.log&&(s.console=!0,o.indexOf("dev")!==-1&&(s.dev=!0),o.indexOf("nr_dev")!==-1&&(s.nrDev=!0))}catch(c){}s.nrDev&&i.on("internal-error",function(t){r(t.stack)}),s.dev&&i.on("fn-err",function(t,e,n){r(n.stack)}),s.dev&&(r("NR AGENT IN DEVELOPMENT MODE"),r("flags: "+a(s,function(t,e){return t}).join(", ")))},{}],2:[function(t,e,n){function r(t,e,n,r,o){try{d?d-=1:i("err",[o||new UncaughtException(t,e,n)])}catch(s){try{i("ierr",[s,(new Date).getTime(),!0])}catch(c){}}return"function"==typeof f&&f.apply(this,a(arguments))}function UncaughtException(t,e,n){this.message=t||"Uncaught error with no additional information",this.sourceURL=e,this.line=n}function o(t){i("err",[t,(new Date).getTime()])}var i=t("handle"),a=t(16),s=t("ee"),c=t("loader"),f=window.onerror,u=!1,d=0;c.features.err=!0,t(1),window.onerror=r;try{throw new Error}catch(l){"stack"in l&&(t(8),t(7),"addEventListener"in window&&t(5),c.xhrWrappable&&t(9),u=!0)}s.on("fn-start",function(t,e,n){u&&(d+=1)}),s.on("fn-err",function(t,e,n){u&&(this.thrown=!0,o(n))}),s.on("fn-end",function(){u&&!this.thrown&&d>0&&(d-=1)}),s.on("internal-error",function(t){i("ierr",[t,(new Date).getTime(),!0])})},{}],3:[function(t,e,n){t("loader").features.ins=!0},{}],4:[function(t,e,n){function r(t){}if(window.performance&&window.performance.timing&&window.performance.getEntriesByType){var o=t("ee"),i=t("handle"),a=t(8),s=t(7),c="learResourceTimings",f="addEventListener",u="resourcetimingbufferfull",d="bstResource",l="resource",p="-start",h="-end",m="fn"+p,w="fn"+h,v="bstTimer",y="pushState";t("loader").features.stn=!0,t(6);var g=NREUM.o.EV;o.on(m,function(t,e){var n=t[0];n instanceof g&&(this.bstStart=Date.now())}),o.on(w,function(t,e){var n=t[0];n instanceof g&&i("bst",[n,e,this.bstStart,Date.now()])}),a.on(m,function(t,e,n){this.bstStart=Date.now(),this.bstType=n}),a.on(w,function(t,e){i(v,[e,this.bstStart,Date.now(),this.bstType])}),s.on(m,function(){this.bstStart=Date.now()}),s.on(w,function(t,e){i(v,[e,this.bstStart,Date.now(),"requestAnimationFrame"])}),o.on(y+p,function(t){this.time=Date.now(),this.startPath=location.pathname+location.hash}),o.on(y+h,function(t){i("bstHist",[location.pathname+location.hash,this.startPath,this.time])}),f in window.performance&&(window.performance["c"+c]?window.performance[f](u,function(t){i(d,[window.performance.getEntriesByType(l)]),window.performance["c"+c]()},!1):window.performance[f]("webkit"+u,function(t){i(d,[window.performance.getEntriesByType(l)]),window.performance["webkitC"+c]()},!1)),document[f]("scroll",r,!1),document[f]("keypress",r,!1),document[f]("click",r,!1)}},{}],5:[function(t,e,n){function r(t){for(var e=t;e&&!e.hasOwnProperty(u);)e=Object.getPrototypeOf(e);e&&o(e)}function o(t){s.inPlace(t,[u,d],"-",i)}function i(t,e){return t[1]}var a=t("ee").get("events"),s=t(17)(a),c=t("gos"),f=XMLHttpRequest,u="addEventListener",d="removeEventListener";e.exports=a,"getPrototypeOf"in Object?(r(document),r(window),r(f.prototype)):f.prototype.hasOwnProperty(u)&&(o(window),o(f.prototype)),a.on(u+"-start",function(t,e){if(t[1]){var n=t[1];if("function"==typeof n){var r=c(n,"nr@wrapped",function(){return s(n,"fn-",null,n.name||"anonymous")});this.wrapped=t[1]=r}else"function"==typeof n.handleEvent&&s.inPlace(n,["handleEvent"],"fn-")}}),a.on(d+"-start",function(t){var e=this.wrapped;e&&(t[1]=e)})},{}],6:[function(t,e,n){var r=t("ee").get("history"),o=t(17)(r);e.exports=r,o.inPlace(window.history,["pushState","replaceState"],"-")},{}],7:[function(t,e,n){var r=t("ee").get("raf"),o=t(17)(r),i="equestAnimationFrame";e.exports=r,o.inPlace(window,["r"+i,"mozR"+i,"webkitR"+i,"msR"+i],"raf-"),r.on("raf-start",function(t){t[0]=o(t[0],"fn-")})},{}],8:[function(t,e,n){function r(t,e,n){t[0]=a(t[0],"fn-",null,n)}function o(t,e,n){this.method=n,this.timerDuration="number"==typeof t[1]?t[1]:0,t[0]=a(t[0],"fn-",this,n)}var i=t("ee").get("timer"),a=t(17)(i),s="setTimeout",c="setInterval",f="clearTimeout",u="-start",d="-";e.exports=i,a.inPlace(window,[s,"setImmediate"],s+d),a.inPlace(window,[c],c+d),a.inPlace(window,[f,"clearImmediate"],f+d),i.on(c+u,r),i.on(s+u,o)},{}],9:[function(t,e,n){function r(t,e){d.inPlace(e,["onreadystatechange"],"fn-",s)}function o(){var t=this,e=u.context(t);t.readyState>3&&!e.resolved&&(e.resolved=!0,u.emit("xhr-resolved",[],t)),d.inPlace(t,w,"fn-",s)}function i(t){v.push(t),h&&(g=-g,b.data=g)}function a(){for(var t=0;t<v.length;t++)r([],v[t]);v.length&&(v=[])}function s(t,e){return e}function c(t,e){for(var n in t)e[n]=t[n];return e}t(5);var f=t("ee"),u=f.get("xhr"),d=t(17)(u),l=NREUM.o,p=l.XHR,h=l.MO,m="readystatechange",w=["onload","onerror","onabort","onloadstart","onloadend","onprogress","ontimeout"],v=[];e.exports=u;var y=window.XMLHttpRequest=function(t){var e=new p(t);try{u.emit("new-xhr",[e],e),e.addEventListener(m,o,!1)}catch(n){try{u.emit("internal-error",[n])}catch(r){}}return e};if(c(p,y),y.prototype=p.prototype,d.inPlace(y.prototype,["open","send"],"-xhr-",s),u.on("send-xhr-start",function(t,e){r(t,e),i(e)}),u.on("open-xhr-start",r),h){var g=1,b=document.createTextNode(g);new h(a).observe(b,{characterData:!0})}else f.on("fn-end",function(t){t[0]&&t[0].type===m||a()})},{}],10:[function(t,e,n){function r(t){var e=this.params,n=this.metrics;if(!this.ended){this.ended=!0;for(var r=0;r<d;r++)t.removeEventListener(u[r],this.listener,!1);if(!e.aborted){if(n.duration=(new Date).getTime()-this.startTime,4===t.readyState){e.status=t.status;var i=o(t,this.lastSize);if(i&&(n.rxSize=i),this.sameOrigin){var a=t.getResponseHeader("X-NewRelic-App-Data");a&&(e.cat=a.split(", ").pop())}}else e.status=0;n.cbTime=this.cbTime,f.emit("xhr-done",[t],t),s("xhr",[e,n,this.startTime])}}}function o(t,e){var n=t.responseType;if("json"===n&&null!==e)return e;var r="arraybuffer"===n||"blob"===n||"json"===n?t.response:t.responseText;return h(r)}function i(t,e){var n=c(e),r=t.params;r.host=n.hostname+":"+n.port,r.pathname=n.pathname,t.sameOrigin=n.sameOrigin}var a=t("loader");if(a.xhrWrappable){var s=t("handle"),c=t(11),f=t("ee"),u=["load","error","abort","timeout"],d=u.length,l=t("id"),p=t(14),h=t(13),m=window.XMLHttpRequest;a.features.xhr=!0,t(9),f.on("new-xhr",function(t){var e=this;e.totalCbs=0,e.called=0,e.cbTime=0,e.end=r,e.ended=!1,e.xhrGuids={},e.lastSize=null,p&&(p>34||p<10)||window.opera||t.addEventListener("progress",function(t){e.lastSize=t.loaded},!1)}),f.on("open-xhr-start",function(t){this.params={method:t[0]},i(this,t[1]),this.metrics={}}),f.on("open-xhr-end",function(t,e){"loader_config"in NREUM&&"xpid"in NREUM.loader_config&&this.sameOrigin&&e.setRequestHeader("X-NewRelic-ID",NREUM.loader_config.xpid)}),f.on("send-xhr-start",function(t,e){var n=this.metrics,r=t[0],o=this;if(n&&r){var i=h(r);i&&(n.txSize=i)}this.startTime=(new Date).getTime(),this.listener=function(t){try{"abort"===t.type&&(o.params.aborted=!0),("load"!==t.type||o.called===o.totalCbs&&(o.onloadCalled||"function"!=typeof e.onload))&&o.end(e)}catch(n){try{f.emit("internal-error",[n])}catch(r){}}};for(var a=0;a<d;a++)e.addEventListener(u[a],this.listener,!1)}),f.on("xhr-cb-time",function(t,e,n){this.cbTime+=t,e?this.onloadCalled=!0:this.called+=1,this.called!==this.totalCbs||!this.onloadCalled&&"function"==typeof n.onload||this.end(n)}),f.on("xhr-load-added",function(t,e){var n=""+l(t)+!!e;this.xhrGuids&&!this.xhrGuids[n]&&(this.xhrGuids[n]=!0,this.totalCbs+=1)}),f.on("xhr-load-removed",function(t,e){var n=""+l(t)+!!e;this.xhrGuids&&this.xhrGuids[n]&&(delete this.xhrGuids[n],this.totalCbs-=1)}),f.on("addEventListener-end",function(t,e){e instanceof m&&"load"===t[0]&&f.emit("xhr-load-added",[t[1],t[2]],e)}),f.on("removeEventListener-end",function(t,e){e instanceof m&&"load"===t[0]&&f.emit("xhr-load-removed",[t[1],t[2]],e)}),f.on("fn-start",function(t,e,n){e instanceof m&&("onload"===n&&(this.onload=!0),("load"===(t[0]&&t[0].type)||this.onload)&&(this.xhrCbStart=(new Date).getTime()))}),f.on("fn-end",function(t,e){this.xhrCbStart&&f.emit("xhr-cb-time",[(new Date).getTime()-this.xhrCbStart,this.onload,e],e)})}},{}],11:[function(t,e,n){e.exports=function(t){var e=document.createElement("a"),n=window.location,r={};e.href=t,r.port=e.port;var o=e.href.split("://");!r.port&&o[1]&&(r.port=o[1].split("/")[0].split("@").pop().split(":")[1]),r.port&&"0"!==r.port||(r.port="https"===o[0]?"443":"80"),r.hostname=e.hostname||n.hostname,r.pathname=e.pathname,r.protocol=o[0],"/"!==r.pathname.charAt(0)&&(r.pathname="/"+r.pathname);var i=!e.protocol||":"===e.protocol||e.protocol===n.protocol,a=e.hostname===document.domain&&e.port===n.port;return r.sameOrigin=i&&(!e.hostname||a),r}},{}],12:[function(t,e,n){function r(){}function o(t,e,n){return function(){return i(t,[(new Date).getTime()].concat(s(arguments)),e?null:this,n),e?void 0:this}}var i=t("handle"),a=t(15),s=t(16),c=t("ee").get("tracer"),f=NREUM;"undefined"==typeof window.newrelic&&(newrelic=f);var u=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit"],d="api-",l=d+"ixn-";a(u,function(t,e){f[e]=o(d+e,!0,"api")}),f.addPageAction=o(d+"addPageAction",!0),e.exports=newrelic,f.interaction=function(){return(new r).get()};var p=r.prototype={createTracer:function(t,e){var n={},r=this,o="function"==typeof e;return i(l+"tracer",[Date.now(),t,n],r),function(){if(c.emit((o?"":"no-")+"fn-start",[Date.now(),r,o],n),o)try{return e.apply(this,arguments)}finally{c.emit("fn-end",[Date.now()],n)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,e){p[e]=o(l+e)}),newrelic.noticeError=function(t){"string"==typeof t&&(t=new Error(t)),i("err",[t,(new Date).getTime()])}},{}],13:[function(t,e,n){e.exports=function(t){if("string"==typeof t&&t.length)return t.length;if("object"==typeof t){if("undefined"!=typeof ArrayBuffer&&t instanceof ArrayBuffer&&t.byteLength)return t.byteLength;if("undefined"!=typeof Blob&&t instanceof Blob&&t.size)return t.size;if(!("undefined"!=typeof FormData&&t instanceof FormData))try{return JSON.stringify(t).length}catch(e){return}}}},{}],14:[function(t,e,n){var r=0,o=navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/);o&&(r=+o[1]),e.exports=r},{}],15:[function(t,e,n){function r(t,e){var n=[],r="",i=0;for(r in t)o.call(t,r)&&(n[i]=e(r,t[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;e.exports=r},{}],16:[function(t,e,n){function r(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,o=n-e||0,i=Array(o<0?0:o);++r<o;)i[r]=t[e+r];return i}e.exports=r},{}],17:[function(t,e,n){function r(t){return!(t&&"function"==typeof t&&t.apply&&!t[a])}var o=t("ee"),i=t(16),a="nr@original",s=Object.prototype.hasOwnProperty,c=!1;e.exports=function(t){function e(t,e,n,o){function nrWrapper(){var r,a,s,c;try{a=this,r=i(arguments),s="function"==typeof n?n(r,a):n||{}}catch(u){d([u,"",[r,a,o],s])}f(e+"start",[r,a,o],s);try{return c=t.apply(a,r)}catch(l){throw f(e+"err",[r,a,l],s),l}finally{f(e+"end",[r,a,c],s)}}return r(t)?t:(e||(e=""),nrWrapper[a]=t,u(t,nrWrapper),nrWrapper)}function n(t,n,o,i){o||(o="");var a,s,c,f="-"===o.charAt(0);for(c=0;c<n.length;c++)s=n[c],a=t[s],r(a)||(t[s]=e(a,f?s+o:o,i,s))}function f(e,n,r){if(!c){c=!0;try{t.emit(e,n,r)}catch(o){d([o,e,n,r])}c=!1}}function u(t,e){if(Object.defineProperty&&Object.keys)try{var n=Object.keys(t);return n.forEach(function(n){Object.defineProperty(e,n,{get:function(){return t[n]},set:function(e){return t[n]=e,e}})}),e}catch(r){d([r])}for(var o in t)s.call(t,o)&&(e[o]=t[o]);return e}function d(e){try{t.emit("internal-error",e)}catch(n){}}return t||(t=o),e.inPlace=n,e.flag=a,e}},{}],ee:[function(t,e,n){function r(){}function o(t){function e(t){return t&&t instanceof r?t:t?s(t,a,i):i()}function n(n,r,o){t&&t(n,r,o);for(var i=e(o),a=l(n),s=a.length,c=0;c<s;c++)a[c].apply(i,r);var u=f[w[n]];return u&&u.push([v,n,r,i]),i}function d(t,e){m[t]=l(t).concat(e)}function l(t){return m[t]||[]}function p(t){return u[t]=u[t]||o(n)}function h(t,e){c(t,function(t,n){e=e||"feature",w[n]=e,e in f||(f[e]=[])})}var m={},w={},v={on:d,emit:n,get:p,listeners:l,context:e,buffer:h};return v}function i(){return new r}var a="nr@context",s=t("gos"),c=t(15),f={},u={},d=e.exports=o();d.backlog=f},{}],gos:[function(t,e,n){function r(t,e,n){if(o.call(t,e))return t[e];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return t[e]=r,r}var o=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(t,e,n){function r(t,e,n,r){o.buffer([t],r),o.emit(t,e,n)}var o=t("ee").get("handle");e.exports=r,r.ee=o},{}],id:[function(t,e,n){function r(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:a(t,i,function(){return o++})}var o=1,i="nr@id",a=t("gos");e.exports=r},{}],loader:[function(t,e,n){function r(){if(!g++){var t=y.info=NREUM.info,e=u.getElementsByTagName("script")[0];if(t&&t.licenseKey&&t.applicationID&&e){c(w,function(e,n){t[e]||(t[e]=n)});var n="https"===m.split(":")[0]||t.sslForHttp;y.proto=n?"https://":"http://",s("mark",["onload",a()],null,"api");var r=u.createElement("script");r.src=y.proto+t.agent,e.parentNode.insertBefore(r,e)}}}function o(){"complete"===u.readyState&&i()}function i(){s("mark",["domContent",a()],null,"api")}function a(){return(new Date).getTime()}var s=t("handle"),c=t(15),f=window,u=f.document,d="addEventListener",l="attachEvent",p=f.XMLHttpRequest,h=p&&p.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:p,REQ:f.Request,EV:f.Event,PR:f.Promise,MO:f.MutationObserver},t(12);var m=""+location,w={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-974.min.js"},v=p&&h&&h[d]&&!/CriOS/.test(navigator.userAgent),y=e.exports={offset:a(),origin:m,features:{},xhrWrappable:v};u[d]?(u[d]("DOMContentLoaded",i,!1),f[d]("load",r,!1)):(u[l]("onreadystatechange",o),f[l]("onload",r)),s("mark",["firstbyte",a()],null,"api");var g=0},{}]},{},["loader",2,10,4,3]);
                ;NREUM.info={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",licenseKey:"fb1aae77e8",applicationID:"15211593",sa:1}
    </script>

</body>
</html>
HTML;

        return $html;
    }
}
