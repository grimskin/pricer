<?php

namespace Tests\ShopParsingBundle\PageFixtures\OleOlePl;

class SamsungGalaxy
{
    /**
     * @return string
     */
    public static function getUrl()
    {
        return 'https://www.oleole.pl/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml';
    }

    /**
     * @return \DateTime
     */
    public static function getFetchDate(): \DateTime
    {
        return new \DateTime('06.09.2017 11:16 CEST');
    }

    /**
     * @return string
     */
    public static function getHtml()
    {
        $html = <<<'HTML'
<!DOCTYPE html>
<html lang="pl">
	<head>
		
		<!-- area: top_header_gwo, elements:0 , column: 159365921 -->
<!-- end area: top_header_gwo -->
<title>Samsung Galaxy S8 SM-G950 (Orchid Grey), Smartfon  - cena i opinie - OleOle!</title>

	
<meta name="description" content="Smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey) w takiej cenie tylko w OleOle! Poznaj szczegóły, przeczytaj opinie użytkowników i zamów ten produkt już teraz!" />
<meta name="keywords" content="Samsung Galaxy S8 SM-G950 (Orchid Grey)" />
<link rel="canonical" href="https://www.oleole.pl/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml"/>
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.oleole.pl/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml"/>
<meta name="robots" content="all,noodp,noarchive" />
	<meta http-equiv="x-dns-prefetch-control" content="on">
<link rel="dns-prefetch" href="//www.googletagmanager.com/">
<link rel="dns-prefetch" href="//www.googleadservices.com/">
<link rel="dns-prefetch" href="//f00.esfr.pl/">
<link rel="dns-prefetch" href="//f01.esfr.pl/">
<link rel="dns-prefetch" href="//s.ytimg.com/">
<link rel="dns-prefetch" href="//chat.altar.com.pl/">
<link rel="dns-prefetch" href="//ad.doubleclick.net/">
<link rel="dns-prefetch" href="//connect.facebook.net/">
<link rel="dns-prefetch" href="//widget.criteo.com/"><meta name="viewport" content="width=960"/>
<link rel="shortcut icon" href="/img/desktop/ole/favicon.ico"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="Author" content="OleOle.pl" />
<meta name="Reply-to" content="sklep.internetowy@oleole.pl" />
<meta name="Language" content="pl" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />

<meta name="msapplication-config" content="none"/>

<link href="https://f01.osfr.pl/ver-043272b53ad9e82cb52075e0f119f99d-160113/css/desktop/ole/style.css" type="text/css" rel="stylesheet" /><!--[if IE 9]>
	<link href="https://f00.osfr.pl/ver-afaa00bac34e22dd2bacb1b7c748c8ae-160113/css/desktop/ole/ie/style.css" type="text/css" rel="stylesheet" /><link href="https://f01.osfr.pl/ver-b851b5a444f6f68281e10d43b7d2e94c-160113/css/desktop/ole/ie/style-part1.css" type="text/css" rel="stylesheet" /><![endif]-->

<link href="https://f01.osfr.pl/ver-e91e4053ac1da6cb97eb23d6fe61095b-160113/css/desktop/ole/product.css" type="text/css" rel="stylesheet" /><link href="https://f01.osfr.pl/ver-a5791363c3b66fb8dd37de92fed4259b-160113/css/ole/jquery.plugins.css" type="text/css" rel="stylesheet" /><link href="https://f00.osfr.pl/ver-a1308ba82c591f761b0b11c4a75011cc-160113/css/ole/print.css" type="text/css" rel="stylesheet" media="print" /><script type="text/javascript" src="https://f00.osfr.pl/ver-5fd1cd281e0656baaf2cc4626090d2f8-160113/javascript/desktop/ole/vendor.js" ></script><script type="text/javascript" src="https://f01.osfr.pl/ver-7bf2fe9f800b9ae19ddd65d125a6ba59-160113/javascript/euro/yt.iframe.api.js" ></script><script type="text/javascript" src="https://f01.osfr.pl/ver-e4af03ec9d4fcaffcdedc4ad5a865a04-160113/javascript/desktop/ole/app.js" ></script><script type="text/javascript" src="https://f01.osfr.pl/ver-e61e75112041d943280d8377d0a2cc8f-160113/javascript/desktop/ole/product.js" ></script><script type="text/javascript">
	app.setParam({
		UA: 'UA-799761-5',
		emailReg : /^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/i,
		hostname : "https://www.oleole.pl",
		activeCategory : 14424762,
		cookieDomain : ".oleole.pl",
		lastViewedProductId : 18063925297,
		
		autocomplete:{
			enabled: true,
			historySuggestions:2,
			maxSuggestions:10,
			charactersInQueryLimit:30
		}
	});
</script>
<script type="text/javascript" src="/ver-c7799e42c108b4c327b048de93056acc-160113/javascript/hotfixes-js.ltr" ></script><link href="/ver-c7799e42c108b4c327b048de93056acc-160113/css/hotfixes-css.ltr" type="text/css" rel="stylesheet" /><script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Organization",
    "url": "https://www.oleole.pl",
    "logo": "https://www.oleole.pl/si_upload/unity/ole/img/logo-oleole.png",
    "contactPoint": [{
        "@type": "ContactPoint",
        "telephone": "+48812812812",
        "contactType": "customer service"
    }]
}
</script>
<script src="//f01.osfr.pl/si_upload/unity/ole/files/ab_ole_v1.4.js?ver=b" type="text/javascript"></script>

<script type="text/javascript">
// uzgodnienie domen kodów adsense i GA (MTU)
google_analytics_domain_name = "none";
_as_codes = {
    /* All-Listing-160x600-prawa-kolumna */
    0: {
        google_ad_client: "ca-pub-0943109351571461",
        google_ad_slot: "5627033233",
        google_ad_width: 160,
        google_ad_height: 600
    },
    /* All-Listing-468x60-stopka */
    1: {
        google_ad_client: "ca-pub-0943109351571461",
        google_ad_slot: "7103766437",
        google_ad_width: 468,
        google_ad_height: 60
    },
    /* All-Karta-produktu-160x600-lewa-kolumna */
    2: {
        google_ad_client: "ca-pub-0943109351571461",
        google_ad_slot: "8580499632",
        google_ad_width: 160,
        google_ad_height: 600
    },
    /* All-Karta-produktu-728x90-stopka */
    3: {
        google_ad_client: "ca-pub-0943109351571461",
        google_ad_slot: "1057232831",
        google_ad_width: 728,
        google_ad_height: 90
    }
};
</script>
<script type="text/javascript">
		var isPrint='false';
		var lastPage=$.cookie('lastPage');
		$(document).ready(function(){
			start('false');
		});
	</script>
	
		<!-- area: header_gwo, elements:0 , column: 79022917 -->
<!-- end area: header_gwo -->
</head>
<body id="product-card">
	<!-- first-body-script -->

<noscript>
		<iframe src="//www.googletagmanager.com/ns.html?id=GTM-8ZSQ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
	</noscript>
	<script>
		(function (w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
			var f = d.getElementsByTagName(s)[0],
			j = d.createElement(s),
			dl = l != 'dataLayer'
			? '&l=' + l
			: '';
			j.async = true;
			j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-8ZSQ');
	</script>
<script type="text/javascript">
	</script>
<script>
    function asyncFunc (fn) {
        setTimeout(fn, 20);
    }

    function executeWhen (test, doThen) {
        asyncFunc(function () {
            if ( test() ) {
                doThen();
            } else {
                asyncFunc(arguments.callee);
            }
        });
    }


</script>
  


<script></script>
<div id="top">
	<div id="top-bar">
		</div>
	
		<!-- area: menu_footer, elements:0 , column: 12149821890 -->
<!-- end area: menu_footer -->
<header id="header">
		<button class="menu-button js-toggle-main-menu collapsed">
				<span class="menu-text">
					Menu</span>
			</button>
			<span class="bottom-line"></span>
		
	<nav class="links">
		<ul>
            <li>
				<a rel="nofollow" href="/cms/dostawa.bhtml?sp=e3&link=sg-naglowek" title="Dostawa - dowiedz się jaki masz wybór">Dostawa</a>
			</li>
            <li>
				<a rel="nofollow" href="/cms/raty.bhtml?link=naglowek" title="Raty 0% - dogodne raty w 15 min">Raty</a>
			</li>
          	<li>
				<a rel="nofollow" href="/cms/poo.bhtml?link=naglowek" title="Znajdź punkt odbioru">Punkty odbioru</a>
			</li>
			<li>
				<a rel="nofollow" href="/customer-order-list.bhtml?link=naglowek" title="Status zamówienia">Status Zamówienia</a>
			</li>
			<li>
				<a rel="nofollow" href="/cms/centrum-informacji.bhtml?link=naglowek" title="Centrum Informacji udzieli odpowiedzi na najczęściej zadawane pytania">Centrum informacji</a>
			</li>
           <li class="livechat">
            <div class="livechat-box" style="">
  <a href="#" class="js-livechat" style="display: none;">Czat</a></div> 

          </li>
			<li class="contact">
				<a rel="nofollow" href="/cms/kontakt.bhtml?link=naglowek" title="Kontakt: 812 812 812 (z tel. stacjonarnych i komórkowych)"> Kontakt<span></span>
				</a>
			</li>
		</ul>
	</nav>
	
	<div id="logo">
		<a href="/">
			<img src="//f00.osfr.pl/si_upload/unity/ole/img/logo-oleole.png" alt="Sklep Internetowy OleOle!" title="Strona główna OleOle!">
		</a>
	</div><div id="search-box">
	<div class="ac-container input-wrap">
		<input type="text"
	class=""
	 id="keyword" placeholder="Wpisz nazwę, markę lub cechę produktu..." maxlength="100" autocomplete="off"
/>
<div id="ac-results"></div>
	</div>
	
		<div class="select3-container theme1">
	<div class="select3-selected-item">
		Wczytywanie...</div>
	<div class="select3-option-list select3-list-hidden">
		<div class="select3-option-list-wrap">
			<div data-value="0" class="select3-option
	
">
	wszystkie</div>
<div data-value="telefony-i-nawigacja-gps" class="select3-option
	 select3-option-level-0
">
	Telefony i zegarki</div>
<div data-value="telefony-komorkowe" class="select3-option
	 select3-option-selected select3-option-level-1
">
	Telefony i Smartfony</div>
</div>
	</div>
	<div class="select3-selection-icon">
	</div>
	<input
		data-autocomplete=""
		 id="search-category"
		data-js=""
		name="select3name"
		class="select3-input"
		
	/>
</div>
<a href="#" title="Wyszukaj" id="search-button"></a>
</div><div id="cart-box">
			<span></span>
		</div>
		<div id="helpline">
				<div class="helpline-phone">
					812 812 812</div>
				<div class="helpline-hours">
					    
      
    Do 21:00!
    
	<style>
		.js-livechat{display:none !important}
	</style>
   </div>
				<div class="helpline-info">
					<div class="helpline-info-header">Zadzwoń i zamów przez telefon</div>
<div>Podaj numer katalogowy produktu podczas rozmowy z konsultantem.</div>
<div class="helpline-info-hours">Poniedziałek - Sobota 8:00 - 21:00<br />Niedziela 9:00 - 19:00</div>
<div class="helpline-info-phone">812 812 812</div></div>
			</div>
			

	<div id="account-box">
		<input type="hidden" id="userEmail" value="" />
		<input type="hidden" id="userName" value="" />

		<a href="#">
			<span class="user-welcome">
				<span class="user-logged">
					Witaj&nbsp;<span class="js-user-name"></span>
				</span>

				<span class="user-not-logged">
					Witaj, zaloguj się</span>
			</span>
			<span class="user-account">
				Twoje Konto</span>
		</a>
		<div id="account-menu">
			<div class="user-not-logged">
				<a href="/customer-account.bhtml" class="log-in selenium-log-in" title="Zaloguj się">
					Zaloguj się</a>
				<a href="/customer-account.bhtml" class="register" title="Zarejestruj się">
					Zarejestruj się</a>
			</div>
			<ul class="account-links">
				<li>
						<a href="/konto.bhtml">
							Twoje Konto
						</a>
					</li>
					<li>
						<a href="/customer-order-list.bhtml?archival=false">
							Twoje Zakupy
						</a>
					</li>
					<li>
						<a href="/customer-opinions.bhtml">
							Twoje Opinie
						</a>
					</li>
					<li>
						<a href="/konto-sledzone-produkty.bhtml">
							Twój Schowek
						</a>
					</li>
					<li>
						<a href="/konto-kupony-rabatowe.bhtml ">
							Kupony Promocyjne
						</a>
					</li>
				</ul>
			<div class="user-logged">
				<span class="js-user-name"></span>&nbsp;to nie Ty?<a href="/konto-wyloguj.bhtml" title="Wyloguj się" class="selenium-log-out">
					Wyloguj się</a>
			</div>
		</div>
	</div>

	<nav id="main-menu">
		<ul id="category-menu">
			<li id="item-1448166">
					<a href="/audio-wideo.bhtml">
						Telewizory - RTV</a>
				</li>
			<li id="item-1447080">
					<a href="/agd.bhtml">
						AGD</a>
				</li>
			<li id="item-1447783">
					<a href="/agd-do-zabudowy.bhtml">
						AGD do zabudowy</a>
				</li>
			<li id="item-10090531">
					<a href="/agd-male.bhtml">
						AGD małe</a>
				</li>
			<li id="item-11665882">
					<a href="/komputery.bhtml">
						Komputery</a>
				</li>
			<li id="item-27511469">
					<a href="/gry-i-konsole.bhtml">
						Gry i konsole</a>
				</li>
			<li id="item-1447932">
					<a href="/foto-i-kamery.bhtml">
						Foto i kamery</a>
				</li>
			<li id="item-14424762">
					<a href="/telefony-i-nawigacja-gps.bhtml">
						Telefony i zegarki</a>
				</li>
			<li id="item-4824990173">
					<a href="/sport.bhtml">
						Sport</a>
				</li>
			<li id="item-7362062503">
					<a href="/narzedzia-i-elektronarzedzia.bhtml">
						Narzędzia</a>
				</li>
			<li id="item-14789651464">
					<a href="/lazienka.bhtml">
						Łazienka</a>
				</li>
			</ul>
		<ul id="subcategory-menu">
		<li>
				<ul>
					<li>
	<ul>
		<li class="node-name">
			<a title="Telewizory OLED, LED, LCD, 3D i 4K UHD" href="/telewizory-led-lcd-plazmowe.bhtml">
				Telewizory
			</a>
			<ul>
				<li>
					<a title="Telewizory LED" href="/telewizory-led-lcd-plazmowe,h1.bhtml">
						Telewizory LED
					</a>
				</li>
				<li>
					<a title="Telewizory 3D" href="/telewizory-led-lcd-plazmowe,j1:2.bhtml">
						Telewizory 3D
					</a>
				</li>
				<li>
					<a title="Telewizory 4K UHD" href="/telewizory-led-lcd-plazmowe,i1.bhtml">
						Telewizory 4K UHD
					</a>
				</li>
				<li>
					<a title="Telewizory Samsung" href="/telewizory-led-lcd-plazmowe,_Samsung.bhtml">
						Telewizory Samsung
					</a>
				</li>
				<li>
					<a title="Telewizory LG" href="/telewizory-led-lcd-plazmowe,_LG.bhtml">
						Telewizory LG
					</a>
				</li>
				<li>
					<a title="Telewizory Panasonic" href="/telewizory-led-lcd-plazmowe,_Panasonic.bhtml">
						Telewizory Panasonic
					</a>
				</li>
				<li>
					<a title="Telewizory Philips" href="/telewizory-led-lcd-plazmowe,_Philips.bhtml">
						Telewizory Philips
					</a>
				</li>
				<li>
					<a href="/telewizory-led-lcd-plazmowe.bhtml" class="more">
						Zobacz więcej
					</a>
				</li>
			</ul>
		</li>
		<li class="node-name">
			<a title="Blu-ray, DVD, nagrywanie" href="/dvd.bhtml">
				Blu-ray, DVD, nagrywanie
			</a>
			<ul>
				<li>
					<a title="Odtwarzacze Blu-ray" href="/odtwarzacze-blu-ray.bhtml">
						Odtwarzacze Blu-ray
					</a>
				</li>
				<li>
					<a title="Odtwarzacze DVD" href="/odtwarzacze-dvd.bhtml">
						Odtwarzacze DVD
					</a>
				</li>
				<li>
					<a title="Odtwarzacze multimedialne" href="/odtwarzacze-multimedialne.bhtml">
						Odtwarzacze multimedialne
					</a>
				</li>
			</ul>
		</li>
		<li class="node-name">
			<a title="Drobny sprzęt RTV" href="/drobny-sprzet-audio-wideo.bhtml">
				Drobny sprzęt RTV
			</a>
			<ul>
				<li>
					<a title="Radioodtwarzacze" href="/radioodtwarzacze-cd.bhtml">
						Radioodtwarzacze
					</a>
				</li>
				<li>
					<a title="Radioodbiorniki" href="/radioodbiorniki.bhtml">
						Radioodbiorniki
					</a>
				</li>
				<li>
					<a title="Radiobudziki" href="/radiobudziki.bhtml">
						Radiobudziki
					</a>
				</li>
			</ul>
		</li>
		<li class="node-name">
			<a title="Głośniki przenośne" href="/glosniki-przenosne.bhtml">
				Głośniki przenośne
			</a>
		</li>
	</ul>
</li>
<li>
	<ul>
		<li class="node-name">
			<a title="Kino domowe" href="/kino-domowe.bhtml">
				Kino domowe
			</a>
			<ul>
				<li>
					<a title="Zestawy kina domowego" href="/zestawy-kina-domowego1.bhtml">
						Zestawy kina domowego
					</a>
				</li>
				<li>
					<a title="Soundbar i speakerbar" href="/soundbar-speakerbar.bhtml">
						Soundbar i speakerbar
					</a>
				</li>
				<li>
					<a title="Amplitunery kiina domowego" href="/amplitunery.bhtml">
						Amplitunery kina domowego
					</a>
				</li>
				<li>
					<a title="Subwoofery" href="/subwoofery.bhtml">
						Subwoofery
					</a>
				</li>
				<li>
					<a title="Kolumny kina domowego" href="/zestawy-kolumn.bhtml">
						Kolumny kina domowego
					</a>
				</li>
<li>
					<a href="/kino-domowe.bhtml" class="more">
						Zobacz więcej
					</a>
				</li>

			</ul>
		</li>
		<li class="node-name">
			<a title="Projektory multimedialne" href="/projektory-multimedialne.bhtml">
				Projektory multimedialne
			</a>
		</li>
		<li class="node-name">
			<a title="HiFi Audio" href="/hifi-audio.bhtml">
				HiFi Audio
			</a>
			<ul>
				<li>
					<a title="Mikro i mini wieże" href="/zestawy-muzyczne.bhtml">
						Mikro i mini wieże
					</a>
				</li>
				<li>
					<a title="Gramofony" href="/gramofony.bhtml">
						Gramofony
					</a>
				</li>
				<li>
					<a title="Odtwarzacze CD i sieciowe" href="/odtwarzacze-cd.bhtml">
						Odtwarzacze CD i sieciowe
					</a>
				</li>
				<li>
					<a title="Systemy Audio, AirPlay i Bluetooth" href="/stacje-dokujace-ipod-iphone.bhtml">
						Systemy Audio, AirPlay i Bluetooth
					</a>
				</li>
				<li>
					<a title="Kolumny audio" href="/kolumny-audio.bhtml">
						Kolumny audio
					</a>
				</li>
				<li>
					<a title="Wzmacniacze" href="/wzmacniacze.bhtml">
						Wzmacniacze
					</a>
				</li>
				<li>
					<a title="Odtwarzacze MP3 i MP4" href="/odtwarzacze-mp3-mp4.bhtml">
						Odtwarzacze MP3 i MP4
					</a>
				</li>
				<li>
					<a title="Systemy multiroom" href="/systemy-muzyczne.bhtml">
						Systemy multiroom
					</a>
				</li>
			</ul>
		</li>
	</ul>
</li>
<li>
	<ul>
		<li class="node-name">
			<a title="Car Audio" href="/car-audio.bhtml">
				Car Audio
			</a>
			<ul>
				<li>
					<a title="Radioodtwarzacze samochodowe" href="/radioodtwarzacze-samochodowe.bhtml">
						Radioodtwarzacze samochodowe
					</a>
				</li>
				<li>
					<a title="Głośniki samochodowe" href="/glosniki-samochodowe.bhtml">
						Głośniki samochodowe
					</a>
				</li>
				<li>
					<a title="Wzmacniacze samochodowe" href="/wzmacniacze-samochodowe.bhtml">
						Wzmacniacze samochodowe
					</a>
				</li>
				<li>
					<a title="CB radia" href="/radia-cb.bhtml">
						CB radia
					</a>
				</li>
				<li>
					<a title="Anteny CB" href="/anteny-cb.bhtml">
						Anteny CB
					</a>
				</li>
			</ul>
		</li>
		<li class="node-name">
			<a title="Tunery DVB & TV SAT" href="/telewizja-satelitarna.bhtml">
				Tunery DVB & TV SAT
			</a>
			<ul>
				<li>
					<a title="Tunery DVB-T" href="/tunery-dvb-t.bhtml">
						Tunery DVB-T
					</a>
				</li>
				<li>
					<a title="Tunery telewizji satelitarnej" href="/tunery-telewizji-satelitarnej.bhtml">
						Tunery telewizji satelitarnej
					</a>
				</li>
				<li>
					<a title="Acesoria TV SAT" href="/akcesoria-do-tunerow-satelitarnych.bhtml">
						Acesoria TV SAT
					</a>
				</li>
				<li>
					<a title="Anteny" href="/anteny.bhtml">
						Anteny
					</a>
				</li>
			</ul>
		</li>
		<li class="node-name">
			<a title="Akcesoria" href="/akcesoria-audio-wideo1.bhtml">
				Akcesoria
			</a>
			<ul>
				<li>
					<a title="Uchwyty do telewizorów" href="/uchwyty-do-telewizorow.bhtml">
						Uchwyty do telewizorów
					</a>
				</li>
				<li>
					<a title="Okulary 3D" href="/okulary-3d.bhtml">
						Okulary 3D
					</a>
				</li>
				<li>
					<a title="Słuchawki" href="/sluchawki.bhtml">
						Słuchawki
					</a>
				</li>
				<li>
					<a title="Słuchawki bezprzewodowe" href="/sluchawki.bhtml">
						Słuchawki bezprzewodowe
					</a>
				</li>
				<li>
					<a title="Kable" href="/kable-audio-wideo.bhtml">
						Kable Audi-Wideo
					</a>
				</li>
				<li>
					<a href="/akcesoria-audio-wideo1.bhtml" class="more">
						Zobacz więcej
					</a>
				</li>
			</ul>
		</li>
	</ul>
</li><li>
						<ul>
							<li class="category-name">
								<a href="/audio-wideo.bhtml">
										Telewizory - RTV</a>
							</li>
							<li>
								<a href="/audio-wideo.bhtml">
									Wszystkie produkty</a>
							</li>
							<li>
									<!-- area: macro, elements:2 , column: 7014100853 -->
<div class="banners banners-macro">
	<!-- banner id: 17055974624 -->
		<div data-id="17055974624-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17055974624-1',
			date: '2017-08-29',
			type: 'B',
			position: '1',
			name: '188x80 Wniesiemy Twój sprzęt za złotówkę (05.09\-11.09)',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/wniesiemy-twoj-sprzet-za-zlotowke.bhtml?link=188x80">
								<img alt="Wniesiemy Tw&oacute;j sprz&#281;t za z&#322;ot&oacute;wk&#281; " class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-064492f3839a762f5b9ad7fff2b372e2/banner/17055974624/f_188x80_wniesiemy_sprzet_za_1_zl.png" /></a>
						</div>
	<!-- banner id: 12613304651 -->
		<div data-id="12613304651-2" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '12613304651-2',
			date: '2016-05-06',
			type: 'B',
			position: '2',
			name: '188x80 Bestsellery TV',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/bestsellery-tv.bhtml?link=188x80">
								<img alt="Bestsellery TV " class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f00.osfr.pl/ver-809069ffa38bf7da03a51e57d3b6ff2a/banner/12613304651/188x80_OLE_75Kb-best-tv.jpg" /></a>
						</div>
	</div>
<!-- end area: macro -->
</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii Telewizory - RTV" href="/audio-wideo.bhtml">
							Zobacz wszystkie produkty z kategorii Telewizory - RTV</a>
					</div>
				</div>
			</li>
		<li>
				<ul>
					<li>
		<ul>
			<li class="node-name">
				<a title="Pralki i suszarki" href="/pralki-i-suszarki.bhtml">
					Pralki i suszarki
				</a>
				<ul>
					<li>
						<a title="Pralki" href="/pralki.bhtml">
							Pralki
						</a>
					</li>
					<li>
						<a title="Pralki Bosch" href="/pralki,_Bosch.bhtml">
							Pralki Bosch
						</a>
					</li>
					<li>
						<a title="Pralki Samsung" href="/pralki,_Samsung.bhtml">
							Pralki Samsung
						</a>
					</li>
					<li>
						<a title="Pralki Amica" href="/pralki,_Amica.bhtml">
							Pralki Amica
						</a>
					</li>
					<li>
						<a title="Pralki Electrolux" href="/pralki,_Electrolux.bhtml">
							Pralki Electrolux
						</a>
					</li>
					<li>
						<a title="Suszarki do ubrań" href="/suszarki.bhtml">
							Suszarki do ubrań
						</a>
					</li>
					<li>
						<a title="Pralko-suszarki" href="/pralko-suszarki.bhtml">
							Pralko-suszarki
						</a>
					</li>
					<li>
						<a class="more" href="/pralki-i-suszarki.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a class="eT_nl" title="Zmywarki" href="/zmywarki.bhtml">
					Zmywarki
				</a>
				<ul>
					<li>
						<a title="Zmywarki Bosch" href="/zmywarki,_Bosch.bhtml">
							Zmywarki Bosch
						</a>
					</li>
					<li>
						<a title="Zmywarki Electrolux " href="/zmywarki,_Electrolux.bhtml">
							Zmywarki Electrolux
						</a>
					</li>
					<li>
						<a title="Zmywarki Samsung" href="/zmywarki,_Samsung.bhtml">
							Zmywarki Samsung
						</a>
					</li>
					<li>
						<a title="Zmywarki Whirlpool" href="/zmywarki,_Whirlpool.bhtml">
							Zmywarki Whirlpool
						</a>
					</li>
					<li>
						<a class="more" href="/zmywarki.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a class="eT_nl" title="Lodówki i zamrażarki" href="/lodowki-i-zamrazarki.bhtml">
					Lodówki i zamrażarki
				</a>
				<ul>
					<li>
						<a title="Lodówki" href="/lodowki.bhtml">
							Lodówki
						</a>
					</li>
					<li>
						<a title="Lodówki Samsung" href="/lodowki,_Samsung.bhtml">
							Lodówki Samsung
						</a>
					</li>
					<li>
						<a title="Lodówki Bosch" href="/lodowki,_Bosch.bhtml">
							Lodówki Bosch
						</a>
					</li>
					<li>
						<a title="Lodówki Electrolux" href="/lodowki,_Electrolux.bhtml">
							Lodówki Electrolux
						</a>
					</li>
					<li>
						<a title="Lodówki Amica" href="/lodowki,_Amica.bhtml">
							Lodówki Amica
						</a>
					</li>
					<li>
						<a title="Zamrażarki" href="/zamrazarki.bhtml">
							Zamrażarki
						</a>
					</li>
					<li>
						<a class="more" href="/lodowki-i-zamrazarki.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Okapy kuchenne" href="/okapy.bhtml">
					Okapy kuchenne
				</a>
			</li>
			<li class="node-name">
				<a title="Kuchenki mikrofalowe" href="/kuchenki-mikrofalowe.bhtml">
					Kuchenki mikrofalowe
				</a>
			</li>
			<li class="node-name">
				<a title="Armatura kuchenna" href="/armatura-kuchenna.bhtml">
					Armatura kuchenna
				</a>
				<ul>
					<li>
						<a title="Zlewozmywaki" href="/zlewozmywaki1.bhtml">
							Zlewozmywaki
						</a>
					</li>
					<li>
						<a title="Baterie kuchenne" href="/baterie-kuchenne1.bhtml">
							Baterie kuchenne
						</a>
					</li>
					<li>
						<a class="more" href="/armatura-kuchenna.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
		 </ul>
	 </li>
	 <li>
		 <ul>
			<li class="node-name">
				<a title="Kuchnie wolnostojące" href="/kuchnie-wolnostojace.bhtml">
					Kuchnie wolnostojące
				</a>

				<ul>
					<li>
						<a title="Kuchenki gazowe" href="/kuchnie,h2.bhtml">
							Kuchenki gazowe
						</a>
					</li>
					<li>
						<a title="Kuchenki elektryczne" href="/kuchnie,h1.bhtml">
							Kuchenki elektryczne
						</a>
					</li>
					<li>
						<a title="Kuchenki indukcyjne" href="/kuchnie,h4.bhtml">
							Kuchenki indukcyjne
						</a>
					</li>
					<li>
						<a class="more" href="/kuchnie-wolnostojace.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Akcesoria AGD" href="/akcesoria-agd1.bhtml">
					Akcesoria AGD
				</a>

				<ul>
					<li>
						<a title="Węże do gazu" href="/weze-do-gazu.bhtml">
							Węże do gazu
						</a>
					</li>
					<li>
						<a title="Filtry do okapów" href="/filtry-do-okapow.bhtml">
							Filtry do okapów
						</a>
					</li>
					<li>
						<a title="Produkty do zmywarek" href="/produkty-do-zmywarek.bhtml">
							Produkty do zmywarek
						</a>
					</li>
					<li>
						<a class="more" href="/akcesoria-agd1.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li><li>
						<ul>
							<li class="category-name">
								<a href="/agd.bhtml">
										AGD</a>
							</li>
							<li>
								<a href="/agd.bhtml">
									Wszystkie produkty</a>
							</li>
							<li>
									<!-- area: macro, elements:4 , column: 4227402453 -->
<div class="banners banners-macro">
	<!-- banner id: 17055974624 -->
		<div data-id="17055974624-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17055974624-1',
			date: '2017-08-29',
			type: 'B',
			position: '1',
			name: '188x80 Wniesiemy Twój sprzęt za złotówkę (05.09\-11.09)',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/wniesiemy-twoj-sprzet-za-zlotowke.bhtml?link=188x80">
								<img alt="Wniesiemy Tw&oacute;j sprz&#281;t za z&#322;ot&oacute;wk&#281; " class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-064492f3839a762f5b9ad7fff2b372e2/banner/17055974624/f_188x80_wniesiemy_sprzet_za_1_zl.png" /></a>
						</div>
	<!-- banner id: 12613273102 -->
		<div data-id="12613273102-2" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '12613273102-2',
			date: '2016-05-06',
			type: 'B',
			position: '2',
			name: '188x80 Bestsellery lodówki',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/bestsellery-lodowki.bhtml?link=188x80">
								<img alt="Bestsellery lod&oacute;wki 188x80" class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-c2c0d67129ceff9865209d0739d0bd77/banner/12613273102/188x80_OLE_75Kb-best-lod.jpg" /></a>
						</div>
	<!-- banner id: 12613276749 -->
		<div data-id="12613276749-3" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '12613276749-3',
			date: '2016-05-06',
			type: 'B',
			position: '3',
			name: '188x80 Bestsellery pralki',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/bestsellery-pralki.bhtml?link=188x80">
								<img alt="Bestsellery pralki 188x80" class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-edccf0ce7f02f345bc4b581f18b0a113/banner/12613276749/188x80_OLE_75Kb-best-pralk.jpg" /></a>
						</div>
	<!-- banner id: 12613267271 -->
		<div data-id="12613267271-4" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '12613267271-4',
			date: '2016-05-06',
			type: 'B',
			position: '4',
			name: '188x80 Bestsellery Kuchnie',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/bestsellery-kuchnie.bhtml?link=188x80">
								<img alt="Bestsellery kuchnie 188x80" class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-75244751efaf11b94fe5735f74658e74/banner/12613267271/188x80_OLE_75Kb-best-kuch.jpg" /></a>
						</div>
	</div>
<!-- end area: macro -->
</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<div class="producers-list">
								<b>Popularni producenci:</b>
	<a href="/cms/sis-miele.bhtml?link=logo-menu"><span class="sisMiele"></span></a></div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii AGD" href="/agd.bhtml">
							Zobacz wszystkie produkty z kategorii AGD</a>
					</div>
				</div>
			</li>
		<li>
				<ul>
					<li>
		<ul>
			<li class="node-name">
				<a title="Zmywarki do zabudowy" href="/zmywarki-do-zabudowy.bhtml">
					Zmywarki do zabudowy
				</a>

				<ul>
					<li>
						<a title="Zmywarki do zabudowy Bosch" href="/zmywarki-do-zabudowy,_Bosch.bhtml">
							Zmywarki do zabudowy Bosch
						</a>
					</li>
					<li>
						<a title="Zmywarki do zabudowy Amica" href="/zmywarki-do-zabudowy,_Amica.bhtml">
							Zmywarki do zabudowy Amica
						</a>
					</li>
					<li>
						<a title="Zmywarki do zabudowy Electrolux" href="/zmywarki-do-zabudowy,_Electrolux.bhtml">
							Zmywarki do zabudowy Electrolux
						</a>
					</li>
					<li>
						<a class="more" href="/zmywarki-do-zabudowy.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Lodówki i zamrażarki do zabudowy" href="/lodowki-i-zamrazarki-do-zabudowy.bhtml">
					Lodówki i zamrażarki do zabudowy
				</a>

				<ul>
					<li>
						<a title="Lodówki do zabudowy" href="/lodowki-do-zabudowy.bhtml">
							Lodówki do zabudowy
						</a>
					</li>
					<li>
						<a title="Zamrażarki do zabudowy Electrolux" href="/lodowki-do-zabudowy,_Electrolux.bhtml">
							Lodówki do zabudowy Electrolux
						</a>
					</li>
					<li>
						<a title="Lodówki do zabudowy Bosch" href="/lodowki-do-zabudowy,_Bosch.bhtml">
							Lodówki do zabudowy Bosch
						</a>
					</li>
					<li>
						<a title="Zamrażarki do zabudowy" href="/zamrazarki-do-zabudowy.bhtml">
							Zamrażarki do zabudowy
						</a>
					</li>
					<li>
						<a class="more" href="/lodowki-i-zamrazarki-do-zabudowy.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Piekarniki do zabudowy" href="/piekarniki-do-zabudowy.bhtml">
					Piekarniki do zabudowy
				</a>

				<ul>
					<li>
						<a title="Piekarniki do zabudowy Amica" href="/piekarniki-do-zabudowy,_Amica.bhtml">
							Piekarniki do zabudowy Amica
						</a>
					</li>
					<li>
						<a title="Piekarniki do zabudowy Bosch" href="/piekarniki-do-zabudowy,_Bosch.bhtml">
							Piekarniki do zabudowy Bosch
						</a>
					</li>
					<li>
						<a title="Piekarniki do zabudowy Electrolux" href="/piekarniki-do-zabudowy,_Electrolux.bhtml">
							Piekarniki do zabudowy Electrolux
						</a>
					</li>
					<li>
						<a class="more" href="/piekarniki-do-zabudowy.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Płyty do zabudowy" href="/plyty-do-zabudowy.bhtml">
					Płyty do zabudowy
				</a>

				<ul>
					<li>
						<a title="Płyty indukcyjne" href="/plyty-do-zabudowy,h1.bhtml">
							Płyty indukcyjne
						</a>
					</li>
					<li>
						<a title="Płyty ceramiczne" href="/plyty-do-zabudowy,h2.bhtml">
							Płyty ceramiczne
						</a>
					</li>
					<li>
						<a title="Płyty gazowe" href="/plyty-do-zabudowy,h3.bhtml">
							Płyty gazowe
						</a>
					</li>
					<li>
						<a class="more" href="/plyty-do-zabudowy.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Kuchnie do zabudowy" href="/kuchnie-do-zabudowy.bhtml">
					Kuchnie do zabudowy
				</a>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Kuchenki mikrofalowe do zabudowy" href="/kuchenki-mikrofalowe-do-zabudowy.bhtml">
					Kuchenki mikrofalowe do zabudowy
				</a>
			</li>
			<li class="node-name">
				<a title="Okapy kuchenne" href="/okapy-do-zabudowy.bhtml">
					Okapy kuchenne
				</a>
			</li>
			<li class="node-name">
			  <a title="Pralki do zabudowy" href="/pralki-do-zabudowy.bhtml">
				  Pralki do zabudowy
			  </a>
			</li>
			<li class="node-name">
				<a title="Ekspresy do zabudowy" href="/ekspresy-do-zabudowy.bhtml">
					Ekspresy do zabudowy
				</a>
			</li>
			<li class="node-name">
				<a title="Armatura kuchenna" href="/armatura-kuchenna.bhtml">
					Armatura kuchenna
				</a>

				<ul>
					<li>
						<a title="Zlewozmywaki" href="/zlewozmywaki.bhtml">
							Zlewozmywaki
						</a>
					</li>
					<li>
						<a title="Baterie kuchenne" href="/baterie-kuchenne.bhtml">
							Baterie kuchenne
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li><li>
						<ul>
							<li class="category-name">
								<a href="/agd-do-zabudowy.bhtml">
										AGD do zabudowy</a>
							</li>
							<li>
								<a href="/agd-do-zabudowy.bhtml">
									Wszystkie produkty</a>
							</li>
							<li>
									<!-- area: macro, elements:3 , column: 4304693698 -->
<div class="banners banners-macro">
	<!-- banner id: 17055974624 -->
		<div data-id="17055974624-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17055974624-1',
			date: '2017-08-29',
			type: 'B',
			position: '1',
			name: '188x80 Wniesiemy Twój sprzęt za złotówkę (05.09\-11.09)',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/wniesiemy-twoj-sprzet-za-zlotowke.bhtml?link=188x80">
								<img alt="Wniesiemy Tw&oacute;j sprz&#281;t za z&#322;ot&oacute;wk&#281; " class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-064492f3839a762f5b9ad7fff2b372e2/banner/17055974624/f_188x80_wniesiemy_sprzet_za_1_zl.png" /></a>
						</div>
	<!-- banner id: 12613283546 -->
		<div data-id="12613283546-2" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '12613283546-2',
			date: '2016-05-06',
			type: 'B',
			position: '2',
			name: '188x80 Bestsellery piekarniki do zabudowy',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/bestsellery-piekarniki.bhtml?link=188x80">
								<img alt="Bestsellery piekarniki do zabudowy 188x80" class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-a97f5ca46507b2128a0dc0a6b5d67471/banner/12613283546/188x80_OLE_75Kb-best-piek.jpg" /></a>
						</div>
	<!-- banner id: 12613299387 -->
		<div data-id="12613299387-3" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '12613299387-3',
			date: '2016-05-06',
			type: 'B',
			position: '3',
			name: '188x80 Bestsellery zmywarki do zabudowy',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/bestsellery-zmywarki.bhtml?link=188x80">
								<img alt="Bestsellery zmywarki do zabudowy 188x80" class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-17b1d830dce1f0da8b230626d7f5ce34/banner/12613299387/188x80_OLE_75Kb-best-zmyw.jpg" /></a>
						</div>
	</div>
<!-- end area: macro -->
</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<div class="producers-list">
								<b>Popularni producenci:</b>
	<a href="/cms/sis-miele.bhtml?link=logo-menu"><span class="sisMiele"></span></a></div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii AGD do zabudowy" href="/agd-do-zabudowy.bhtml">
							Zobacz wszystkie produkty z kategorii AGD do zabudowy</a>
					</div>
				</div>
			</li>
		<li>
				<ul>
					<li>
		<ul>
			<li class="node-name">
				<a title="AGD do domu" href="/do-domu.bhtml">
					AGD do domu
				</a>
				<ul>
					<li>
						<a title="Odkurzacze" href="/odkurzacze.bhtml">
							Odkurzacze
						</a>
					</li>
					<li>
						<a title="Odkurzacze automatyczne" href="/odkurzacze-automatyczne.bhtml">
							Odkurzacze automatyczne
						</a>
					</li>
					<li>
						<a title="Żelazka" href="/zelazka.bhtml">
							Żelazka
						</a>
					</li>
					<li>
						<a title="Żelazka systemowe" href="/zelazka-systemowe.bhtml">
							Żelazka systemowe
						</a>
					</li>
					<li>
						<a title="Maszyny do szycia" href="/maszyny-do-szycia.bhtml">
							Maszyny do szycia
						</a>
					</li>
					<li>
						<a title="Parownice" href="/czysciki-parowe.bhtml">
							Parownice
						</a>
					</li>
					<li>
						<a href="/do-domu.bhtml" class="more">Zobacz więcej</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="AGD do higieny i pielęgnacji" href="/do-higieny-i-pielegnacji.bhtml">
					AGD do higieny i pielęgnacji
				</a>
				<ul>
					<li>
						<a title="Depilatory" href="/depilatory.bhtml">
							Depilatory
						</a>
					</li>
					<li>
						<a title="Golarki męskie" href="/golarki.bhtml">
							Golarki męskie
						</a>
					</li>
					<li>
						<a title="Szczoteczki elektryczne" href="/szczoteczki-elektryczne.bhtml">
							Szczoteczki elektryczne
						</a>
					</li>
					<li>
						<a title="Lokówki" href="/lokowki.bhtml">
							Lokówki
						</a>
					</li>
					<li>
						<a title="Suszarki do włosów" href="/suszarki-do-wlosow.bhtml">
							Suszarki do włosów
						</a>
					</li>
					<li>
						<a title="Prostownice i karbownice" href="/prostownice-i-karbownice.bhtml">
							Prostownice i karbownice
						</a>
					</li>
					<li>
						<a title="Maszynki do strzyżenia" href="/maszynki-do-strzyzenia.bhtml">
							Maszynki do strzyżenia
						</a>
					</li>
					<li>
						<a href="/do-higieny-i-pielegnacji.bhtml" class="more">Zobacz więcej</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="AGD do kuchni" href="/do-kuchni.bhtml">
					AGD do kuchni
				</a>
				<ul>
					<li>
						<a title="Ekspresy ciśnieniowe" href="/ekspresy-cisnieniowe.bhtml">
							Ekspresy ciśnieniowe
						</a>
					</li>
					<li>
						<a title="Ekspresy Krups" href="/ekspresy-cisnieniowe,d6,_Krups.bhtml?link=menu_Krups">
							Ekspresy Krups
						</a>
					</li>
					<li>
						<a title="Ekspresy Saeco" href="/ekspresy-cisnieniowe,_Saeco.bhtml?link=menu_Saeco">
							Ekspresy Saeco
						</a>
					</li>
					<li>
						<a title="Czajniki elektryczne" href="/czajniki.bhtml">
							Czajniki elektryczne
						</a>
					</li>
					<li>
						<a title="Roboty kuchenne" href="/roboty-wieloczynnosciowe.bhtml">
							Roboty kuchenne
						</a>
					</li>
					<li>
						<a title="Blendery" href="/blendery.bhtml">
							Blendery
						</a>
					</li>
					<li>
						<a title="Blendery kielichowe" href="/miksery-kielichowe.bhtml">
							Blendery kielichowe
						</a>
					</li>
					<li>
						<a title="Miksery ręczne" href="/miksery-reczne.bhtml">
							Miksery ręczne
						</a>
					</li>
					<li>
						<a title="Sokowirówki" href="/sokowirowki.bhtml">
							Sokowirówki
						</a>
					</li>
					<li>
						<a title="Wyciskarki wolnoobrotowe" href="/wyciskarki-wolnoobrotowe.bhtml">
							Wyciskarki wolnoobrotowe
						</a>
					</li>
					<li>
						<a title="Wyciskarki do cytrysów" href="/wyciskarki-do-cytrusow.bhtml">
							Wyciskarki do cytrysów
						</a>
					</li>
					<li>
						<a title="Maszynki do mielenia" href="/maszynki-do-mielenia.bhtml">
							Maszynki do mielenia
						</a>
					</li>
					<li>
						<a title="Parowary" href="/parowary.bhtml">
							Parowary
						</a>
					</li>
					<li>
						<a title="Tostery" href="/tostery.bhtml">
							Tostery
						</a>
					</li>
					<li>
						<a title="Grille elektryczne" href="/grille.bhtml">
							Grille elektryczne
						</a>
					</li>
					<li>
						<a href="/do-kuchni.bhtml" class="more">Zobacz więcej</a>
					</li>
				</ul>
            </li>
		</ul>
	</li>
    <li>
		<ul>
			<li class="node-name">
				<a title="Uzdatniacze powietrza" href="/ogrzewanie.bhtml">
					Uzdatniacze powietrza
				</a>

				<ul>
					<li>
						<a title="Oczyszczacze powietrza" href="/oczyszczacze-powietrza.bhtml">
							Oczyszczacze powietrza
						</a>
					</li>
					<li>
						<a title="Nawilżacze powietrza" href="/nawilzacze-1.bhtml">
							Nawilżacze powietrza
						</a>
					</li>
					<li>
						<a title="Grzejniki" href="/grzejniki-1.bhtml">
							Grzejniki
						</a>
					</li>
					<li>
						<a title="Osuszacze powietrza" href="/osuszacze-powietrza-1.bhtml">
							Osuszacze powietrza
						</a>
					</li>
					<li>
						<a title="Termowentylatory" href="/termowentylatory-1.bhtml">
							Termowentylatory
						</a>
					</li>
					<li>
						<a title="Stacje pogody" href="/stacje-pogody-1.bhtml">
							Stacje pogody
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
	            <a title="Akcesoria AGD-małe" href="/akcesoria-agd-male-.bhtml">
	            	Akcesoria AGD-małe
	            </a>
                <ul>

                    <li>
                        <a title="Garnki" href="/garnki-i-czajniki.bhtml">
                       		Garnki
                       	</a>
                    </li>
                    <li>
                        <a title="Patelnie" href="/patelnie1.bhtml">
                       		Patelnie
                       	</a>
                    </li>
                    <li>
	                    <a title="Noże kuchenne" href="/noze-kuchenne2.bhtml">
	                    	Noże kuchenne
	                    </a>
                    </li>
                    <li>
                        <a title="Sztućce" href="/sztucce2.bhtml">
                        	Sztućce
                        </a>
                    </li>
                    <li>
                        <a title="Worki do odkurzaczy" href="/worki-i-filtry-do-odkurzaczy.bhtml">
                        	Worki do odkurzaczy
                        </a>
                    </li>
                    <li>
                        <a title="Końcówki do szczoteczek" href="/koncowki-do-szczoteczek.bhtml">
                       		Końcówki do szczoteczek
                       	</a>
                    </li>
                    <li>
	                    <a title="Folie i ostrza do golarek" href="/folie-i-ostrza-do-golarek.bhtml">
	                    	Folie i ostrza do golarek
	                    </a>
                    </li>
                    <li>
	                    <a title="Akcesoria podróżne" href="/walizki-i-torby-podrozne-ad.bhtml">
	                    	Akcesoria podróżne
	                    </a>
                    </li>
                    <li>
                        <a href="/akcesoria-agd-male-.bhtml" class="more">Zobacz więcej</a>
                    </li>
                </ul>
            </li>
		</ul>
	</li><li>
						<ul>
							<li class="category-name">
								<a href="/agd-male.bhtml">
										AGD małe</a>
							</li>
							<li>
								<a href="/agd-male.bhtml">
									Wszystkie produkty</a>
							</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<div class="producers-list">
								<b>Popularni producenci:</b>
	<a href="/cms/sis-miele.bhtml?link=logo-menu"><span class="sisMiele"></span></a></div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii AGD małe" href="/agd-male.bhtml">
							Zobacz wszystkie produkty z kategorii AGD małe</a>
					</div>
				</div>
			</li>
		<li>
				<ul>
					<li>
		<ul>
			<li class="node-name">
				<a title="Laptopy" href="/laptopy-i-netbooki.bhtml">
					Laptopy
				</a>

				<ul>
					<li>
						<a title="Laptopy Lenovo" href="/laptopy-i-netbooki,_Lenovo.bhtml">
							Laptopy Lenovo
						</a>
					</li>
					<li>
						<a title="Laptopy Asus" href="/laptopy-i-netbooki,_Asus.bhtml">
							Laptopy Asus
						</a>
					</li>
					<li>
						<a class="more" href="/laptopy-i-netbooki.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Tablety" href="/ipad-i-tablety-multimedialne.bhtml">
					Tablety
				</a>

				<ul>
					<li>
						<a title="Tablety Samsung" href="/ipad-i-tablety-multimedialne,_Samsung.bhtml">
							Tablety Samsung
						</a>
					</li>
					<li>
						<a title="Tablety Asus" href="/ipad-i-tablety-multimedialne,_Asus.bhtml">
							Tablety Asus
						</a>
					</li>
					<li>
						<a title="Tablety Lenovo" href="/ipad-i-tablety-multimedialne,_Lenovo.bhtml">
							Tablety Lenovo
						</a>
					</li>
					<li>
						<a title="IPad" href="/ipad-i-tablety-multimedialne,_Apple.bhtml">
							IPad
						</a>
					</li>
					<li>
						<a class="more" href="/ipad-i-tablety-multimedialne.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Czytniki E-booków" href="/czytniki-ebookow.bhtml">
					Czytniki E-booków
				</a>
			</li>
			<li class="node-name">
				<a title="Monitory" href="/monitory-led-i-lcd.bhtml">
					Monitory
				</a>
			</li>
			<li class="node-name">
				<a title="Projektory multimedialne" href="/projektory-multimedialne1.bhtml">
					Projektory multimedialne
				</a>
			</li>
			<li class="node-name">
				<a title="Komputery stacjonarne PC" href="/komputery-stacjonarne-pc.bhtml">
					Komputery stacjonarne PC
				</a>
			</li>
			<li class="node-name">
				<a title="Komputery All-In-One" href="/komputery-all-in-one.bhtml">
					Komputery All-In-One
				</a>
			</li>
			<li class="node-name">
				<a title="Podzespoły komputerowe" href="/podzespoly-komputerowe-pc.bhtml">
					Podzespoły komputerowe
				</a>
			</li>
			<li class="node-name">
				<a title="Oprogramowanie" href="/oprogramowanie.bhtml">
					Oprogramowanie
				</a>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Drukarki i urządzenia biurowe" href="/drukarki-i-urzadzenia-biurowe.bhtml">
					Drukarki i urządzenia biurowe
				</a>
				<ul>
					<li>
						<a title="Urządzenia wielofunkcyjne" href="/urzadzenia-wielofunkcyjne.bhtml">
							Urządzenia wielofunkcyjne
						</a>
					</li>
					<li>
						<a title="Drukarki laserowe" href="/drukarki-laserowe.bhtml">
							Drukarki laserowe
						</a>
					</li>
					<li>
						<a title="Drukarki atramentowe" href="/drukarki-atramentowe.bhtml">
							Drukarki atramentowe
						</a>
					</li>
					<li>
						<a title="Niszczarki" href="/niszczarki.bhtml">
							Niszczarki
						</a>
					</li>
					<li>
						<a href="/drukarki-i-urzadzenia-biurowe.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Tusze i tonery" href="/tusze-i-tonery1.bhtml">
					Tusze i tonery
				</a>
			</li>
			<li class="node-name">
				<a title="Dyski i Pendrive" href="/dyski-pendrive-i-napedy.bhtml">
					Dyski i Pendrive
				</a>
				<ul>
					<li>
						<a title="Dyski twarde zewnętrzne" href="/dyski-twarde-zewnetrzne.bhtml">
							Dyski twarde zewnętrzne
						</a>
					</li>
					<li>
						<a title="Dyski SSD" href="/dyski-wewnetrzne-ssd.bhtml">
							Dyski SSD
						</a>
					</li>
					<li>
						<a title="PenDrive (pamięci USB)" href="/pendrive-pamieci-usb.bhtml">
							PenDrive (pamięci USB)
						</a>
					</li>
					<li>
						<a href="/tusze-i-tonery1.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Urządzenia sieciowe" href="/urzadzenia-sieciowe.bhtml">
					Urządzenia sieciowe
				</a>
				<ul>
					<li>
						<a title="Routery Wi-Fi" href="/routery.bhtml">
							Routery Wi-Fi
						</a>
					</li>
					<li>
						<a title="Modemy" href="/modemy-gsm.bhtml">
							Modemy
						</a>
					</li>
					<li>
						<a title="Dyski sieciowe" href="/dyski-sieciowe1.bhtml">
							Dyski sieciowe
						</a>
					</li>
					<li>
						<a href="/urzadzenia-sieciowe.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Kamery, głośniki i słuchawki" href="/kamery-glosniki-i-sluchawki.bhtml">
					Kamery, głośniki i słuchawki
				</a>

				<ul>
					<li>
						<a title="Głóśniki komputerowe" href="/glosniki-komputerowe.bhtml">
							Głóśniki komputerowe
						</a>
					</li>
					<li>
						<a title="Głóśniki komputerowe" href="/sluchawki-z-mikrofonem.bhtml">
							Słuchawki z mikrofonem
						</a>
					</li>
					<li>
						<a class="more" href="/kamery-glosniki-i-sluchawki.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Akcesoria komputerowe" href="/akcesoria-komputerowe.bhtml">
					Akcesoria komputerowe
				</a>
				<ul>
					<li>
						<a title="Myszy komputerowe" href="/myszy-komputerowe.bhtml">
							Myszy komputerowe
						</a>
					</li>
					<li>
						<a title="Klawiatury komputerowe" href="/klawiatury-komputerowe.bhtml">
							Klawiatury komputerowe
						</a>
					</li>
					<li>
						<a title="Torby do laptopów" href="/torby-do-laptopow.bhtml">
							Torby do laptopów
						</a>
					</li>
					<li>
						<a title="Etui tabletów multimedialnych" href="/etui-do-ipada-i-tabletow-multimedialnych.bhtml">
							Etui tabletów multimedialnych
						</a>
					</li>
					<li>
						<a title="Akcesoria do projektorów multimedialnych" href="/akcesoria-do-projektorow-multimedialnych1.bhtml">
							Akcesoria do projektorów multimedialnych
						</a>
					</li>
					<li>
						<a title="Akcesoria do laptopów" href="/akcesoria-do-laptopow.bhtml">
							Akcesoria do laptopów
						</a>
					</li>
					<li>
						<a title="Akcesoria do dysków twardych" href="/akcesoria-do-dyskow-twardych.bhtml">
							Akcesoria do dysków twardych
						</a>
					</li>
					<li>
						<a title="Akcesoria do tabletów mulimedialnych" href="/akcesoria-do-ipada-i-tabletow-multimedialnych.bhtml">
							Akcesoria do tabletów mulimedialnych
						</a>
					</li>
					<li>
						<a title="Kable i adaptery" href="/kable-i-adaptery-komputerowe.bhtml">
							Kable i adaptery
						</a>
					</li>
					<li>
						<a title="Zasilacze do laptopów" href="/zasilacze-do-laptopow.bhtml">
							Zasilacze do laptopów
						</a>
					</li>
					<li>
						<a title="Baterie do laptopów" href="/baterie-do-laptopow.bhtml">
							Baterie do laptopów
						</a>
					</li>
					<li>
						<a href="/akcesoria-komputerowe.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li><li>
						<ul>
							<li class="category-name">
								<a href="/komputery.bhtml">
										Komputery</a>
							</li>
							<li>
								<a href="/komputery.bhtml">
									Wszystkie produkty</a>
							</li>
							<li>
									<!-- area: macro, elements:2 , column: 4487583333 -->
<div class="banners banners-macro">
	<!-- banner id: 17111062488 -->
		<div data-id="17111062488-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17111062488-1',
			date: '2017-09-01',
			type: 'B',
			position: '1',
			name: '188x80 Otrzymasz za zeta na powrót do szkoły (01.09\-18.09)',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/otrzymasz-za-zeta-na-powrot-do-szkoly.bhtml?link=188x80">
								<img alt="Otrzymasz za zeta na powr&oacute;t do szko&#322;y" class="lazy thin" src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-f888b174a9f6197b0c0f899532a665b4/banner/17111062488/f_188x80_oleole-otrzymaj-za-zeta.png" /><img alt="Otrzymasz za zeta na powr&oacute;t do szko&#322;y" class="lazy wide" src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-6f02c697b960d8934e6a816776260f1c/banner/17111062488/af_188x80_oleole-otrzymaj-za-zeta.png" /></a>
						</div>
	<!-- banner id: 12856215438 -->
		<div data-id="12856215438-2" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '12856215438-2',
			date: '2016-06-01',
			type: 'B',
			position: '2',
			name: '188x80 Bestsellery Laptopy',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/bestsellery-laptopy.bhtml?link=188x80">
								<img alt="Bestsellery laptopy 188x80" class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f00.osfr.pl/ver-9422ab5ce82cdb71649bb969c8bacd3b/banner/12856215438/188x80_OLE_75Kb-best-laptopy.jpg" /></a>
						</div>
	</div>
<!-- end area: macro -->
</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii Komputery" href="/komputery.bhtml">
							Zobacz wszystkie produkty z kategorii Komputery</a>
					</div>
				</div>
			</li>
		<li>
				<ul>
					<li>
		<ul>
			<li class="node-name">
				<a title="PlayStation 4" href="/playstation-4.bhtml">
					PlayStation 4
				</a>
				<ul>
					<li>
						<a title="Konsole PS4" href="/konsole-playstation-4.bhtml">
						 Konsole PS4
						</a>
					</li>
					<li>
						<a title="Gry PS4" href="/gry-playstation-4.bhtml">
						Gry PS4
						</a>
					</li>
					<li>
						<a title="Akcesoria PS4" href="/akcesoria-playstation-4.bhtml">
						Akcesoria PS4
						</a>
					</li>
					<li>
						<a href="/playstation-4.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="PlayStation 3" href="/playstation-3.bhtml">
					PlayStation 3
				</a>
			</li>
			<li class="node-name">
				<a title="PlayStation 2" href="/playstation-2.bhtml">
					PlayStation 2
				</a>
			</li>
			<li class="node-name">
				<a title="PlayStation Portable PSP" href="/playstation-portable-psp.bhtml">
					PlayStation Portable PSP
				</a>
			</li>
			<li class="node-name">
				<a title="PlayStation Vita" href="/playstation-vita.bhtml">
					PlayStation Vita
				</a>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Xbox One" href="/xbox-one.bhtml">
					Xbox One
				</a>
				<ul>
					<li>
						<a title="Konsole Xbox One" href="/konsole-xbox-one.bhtml">
						Konsole Xbox One
						</a>
					</li>
					<li>
						<a title="Gry Xbox One" href="/gry-xbox-one.bhtml">
						Gry Xbox One
						</a>
					</li>
					<li>
						<a title="Akcesoria Xbox One" href="/akcesoria-xbox-one.bhtml">
						Akcesoria Xbox One
						</a>
					</li>
					<li>
						<a href="/xbox-one.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Xbox 360" href="/xbox-360.bhtml">
					Xbox 360
				</a>
				<ul>
					<li>
						<a title="Konsole Xbox 360" href="/konsole-xbox-360.bhtml">
							Konsole Xbox 360
						</a>
					</li>

					<li>
						<a title="Gry Xbox 360" href="/gry-xbox-3601.bhtml">
							Gry Xbox 360
						</a>
					</li>
					<li>
						<a title="Kontrolery Xbox 360" href="/kontrolery-do-konsoli-xbox,h4.bhtml">
						Kontrolery Xbox 360
						</a>
					</li>
					<li>
						<a href="/xbox-360.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Nintendo Switch" href="/nintendo-switch.bhtml">
					Nintendo Switch
				</a>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="PC" href="/pc.bhtml">
					PC
				</a>
				<ul>
					<li>
						<a title="Gry PC" href="/gry-pc.bhtml">
							Gry PC
						</a>
					</li>
					<li>
						<a title="Kontrolery do gier" href="/kontrolery-do-gier1,h1.bhtml">
							Kontrolery do gier PC
						</a>
					</li>
					<li>
						<a title="Kierownice" href="/kierownice-pc,h1.bhtml">
							Kierownice PC
						</a>
					</li>
					<li>
						<a class="more" href="/pc.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Zabawki i artykuły dla dzieci" href="/zabawki-i-artykuly-dla-dzieci.bhtml">
					Zabawki i artykuły dla dzieci
				</a>
				<ul>
					<li>
						<a title="Zabawki interaktywne i zdalnie sterowane" href="/zabawki-zdalnie-sterowane.bhtml">
							Zabawki interaktywne i zdalnie sterowane
						</a>
					</li>
					<li>
						<a title="Lalki i akcesoria" href="/lalki-i-akcesoria.bhtml">
							Lalki i akcesoria
						</a>
					</li>
					<li>
						<a title="Deskorolki elektryczne" href="/deskorolki-elektryczne.bhtml">
							Deskorolki elektryczne
						</a>
					</li>
					<li>
						<a title="Plecaki szkolne" href="/plecaki-szkolne.bhtml">
							Plecaki szkolne
						</a>
					</li>
					<li>
						<a href="/zabawki-i-artykuly-dla-dzieci.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li><li>
						<ul>
							<li class="category-name">
								<a href="/gry-i-konsole.bhtml">
										Gry i konsole</a>
							</li>
							<li>
								<a href="/gry-i-konsole.bhtml">
									Wszystkie produkty</a>
							</li>
							<li>
									<!-- area: macro, elements:1 , column: 7014101343 -->
<div class="banners banners-macro">
	<!-- banner id: 17111062488 -->
		<div data-id="17111062488-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17111062488-1',
			date: '2017-09-01',
			type: 'B',
			position: '1',
			name: '188x80 Otrzymasz za zeta na powrót do szkoły (01.09\-18.09)',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/otrzymasz-za-zeta-na-powrot-do-szkoly.bhtml?link=188x80">
								<img alt="Otrzymasz za zeta na powr&oacute;t do szko&#322;y" class="lazy thin" src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-f888b174a9f6197b0c0f899532a665b4/banner/17111062488/f_188x80_oleole-otrzymaj-za-zeta.png" /><img alt="Otrzymasz za zeta na powr&oacute;t do szko&#322;y" class="lazy wide" src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-6f02c697b960d8934e6a816776260f1c/banner/17111062488/af_188x80_oleole-otrzymaj-za-zeta.png" /></a>
						</div>
	</div>
<!-- end area: macro -->
</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii Gry i konsole" href="/gry-i-konsole.bhtml">
							Zobacz wszystkie produkty z kategorii Gry i konsole</a>
					</div>
				</div>
			</li>
		<li>
				<ul>
					<li>
		<ul>
			<li class="node-name">
				<a title="Lustrzanki cyfrowe" href="/lustrzanki.bhtml">
					Lustrzanki cyfrowe
				</a>

				<ul>
					<li>
						<a title="Lustrzanki Canon" href="/lustrzanki,_Canon.bhtml">
							Lustrzanki Canon
						</a>
					</li>
					<li>
						<a title="Lustrzanki Nikon" href="/lustrzanki,_Nikon.bhtml">
							Lustrzanki Nikon
						</a>
					</li>
					<li>
						<a title="Lustrzanki Pentax" href="/lustrzanki,_Pentax.bhtml">
							Lustrzanki Pentax
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Aparaty cyfrowe" href="/kompakty.bhtml">
					Aparaty cyfrowe
				</a>

				<ul>
					<li>
						<a title="Aparaty Sony" href="/kompakty,_Sony.bhtml">
							Aparaty Sony
						</a>
					</li>
					<li>
						<a title="Aparaty Canon" href="/kompakty,_Canon.bhtml">
							Aparaty Canon
						</a>
					</li>
					<li>
						<a title="Aparaty Nikon" href="/kompakty,_Nikon.bhtml">
							Aparaty Nikon
						</a>
					</li>
					<li>
						<a title="Aparaty Fujifilm" href="/kompakty,_Fujifilm.bhtml">
							Aparaty Fujifilm
						</a>
					</li>
					<li>
						<a title="Aparaty Panasonic" href="/kompakty,_Panasonic.bhtml">
							Aparaty Panasonic
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Kompakty z wymienną optyką" href="/kompakty-z-wymienna-optyka.bhtml">
					Kompakty z wymienną optyką
				</a>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Kamery sportowe" href="/kamery-sportowe.bhtml">
					Kamery sportowe
				</a>

				<ul>
					<li>
						<a title="Kamery GoPro Hero" href="/kamery-sportowe,_GoPro.bhtml">
							Kamery GoPro Hero
						</a>
					</li>
					<li>
						<a title="Kamery TomTom" href="/kamery-sportowe,_TomTom.bhtml">
							Kamery TomTom
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Kamery cyfrowe" href="/kamery.bhtml">
					Kamery cyfrowe
				</a>

				<ul>
					<li>
						<a title="Kamery Panasonic" href="/kamery,_Panasonic.bhtml">
							Kamery Panasonic
						</a>
					</li>
					<li>
						<a title="Kamery Sony" href="/kamery,_Sony.bhtml">
							Kamery Sony
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Kamery 360°" href="/kamery-360.bhtml">
					Kamery 360°
				</a>
			</li>
			<li class="node-name">
				<a title="Lornetki" href="/lornetki.bhtml">
					Lornetki
				</a>
			</li>
			<li class="node-name">
				<a title="Drony" href="/drony.bhtml">
					Drony
				</a>
			</li>
			<li class="node-name">
				<a title="Rejestratory jazdy" href="/rejestratory-jazdy.bhtml">
					Rejestratory jazdy
				</a>
			</li>
			<li class="node-name">
				<a title="Cyfrowe ramki do zdjęć" href="/cyfrowe-ramki-do-zdjec.bhtml">
					Cyfrowe ramki do zdjęć
				</a>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Obiektywy" href="/obiektywy2.bhtml">
					Obiektywy
				</a>

				<ul>
					<li>
						<a title="Filtry do obiektywów" href="/filtry-do-obiektywow.bhtml">
							Filtry do obiektywów
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Akcesoria do foto i kamer" href="/akcesoria-do-foto-i-kamer1.bhtml">
					Akcesoria do foto i kamer
				</a>
				<ul>
					<li>
						<a title="Karty pamięci" href="/karty-pamieci1.bhtml">
							Karty pamięci
						</a>
					</li>
					<li>
						<a title="Lampy błyskowe" href="/lampy-blyskowe.bhtml">
							Lampy błyskowe
						</a>
					</li>
					<li>
						<a title="Torby do aparatów" href="/torby-do-aparatow-i-kamer.bhtml">
							Torby do aparatów
						</a>
					</li>
					<li>
						<a title="Akumulatory" href="/akumulatory1.bhtml">
							Akumulatory
						</a>
					</li>
					<li>
						<a title="Statywy" href="/statywy.bhtml">
							Statywy
						</a>
					</li>
					<li>
						<a class="more" href="/statywy.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li><li>
						<ul>
							<li class="category-name">
								<a href="/foto-i-kamery.bhtml">
										Foto i kamery</a>
							</li>
							<li>
								<a href="/foto-i-kamery.bhtml">
									Wszystkie produkty</a>
							</li>
							<li>
									<!-- area: macro, elements:1 , column: 7014102108 -->
<div class="banners banners-macro">
	<!-- banner id: 12613259389 -->
		<div data-id="12613259389-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '12613259389-1',
			date: '2016-05-06',
			type: 'B',
			position: '1',
			name: '188x80 Bestsellery Foto',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/bestsellery-foto.bhtml?link=188x80">
								<img alt="Bestsellery foto 188x80" class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-fa2d44c605fbb81be18149331919585a/banner/12613259389/188x80_OLE_75Kb-best-foto.jpg" /></a>
						</div>
	</div>
<!-- end area: macro -->
</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii Foto i kamery" href="/foto-i-kamery.bhtml">
							Zobacz wszystkie produkty z kategorii Foto i kamery</a>
					</div>
				</div>
			</li>
		<li>
				<ul>
					<li>
		<ul>
			<li class="node-name">
				<a title="Telefony i smartfony" href="/telefony-komorkowe.bhtml">
					Telefony i smartfony
				</a>

				<ul>
					<li>
						<a title="Smartfony" href="/telefony-komorkowe,g1.bhtml">
							Smartfony
						</a>
					</li>
					<li>
						<a title="Smartfony Dual Sim" href="/telefony-komorkowe,g1,l1.bhtml">
							Smartfony Dual Sim
						</a>
					</li>
					<li>
						<a title="Samsung Galaxy" href="/search/telefony-komorkowe.bhtml?keyword=Samsung%20Galaxy&searchType=tag">
							Samsung Galaxy
						</a>
					</li>
					<li>
						<a title="iPhone" href="/telefony-komorkowe,_Apple.bhtml">
							iPhone
						</a>
					</li>
					<li>
						<a title="Telefony Nokia" href="/telefony-komorkowe,_Nokia.bhtml">
							Telefony Nokia
						</a>
					</li>
					<li>
						<a title="Telefony LG" href="/telefony-komorkowe,_LG.bhtml">
							Telefony LG
						</a>
					</li>
					<li>
						<a title="Telefony Sony" href="/telefony-komorkowe,_Sony.bhtml">
							Telefony Sony
						</a>
					</li>
					<li>
						<a title="Telefony HTC" href="/telefony-komorkowe,_HTC.bhtml">
							Telefony HTC
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Telefony" href="/telefony-i-nawigacja-gps.bhtml">
					Telefony
				</a>

				<ul>
					<li>
						<a title="Telefony bezprzewodowe" href="/telefony-bezprzewodowe.bhtml">
							Telefony bezprzewodowe
						</a>
					</li>
					<li>
						<a title="Telefony przewodowe" href="/telefony-przewodowe.bhtml">
							Telefony przewodowe
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Tablety" href="/tablety-graficzne.bhtml">
					Tablety
				</a>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Nawigacja samochodowa" href="/nawigacja-samochodowa-gps.bhtml">
					Nawigacja samochodowa
				</a>
			</li>
			<li class="node-name">
				<a title="Rejestratory jazdy" href="/rejestratory-jazdy.bhtml">
					Rejestratory jazdy
				</a>
			</li>
			<li class="node-name">
				<a title="Mapy GPS" href="/mapy-gps.bhtml">
					Mapy GPS
				</a>
			</li>
			<li class="node-name">
				<a title="Zegarki" href="/sprzet-fitness1.bhtml">
					Zegarki
				</a>
				<ul>
					<li>
						<a title="Smartwatch" href="/smartwatch.bhtml">
							Smartwatch
						</a>
					</li>
					<li>
						<a title="Apple Watch" href="/smartwatch,_Apple.bhtml">
							Apple Watch
						</a>
					</li>
					<li>
						<a title="Zegarki sportowe" href="/zegarki-sportowe.bhtml">
							Zegarki sportowe
						</a>
					</li>
					<li>
						<a title="Zegarki stylowe" href="/zegarki-stylowe.bhtml">
							Zegarki stylowe
						</a>
					</li>
					<li>
						<a title="Opaski monitorujące" href="/opaski-monitorujace-aktywnosc.bhtml">
							Opaski monitorujące
						</a>
					</li>
					<li>
						<a class="more" href="/sprzet-fitness1.bhtml">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Akcesoria do telefonów" href="/akcesoria-do-telefonow2.bhtml">
					Akcesoria do telefonów
				</a>
				<ul>
					<li>
						<a title="Etui do telefonów" href="/etui-do-telefonow.bhtml">
							Etui do telefonów
						</a>
					</li>
					<li>
						<a title="Karty pamięci" href="/karty-pamieci2.bhtml">
							Karty pamięci
						</a>
                                        </li>
					<li>
						<a title="Kable do telefonów" href="/kable-do-telefonow1.bhtml">
							Kable do telefonów
						</a>
					</li>
					<li>
						<a title="Zestawy Bluetooth" href="/zestawy-sluchawkowe-bluetooth.bhtml">
							Zestawy Bluetooth
						</a>
					</li>
					<li>
						<a title="Ładowarki samochodowe" href="/ladowarki-samochodowe.bhtml">
							Ładowarki samochodowe
						</a>
					</li>
					<li>
						<a title="Uchwyty do telefonów" href="/uchwyty-do-telefonow1.bhtml">
							Uchwyty do telefonów
						</a>
					</li>
					<li>
						<a title="Głośniki przenośne" href="/glosniki-przenosne4.bhtml">
							Głośniki przenośne
						</a>
					</li>
					<li>
						<a title="Powerbanki" href="/ladowarki-i-akumulatory-przenosne.bhtml">
							Powerbanki
						</a>
					</li>
					<li>
						<a title="Folie ochronne" href="/folie-ochronne.bhtml">
							Folie ochronne
						</a>
					</li>
					<li>
						<a href="/akcesoria-do-telefonow2.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li><li>
						<ul>
							<li class="category-name">
								<a href="/telefony-i-nawigacja-gps.bhtml">
										Telefony i zegarki</a>
							</li>
							<li>
								<a href="/telefony-i-nawigacja-gps.bhtml">
									Wszystkie produkty</a>
							</li>
							<li>
									<!-- area: macro, elements:1 , column: 4289756888 -->
<div class="banners banners-macro">
	<!-- banner id: 17111062488 -->
		<div data-id="17111062488-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17111062488-1',
			date: '2017-09-01',
			type: 'B',
			position: '1',
			name: '188x80 Otrzymasz za zeta na powrót do szkoły (01.09\-18.09)',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/otrzymasz-za-zeta-na-powrot-do-szkoly.bhtml?link=188x80">
								<img alt="Otrzymasz za zeta na powr&oacute;t do szko&#322;y" class="lazy thin" src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-f888b174a9f6197b0c0f899532a665b4/banner/17111062488/f_188x80_oleole-otrzymaj-za-zeta.png" /><img alt="Otrzymasz za zeta na powr&oacute;t do szko&#322;y" class="lazy wide" src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-6f02c697b960d8934e6a816776260f1c/banner/17111062488/af_188x80_oleole-otrzymaj-za-zeta.png" /></a>
						</div>
	</div>
<!-- end area: macro -->
</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii Telefony i zegarki" href="/telefony-i-nawigacja-gps.bhtml">
							Zobacz wszystkie produkty z kategorii Telefony i zegarki</a>
					</div>
				</div>
			</li>
		<li>
				<ul>
					<li>
		<ul>
			<li class="node-name">
				<a title="Sprzęt fitness" href="/sprzet-fitness.bhtml">
					Sprzęt fitness
				</a>
				<ul>
					<li>
						<a title="Orbitreki i trenażery" href="/orbitreki-i-trenazery.bhtml">
							Orbitreki i trenażery
						</a>
					</li>
					<li>
						<a title="Rowery treningowe" href="/rowery-treningowe.bhtml">
							Rowery treningowe
						</a>
					</li>
					<li>
						<a title="Bieżnie" href="/bieznie.bhtml">
							Bieżnie
						</a>
					</li>
					<li>
						<a title="Wioślarze" href="/wioslarze.bhtml">
							Wioślarze
						</a>
					</li>
					<li>
						<a title="Atlasy do ćwiczeń" href="/atlasy-do-cwiczen.bhtml">
							Atlasy do ćwiczeń
						</a>
					</li>
					<li>
						<a title="Hantle" href="/hantle.bhtml">
							Hantle
						</a>
					</li>
					<li>
						<a title="Ławeczki do ćwiczeń" href="/laweczki-treningowe.bhtml">
							Ławeczki do ćwiczeń
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
					<a title="Sprzęt sportowy" href="/sprzet-sportowy.bhtml">
						Sprzęt sportowy
					</a>
					<ul>
						<li>
							<a title="Kamery sportowe" href="/kamery-sportowe1.bhtml">
								Kamery sportowe
							</a>
						</li>
						<li>
							<a title="Zegarki sportowe" href="/zegarki-sportowe1.bhtml">
								Zegarki sportowe
							</a>
                                                </li>
						<li>
							<a title="Opaski monitorujące aktywność" href="/opaski-monitorujace-aktywnosc2.bhtml">
								Opaski monitorujące aktywność
							</a>
						</li>
						<li>
							<a title="Deskorolki elektryczne" href="/deskorolki-elektryczne2.bhtml">
								Deskorolki elektryczne
							</a>
						</li>
						<li>
							<a title="Krótkofalówki" href="/krotkofalowki2.bhtml">
								Krótkofalówki
							</a>
						</li>
						<li>
							<a title="Hulajnogi" href="/hulajnogi.bhtml">
								Hulajnogi
							</a>
						</li>
						<li>
							<a title="Sprzęt do koszykówki" href="/sprzet-do-koszykowki.bhtml">
								Sprzęt do koszykówki
							</a>
						</li>
						<li>
							<a title="Słuchawki sportowe" href="/sluchawki,przeznaczenie!sportowe.bhtml">
								Słuchawki sportowe
							</a>
						</li>
						<li>
							<a title="Deskorolki" href="/deskorolki.bhtml">
								Deskorolki
							</a>
						</li>
						<li>
							<a title="Rolki" href="/rolki.bhtml">
								Rolki
							</a>
						</li>
						<li>
							<a title="Rowerki dziecięce" href="/rowerki-dzieciece-1.bhtml">
								Rowerki dziecięce
							</a>
						</li>
						<li>
							<a title="Kaski" href="/kaski.bhtml">
								Kaski
							</a>
						</li>
					</ul>
				</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
					<a title="Akcesoria" href="/akcesoria-sportowe-i-fitness.bhtml">
						Akcesoria
					</a>
					<ul>
						<li>
							<a title="Akcesoria do kamer sportowych" href="/akcesoria-do-kamer-sportowych2.bhtml">
								Akcesoria do kamer sportowych
							</a>
						</li>
						<li>
							<a title="Akcesoria do zegarków" href="/akcesoria-do-zegarkow2.bhtml">
								Akcesoria do zegarków
							</a>
						</li>
						<li>
							<a title="Powerbanki" href="/powerbanki2.bhtml">
								Powerbanki
							</a>
						</li>
												<li>
							<a title="Uchwyty do selfie" href="/uchwyty-do-selfie2.bhtml">
								Uchwyty do selfie
							</a>
						</li>
						<li>
							<a title="Latarki" href="/latarki3.bhtml">
								Latarki
							</a>
						</li>
					</ul>
				</li>
				<li class="node-name">
					<a title="Odżywki i suplementy" href="/odzywki-i-suplementy.bhtml">
						Odżywki i suplementy
					</a>
					<ul>
						<li>
							<a title="Odżywki białkowe" href="/odzywki-i-suplementy,g!odzywka-bialkowa.bhtml">
								Odżywki białkowe
							</a>
						</li>
						<li>
							<a title="Aminokwasy" href="/odzywki-i-suplementy,g!aminokwasy.bhtml">
								Aminokwasy
							</a>
						</li>
						<li>
							<a title="Kreatyna" href="/odzywki-i-suplementy,g!kreatyna.bhtml">
								Kreatyna
							</a>
						</li>
						<li>
							<a title="Boostery" href="/odzywki-i-suplementy,g!booster.bhtml">
								Boostery
							</a>
						</li>
						<li>
							<a title="Gainery" href="/odzywki-i-suplementy,g!gainer.bhtml">
								Gainery
							</a>
						</li>
					</ul>
				</li>
		</ul>
	</li><li>
						<ul>
							<li class="category-name">
								<a href="/sport.bhtml">
										Sport</a>
							</li>
							<li>
								<a href="/sport.bhtml">
									Wszystkie produkty</a>
							</li>
							<li>
									<!-- area: macro, elements:1 , column: 6913731583 -->
<div class="banners banners-macro">
	<!-- banner id: 17043724992 -->
		<div data-id="17043724992-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17043724992-1',
			date: '2017-08-28',
			type: 'B',
			position: '1',
			name: '188x80 Bądź zawsze w formie 28.08\-06.09',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/sp-badz-zawsze-w-formie.bhtml?link=188x80">
								<img alt="B&#261;d&#378; zawsze w formie" class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f00.osfr.pl/ver-34d5e559d3280f8201129adf82622ee2/banner/17043724992/f_188x80_OLE_fitness.png" /></a>
						</div>
	</div>
<!-- end area: macro -->
</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii Sport" href="/sport.bhtml">
							Zobacz wszystkie produkty z kategorii Sport</a>
					</div>
				</div>
			</li>
		<li>
				<ul>
					<li>
		<ul>
			<li class="node-name">
				<a title="Do domu i garażu">
					Do domu i garażu
				</a>
				<ul>
					<li>
						<a title="Wiertarko-wkrętarki" href="/wiertarko-wkretarki-1,h1.bhtml">
							Wiertarko-wkrętarki
						</a>
					</li>
					<li>
						<a title="Szlifierki" href="/szlifierki-katawe-1,h1.bhtml">
							Szlifierki
						</a>
					</li>
					<li>
						<a title="Wiertarki" href="/wiertarki-1,h1.bhtml">
							Wiertarki
						</a>
					</li>
					<li>
						<a title="Wyrzynarki" href="/wyrzynarki,h1.bhtml">
							Wyrzynarki
						</a>
					</li>
					<li>
						<a title="Pilarki" href="/pilarki-tarczowe,h1.bhtml">
							Pilarki
						</a>
					</li>
					<li>
						<a title="Piły szablaste" href="/pily-szablaste,h1.bhtml">
							Piły szablaste
						</a>
					</li>
					<li>
						<a title="Pistolety do malowania" href="/systemy-malowania,h1.bhtml">
							Pistolety do malowania
						</a>
					</li>
					<li>
						<a title="Spawarki" href="/spawarki.bhtml">
							Spawarki
						</a>
					</li>
					<li>
						<a title="Elektronarzędzia wielofunkcyjne" href="/elektronarzedzia-wielofunkcyjne-1,h1.bhtml">
							Elektronarzędzia wielofunkcyjne
						</a>
					</li>
					<li>
						<a title="Odkurzacze przemysłowe" href="/odkurzacze-przemyslowe1,h1.bhtml">
							Odkurzacze przemysłowe
						</a>
					</li>
					<li>
						<a title="Ukośnice" href="/ukosnice,h1.bhtml">
							Ukośnice
						</a>
					</li>
					<li>
						<a href="/elektronarzedzia.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Narzędzia ręczne" href="/narzedzia.bhtml">
					Narzędzia ręczne
				</a>
				<ul>
					<li>
						<a title="Zestawy narzędzi" href="/narzedzia-reczne.bhtml">
							Zestawy narzędzi
						</a>
					</li>
					<li>
						<a title="Śrubokręty i wkrętarki" href="/wkretaki.bhtml">
							Śrubokręty i wkrętarki
						</a>
					</li>
					<li>
						<a title="Zestawy kluczy" href="/klucze-nasadowe.bhtml">
							Zestawy kluczy
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Do ogrodu" href="/dzialka-i-ogrod1.bhtml">
					Do ogrodu
				</a>
				<ul>
					<li>
						<a title="Myjki wysokociśnieniowe" href="/myjki-wysokocisnieniowe-1.bhtml">
							Myjki wysokociśnieniowe
						</a>
					</li>
					<li>
						<a title="Nożyce do żywopłotu" href="/nozyce-do-zywoplotow.bhtml">
							Nożyce do żywopłotu
						</a>
					</li>
					<li>
						<a title="Kosiarki" href="/kosiarki.bhtml">
							Kosiarki
						</a>
					</li>
					<li>
						<a title="Kosiarki spalinowe" href="/kosiarki,g1.bhtml">
							Kosiarki spalinowe
						</a>
					</li>
					<li>
						<a title="Kosiarki elektryczne" href="/kosiarki,g2.bhtml">
							Kosiarki elektryczne
						</a>
					</li>
					<li>
						<a title="Piły łańcuchowe" href="/pily-lancuchowe.bhtml">
							Piły łańcuchowe
						</a>
					</li>
					<li>
						<a title="Podkaszarki elektryczne" href="/kosy-i-podkaszarki.bhtml">
							Podkaszarki elektryczne
						</a>
					</li>
					<li>
						<a title="Odkurzacze ogrodowe" href="/odkurzacze-ogrodowe.bhtml">
							Odkurzacze ogrodowe
						</a>
					</li>
					<li>
						<a title="Rozdrabniacze do gałęzi" href="/rozdrabniarki-do-galezi.bhtml">
							Rozdrabniacze do gałęzi
						</a>
					</li>
					<li>
						<a title="Wertykulatory" href="/wertykulatory-i-aeratory.bhtml">
							Wertykulatory
						</a>
					</li>
					<li>
						<a title="Zamiatarki" href="/zamiatarki.bhtml">
							Zamiatarki
						</a>
					</li>
					<li>
						<a title="Pompy wodne i hydrofory" href="/pompy-ogrodowe-i-hydroforowe.bhtml">
							Pompy wodne i hydrofory
						</a>
					</li>
					<li>
						<a title="Kompostowniki" href="/kompostowniki.bhtml">
							Kompostowniki
						</a>
					</li>
					<li>
						<a title="Grille" href="/grille-ogrodowe.bhtml">
							Grille
						</a>
					</li>
					<li>
						<a href="/dzialka-i-ogrod1.bhtml" class="more">
							Zobacz więcej
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Bezpieczeństwo pracy" href="/bezpieczenstwo-pracy.bhtml">
					Bezpieczeństwo pracy
				</a>
				<ul>
					<li>
						<a title="Odzież ochronna" href="/odziez-ochronna.bhtml">
							Odzież ochronna
						</a>
					</li>
					<li>
						<a title="Obuwie robocze" href="/obuwie-robocze.bhtml">
							Obuwie robocze
						</a>
					</li>
					<li>
						<a title="Rękawice robocze" href="/rekawice-robocze.bhtml">
							Rękawice robocze
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Dla profesjonalistów">
					Dla profesjonalistów
				</a>
				<ul>
					<li>
						<a title="Wiertarko-wkrętarki" href="/wiertarko-wkretarki-1,h2.bhtml">
							Wiertarko-wkrętarki
						</a>
					</li>
					<li>
						<a title="Młotowiertarki" href="/mlotowiertarki,h2.bhtml">
							Młotowiertarki
						</a>
					</li>
					<li>
						<a title="Młoty udarowe" href="/mloty-udarowe,h2.bhtml">
							Młoty udarowe
						</a>
					</li>
					<li>
						<a title="Wiertarki" href="/wiertarki-1,h2.bhtml">
							Wiertarki
						</a>
					</li>
					<li>
						<a title="Szlifierki" href="/szlifierki-katawe-1,h2.bhtml">
							Szlifierki
						</a>
					</li>
					<li>
						<a title="Piły tarczowe" href="/pilarki-tarczowe,h2.bhtml">
							Piły tarczowe
						</a>
					</li>
					<li>
						<a title="Cyfrowe przyrządy pomiarowe" href="/cyfrowe-przyrzady-pomiarowe,h2.bhtml">
							Cyfrowe przyrządy pomiarowe
						</a>
					</li>
					<li>
						<a title="Odkurzacze przemysłowe" href="/odkurzacze-przemyslowe1,h2.bhtml">
						        Odkurzacze przemysłowe
						</a>
					</li>
					<li>
						<a title="Wyrzynarki" href="/wyrzynarki,h2.bhtml">
							Wyrzynarki
						</a>
					</li>
					<li>
						<a title="Kompresory" href="/kompresory.bhtml">
							Kompresory
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Akcesoria" href="/akcesoria1.bhtml">
					Akcesoria
				</a>
				<ul>
					<li>
						<a title="Akcesoria do elektronarzędzi" href="/glowice-i-akcesoria-do-elektronarzedzi1.bhtml">
							Akcesoria do elektronarzędzi
						</a>
					</li>
					<li>
						<a title="Narzędzia pomiarowe" href="/cyfrowe-przyrzady-pomiarowe.bhtml">
							Narzędzia pomiarowe
						</a>
					</li>
					<li>
						<a title="Akcesoria do myjek" href="/akcesoria-do-myjek-wysokocisnieniowych.bhtml">
							Akcesoria do myjek
						</a>
					</li>
					<li>
						<a title="Akcesoria ogrodowe" href="/akcesoria-ogrodowe.bhtml">
							Akcesoria ogrodowe
						</a>
					</li>
					<li>
						<a title="Wiertła i końcówki wkrętarskie" href="/wiertla-i-koncowki-wkretarskie.bhtml">
							Wiertła i końcówki wkrętarskie
						</a>
					</li>
                  					<li>
						<a title="Materiały ścierne i polerskie" href="/materialy-scierne-i-polerskie.bhtml">
							Materiały ścierne i polerskie
						</a>
					</li>
					<li>
						<a title="Tarcze i brzeszczoty" href="/tarcze.bhtml">
							Tarcze i brzeszczoty
						</a>
					</li>
					<li>
						<a title="Latarki" href="/latarki2.bhtml">
							Latarki
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li><li>
						<ul>
							<li class="category-name">
								<a href="/narzedzia-i-elektronarzedzia.bhtml">
										Narzędzia</a>
							</li>
							<li>
								<a href="/narzedzia-i-elektronarzedzia.bhtml">
									Wszystkie produkty</a>
							</li>
							<li>
									<!-- area: macro, elements:3 , column: 7367882863 -->
<div class="banners banners-macro">
	<!-- banner id: 17043714736 -->
		<div data-id="17043714736-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17043714736-1',
			date: '2017-08-28',
			type: 'B',
			position: '1',
			name: '188x200 Duże rabaty na sprzęt do ogrodu 28.08\-06.09',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/nr-duze-rabaty-na-sprzet-do-ogrodu.bhtml?link=188x200">
								<img alt="Du&#380;e rabaty na sprz&#281;t do ogrodu" class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f00.osfr.pl/ver-d76ba665a2b57f91d918f85dc018be0c/banner/17043714736/f_188x200_OLE_ogrod.png" /></a>
						</div>
	<!-- banner id: 10758382463 -->
		<div data-id="10758382463-2" class="banner-item banner-text banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '10758382463-2',
			date: '2015-11-03',
			type: 'B',
			position: '2',
			name: '188 Bosch sis zielony [1.08 \- 31.12]',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<div><a href="/cms/sis-bosch-green.bhtml?link=188"><img src="//f00.osfr.pl/banner/10758382463/sticker-bosch_green.jpg" alt="Elektronarzędzia dla majsterkowiczów"></a></div></div>
	<!-- banner id: 10758374952 -->
		<div data-id="10758374952-3" class="banner-item banner-text" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '10758374952-3',
			date: '2015-11-03',
			type: 'B',
			position: '3',
			name: '188 Bosch niebieski sis [1.08 \- 31.12]',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<div><a href="/cms/sis-bosch-blue.bhtml?link=188"><img src="//f00.osfr.pl/banner/10758374952/sticker-bosch_blue.jpg" alt="Elektronarzędzia dla profesjonalistów"></a></div></div>
	</div>
<!-- end area: macro -->
</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<div class="producers-list">
								<b>Popularni producenci:</b>
	<a href="/cms/sis-bosch.bhtml?link=logo-menu"><span class="sisBosch"></span></a></div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii Narzędzia" href="/narzedzia-i-elektronarzedzia.bhtml">
							Zobacz wszystkie produkty z kategorii Narzędzia</a>
					</div>
				</div>
			</li>
		<li>
				<ul>
					<li>
		<ul>
			<li class="node-name">
				<a title="Baterie łazienkowe" href="/baterie-lazienkowe.bhtml">
					Baterie łazienkowe
				</a>
				<ul>
					<li>
						<a title="Baterie stojące" href="/baterie-lazienkowe,sposob-montazu!stojaca.bhtml">
							Baterie stojące
						</a>
					</li>
					<li>
						<a title="Baterie ścienne" href="/baterie-lazienkowe,sposob-montazu!scienna.bhtml">
							Baterie ścienne
						</a>
					</li>
					<li>
						<a title="Baterie podtynkowe" href="/baterie-lazienkowe,sposob-montazu!podtynkowa.bhtml">
							Baterie podtynkowe
						</a>
					</li>
				</ul>
			</li>
			<li class="node-name">
				<a title="Zestawy prysznicowe" href="/zestawy-prysznicowe.bhtml">
					Zestawy prysznicowe
				</a>
			</li>
			<li class="node-name">
				<a title="Panele natryskowe" href="/panele-natryskowe.bhtml">
					Panele natryskowe
				</a>
			</li>
			<li class="node-name">
				<a title="Odpływy prysznicowe" href="/odplywy-prysznicowe.bhtml">
					Odpływy prysznicowe
				</a>
			</li>
		</ul>
	</li>
	<li>
		<ul>
			<li class="node-name">
				<a title="Akcesoria" href="/akcesoria-lazienkowe.bhtml">
					Akcesoria
				</a>
				<ul>
					<li>
						<a title="Akcesoria do systemów podtynkowych" href="/akcesoria-do-systemow-podtynkowych.bhtml">
							Akcesoria do systemów podtynkowych
						</a>
					</li>
					<li>
						<a title="Słuchawki prysznicowe" href="/sluchawki-prysznicowe.bhtml">
							Słuchawki prysznicowe
						</a>
					</li>
					<li>
						<a title="Węże natryskowe" href="/weze-natryskowe.bhtml">
							Węże natryskowe
						</a>
					</li>
					<li>
						<a title="Korki odpływowe" href="/korki-odplywowe.bhtml">
							Korki odpływowe
						</a>
					</li>
					<li>
						<a title="Uchwyty łazienkowe" href="/uchwyty-lazienkowe.bhtml">
							Uchwyty łazienkowe
						</a>
					</li>
					<li>
						<a title="Wieszaki na ręczniki" href="/wieszaki-na-reczniki.bhtml">
							Wieszaki na ręczniki
						</a>
					</li>
					<li>
						<a title="Dozowniki" href="/dozowniki-lazienkowe.bhtml">
							Dozowniki
						</a>
					</li>
					<li>
						<a title="Haczyki łazienkowe" href="/haczyki-i-uchwyty-lazienkowe.bhtml">
							Haczyki łazienkowe
						</a>
					</li>
					<li>
						<a title="Kubki" href="/kubki-lazienkowe.bhtml">
							Kubki
						</a>
					</li>
					<li>
						<a title="Mydelniczki" href="/mydelniczki.bhtml">
							Mydelniczki
						</a>
					</li>
					<li>
						<a title="Szczotki do WC" href="/szczotki-do-wc.bhtml">
							Szczotki do WC
						</a>
					</li>
					<li>
						<a title="Uchwyty na papier toaletowy" href="/uchwyty-na-papier-toaletowy.bhtml">
							Uchwyty na papier toaletowy
						</a>
					</li>
					<li>
						<a title="Półki pod lustro" href="/polki-pod-lustro.bhtml">
							Półki pod lustro
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li><li>
						<ul>
							<li class="category-name">
								<a href="/lazienka.bhtml">
										Łazienka</a>
							</li>
							<li>
								<a href="/lazienka.bhtml">
									Wszystkie produkty</a>
							</li>
							<li>
									<!-- area: macro, elements:1 , column: 15122193040 -->
<div class="banners banners-macro">
	<!-- banner id: 16609574344 -->
		<div data-id="16609574344-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '16609574344-1',
			date: '2017-07-10',
			type: 'B',
			position: '1',
			name: '188x80 Odmień swoją łazienkę za mniej niż myślisz (04.09\-10.09)',
			area: {
				column: '',
				name: ''
			}
		});
	</script>
<a href="/cms/lk-odmien-swoja-lazienke-za-mniej-niz-myslisz.bhtml?link=188x80">
								<img alt="Odmie&#324; swoj&#261; &#322;azienk&#281; za mniej ni&#380; my&#347;lisz" class="lazy thin" src="//f00.osfr.pl/images/www/blank.gif" data-original="//f00.osfr.pl/ver-3355021e6559d705e5d2ad8bc4149624/banner/16609574344/f_188x80__odmien_swoja_lazienke.png" /><img alt="Odmie&#324; swoj&#261; &#322;azienk&#281; za mniej ni&#380; my&#347;lisz" class="lazy wide" src="//f00.osfr.pl/images/www/blank.gif" data-original="//f00.osfr.pl/ver-90817526c733a06f33e27da300eb18b9/banner/16609574344/af_188x80__odmien_swoja_lazienke.png" /></a>
						</div>
	</div>
<!-- end area: macro -->
</li>
							</ul>
					</li>
				</ul>
				<div class="producers-bar">
					<div>
						<a class="all-products" title="Zobacz wszystkie produkty z kategorii Łazienka" href="/lazienka.bhtml">
							Zobacz wszystkie produkty z kategorii Łazienka</a>
					</div>
				</div>
			</li>
		</ul>
	</nav>
</header>
	<div class="none">
			<div id="notify-cookie-content">
				<div>
					<p>Wykorzystujemy ciasteczka (ang. cookies) oraz podobne technologie w celu świadczenia usług, dostosowania serwisu do indywidualnych preferencji użytkowników oraz w celach statystycznych i reklamowych. Możesz zawsze wyłączyć ten mechanizm w ustawieniach przeglądarki. Korzystanie z naszego serwisu bez zmiany ustawień przeglądarki oznacza, że cookies będą zapisane w pamięci urządzenia. Więcej w <a href="/cms/polityka-prywatnosci.bhtml?link=belkaCookie" class="polityka">Polityce prywatności</a>.</p><a id="notify-cookie-close" class="close" href="#">Akceptuję</a>
				</div>
			</div>
		</div>
	<div id="services-bar">
			<button class="js-toggle-main-menu categories-button collapsed">
				Wszystkie kategorie<span class="ico-arrow"></span>
			</button>
			<style type="text/css">
/* belka usług */

#services-bar .item {
  display: block;
  float:left;
  margin-top: 7px;
  margin-left: 32px;
  color: white;
}

#services-bar .item > span {
  display: block;
  margin-left: 34px;
  line-height: 14px;
  font-weight: 200;
  font-size: 11px;
  color: #e3e3e3;
}
  #services-bar .item > span.bold {
   font-weight: 600; 

    color: white;
  font-size: 13px;
  }
  

#services-bar .item::before {
  vertical-align: initial !important;
  margin-top:2px;
  display:block;
    float:left;
  
}

#services-bar .item.service-delivery {
	
}
 
#services-bar .item.service-delivery span {
  margin-left: 41px;
}

#services-bar .item.service-delivery::before {

  background-image: url(//f01.osfr.pl/si_upload/upload_ole/serwis/ikony/usp/services-delivery.png);
  background-repeat: no-repeat;
  background-position: 0 1px;
  width: 31px;
  height: 24px;
  content: "";
}

#services-bar .item.service-marker {

}
  #services-bar .item.service-marker span{
  	margin-left: 35px;
  }
  
#services-bar .item.service-marker::before {

  background-image: url(//f01.osfr.pl/si_upload/upload_ole/serwis/ikony/usp/ico_usp_services.png);
  background-repeat: no-repeat;
  width: 25px;
  height: 24px;
  content: "";
}

#services-bar .item.service-instalments {

}
  
#services-bar .item.service-instalments span {
  
}

#services-bar .item.service-instalments::before {
  display: block;
  float:left;
  background-image: url(//f01.osfr.pl/si_upload/upload_ole/serwis/ikony/usp/ico_usp_raty.png);
  background-repeat: no-repeat;
  width: 28px;
  height: 24px;
  content: "";
}

#services-bar .item.service-best-price {

}

#services-bar .item.service-best-price::before {
  display: block;
  float:left;
  background-image: url(//f01.osfr.pl/si_upload/upload_ole/serwis/ikony/usp/ico_usp_gwarancja.png);
  background-repeat: no-repeat;
  width: 23px;
  height: 23px;
  content: "";
}
#services-bar .item.service-best-price span {
  margin-left: 33px;
}

#services-bar .item.service-setup {

}

#services-bar .item.service-setup::before {
    display: block;
  float:left;
  background-image: url(//f01.osfr.pl/si_upload/upload_ole/serwis/ikony/usp/ico_usp_lock.png);
  background-repeat: no-repeat;
  width: 18px;
  height: 24px;
  content: "";
}

#services-bar .item.service-eco::before {
   display: block;
  float:left;
  background-image: url(//f01.osfr.pl/si_upload/upload_ole/serwis/ikony/usp/services-eco.png);
  background-repeat: no-repeat;
  width: 23px;
  height: 24px;
  content: "";
}


@media all and (max-width: 1230px) {
  #services-bar .services .item {
    
  }
  #services-bar .services .item.service-delivery {
  	
  }
  #services-bar .services .item.service-delivery span {
    
  }
}

</style>

<div class="services">
  
  <a href="/cms/obietnica_nizszej_ceny.bhtml?link=belka" class="item service-best-price" style="margin-left:17px;">
    <span class="bold">Gwarancja niższej ceny</span><span>Przebijamy ceny innych e-sklepów</span>
  </a>
   
  <a href="/cms/bezplatny_transport.bhtml?link=belka" class="item service-delivery">
    <span class="bold">Bezpłatny transport</span><span>0 zł na terenie całego kraju</span>
  </a>
  <a href="/cms/bezpieczne-zakupy.bhtml?link=belka" class="item service-setup">
    <span class="bold" style="margin-left:28px;">Bezpieczne zakupy</span><span  style="margin-left:28px;">265 tys. pozytywnych opinii</span>
  </a>
  <a href="/cms/wygodna-dostawa.bhtml?link=belka" class="item service-marker">
    <span class="bold">Wygodna dostawa</span><span>Wybierasz termin, my wnosimy i instalujemy</span>
  </a>
</div>
	<a class="promotion  static-link" href="/cms/nasze_promocje.bhtml?link=menu">PROMOCJE</a>
	</div>
	</div>
<div id="ears">
		<div id="ear-box">
			<div class="list">
				
		<!-- area: left_ears, elements:0 , column: 15550258720 -->
<!-- end area: left_ears -->

		<!-- area: right_ears, elements:0 , column: 15550290472 -->
<!-- end area: right_ears -->
</div>
		</div>
	</div>
<div class="page">
		
		<!-- area: header, elements:1 , column: 50336817 -->
<div class="banners banners-header">
	<!-- banner id: 17197029464 -->
		<div data-id="17197029464-1" class="banner-item  js-UA-promotion banner-photo banner-link" >
			<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17197029464-1',
			date: '2017-09-06',
			type: 'B',
			position: '1',
			name: '940x80 citi bank (06.09\-19.09)',
			area: {
				column: 'PRODUCT',
				name: 'Telefony i zegarki\/Telefony i Smartfony \- produkty'
			}
		});
	</script>
<a href="/cms/citi-bank.bhtml?link=ban-940x80">
								<img alt="400 z&#322; na Twoje zakupy w OleOle!" class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/ver-1701f2ff189b12dbac74090e1d2cbf42/banner/17197029464/f_940x80_OLE_citi.png" /></a>
						</div>
	</div>
<!-- end area: header -->

	<div id="breadcrumb">
		<ul>
			<li class="home">
   <a href="/">Sklep OleOle!</a>
</li><li>
								<span>
									...</span>
							</li>
						<li>
								<a href="/telefony-komorkowe.bhtml">
									Telefony i Smartfony</a>
							</li>
						<li>
								<span>
									Samsung Galaxy S8 SM-G950 (Orchid Grey)</span>
							</li>
						</ul>

		<script type="application/ld+json">
			{
			"@context": "http://schema.org",
			"@type": "BreadcrumbList",
			"itemListElement": [
				{
							"@type": "ListItem",
							"position": 1,
							"item": {
								"@id": "https://www.oleole.pl/telefony-komorkowe.bhtml",
								"name": "Telefony i Smartfony",
								"image": ""
							}
						}
					
			]}
		</script>
	</div>

		<!-- area: listing_header, elements:0 , column: 76321481 -->
<!-- end area: listing_header -->

	<script type="text/javascript">
	 // Google Tag Manager dataLayer
	UA.push({
		'products' : [ {
			'id' : '1109569',
			'name' : 'Samsung Galaxy S8 SM-G950 (Orchid Grey)',
			'category' : 'Telefony i smartfony',
			'category-url' : 'telefony\-komorkowe',
			'root-category' : 'Telefony i zegarki',
			'price' : '3398.00',
			'link' : '/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml',
			'foto' : '/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_2.jpg',
			'brand' : 'Samsung'
		} ]
	});
	// koniec Google Tag Manager dataLayer

	app.pageConfig('productCard', {
		id:18063925297,
		title:'Samsung Galaxy S8 SM\-G950 (Orchid Grey)',
		plu: 1109569,
		
		groupId:698969,
		groupName:'Telefony i smartfony',
		name:'smartfon',
		categoryName:'Telefony i zegarki',
		linkName:'samsung\-galaxy\-s8\-sm\-g950\-szary',
		nodeId:'telefony-komorkowe',
		preview:false
	});

	UA.productDetails({
		plu: '1109569',
		name: 'Samsung Galaxy S8 SM\-G950 (Orchid Grey)',
		price: '3398.00',
		brand: 'Samsung',
		category: 'smartfon',
		variant: '',
		list: 'KP'
	});

</script>

<script type="application/ld+json">
{
	"@context": "http://schema.org/",
	"@type": "Product",
	"name": "Samsung Galaxy S8 SM-G950 (Orchid Grey)",
	"image": "https://www.oleole.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_2.jpg",
	"sku": "1109569",
	"brand": {
		"@type": "Thing",
		"name": "Samsung"
	},
	
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "5",
            "reviewCount": "1",
			"bestRating": "5",
			"worstRating": "1"
		},
	
	"offers": {
		"@type": "Offer",
		"priceCurrency": "PLN",
		"price": "3398.00",
		"itemCondition": "http://schema.org/NewCondition",
		"availability": "http://schema.org/InStock",
		"seller": {
			"@type": "Organization",
			"name": "OleOle!"
		}
	}
}
</script>



<script type="text/javascript">
	UA && UA.addProduct({
		id: "18063925297",
		plu: "1109569",
		list: "Karta produktu",
		position: "",
		price: "3398.00",
		category: "smartfon",
		name: "Samsung Galaxy S8 SM-G950 (Orchid Grey)",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="18063925297" class="productId"/>
<input type="hidden" value="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml" class="productHref"/>
<input type="hidden" value="1109569" class="plu"/>	
<input type="hidden" value="1109569" class="productPlu"/>
<input type="hidden" value="smartfon" class="productsName"/>
<input type="hidden" value="Karta produktu" class="productList"/><input type="hidden" name="nodeid" id="nodeid" value="telefony-komorkowe"/>
	<input type="hidden" name="print" id="print" value="false"/>
	<input type="hidden" name="showAll" id="showAll" value="false"/>
	<input type="hidden" name="voucher-code" id="voucher-code" value=""/>
	<input type="hidden" name="preview" id="preview" value="false"/>

	<input type="hidden" name="product-id" id="product-id" value="samsung-galaxy-s8-sm-g950-szary"/>

	<input type="hidden" name="group-id" id="group-id" value=""/>

	<input type="hidden" id="opinionMailId" name="opinionMailId" value=""/>
	<input type="hidden" id="opinionId" name="opinionId" value=""/>
	<input type="hidden" id="opinionQuestionId" name="opinionQuestionId" value=""/>
	<input type="hidden" id="opinionUser" name="opinionUser" value=""/>
<div id="after-search-message"></div>
	<div id="product-top" data-type="standard" data-product="18063925297" data-plu="1109569" data-group="698969" class="product-box js-notify-friend">
	<div class="product-main">
		<div class="product-header">
			<h1 class="selenium-product-name">
				Samsung Galaxy S8 SM-G950 (Orchid Grey)</h1>

			<div class="stars-rating">
		<a class="js-save-keyword js-scroll-by-hash" title="5 na 5, przeczytaj 1 opinię o Samsung Galaxy S8 SM-G950 (Orchid Grey)" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#opinie">
			<span style="width:100%"></span>
		</a>
		<a class="js-save-keyword js-scroll-by-hash rating-count" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#opinie">
			<em>(1 opinia)</em>
		</a>
	</div>
<p class="product-category">
	<a href="/telefony-komorkowe.bhtml" title="Telefony i Smartfony">
				smartfon</a>
		</p>
<a class="product-brand" href="/search.bhtml?keyword=Samsung&searchType=tag" title="Samsung" rel="nofollow">
	Samsung</a>
<div class="product-code">
	nr kat.
	<div>1109569<div class="product-call-and-order">
				<div class="call-order-container">
					<p>Podaj numer katalagowy produktu podczas rozmowy z konsultantem.</p>
					<p class="call-order-phone">812 812 812</p>
					<p>pon.- sob. 8:00 - 21:00<br/>nd. 9:00 - 19:00</p></div>
			</div>
		</div>
</div>

</div>
		<div class="product-photos">
			<div id="product-photo">
					<a href="https://f01.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" id="big-photo" data-type="photo" data-index="0" title="smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)">
								<img alt="smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" src="//f01.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" /></a>
						</div>
			<div id="miniatures" class="">
			<a href="#" id="miniatures-previous" class="disabled"></a>
			<div id="miniatures-list">
				<ul class="js-miniatures-carousel miniatures-carousel shift-0" data-visible="4">
					<li class="js-miniatures-photo miniatures-photo miniatures-photo-active">
								<a href="https://f01.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" data-img="https://f01.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" data-type="photo" data-index="0" title="smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)">
									<img alt="smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" src="//f01.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_1.jpg" /></a>
							</li>
						<li class="js-miniatures-photo miniatures-photo ">
								<a href="https://f00.osfr.pl/foto/2/18063925297/6db37fe200177643935301ff971a010c/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" data-img="https://f00.osfr.pl/foto/2/18063925297/6db37fe200177643935301ff971a010c/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" data-type="photo" data-index="1" title="smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)">
									<img alt="Samsung Galaxy S8 SM-G950 (Orchid Grey) smartfon" src="//f00.osfr.pl/foto/2/18063925297/6db37fe200177643935301ff971a010c/samsung-galaxy-s8-sm-g950-szary,18063925297_1.jpg" /></a>
							</li>
						<li class="js-miniatures-photo miniatures-photo ">
								<a href="https://f00.osfr.pl/foto/2/18063925297/faf52bbc3c5641571559de0d557fdb7b/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" data-img="https://f00.osfr.pl/foto/2/18063925297/faf52bbc3c5641571559de0d557fdb7b/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" data-type="photo" data-index="2" title="smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)">
									<img alt="Samsung Galaxy S8 SM-G950 (Orchid Grey)" src="//f00.osfr.pl/foto/2/18063925297/faf52bbc3c5641571559de0d557fdb7b/samsung-galaxy-s8-sm-g950-szary,18063925297_1.jpg" /></a>
							</li>
						<li class="js-miniatures-photo miniatures-photo ">
								<a href="https://f00.osfr.pl/foto/2/18063925297/8343094a18b0f3d1b2288e53e036b5c8/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" data-img="https://f00.osfr.pl/foto/2/18063925297/8343094a18b0f3d1b2288e53e036b5c8/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" data-type="photo" data-index="3" title="smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)">
									<img alt="smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" src="//f00.osfr.pl/foto/2/18063925297/8343094a18b0f3d1b2288e53e036b5c8/samsung-galaxy-s8-sm-g950-szary,18063925297_1.jpg" /></a>
							</li>
						<li class="js-miniatures-photo miniatures-photo ">
								<a href="https://f01.osfr.pl/foto/2/18063925297/86f1bace8624d3fe823d41a1ae4c5c93/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" data-img="https://f01.osfr.pl/foto/2/18063925297/86f1bace8624d3fe823d41a1ae4c5c93/samsung-galaxy-s8-sm-g950-szary,18063925297_7.jpg" data-type="photo" data-index="4" title="smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)">
									<img alt="Samsung Galaxy S8 SM-G950 (Orchid Grey) smartfon" src="//f01.osfr.pl/foto/2/18063925297/86f1bace8624d3fe823d41a1ae4c5c93/samsung-galaxy-s8-sm-g950-szary,18063925297_1.jpg" /></a>
							</li>
						</ul>
			</div>
			<a href="#" id="miniatures-next"></a>
		</div>
	</div>
		<div class="product-info">
			<div class="advertising-placement advertising-placement-product advertising-placement-big">
		<div data-id="17098879968-1" class="js-UA-promotion advertising-small">
				<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17098879968-1',
			date:'2017-09-01',
			type: 'C',
			
			position: '1',
			
			name: 'Otrzymasz za zeta smartwatch (01.09\-18.09)',
		});
	</script>
<a href="/cms/otrzymasz-za-zeta-na-powrot-do-szkoly.bhtml?zestaw=zestaw-2&amp;link=kafel_kp">
							<img class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/files/cms-promotion/75/02/1e/17098879968/c586fd149b1d287582ed43f689bd7920/17098879968_product-placement-small.png" /></a>
					</div>
		<div data-id="17098879968-2" class="js-UA-promotion advertising-big">
				<script type="text/javascript">
		UA && UA.addPromotion({
			id: '17098879968-2',
			date:'2017-09-01',
			type: 'C',
			
			position: '2',
			
			name: 'Otrzymasz za zeta smartwatch (01.09\-18.09)',
		});
	</script>
<a href="/cms/otrzymasz-za-zeta-na-powrot-do-szkoly.bhtml?zestaw=zestaw-2&amp;link=kafel_kp">
							<img class="lazy " src="//f00.osfr.pl/images/www/blank.gif" data-original="//f01.osfr.pl/files/cms-promotion/75/02/1e/17098879968/3e5293054d72d09739fc6f7e1b376313/17098879968_product-placement-large.png" /></a>
					</div>
		</div>
<div class="product-attributes">
		<div class="attributes-row">
						<span class="attribute-name">
							<a title="Wyświetlacz telefonu" class="js-dictionary" href="/slownik.bhtml?definitionId=14147900585">
										Wyświetlacz&nbsp;
									</a>
								</span>
						<span class="attribute-value">
							5,8 cala, 2960 x 1440 pikseli</span>
					</div>
				<div class="attributes-row">
						<span class="attribute-name">
							<a title="Procesor - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=14154878697">
										Procesor&nbsp;
									</a>
								</span>
						<span class="attribute-value">
							ośmiordzeniowy 4 x 2,3 GHz + 4 x 1,9 GHz</span>
					</div>
				<div class="attributes-row">
						<span class="attribute-name">
							<a title="System operacyjny - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=320995948">
										System operacyjny&nbsp;
									</a>
								</span>
						<span class="attribute-value">
							Android 7 Nougat</span>
					</div>
				<div class="attributes-row">
						<span class="attribute-name">
							<a title="Pamięć RAM - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=14136441503">
										Pamięć RAM&nbsp;
									</a>
								</span>
						<span class="attribute-value">
							4 GB</span>
					</div>
				<div class="attributes-row">
						<span class="attribute-name">
							<a title="Pamięć wbudowana - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=2808454592">
										Pamięć wewnętrzna&nbsp;
									</a>
								</span>
						<span class="attribute-value">
							64 GB</span>
					</div>
				<div class="attributes-row">
						<span class="attribute-name">
							<a title="Rozdzielczość aparatu - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=14146105929">
										Aparat foto&nbsp;
									</a>
								</span>
						<span class="attribute-value">
							12 Mpix</span>
					</div>
				<div class="attributes-row">
						<span class="attribute-name">
							Komunikacja&nbsp;
								</span>
						<span class="attribute-value">
							Wi-Fi, Wi-Fi Direct, DLNA, NFC, USB, słuchawkowe, Bluetooth 5.0</span>
					</div>
				<div class="attributes-row">
						<span class="attribute-name">
							<a title="Pojemność akumulatora - Smartfony" class="js-dictionary" href="/slownik.bhtml?definitionId=17921322313">
										Pojemność akumulatora w komplecie&nbsp;
									</a>
								</span>
						<span class="attribute-value">
							3000 mAh</span>
					</div>
				</div>
<div class="product-variants">
		<div class="js-variant variant">
				<div class="current-variant">
							<div class="name">
								Kolor:</div>
							<div class="value">
								szary</div>
						</div>

						<div class="variant-list disable-active-item">
							<div>
								<a data-event="Kolor|1109569|szary|1109567|czarny|Telefony i smartfony" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-czarny.bhtml"
									title="Kolor - czarny" class="variant-item js-variant-item">
									czarny</a>
							<a data-event="Kolor|1109569|szary|1109568|srebrny|Telefony i smartfony" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-srebrny.bhtml"
									title="Kolor - srebrny" class="variant-item js-variant-item">
									srebrny</a>
							<a data-event="" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml"
									title="Kolor - szary" class="variant-item js-variant-item active-item">
									szary</a>
							</div>
					</div>

				</div>
	</div>
<div id="product-variants">
		<div class="variant-group">
				<div class="variant-name">
					Kolor</div>
				<div class="variant-list">
					<a href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-czarny.bhtml"
							
							data-event="Kolor|1109569|szary|1109567|czarny|Telefony i smartfony"
							
							title="Kolor - czarny"
							class="
								js-variant-button variant-button
								">
							czarny</a>
					<a href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-srebrny.bhtml"
							
							data-event="Kolor|1109569|szary|1109568|srebrny|Telefony i smartfony"
							
							title="Kolor - srebrny"
							class="
								js-variant-button variant-button
								">
							srebrny</a>
					<a href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml"
							
							data-event=""
							
							title="Aktualny produkt, Kolor - szary"
							class="
								js-variant-button variant-button
								 variant-current">
							szary</a>
					</div>
			</div>
		</div>
</div>
		<div class="product-community">
			<ul class="community-list">
	<li>
		<a href="#"
	class="compare-button js-add-to-compare"
	data-base="Porównaj"
	data-checked-hovered="Usuń"
	data-checked="W porównaniu"
	data-group="698969"
	data-product="18063925297"
	>
</a>
</li>
	<li>
			<a href="#"
		class="clipboard-button js-add-to-clipboard"
		data-base="Do schowka"
		data-checked-hovered="Usuń"
		data-checked="W schowku"
		data-product="18063925297"
		data-plu="1109569"
		>
	</a>
</li>
	<li>
		</li>
	<li>
			<a class="call-button js-call" href="#" data-event="smartfon|Samsung|1109569|Samsung Galaxy S8 SM-G950 (Orchid Grey)|3398.00|Telefony i zegarki|/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_2.jpg">
				Zadzwoń i zamów</a>
		</li>
	<li>
			<a class="print-button js-print" href="#">
	Wydrukuj</a>
</li>
	</ul>
<div class="social-box">
		<ul>
			<li>
				<a class="social-fb" href="" target="_blank" title="Dodaj do Facebooka"></a>
			</li>
			<li>
				<a class="social-mail js-report-friend" href="#" title="Poleć znajomemu"></a>
			</li>
		</ul>
	</div>
</div>
	</div>
	<div class="product-sales">
		<div class="product-sales-print-logo">
	<img alt="" src="//f01.osfr.pl/img/desktop/ole/logo-print.png" /></div>
<div class="allegro-voucher" data-id="18063925297"></div>
	<div class="product-price  price-show instalments-show">

	<div class="price-normal selenium-price-normal">
			3 398&nbsp;zł</div>

		<a href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#raty" class="price-instalments selenium-price-instalments js-scroll-by-hash">
					 339,80 zł x 10 rat 0%</a>
			</div>
<div class="product-articles">
	<ul>
			<li>
					<a href="/cms/otrzymasz-za-zeta-na-powrot-do-szkoly.bhtml?zestaw=zestaw-2&link=art_kp"  title="Otrzymasz za zeta smartwatch">Otrzymasz za zeta smartwatch</a>
				</li>
			<li>
					<a href="/cms/bezplatny_transport.bhtml?link=wlepka"  title="">Darmowy transport</a>
				</li>
			</ul>
	</div>
<div class="product-button">
	<div class="action-secondary">
			<a href="#"
	class="home-delivery-action js-home-delivery"
	data-product="18063925297"
	data-status="1"
	>
	Do domu: już jutro!</a>
</div>
		<div class="action-primary">
		<button
	class="add-to-cart add-to-cart-big btn-type-ADD_TO_CART js-add-to-cart selenium-ADD_TO_CART"
	title="Do koszyka"
	data-product="18063925297"
	data-plu="1109569"
	data-button="ADD_TO_CART"
	data-added-from="PRODUCT_CARD"
>
	Do koszyka</button>
</div>
	</div>
<div class="call-box">
				<p>
					Zadzwoń i zamów</p>
				<button class="js-call" type="button" data-event="smartfon|Samsung|1109569|Samsung Galaxy S8 SM-G950 (Orchid Grey)|3398.00|Telefony i zegarki|/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_2.jpg">
					812 812 812</button>
			</div>
		<div class="product-sales-print-info">
	<div id="print-info-date">
		Data wydruku: <span>2017-09-06</span></div>
	<div id="print-info-phone">
		Zadzwoń i zamów<br>812 812 812</div>
</div>
</div>
</div>
<!-- pusty --><ul id="menu-product">
	<li class="zestawy">
			<a data-tab="zestawy" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#zestawy" title="Polecane zestawy - smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" data-url="|telefony-komorkowe|zestawy-samsung-galaxy-s8-sm-g950-szary.bhtml">
				<span>
					Zestawy</span>
			</a>
		</li>
	<li class="akcesoria">
			<a data-tab="akcesoria" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#akcesoria" title="Akcesoria - smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" data-url="|telefony-komorkowe|akcesoria-samsung-galaxy-s8-sm-g950-szary.bhtml">
				<span>
					Akcesoria</span>
			</a>
		</li>
	<li class="opis">
			<a data-tab="opis" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#opis" title="Opis i dane - smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" data-url="|telefony-komorkowe|opis-samsung-galaxy-s8-sm-g950-szary.bhtml">
				<span>
					Opis i dane</span>
			</a>
		</li>
	<li class="opinie">
			<a data-tab="opinie" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#opinie" title="Opinie - smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" data-url="|telefony-komorkowe|opinie-samsung-galaxy-s8-sm-g950-szary.bhtml">
				<span>
					Opinie</span>
			</a>
		</li>
	<li class="uslugi">
			<a data-tab="uslugi" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#uslugi" title="Usługi - smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" data-url="|telefony-komorkowe|uslugi-samsung-galaxy-s8-sm-g950-szary.bhtml">
				<span>
					Usługi</span>
			</a>
		</li>
	<li class="gwarancje">
			<a data-tab="gwarancje" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#gwarancje" title="Gwarancje - smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" data-url="|telefony-komorkowe|gwarancje-samsung-galaxy-s8-sm-g950-szary.bhtml">
				<span>
					Ekstra Ochrona</span>
			</a>
		</li>
	<li class="raty">
			<a data-tab="raty" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#raty" title="Raty - smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" data-url="|telefony-komorkowe|raty-samsung-galaxy-s8-sm-g950-szary.bhtml">
				<span>
					Raty</span>
			</a>
		</li>
	<li class="promocje">
			<a data-tab="promocje" href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#promocje" title="Promocje dla tego produktu - smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" data-url="|telefony-komorkowe|promocje-samsung-galaxy-s8-sm-g950-szary.bhtml">
				<span>
					Promocje</span>
			</a>
		</li>
	</ul>
<div id="product-tabs">
	<div id="product-for-sale-box">
	<div id="product-for-sale" class="product-sales" >
		<div class="product-for-sale-content">
			<div class="allegro-voucher" data-id="18063925297"></div>
				<div id="product-image-sales" class="product-image">
				<img alt="smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_2.jpg" /></div>
			<span class="product-for-sale-title ">
				Samsung Galaxy S8 SM-G950 (Orchid Grey)</span>
			<div class="product-price  price-show instalments-show">

	<div class="price-normal selenium-price-normal">
			3 398&nbsp;zł</div>

		<a href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-szary.bhtml#raty" class="price-instalments selenium-price-instalments js-scroll-by-hash">
					 339,80 zł x 10 rat 0%</a>
			</div>
<div class="product-button">
	<div class="action-secondary">
			<a href="#"
	class="home-delivery-action js-home-delivery"
	data-product="18063925297"
	data-status="1"
	>
	Do domu: już jutro!</a>
</div>
		<div class="action-primary">
		<button
	class="add-to-cart add-to-cart-big btn-type-ADD_TO_CART js-add-to-cart selenium-ADD_TO_CART"
	title="Do koszyka"
	data-product="18063925297"
	data-plu="1109569"
	data-button="ADD_TO_CART"
	data-added-from="PRODUCT_CARD"
>
	Do koszyka</button>
</div>
	</div>
</div>
		<div class="product-additional-services">
			</div>
		</div>
</div>
<div class="product-tab product-tab-zestawy">
			<div class="product-tab-header" data-tab="zestawy">
				Polecane zestawy</div>
			<div class="product-tab-content">
				<div id="suites" class="">
	<div class="suite-list">
		<div class="suite-item
				suite-item-visible
			">
				<div class="suite-product-list js-UA-product suite-product-list-3">
					

<script type="text/javascript">
	UA && UA.addProduct({
		id: "16696395752",
		plu: "90876872",
		list: "Zestawy",
		position: "1",
		price: "3533.99",
		category: "smartfon + powerbank + karta pamięci",
		name: "Samsung Galaxy S8 SM-G950 (Orchid Grey) + Samsung EB-PG850BW (bia\u0142y) + Samsung microSDHC EVO Plus 32GB 95 MB\/s Class 10",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="16696395752" class="productId"/>
<input type="hidden" value="/zestawy/telefony-komorkowe/16696395752/samsung-galaxy-s8-sm-g950-szary.bhtml" class="productHref"/>
<input type="hidden" value="90876872" class="plu"/>	
<input type="hidden" value="90876872" class="productPlu"/>
<input type="hidden" value="smartfon + powerbank + karta pamięci" class="productsName"/>
<input type="hidden" value="Zestawy" class="productList"/><div class="suite-photos">
						<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/16696395752/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Galaxy S8 SM-G950 (Orchid Grey)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_4.jpg" /></a>>

<span class="suites-plus">+</span>
<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/16696395752/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung EB-PG850BW (bia&#322;y)" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/3/14450944929/7dce8fe33a196b92741368f94badf30e/samsung-eb-pg850bw-bialy,14450944929_4.jpg" /></a>>

<span class="suites-plus">+</span>
<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/16696395752/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung microSDHC EVO Plus 32GB 95 MB/s Class 10" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/5/18394122297/e27cb258ab35a843d18a6001c5493ea9/samsung-microsdhc-evo-plus-32gb-class-10,18394122297_4.jpg" /></a>>

</div>
					<div>
						<a class="suite-name" href="/zestawy/telefony-komorkowe/16696395752/samsung-galaxy-s8-sm-g950-szary.bhtml">
							smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)  + powerbank Samsung EB-PG850BW (biały)  + karta pamięci Samsung microSDHC EVO Plus 32GB 95 MB/s Class 10 </a>
					</div>
				</div>
				<div class="suite-price">
					<div class="price-normal">
						3 533,99&nbsp;zł</div>
					<button class="add-suite-to-cart js-add-suite-to-cart" data-suite="16696395752" data-product="18063925297" data-plu="90876872" title="Do koszyka" >Do koszyka</button>
					</div>
			</div>
		<div class="suite-item
				suite-item-visible
			">
				<div class="suite-product-list js-UA-product suite-product-list-3">
					

<script type="text/javascript">
	UA && UA.addProduct({
		id: "15973992832",
		plu: "90817674",
		list: "Zestawy",
		position: "2",
		price: "3532.10",
		category: "smartfon + etui dedykowane",
		name: "Samsung Galaxy S8 SM-G950 (Orchid Grey) + Samsung Galaxy S8 LED View Cover EF-NG950PB (czarny)",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="15973992832" class="productId"/>
<input type="hidden" value="/zestawy/telefony-komorkowe/15973992832/samsung-galaxy-s8-sm-g950-szary.bhtml" class="productHref"/>
<input type="hidden" value="90817674" class="plu"/>	
<input type="hidden" value="90817674" class="productPlu"/>
<input type="hidden" value="smartfon + etui dedykowane" class="productsName"/>
<input type="hidden" value="Zestawy" class="productList"/><div class="suite-photos">
						<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/15973992832/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Galaxy S8 SM-G950 (Orchid Grey)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_4.jpg" /></a>>

<span class="suites-plus">+</span>
<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/15973992832/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Galaxy S8 LED View Cover EF-NG950PB (czarny)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/6/18120392337/aea5b897581b92ace53f895b489fca47/samsung-galaxy-s8-led-view-ef-ng950pbegww-czarny,18120392337_4.jpg" /></a>>

<div class="suite-total-discount">
								- 14&nbsp;zł</div>
						</div>
					<div>
						<a class="suite-name" href="/zestawy/telefony-komorkowe/15973992832/samsung-galaxy-s8-sm-g950-szary.bhtml">
							smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)  + etui dedykowane Samsung Galaxy S8 LED View Cover EF-NG950PB (czarny) </a>
					</div>
				</div>
				<div class="suite-price">
					<div class="price-normal">
						3 532,10&nbsp;zł</div>
					<div class="price-old">
							3 547&nbsp;zł</div>
					<div class="product-saved">
		oszczędzasz 14zł</div>
<button class="add-suite-to-cart js-add-suite-to-cart" data-suite="15973992832" data-product="18063925297" data-plu="90817674" title="Do koszyka" >Do koszyka</button>
					</div>
			</div>
		<div class="suite-item
				suite-item-visible
			">
				<div class="suite-product-list js-UA-product suite-product-list-3">
					

<script type="text/javascript">
	UA && UA.addProduct({
		id: "15973997512",
		plu: "90817677",
		list: "Zestawy",
		position: "3",
		price: "3526.82",
		category: "smartfon + etui dedykowane",
		name: "Samsung Galaxy S8 SM-G950 (Orchid Grey) + Samsung Galaxy S8 LED View Cover EF-NG950PV (fioletowy)",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="15973997512" class="productId"/>
<input type="hidden" value="/zestawy/telefony-komorkowe/15973997512/samsung-galaxy-s8-sm-g950-szary.bhtml" class="productHref"/>
<input type="hidden" value="90817677" class="plu"/>	
<input type="hidden" value="90817677" class="productPlu"/>
<input type="hidden" value="smartfon + etui dedykowane" class="productsName"/>
<input type="hidden" value="Zestawy" class="productList"/><div class="suite-photos">
						<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/15973997512/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Galaxy S8 SM-G950 (Orchid Grey)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_4.jpg" /></a>>

<span class="suites-plus">+</span>
<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/15973997512/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Galaxy S8 LED View Cover EF-NG950PV (fioletowy)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/5/18120449305/ca29a05d0dff1425251daaacaf9aac9c/samsung-galaxy-s8-led-view-ef-ng950pvegww-fioletowy,18120449305_4.jpg" /></a>>

<div class="suite-total-discount">
								- 14&nbsp;zł</div>
						</div>
					<div>
						<a class="suite-name" href="/zestawy/telefony-komorkowe/15973997512/samsung-galaxy-s8-sm-g950-szary.bhtml">
							smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)  + etui dedykowane Samsung Galaxy S8 LED View Cover EF-NG950PV (fioletowy) </a>
					</div>
				</div>
				<div class="suite-price">
					<div class="price-normal">
						3 526,82&nbsp;zł</div>
					<div class="price-old">
							3 541,13&nbsp;zł</div>
					<div class="product-saved">
		oszczędzasz 14zł</div>
<button class="add-suite-to-cart js-add-suite-to-cart" data-suite="15973997512" data-product="18063925297" data-plu="90817677" title="Do koszyka" >Do koszyka</button>
					</div>
			</div>
		<div class="suite-item
				suite-item-hidden
			">
				<div class="suite-product-list js-UA-product suite-product-list-3">
					

<script type="text/javascript">
	UA && UA.addProduct({
		id: "16204302720",
		plu: "90833714",
		list: "Zestawy",
		position: "4",
		price: "3549.05",
		category: "smartfon + etui dedykowane",
		name: "Samsung Galaxy S8 SM-G950 (Orchid Grey) + Samsung Galaxy S8 Clear View Standing Cover EF-ZG950CB (czarny)",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="16204302720" class="productId"/>
<input type="hidden" value="/zestawy/telefony-komorkowe/16204302720/samsung-galaxy-s8-sm-g950-szary.bhtml" class="productHref"/>
<input type="hidden" value="90833714" class="plu"/>	
<input type="hidden" value="90833714" class="productPlu"/>
<input type="hidden" value="smartfon + etui dedykowane" class="productsName"/>
<input type="hidden" value="Zestawy" class="productList"/><div class="suite-photos">
						<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/16204302720/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Galaxy S8 SM-G950 (Orchid Grey)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_4.jpg" /></a>>

<span class="suites-plus">+</span>
<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/16204302720/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Galaxy S8 Clear View Standing Cover EF-ZG950CB (czarny)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/4/18120742369/c023a2d0ec9661f818e146b9764dc997/samsung-galaxy-s8-clear-view-standing-cover-ef-zg950cbegww-czarny,18120742369_4.jpg" /></a>>

<div class="suite-total-discount">
								- 7&nbsp;zł</div>
						</div>
					<div>
						<a class="suite-name" href="/zestawy/telefony-komorkowe/16204302720/samsung-galaxy-s8-sm-g950-szary.bhtml">
							smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)  + etui dedykowane Samsung Galaxy S8 Clear View Standing Cover EF-ZG950CB (czarny) </a>
					</div>
				</div>
				<div class="suite-price">
					<div class="price-normal">
						3 549,05&nbsp;zł</div>
					<div class="price-old">
							3 557&nbsp;zł</div>
					<div class="product-saved">
		oszczędzasz 7zł</div>
<button class="add-suite-to-cart js-add-suite-to-cart" data-suite="16204302720" data-product="18063925297" data-plu="90833714" title="Do koszyka" >Do koszyka</button>
					</div>
			</div>
		<div class="suite-item
				suite-item-hidden
			">
				<div class="suite-product-list js-UA-product suite-product-list-3">
					

<script type="text/javascript">
	UA && UA.addProduct({
		id: "16340143056",
		plu: "90850106",
		list: "Zestawy",
		position: "5",
		price: "3498.77",
		category: "smartfon + słuchawki przewodowe + karta pamięci",
		name: "Samsung Galaxy S8 SM-G950 (Orchid Grey) + Samsung In Ear EO-IG935BB (czarny) + Samsung microSDHC EVO Plus 32GB 95 MB\/s Class 10",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="16340143056" class="productId"/>
<input type="hidden" value="/zestawy/telefony-komorkowe/16340143056/samsung-galaxy-s8-sm-g950-szary.bhtml" class="productHref"/>
<input type="hidden" value="90850106" class="plu"/>	
<input type="hidden" value="90850106" class="productPlu"/>
<input type="hidden" value="smartfon + słuchawki przewodowe + karta pamięci" class="productsName"/>
<input type="hidden" value="Zestawy" class="productList"/><div class="suite-photos">
						<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/16340143056/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Galaxy S8 SM-G950 (Orchid Grey)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_4.jpg" /></a>>

<span class="suites-plus">+</span>
<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/16340143056/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung In Ear EO-IG935BB (czarny)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/3/14042162101/98e5bf1338872e8adc466da3ba6b4c75/samsung-in-ear-eo-ig935-czarny,14042162101_4.jpg" /></a>>

<span class="suites-plus">+</span>
<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/16340143056/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung microSDHC EVO Plus 32GB 95 MB/s Class 10" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/5/18394122297/e27cb258ab35a843d18a6001c5493ea9/samsung-microsdhc-evo-plus-32gb-class-10,18394122297_4.jpg" /></a>>

</div>
					<div>
						<a class="suite-name" href="/zestawy/telefony-komorkowe/16340143056/samsung-galaxy-s8-sm-g950-szary.bhtml">
							smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)  + słuchawki przewodowe Samsung In Ear EO-IG935BB (czarny)  + karta pamięci Samsung microSDHC EVO Plus 32GB 95 MB/s Class 10 </a>
					</div>
				</div>
				<div class="suite-price">
					<div class="price-normal">
						3 498,77&nbsp;zł</div>
					<button class="add-suite-to-cart js-add-suite-to-cart" data-suite="16340143056" data-product="18063925297" data-plu="90850106" title="Do koszyka" >Do koszyka</button>
					</div>
			</div>
		<div class="suite-item
				suite-item-hidden
			">
				<div class="suite-product-list js-UA-product suite-product-list-2">
					

<script type="text/javascript">
	UA && UA.addProduct({
		id: "15966108224",
		plu: "90817428",
		list: "Zestawy",
		position: "6",
		price: "3427.99",
		category: "smartfon + oprogramowanie",
		name: "Samsung Galaxy S8 SM-G950 (Orchid Grey) + McAfee Mobile Security",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="15966108224" class="productId"/>
<input type="hidden" value="/zestawy/telefony-komorkowe/15966108224/samsung-galaxy-s8-sm-g950-szary.bhtml" class="productHref"/>
<input type="hidden" value="90817428" class="plu"/>	
<input type="hidden" value="90817428" class="productPlu"/>
<input type="hidden" value="smartfon + oprogramowanie" class="productsName"/>
<input type="hidden" value="Zestawy" class="productList"/><div class="suite-photos">
						<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/15966108224/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Galaxy S8 SM-G950 (Orchid Grey)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_4.jpg" /></a>>

<span class="suites-plus">+</span>
<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/15966108224/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="McAfee Mobile Security" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/7/9820060557/f2a63f9be04a259954cc738130f92ca6/mcafee-mobile-security,9820060557_4.jpg" /></a>>

</div>
					<div>
						<a class="suite-name" href="/zestawy/telefony-komorkowe/15966108224/samsung-galaxy-s8-sm-g950-szary.bhtml">
							smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)  + oprogramowanie McAfee Mobile Security <span style="color: rgb(237, 28, 36);"><strong>Zestaw z oprogramowaniem antywirusowym McAfee na system Android.</strong></span></a>
					</div>
				</div>
				<div class="suite-price">
					<div class="price-normal">
						3 427,99&nbsp;zł</div>
					<button class="add-suite-to-cart js-add-suite-to-cart" data-suite="15966108224" data-product="18063925297" data-plu="90817428" title="Do koszyka" >Do koszyka</button>
					</div>
			</div>
		<div class="suite-item
				suite-item-hidden
			">
				<div class="suite-product-list js-UA-product suite-product-list-2">
					

<script type="text/javascript">
	UA && UA.addProduct({
		id: "16736381512",
		plu: "90881687",
		list: "Zestawy",
		position: "7",
		price: "3927.00",
		category: "smartfon + okulary vr",
		name: "Samsung Galaxy S8 SM-G950 (Orchid Grey) + Samsung Gear VR SM-R324",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="16736381512" class="productId"/>
<input type="hidden" value="/zestawy/telefony-komorkowe/16736381512/samsung-galaxy-s8-sm-g950-szary.bhtml" class="productHref"/>
<input type="hidden" value="90881687" class="plu"/>	
<input type="hidden" value="90881687" class="productPlu"/>
<input type="hidden" value="smartfon + okulary vr" class="productsName"/>
<input type="hidden" value="Zestawy" class="productList"/><div class="suite-photos">
						<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/16736381512/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Galaxy S8 SM-G950 (Orchid Grey)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/2/18063925297/ef9c589184a1917f79862708a4a166b8/samsung-galaxy-s8-sm-g950-szary,18063925297_4.jpg" /></a>>

<span class="suites-plus">+</span>
<a class="suite-photo-link" href="/zestawy/telefony-komorkowe/16736381512/samsung-galaxy-s8-sm-g950-szary.bhtml">
	<img alt="Samsung Gear VR SM-R324" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/8/18831880577/1845e140e788711e329236feee62186a/samsung-gear-vr-sm-r324,18831880577_4.jpg" /></a>>

</div>
					<div>
						<a class="suite-name" href="/zestawy/telefony-komorkowe/16736381512/samsung-galaxy-s8-sm-g950-szary.bhtml">
							smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)  + okulary VR Samsung Gear VR SM-R324 </a>
					</div>
				</div>
				<div class="suite-price">
					<div class="price-normal">
						3 927&nbsp;zł</div>
					<button class="add-suite-to-cart js-add-suite-to-cart" data-suite="16736381512" data-product="18063925297" data-plu="90881687" title="Do koszyka" >Do koszyka</button>
					</div>
			</div>
		</div>
	<button id="show-more-suites" data-more="Więcej zestawów" data-less="Mniej zestawów"></button>
	</div>
</div>
		</div>
	<div class="product-tab product-tab-akcesoria">
			<div class="product-tab-header" data-tab="akcesoria">
				Akcesoria</div>
			<div class="product-tab-content">
				<div id="accessories" class="accessories">
							<div class="accessories-groups">
		<button class="active category-access js-category-access " data-group="0">
				Rekomendowane</button>
		<button class="inactive category-access js-category-access " data-group="358439">
				Słuchawki</button>
		<button class="inactive category-access js-category-access " data-group="54906898">
				Etui do telefonów</button>
		<button class="inactive category-access js-category-access " data-group="54908236">
				Ładowarki samochodowe</button>
		<button class="inactive category-access js-category-access " data-group="4741">
				Karty pamięci</button>
		<button class="inactive category-access js-category-access " data-group="5156">
				Produkty czyszczące</button>
		<button class="inactive category-access js-category-access " data-group="514896566">
				Systemy audio i aktywne głośniki Bluetooth</button>
		<button class="inactive category-access js-category-access " data-group="14762762">
				Mapy GPS</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="2721835598">
				Adaptery i przejściówki</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="54908508">
				Akcesoria do telefonów</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="2721840360">
				Folie ochronne</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="2721845932">
				Kable do telefonów</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="12138785857">
				Okulary VR</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="10130822">
				Oprogramowanie</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="4011120742">
				Powerbanki</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="54101268">
				Rozdzielacze gniazd</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="13503413737">
				Uchwyty do selfie</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="2721846418">
				Uchwyty do telefonów</button>
		<button class="inactive category-access js-category-access category-access-hidden" data-group="4298160772">
				Ładowarki sieciowe</button>
		<button class="js-category-access-more category-access-more">
				Pokaż pozostałe (11)</button>
		</div>
<div class="accessories-list js-accessories-list">
		<div id="accessories-list-view">
		<ul class="js-accessories-list-carousel accessories-list-carousel">
			<li class="accessory-item accessory-item-18120392337 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "18120392337",
		plu: "1110216",
		list: "Akcesoria",
		position: "",
		price: "149.00",
		category: "etui dedykowane",
		name: "Samsung Galaxy S8 LED View Cover EF-NG950PB (czarny)",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="18120392337" class="productId"/>
<input type="hidden" value="/etui-do-telefonow/samsung-galaxy-s8-led-view-ef-ng950pbegww-czarny.bhtml" class="productHref"/>
<input type="hidden" value="1110216" class="plu"/>	
<input type="hidden" value="1110216" class="productPlu"/>
<input type="hidden" value="etui dedykowane" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<a class="product-image" href="/etui-do-telefonow/samsung-galaxy-s8-led-view-ef-ng950pbegww-czarny.bhtml">
					<img alt="etui dedykowane Samsung Galaxy S8 LED View Cover EF-NG950PB (czarny)" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/6/18120392337/aea5b897581b92ace53f895b489fca47/samsung-galaxy-s8-led-view-ef-ng950pbegww-czarny,18120392337_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/etui-do-telefonow/samsung-galaxy-s8-led-view-ef-ng950pbegww-czarny.bhtml#opinie">
					<span style="width:100%"></span>
				</a>
				<em>(4)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								149&nbsp;zł</div>
							</div>
			<h3>
				<a href="/etui-do-telefonow/samsung-galaxy-s8-led-view-ef-ng950pbegww-czarny.bhtml">
					Etui dedykowane Samsung Galaxy S8 LED View Cover EF-NG950PB (czarny)</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="18120392337" data-item="Samsung Galaxy S8 LED View Cover EF-NG950PB (czarny)" data-plu="1110216"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
<li class="accessory-item accessory-item-18120449305 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "18120449305",
		plu: "1110217",
		list: "Akcesoria",
		position: "",
		price: "143.13",
		category: "etui dedykowane",
		name: "Samsung Galaxy S8 LED View Cover EF-NG950PV (fioletowy)",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="18120449305" class="productId"/>
<input type="hidden" value="/etui-do-telefonow/samsung-galaxy-s8-led-view-ef-ng950pvegww-fioletowy.bhtml" class="productHref"/>
<input type="hidden" value="1110217" class="plu"/>	
<input type="hidden" value="1110217" class="productPlu"/>
<input type="hidden" value="etui dedykowane" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<a class="product-image" href="/etui-do-telefonow/samsung-galaxy-s8-led-view-ef-ng950pvegww-fioletowy.bhtml">
					<img alt="etui dedykowane Samsung Galaxy S8 LED View Cover EF-NG950PV (fioletowy)" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/5/18120449305/ca29a05d0dff1425251daaacaf9aac9c/samsung-galaxy-s8-led-view-ef-ng950pvegww-fioletowy,18120449305_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/etui-do-telefonow/samsung-galaxy-s8-led-view-ef-ng950pvegww-fioletowy.bhtml#opinie">
					<span style="width:100%"></span>
				</a>
				<em>(4)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								143,13&nbsp;zł</div>
							</div>
			<h3>
				<a href="/etui-do-telefonow/samsung-galaxy-s8-led-view-ef-ng950pvegww-fioletowy.bhtml">
					Etui dedykowane Samsung Galaxy S8 LED View Cover EF-NG950PV (fioletowy)</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="18120449305" data-item="Samsung Galaxy S8 LED View Cover EF-NG950PV (fioletowy)" data-plu="1110217"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
<li class="accessory-item accessory-item-13547851529 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "13547851529",
		plu: "1087741",
		list: "Akcesoria",
		position: "",
		price: "99.99",
		category: "powerbank",
		name: "Samsung EB-PG850BL (niebieski)",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="13547851529" class="productId"/>
<input type="hidden" value="/ladowarki-i-akumulatory-przenosne/samsung-eb-pg850b-niebieski.bhtml" class="productHref"/>
<input type="hidden" value="1087741" class="plu"/>	
<input type="hidden" value="1087741" class="productPlu"/>
<input type="hidden" value="powerbank" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<a class="product-image" href="/ladowarki-i-akumulatory-przenosne/samsung-eb-pg850b-niebieski.bhtml">
					<img alt="powerbank Samsung EB-PG850BL (niebieski)" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/8/13547851529/8583fb6045cc9542445eab11efc71aef/samsung-eb-pg850b-niebieski,13547851529_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/ladowarki-i-akumulatory-przenosne/samsung-eb-pg850b-niebieski.bhtml#opinie">
					<span style="width:100%"></span>
				</a>
				<em>(8)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								99,99&nbsp;zł</div>
							</div>
			<h3>
				<a href="/ladowarki-i-akumulatory-przenosne/samsung-eb-pg850b-niebieski.bhtml">
					Powerbank Samsung EB-PG850BL (niebieski)</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="13547851529" data-item="Samsung EB-PG850BL (niebieski)" data-plu="1087741"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
<li class="accessory-item accessory-item-12697756332 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "12697756332",
		plu: "1079393",
		list: "Akcesoria",
		position: "",
		price: "19.90",
		category: "ładowarka samochodowa",
		name: "Forever GSM001532",
		brand: "Forever"
	});
</script>

<input type="hidden" value="12697756332" class="productId"/>
<input type="hidden" value="/ladowarki-do-telefonow1/forever-gsm001532.bhtml" class="productHref"/>
<input type="hidden" value="1079393" class="plu"/>	
<input type="hidden" value="1079393" class="productPlu"/>
<input type="hidden" value="ładowarka samochodowa" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<a class="product-image" href="/ladowarki-do-telefonow1/forever-gsm001532.bhtml">
					<img alt="&#322;adowarka samochodowa Forever GSM001532" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/2/12697756332/4e49bdbafc18fd77c8b4841626577566/forever-gsm001532,12697756332_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/ladowarki-do-telefonow1/forever-gsm001532.bhtml#opinie">
					<span style="width:0%"></span>
				</a>
				<em>(0)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								19,90&nbsp;zł</div>
							</div>
			<h3>
				<a href="/ladowarki-do-telefonow1/forever-gsm001532.bhtml">
					Ładowarka samochodowa Forever GSM001532</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="12697756332" data-item="Forever GSM001532" data-plu="1079393"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
<li class="accessory-item accessory-item-9551503702 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "9551503702",
		plu: "1054855",
		list: "Akcesoria",
		position: "",
		price: "38.98",
		category: "słuchawki przewodowe",
		name: "Samsung In-Ear Fit EO-EG920BB (czarny)",
		brand: "Samsung"
	});
</script>

<input type="hidden" value="9551503702" class="productId"/>
<input type="hidden" value="/sluchawki/samsung-eo-eg920bb-czarny.bhtml" class="productHref"/>
<input type="hidden" value="1054855" class="plu"/>	
<input type="hidden" value="1054855" class="productPlu"/>
<input type="hidden" value="słuchawki przewodowe" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<a class="product-image" href="/sluchawki/samsung-eo-eg920bb-czarny.bhtml">
					<img alt="s&#322;uchawki przewodowe Samsung In-Ear Fit EO-EG920BB (czarny)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/1/9551503702/600df99b2d1ae061d3604cfa515adffa/samsung-eo-eg920bb-czarny,9551503702_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/sluchawki/samsung-eo-eg920bb-czarny.bhtml#opinie">
					<span style="width:100%"></span>
				</a>
				<em>(10)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								38,98&nbsp;zł</div>
							</div>
			<h3>
				<a href="/sluchawki/samsung-eo-eg920bb-czarny.bhtml">
					Słuchawki przewodowe Samsung In-Ear Fit EO-EG920BB (czarny)</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="9551503702" data-item="Samsung In-Ear Fit EO-EG920BB (czarny)" data-plu="1054855"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
<li class="accessory-item accessory-item-15125202561 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "15125202561",
		plu: "1096937",
		list: "Akcesoria",
		position: "",
		price: "34.99",
		category: "uniwersalny uchwyt samochodowy",
		name: "Celly MiniGrip Evo",
		brand: "Celly"
	});
</script>

<input type="hidden" value="15125202561" class="productId"/>
<input type="hidden" value="/uchwyty-do-telefonow/celly-minigrip-evo.bhtml" class="productHref"/>
<input type="hidden" value="1096937" class="plu"/>	
<input type="hidden" value="1096937" class="productPlu"/>
<input type="hidden" value="uniwersalny uchwyt samochodowy" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<a class="product-image" href="/uchwyty-do-telefonow/celly-minigrip-evo.bhtml">
					<img alt="uniwersalny uchwyt samochodowy Celly MiniGrip Evo" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/2/15125202561/a4d53dde4213e4890b4922f939b40a1a/celly-minigrip-evo,15125202561_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/uchwyty-do-telefonow/celly-minigrip-evo.bhtml#opinie">
					<span style="width:100%"></span>
				</a>
				<em>(1)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								34,99&nbsp;zł</div>
							</div>
			<h3>
				<a href="/uchwyty-do-telefonow/celly-minigrip-evo.bhtml">
					Uniwersalny uchwyt samochodowy Celly MiniGrip Evo</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="15125202561" data-item="Celly MiniGrip Evo" data-plu="1096937"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
<li class="accessory-item accessory-item-5430110897 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "5430110897",
		plu: "1005182",
		list: "Akcesoria",
		position: "",
		price: "39.99",
		category: "karta pamięci",
		name: "Sony microSDHC Class 10 16GB",
		brand: "Sony"
	});
</script>

<input type="hidden" value="5430110897" class="productId"/>
<input type="hidden" value="/karty-pamieci/sony-microsdhc-class-10-16gb.bhtml" class="productHref"/>
<input type="hidden" value="1005182" class="plu"/>	
<input type="hidden" value="1005182" class="productPlu"/>
<input type="hidden" value="karta pamięci" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<a class="product-image" href="/karty-pamieci/sony-microsdhc-class-10-16gb.bhtml">
					<img alt="karta pami&#281;ci Sony microSDHC Class 10 16GB" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/1/5430110897/20c60fb5ac4b75c233d8c78646b149c3/sony-microsdhc-class-10-16gb,5430110897_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/karty-pamieci/sony-microsdhc-class-10-16gb.bhtml#opinie">
					<span style="width:100%"></span>
				</a>
				<em>(34)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								39,99&nbsp;zł</div>
							</div>
			<h3>
				<a href="/karty-pamieci/sony-microsdhc-class-10-16gb.bhtml">
					Karta pamięci Sony microSDHC Class 10 16GB</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="5430110897" data-item="Sony microSDHC Class 10 16GB" data-plu="1005182"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
<li class="accessory-item accessory-item-3127541700 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "3127541700",
		plu: "606088",
		list: "Akcesoria",
		position: "",
		price: "39.99",
		category: "uniwersalny uchwyt samochodowy",
		name: "Nokia CR-123",
		brand: "Nokia"
	});
</script>

<input type="hidden" value="3127541700" class="productId"/>
<input type="hidden" value="/uchwyty-do-telefonow/nokia-cr-123.bhtml" class="productHref"/>
<input type="hidden" value="606088" class="plu"/>	
<input type="hidden" value="606088" class="productPlu"/>
<input type="hidden" value="uniwersalny uchwyt samochodowy" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<a class="product-image" href="/uchwyty-do-telefonow/nokia-cr-123.bhtml">
					<img alt="uniwersalny uchwyt samochodowy Nokia CR-123" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/4/3127541700/b2deb1669dcc67a6d38b9213a2989cab/nokia-cr-123,3127541700_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/uchwyty-do-telefonow/nokia-cr-123.bhtml#opinie">
					<span style="width:100%"></span>
				</a>
				<em>(6)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								39,99&nbsp;zł</div>
							</div>
			<h3>
				<a href="/uchwyty-do-telefonow/nokia-cr-123.bhtml">
					Uniwersalny uchwyt samochodowy Nokia CR-123</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="3127541700" data-item="Nokia CR-123" data-plu="606088"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
<li class="accessory-item accessory-item-17939973401 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "17939973401",
		plu: "1106741",
		list: "Akcesoria",
		position: "",
		price: "12.99",
		category: "ściereczki",
		name: "Reinston chusteczki czyszcz\u0105ce 100 szt ECH002",
		brand: "Reinston"
	});
</script>

<input type="hidden" value="17939973401" class="productId"/>
<input type="hidden" value="/produkty-czyszczace/reinston-ech002.bhtml" class="productHref"/>
<input type="hidden" value="1106741" class="plu"/>	
<input type="hidden" value="1106741" class="productPlu"/>
<input type="hidden" value="ściereczki" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<span class="product-status nowosc">Nowość</span><a class="product-image" href="/produkty-czyszczace/reinston-ech002.bhtml">
					<img alt="&#347;ciereczki Reinston chusteczki czyszcz&#261;ce 100 szt ECH002" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/3/17939973401/4866544cecd9feb8bbc2df506e96c617/reinston-ech002,17939973401_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/produkty-czyszczace/reinston-ech002.bhtml#opinie">
					<span style="width:100%"></span>
				</a>
				<em>(1)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								12,99&nbsp;zł</div>
							</div>
			<h3>
				<a href="/produkty-czyszczace/reinston-ech002.bhtml">
					Ściereczki Reinston chusteczki czyszczące 100 szt ECH002</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="17939973401" data-item="Reinston chusteczki czyszczące 100 szt ECH002" data-plu="1106741"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
<li class="accessory-item accessory-item-1925891718 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "1925891718",
		plu: "349251",
		list: "Akcesoria",
		position: "",
		price: "369.00",
		category: "stacja dokująca z głośnikiem",
		name: "Pioneer Steez Type-S STZ-D10S-L",
		brand: "Pioneer"
	});
</script>

<input type="hidden" value="1925891718" class="productId"/>
<input type="hidden" value="/stacje-dokujace-ipod-iphone/pioneer-steez-stz-d10s-l.bhtml" class="productHref"/>
<input type="hidden" value="349251" class="plu"/>	
<input type="hidden" value="349251" class="productPlu"/>
<input type="hidden" value="stacja dokująca z głośnikiem" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<a class="product-image" href="/stacje-dokujace-ipod-iphone/pioneer-steez-stz-d10s-l.bhtml">
					<img alt="stacja dokuj&#261;ca z g&#322;o&#347;nikiem Pioneer Steez Type-S STZ-D10S-L" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/foto/6/1925891718/4dda2a159f5e7df567c56af31970a25f/pioneer-steez-stz-d10s-l,1925891718_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/stacje-dokujace-ipod-iphone/pioneer-steez-stz-d10s-l.bhtml#opinie">
					<span style="width:0%"></span>
				</a>
				<em>(0)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								369&nbsp;zł</div>
							<div class="price-instalments-rates">
									 36,90 zł x 10 rat 0%</div>
							</div>
			<h3>
				<a href="/stacje-dokujace-ipod-iphone/pioneer-steez-stz-d10s-l.bhtml">
					Stacja dokująca z głośnikiem Pioneer Steez Type-S STZ-D10S-L</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="1925891718" data-item="Pioneer Steez Type-S STZ-D10S-L" data-plu="349251"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
<li class="accessory-item accessory-item-2457148454 js-UA-product">
		

<script type="text/javascript">
	UA && UA.addProduct({
		id: "2457148454",
		plu: "605462",
		list: "Akcesoria",
		position: "",
		price: "9.00",
		category: "mapa GPS",
		name: "MapaMap Polska (90 dni)",
		brand: "MapaMap"
	});
</script>

<input type="hidden" value="2457148454" class="productId"/>
<input type="hidden" value="/mapy-gps/mapamap-polska-90-dni.bhtml" class="productHref"/>
<input type="hidden" value="605462" class="plu"/>	
<input type="hidden" value="605462" class="productPlu"/>
<input type="hidden" value="mapa GPS" class="productsName"/>
<input type="hidden" value="Akcesoria" class="productList"/><div class="col-img">
			<a class="product-image" href="/mapy-gps/mapamap-polska-90-dni.bhtml">
					<img alt="mapa GPS MapaMap Polska (90 dni)" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/foto/3/2457148454/922a577d06419a0027dd32f49bbe2a37/mapamap-polska-90-dni,2457148454_1.jpg" /></a>
			</div>
		<div class="accessory-grade">
			<div class="stars-rating">
				<a class="js-scroll-by-hash" href="/mapy-gps/mapamap-polska-90-dni.bhtml#opinie">
					<span style="width:0%"></span>
				</a>
				<em>(0)</em>
			</div>
		</div>
		<div class="col-desc">

			<div class="product-prices">
					<div class="price-normal">
								9&nbsp;zł</div>
							</div>
			<h3>
				<a href="/mapy-gps/mapamap-polska-90-dni.bhtml">
					Mapa GPS MapaMap Polska (90 dni)</a>
			</h3>
		</div>
		<div class="add-accessory-to-cart">
					<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="accessory" class="js-add-accessory-to-cart-checkbox" value="2457148454" data-item="MapaMap Polska (90 dni)" data-plu="605462"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</div>
			</li>
</ul>
	</div>
</div>
</div>
					</div>
		</div>
	<div class="product-tab product-tab-opis">
			<div class="product-tab-header" data-tab="opis">
				Opis i dane</div>
			<div class="product-tab-content">
				<div id="description">
							<div id="description-content" data-max="250" data-min="190" style="max-height:250px;">

	<div id="description-text">
			<style type="text/css">@media (max-width: 920px) {        #samsung_m_00 #samsung_m_01 p {          font-size: 11px!important;          line-height: 18px!important; } }@media (max-width: 920px) {        #samsung_m_00 #samsung_m_01 .sam_text-star-content {          font-size: 10px!important;          line-height: 14px!important; } }@media (max-width: 479px) {    #samsung_m_00 #samsung_m_01 .sam_col-xs-3, #samsung_m_00 #samsung_m_01 .sam_col-xs-4, #samsung_m_00 #samsung_m_01 .sam_col-xs-6, #samsung_m_00 #samsung_m_01 .sam_col-xs-8, #samsung_m_00 #samsung_m_01 .sam_col-xs-9, #samsung_m_00 #samsung_m_01 .sam_col-xs-12 {      float: left!important; }    #samsung_m_00 #samsung_m_01 .sam_col-xs-12 {      width: 100%!important; }    #samsung_m_00 #samsung_m_01 .sam_col-xs-9 {      width: 75%!important; }    #samsung_m_00 #samsung_m_01 .sam_col-xs-8 {      width: 66.66666667%!important; }    #samsung_m_00 #samsung_m_01 .sam_col-xs-6 {      width: 50%!important; }    #samsung_m_00 #samsung_m_01 .sam_col-xs-4 {      width: 33.33333333%!important; }    #samsung_m_00 #samsung_m_01 .sam_col-xs-3 {      width: 25%!important; }    #samsung_m_00 #samsung_m_01 .sam_col-xs-pull-4 {      right: 33.33333333%!important; }    #samsung_m_00 #samsung_m_01 .sam_col-xs-pull-0 {      right: auto!important; }    #samsung_m_00 #samsung_m_01 .sam_col-xs-push-8 {      left: 66.66666667%!important; }    #samsung_m_00 #samsung_m_01 .sam_col-xs-push-0 {      left: auto!important; } }@media (max-width: 479px) {    #samsung_m_00 #samsung_m_01 .sam_visible-xs {      display: block !important; } }@media (max-width: 479px) {    #samsung_m_00 #samsung_m_01 .sam_hidden-xs {      display: none !important; }    #samsung_m_00 #samsung_m_01 .sam_visible-sm {      display: none !important; } }@media (max-width: 920px) {      #samsung_m_00 #samsung_m_01 .sam_vertical-center {        display: block!important; } }@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_separator-30--module {height: 15px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_separator-30--module {height: 15px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_tabelka_tabela_a5--module td {padding: 2px 4px!important;font-size: 9px!important;line-height: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_tabelka_tabela_a5--module td {padding: 2px 4px!important;font-size: 7px!important;line-height: 10px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_naglowek_wysrodkowany_z_kreskami-2--module h2 {font-size: 17px!important;margin: 20px 0px 20px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_heading-twocolors-h3-module--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_heading-twocolors-h3-module--module h3 {margin-top: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_heading-twocolors-h3-module--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_heading-twocolors-h3-module--module h3 {margin-top: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_separator-20--module {height: 10px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_separator-20--module {height: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs {position: relative!important;padding-bottom: 56.25%!important;padding-top: 25px!important;height: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs iframe {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;height: 100%!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs {position: relative!important;padding-bottom: 56.25%!important;padding-top: 25px!important;height: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs iframe {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;height: 100%!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs {position: relative!important;padding-bottom: 56.25%!important;padding-top: 25px!important;height: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs iframe {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;height: 100%!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs {position: relative!important;padding-bottom: 56.25%!important;padding-top: 25px!important;height: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs iframe {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;height: 100%!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs {position: relative!important;padding-bottom: 56.25%!important;padding-top: 25px!important;height: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs iframe {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;height: 100%!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_4_kolumny--module .sam_inner_padding {padding: 20px!important;} 	#samsung_m_00 #samsung_m_01 .sam_4_kolumny--module h3 {font-size: 16px!important;color: #06c!important;margin: 20px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_menu-1 a {width: 50% !important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_menu-1 a {width: 100% !important;text-align: left!important;border-bottom: 0 none!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_menu-1 a {width: 50% !important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_menu-1 a {width: 100% !important;text-align: left!important;border-bottom: 0 none!important;}}@media (max-width: 920px) {      #samsung_m_00 #samsung_m_01 .sam_vertical-center {        display: block!important; } }@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_separator-30--module {height: 15px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_separator-30--module {height: 15px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_tabelka_tabela_a5--module td {padding: 2px 4px!important;font-size: 9px!important;line-height: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_tabelka_tabela_a5--module td {padding: 2px 4px!important;font-size: 7px!important;line-height: 10px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_naglowek_wysrodkowany_z_kreskami-2--module h2 {font-size: 17px!important;margin: 20px 0px 20px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module {font-size: 11px!important;line-height: 18px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module p.sam_text-star-content {font-size: 10px!important;line-height: 14px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: -webkit-flex!important;display: flex!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {padding-left: 0px!important;padding-right: 0px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module h3 {margin-top: 30px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-right {padding-right: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_no-padding-left {padding-left: 10px!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_vertical-center {display: block!important;} 	#samsung_m_00 #samsung_m_01 .sam_image_text-chess--module .sam_xyz-inner {margin-bottom: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_heading-twocolors-h3-module--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_heading-twocolors-h3-module--module h3 {margin-top: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_heading-twocolors-h3-module--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 10px 0px 10px 0px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_heading-twocolors-h3-module--module h3 {margin-top: 30px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_icon_heading_text_packshot--module h3 {font-size: 17px!important;line-height: 19px!important;margin: 0px 0px 10px 0px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_separator-20--module {height: 10px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_separator-20--module {height: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module .sam_inner_padding {padding: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module  .sam_col-xs-12 {width: 100%!important;margin: 10px 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_3_kolumny_ikona_text--module h4 {margin-top: 10px!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs {position: relative!important;padding-bottom: 56.25%!important;padding-top: 25px!important;height: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs iframe {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;height: 100%!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs {position: relative!important;padding-bottom: 56.25%!important;padding-top: 25px!important;height: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs iframe {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;height: 100%!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs {position: relative!important;padding-bottom: 56.25%!important;padding-top: 25px!important;height: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs iframe {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;height: 100%!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs {position: relative!important;padding-bottom: 56.25%!important;padding-top: 25px!important;height: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs iframe {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;height: 100%!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs {position: relative!important;padding-bottom: 56.25%!important;padding-top: 25px!important;height: 0!important;} 	#samsung_m_00 #samsung_m_01 .sam_video_wysrodkowane--module .sam_video-wrapper-xs iframe {position: absolute!important;top: 0!important;left: 0!important;width: 100%!important;height: 100%!important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_4_kolumny--module .sam_inner_padding {padding: 20px!important;} 	#samsung_m_00 #samsung_m_01 .sam_4_kolumny--module h3 {font-size: 16px!important;color: #06c!important;margin: 20px!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_menu-1 a {width: 50% !important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_menu-1 a {width: 100% !important;text-align: left!important;border-bottom: 0 none!important;}}@media (max-width: 920px) {#samsung_m_00 #samsung_m_01 .sam_menu-1 a {width: 50% !important;}}@media (max-width: 479px) {#samsung_m_00 #samsung_m_01 .sam_menu-1 a {width: 100% !important;text-align: left!important;border-bottom: 0 none!important;}}</style>
<div id="samsung_m_00" class="sam_card-238">
<div id="samsung_m_01" style="font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20px; color: #616266;">
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_sections-menu--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_sections-menu" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<div class="sam_menu-1" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #202020; padding: 16px 8px 15px 8px; font-size: 0;"><a href="#sam_section-1" style="width: 12.5%; position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #fff; text-decoration: none; display: inline-block; vertical-align: middle; padding: 0px 6px 0px 6px; font-size: 11px; text-align: center; text-transform: uppercase; font-weight: bold;" class="sam_ed-0"><span style="color: #fff; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; display: block; padding-bottom: 10px; padding-top: 10px; padding-right: 12px; border-bottom: 1px solid transparent; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/menu-1-arrow.png&quot;) center right no-repeat;" data-mce-style="color: #fff;"> Design </span></a><a href="#sam_section-2" style="width: 12.5%; position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #fff; text-decoration: none; display: inline-block; vertical-align: middle; padding: 0px 6px 0px 6px; font-size: 11px; text-align: center; text-transform: uppercase; font-weight: bold;" class="sam_ed-0"><span style="color: #fff; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; display: block; padding-bottom: 10px; padding-top: 10px; padding-right: 12px; border-bottom: 1px solid transparent; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/menu-1-arrow.png&quot;) center right no-repeat;" data-mce-style="color: #fff;">ekran</span></a><a href="#sam_section-3" style="width: 12.5%; position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #fff; text-decoration: none; display: inline-block; vertical-align: middle; padding: 0px 6px 0px 6px; font-size: 11px; text-align: center; text-transform: uppercase; font-weight: bold;" class="sam_ed-0"><span style="color: #fff; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; display: block; padding-bottom: 10px; padding-top: 10px; padding-right: 12px; border-bottom: 1px solid transparent; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/menu-1-arrow.png&quot;) center right no-repeat;" data-mce-style="color: #fff;"> Bezpieczeństwo </span></a><a href="#sam_section-4" style="width: 12.5%; position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #fff; text-decoration: none; display: inline-block; vertical-align: middle; padding: 0px 6px 0px 6px; font-size: 11px; text-align: center; text-transform: uppercase; font-weight: bold;" class="sam_ed-0"><span style="color: #fff; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; display: block; padding-bottom: 10px; padding-top: 10px; padding-right: 12px; border-bottom: 1px solid transparent; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/menu-1-arrow.png&quot;) center right no-repeat;" data-mce-style="color: #fff;">Aparat</span></a><a href="#sam_section-5" style="width: 12.5%; position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #fff; text-decoration: none; display: inline-block; vertical-align: middle; padding: 0px 6px 0px 6px; font-size: 11px; text-align: center; text-transform: uppercase; font-weight: bold;" class="sam_ed-0"><span style="color: #fff; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; display: block; padding-bottom: 10px; padding-top: 10px; padding-right: 12px; border-bottom: 1px solid transparent; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/menu-1-arrow.png&quot;) center right no-repeat;" data-mce-style="color: #fff;"> Wydajność </span></a><a href="#sam_section-7" style="width: 12.5%; position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #fff; text-decoration: none; display: inline-block; vertical-align: middle; padding: 0px 6px 0px 6px; font-size: 11px; text-align: center; text-transform: uppercase; font-weight: bold;" class="sam_ed-0"><span style="color: #fff; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; display: block; padding-bottom: 10px; padding-top: 10px; padding-right: 12px; border-bottom: 1px solid transparent; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/menu-1-arrow.png&quot;) center right no-repeat;" data-mce-style="color: #fff;"> Aplikacje</span></a><a href="#sam_section-10" style="width: 12.5%; position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #fff; text-decoration: none; display: inline-block; vertical-align: middle; padding: 0px 6px 0px 6px; font-size: 11px; text-align: center; text-transform: uppercase; font-weight: bold;" class="sam_ed-0"><span style="color: #fff; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; display: block; padding-bottom: 10px; padding-top: 10px; padding-right: 12px; border-bottom: 1px solid transparent; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/menu-1-arrow.png&quot;) center right no-repeat;" data-mce-style="color: #fff;"> Akcesoria</span></a><a href="#sam_section-11" style="width: 12.5%; position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #fff; text-decoration: none; display: inline-block; vertical-align: middle; padding: 0px 6px 0px 6px; font-size: 11px; text-align: center; text-transform: uppercase; font-weight: bold;" class="sam_ed-0"><span style="color: #fff; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; display: block; padding-bottom: 10px; padding-top: 10px; padding-right: 12px; border-bottom: 1px solid transparent; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/menu-1-arrow.png&quot;) center right no-repeat;" data-mce-style="color: #fff;">W pudełku</span></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_KV--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-kv_galaxy_s8-b833.jpg" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span></div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-0" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Poznaj lepiej Galaxy S8+</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_3_kolumny_ikona_text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-md-4 sam_col-sm-4 sam_col-xs-12 sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_col-md-3 sam_col-sm-12 sam_col-xs-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><a data-mce-href="#sam_wyswietlacz" href="#sam_wyswietlacz" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_wyswietlacz-a9cb.png" class="sam_img-responsive sam_center-block" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 60px; height: auto; margin-left: auto; margin-right: auto;" data-mce-style="/* max-width: 80px;" alt="Samsung Wyświetlacz" title="Samsung Wyświetlacz" /></a></span></div>
<div class="sam_col-md-9 sam_col-sm-12 sam_col-xs-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;">
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 0px 0;">
<h4 class="sam_ed-0" style="position: relative; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: rgb(0, 102, 204); margin-top: 0px; margin-bottom: 10px; font-size: 16px; padding-top: 0px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"><a href="#sam_wyswietlacz" data-mce-href="#sam_wyswietlacz" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;">Wyświetlacz</a></strong></h4>
<p class="sam_ed-0" style="position: relative; box-sizing: border-box; margin: 0px 0px 10px; color: rgb(97, 98, 102); font-size: 13px; line-height: 20px; text-align: center;">6.2&quot; Quad HD+ sAMOLED o proporcjach 18.5:9</p>
</div>
</div>
</div>
<div class="sam_col-md-4 sam_col-sm-4 sam_col-xs-12 sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_col-md-3 sam_col-sm-12 sam_col-xs-4 sam_col-sm-push-0 sam_col-xs-push-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%; left: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><a data-mce-href="#sam_aparat" href="#sam_aparat" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_aparat-a9cb.png" class="sam_img-responsive sam_center-block" alt="Samsung Aparat" title="Samsung Aparat" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 60px; height: auto; margin-left: auto; margin-right: auto;" /></a></span></div>
<div class="sam_col-md-9 sam_col-sm-12 sam_col-xs-8 sam_col-sm-pull-0 sam_col-xs-pull-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%; right: auto;">
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 0px 0;">
<h4 class="sam_ed-0" style="position: relative; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: rgb(0, 102, 204); margin-top: 0px; margin-bottom: 10px; font-size: 16px; padding-top: 0px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"><a href="#sam_aparat" data-mce-href="#sam_aparat" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;">Aparat</a></strong></h4>
<p class="sam_ed-0" style="position: relative; box-sizing: border-box; margin: 0px 0px 10px; color: rgb(97, 98, 102); font-size: 13px; line-height: 20px; text-align: center;">Tył: Dual Pixel 12MP F1,7 AF OIS<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
Prz&oacute;d: 8MP F1,7 AF</p>
</div>
</div>
</div>
<div class="sam_col-md-4 sam_col-sm-4 sam_col-xs-12 sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_col-md-3 sam_col-sm-12 sam_col-xs-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><a data-mce-href="#sam_zabezpieczenia" href="#sam_zabezpieczenia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_security-a9cb.png" class="sam_img-responsive sam_center-block" alt="Samsung Zabezpieczenia" title="Samsung Zabezpieczenia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 60px; height: auto; margin-left: auto; margin-right: auto;" /></a></span></div>
<div class="sam_col-md-9 sam_col-sm-12 sam_col-xs-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;">
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 0px 0;">
<h4 class="sam_ed-0" style="position: relative; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: rgb(0, 102, 204); margin-top: 0px; margin-bottom: 10px; font-size: 16px; padding-top: 0px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"><a href="#sam_zabezpieczenia" data-mce-href="#sam_zabezpieczenia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;">Zabezpieczenia</a></strong></h4>
<p class="sam_ed-0" style="position: relative; box-sizing: border-box; margin: 0px 0px 10px; color: rgb(97, 98, 102); font-size: 13px; line-height: 20px; text-align: center;">Skaner tęcz&oacute;wki, skaner linii papilarnych, rozpoznawanie twarzy, bezpieczny katalog</p>
</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
<div class="sam_separator-30" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 30px; width: 100%; clear: both;">&nbsp;</div>
<div class="sam_col-md-4 sam_col-sm-4 sam_col-xs-12 sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_col-md-3 sam_col-sm-12 sam_col-xs-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><a data-mce-href="#sam_wydajn" href="#sam_wydajn" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_wydajnosc-a9cb.png" class="sam_img-responsive sam_center-block" data-mce-selected="1" alt="Samsung Wydajność" title="Samsung Wydajność" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 60px; height: auto; margin-left: auto; margin-right: auto;" /></a></span></div>
<div class="sam_col-md-9 sam_col-sm-12 sam_col-xs-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;">
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 0px 0;">
<h4 class="sam_ed-0" style="position: relative; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: rgb(0, 102, 204); margin-top: 0px; margin-bottom: 10px; font-size: 16px; padding-top: 0px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"><a href="#sam_wydajn" data-mce-href="#sam_wydajn" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;">Wydajność</a></strong></h4>
<p class="sam_ed-0" style="position: relative; box-sizing: border-box; margin: 0px 0px 10px; color: rgb(97, 98, 102); font-size: 13px; line-height: 20px; text-align: center;">Wydajny Octa-core (2.3GHz Quad + 1.7GHz Quad), 64 bit, 10nm</p>
</div>
</div>
</div>
<div class="sam_col-md-4 sam_col-sm-4 sam_col-xs-12 sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_col-md-3 sam_col-sm-12 sam_col-xs-4 sam_col-sm-push-0 sam_col-xs-push-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%; left: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><a data-mce-href="#sam_odpornosc" href="#sam_odpornosc" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_odpornosc-a9cb.png" class="sam_img-responsive sam_center-block" alt="Samsung Odporność" title="Samsung Odporność" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 60px; height: auto; margin-left: auto; margin-right: auto;" /></a></span></div>
<div class="sam_col-md-9 sam_col-sm-12 sam_col-xs-8 sam_col-sm-pull-0 sam_col-xs-pull-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%; right: auto;">
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 0px 0;">
<h4 class="sam_ed-0" style="position: relative; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: rgb(0, 102, 204); margin-top: 0px; margin-bottom: 10px; font-size: 16px; padding-top: 0px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"><a href="#sam_odpornosc" data-mce-href="#sam_odpornosc" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;">Odporność</a></strong></h4>
<p class="sam_ed-0" style="position: relative; box-sizing: border-box; margin: 0px 0px 10px; color: rgb(97, 98, 102); font-size: 13px; line-height: 20px; text-align: center;">Odporność na wodę i pył (IP68)</p>
</div>
</div>
</div>
<div class="sam_col-md-4 sam_col-sm-4 sam_col-xs-12 sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_col-md-3 sam_col-sm-12 sam_col-xs-4 sam_col-sm-push-0 sam_col-xs-push-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%; left: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><a data-mce-href="#sam_pamiec" href="#sam_pamiec" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_pamiec-ae14.png" class="sam_img-responsive sam_center-block" alt="Samsung Pamięć" title="Samsung Pamięć" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 60px; height: auto; margin-left: auto; margin-right: auto;" /></a></span></div>
<div class="sam_col-md-9 sam_col-sm-12 sam_col-xs-8 sam_col-sm-pull-0 sam_col-xs-pull-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%; right: auto;">
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 0px 0;">
<h4 class="sam_ed-0" style="position: relative; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: rgb(0, 102, 204); margin-top: 0px; margin-bottom: 10px; font-size: 16px; padding-top: 0px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"><a href="#sam_pamiec" data-mce-href="#sam_pamiec" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;">Pamięć</a></strong></h4>
<p class="sam_ed-0" style="position: relative; box-sizing: border-box; margin: 0px 0px 10px; color: rgb(97, 98, 102); font-size: 13px; line-height: 20px; text-align: center;">64 GB wbudowanej pamięci, możliwość rozszerzenia o 256 GB</p>
</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_3_kolumny_ikona_text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_separator-30" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 30px; width: 100%; clear: both;">&nbsp;</div>
<div class="sam_col-md-4 sam_col-sm-4 sam_col-xs-12 sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_col-md-3 sam_col-sm-12 sam_col-xs-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><a data-mce-href="#sam_bateria" href="#sam_bateria" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_bateria-a9cb.png" class="sam_img-responsive sam_center-block" alt="Samsung Bateria" title="Samsung Bateria" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 60px; height: auto; margin-left: auto; margin-right: auto;" /></a></span></div>
<div class="sam_col-md-9 sam_col-sm-12 sam_col-xs-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;">
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 0px 0;">
<h4 class="sam_ed-0" style="position: relative; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: rgb(0, 102, 204); margin-top: 0px; margin-bottom: 10px; font-size: 16px; padding-top: 0px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"><a href="#sam_bateria" data-mce-href="#sam_bateria" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;">Bateria</a></strong></h4>
<p class="sam_ed-0" style="position: relative; box-sizing: border-box; margin: 0px 0px 10px; color: rgb(97, 98, 102); font-size: 13px; line-height: 20px; text-align: center;">Pojemna bateria 3500 mAh</p>
</div>
</div>
</div>
<div class="sam_col-md-4 sam_col-sm-4 sam_col-xs-12 sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_col-md-3 sam_col-sm-12 sam_col-xs-4 sam_col-sm-push-0 sam_col-xs-push-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%; left: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><a data-mce-href="#sam_guard_s8" href="#sam_guard_s8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/xs/samsung-s_guard-7d71.png" class="sam_img-responsive sam_center-block" alt="Samsung Guard S8" title="Samsung Guard S8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 60px; height: auto; margin-left: auto; margin-right: auto;" /></a></span></div>
<div class="sam_col-md-9 sam_col-sm-12 sam_col-xs-8 sam_col-sm-pull-0 sam_col-xs-pull-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%; right: auto;">
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 0px 0;">
<h4 class="sam_ed-0" style="position: relative; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: rgb(0, 102, 204); margin-top: 0px; margin-bottom: 10px; font-size: 16px; padding-top: 0px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"><a href="#sam_guard_s8" data-mce-href="#sam_guard_s8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;">Guard S8</a></strong></h4>
<p class="sam_ed-0" style="position: relative; box-sizing: border-box; margin: 0px 0px 10px; color: rgb(97, 98, 102); font-size: 13px; line-height: 20px; text-align: center;">Pakiet usług serwisowych premium</p>
</div>
</div>
</div>
<div class="sam_col-md-4 sam_col-sm-4 sam_col-xs-12 sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_col-md-3 sam_col-sm-12 sam_col-xs-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><a data-mce-href="#sam_dzwiek" href="#sam_dzwiek" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_dzwiek_sluchawki_akg-0a01.png" class="sam_img-responsive sam_center-block" alt="Samsung Galaxy S8 Dzwiek Sluchawki Akg" title="Samsung Galaxy S8 Dzwiek Sluchawki Akg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 60px; height: auto; margin-left: auto; margin-right: auto;" /></strong></a></span></div>
<div class="sam_col-md-9 sam_col-sm-12 sam_col-xs-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;">
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 0px 0;">
<h4 class="sam_ed-0" style="position: relative; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: rgb(0, 102, 204); margin-top: 0px; margin-bottom: 10px; font-size: 16px; padding-top: 0px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"><a href="#sam_dzwiek" data-mce-href="#sam_dzwiek" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background-color: transparent; color: #337ab7; text-decoration: none;">Krystalicznie czysty dźwięk</a></strong></h4>
<p class="sam_ed-0" style="position: relative; box-sizing: border-box; margin: 0px 0px 10px; color: rgb(97, 98, 102); font-size: 13px; line-height: 20px; text-align: center;">Nowe słuchawki opracowane przez AKG, oferują dźwięk w jakości premium.</p>
</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-20--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 10px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator_linia--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_col-sm-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 100%;">
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; height: 1px; margin: 20px 0px; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_KV--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-guard-e321.jpg" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span></div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_full-width-text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;"><span class="sam_text-star-content" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #999999; font-size: 11px; line-height: 15px; margin-top: 10px; display: inline-block;">* Dowiedz się więcej o ofercie oraz zarejestruj sw&oacute;j produkt na stronie: www.guards8.samsung.pl.&nbsp;</span></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_heading-twocolors-h3-module--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid sam_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto; background: #000; padding: 0;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<h3 style="text-align: center; margin-bottom: 0; line-height: 20px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; color: #fff; margin-top: 20px; font-size: 17px; margin: 10px 0px 10px 0px; padding-bottom: 8px;"><span style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;" data-mce-style="font-size: 23px;">Galaxy S8+.</strong></span></span>                                         <span style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Zupełnie nowy smartfon.</strong></span></h3>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_heading-twocolors-h3-module--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid sam_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto; background: #000; padding: 0;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<h3 style="text-align: center; margin-bottom: 0; line-height: 20px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; color: #fff; margin-top: 20px; font-size: 17px; margin: 10px 0px 10px 0px; padding-bottom: 8px;">&nbsp;</h3>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_full-width-text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid sam_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto; background: #000; padding: 0;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p class="sam_ed-0" style="position: relative; text-align: center; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #fff; font-size: 13px; line-height: 20px;"><span class="sam_big-txt-centered" style="margin: auto 40px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-size: 16px; text-align: center; line-height: 22px; display: block;" data-mce-style="margin: auto 40px;">Zaprojektowaliśmy <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">smartfon nowej generacji</strong>, odrzucając dotychczasowe ograniczenia ekranu. Boczne <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">krawędzie są teraz niemal niewidoczne</strong>, dzięki temu&nbsp;obraz jest większy i bardziej wciągający. Całość zamknięta została w <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">smukłej obudowie</strong>, kt&oacute;ra <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">doskonale układa się w dłoni</strong>.</span></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_icon_heading_text_packshot--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; overflow-x: hidden;">
<div class="sam_container-fluid sam_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto; background: #000; padding: 0;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-8" style="margin-right: 30px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<div class="sam_row sam_icon_heading_text_packshot--module-clone-row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; margin-bottom: 20px; position: relative;">
<div class="sam_col-md-3 sam_col-sm-2 sam_col-xs-3 sam_ed-0" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 16.66666667%;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_wyswietlacz_bialy-88e0.png" class="sam_img-responsive sam_ikon_70 sam_img-responsive sam_visible-sm" alt="Samsung Przełomowa technologia wyświetlacza" title="Samsung Przełomowa technologia wyświetlacza" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; width: 50%; margin: 0 auto;" /></div>
<div class="sam_col-md-9 sam_col-sm-10 sam_col-xs-9" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 83.33333333%;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: #fff; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 0px 0px 10px 0px;"><span style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0">Przełomowa technologia wyświetlacza<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
</span></h3>
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #fff; font-size: 13px; line-height: 20px;">Ekran Galaxy S8+ 6.2&rdquo; wyświetla obraz o proporcjach 18.5:9 lepiej dostosowanych do odtwarzania plik&oacute;w video. Nowa technologia wyświetlacza o maksymalnie zredukowanych ramkach gwarantuje więcej miejsca do działania w por&oacute;wnaniu do poprzednich modeli Samsung.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
<div class="sam_row sam_icon_heading_text_packshot--module-clone-row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; margin-bottom: 20px; position: relative;">
<div class="sam_col-md-3 sam_col-sm-2 sam_col-xs-3 sam_ed-0" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 16.66666667%;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_aparat-bialy-88e0.png" class="sam_img-responsive sam_ikon_70 sam_img-responsive sam_visible-sm" alt="Samsung Doskonałe aparaty" title="Samsung Doskonałe aparaty" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; width: 50%; margin: 0 auto;" /></div>
<div class="sam_col-md-9 sam_col-sm-10 sam_col-xs-9" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 83.33333333%;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: #fff; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 0px 0px 10px 0px;"><span style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0">Doskonałe aparaty</span></h3>
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #fff; font-size: 13px; line-height: 20px;">Szybki autofocus w tylnym, a od teraz r&oacute;wnież przednim aparacie pozwoli uchwycić bardzo dynamiczne sytuacje. Detale zwykle zanikające w mroku zostaną doskonale uwiecznione, dzięki bardzo jasnemu obiektywowi F1.7 i dużemu rozmiarowi pikseli 1,4 &micro;m w tylnym aparacie.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
<div class="sam_row sam_icon_heading_text_packshot--module-clone-row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: block; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center; margin-bottom: 20px; position: relative;">
<div class="sam_col-md-3 sam_col-sm-2 sam_col-xs-3 sam_ed-0" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 16.66666667%;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_zabezpieczenia-bialy-849b.png" class="sam_img-responsive sam_ikon_70 sam_img-responsive sam_visible-sm" alt="Samsung Wyjątkowe zabezpieczenia telefonu" title="Samsung Wyjątkowe zabezpieczenia telefonu" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; width: 50%; margin: 0 auto;" /></div>
<div class="sam_col-md-9 sam_col-sm-10 sam_col-xs-9" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 83.33333333%;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: #fff; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 0px 0px 10px 0px;"><span style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0">Wyjątkowe zabezpieczenia telefonu</span></h3>
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #fff; font-size: 13px; line-height: 20px;">Ultrabezpieczny skaner tęcz&oacute;wki oka gwarantuje najwyższy poziom ochrony telefonu. Dodatkowa funkcja &bdquo;Bezpieczny katalog&rdquo; pozwala na zabezpieczenie Twoich danych w oddzielnej, ukrytej przestrzeni.<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
&nbsp;</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_heading-twocolors-h3-module--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid sam_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto; background: #000; padding: 0;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<h3 style="text-align: center; margin-bottom: 0; line-height: 20px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; color: #fff; margin-top: 20px; font-size: 17px; margin: 10px 0px 10px 0px; padding-bottom: 8px;">&nbsp;</h3>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_video_wysrodkowane--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid sam_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto; background: #000; padding: 0;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<div class="sam_video-wrapper-xs sam_video-wrapper-sm sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: center;"><span data-mce-object="iframe" data-mce-p-allowfullscreen="allowfullscreen" data-mce-p-frameborder="0" data-mce-p-src="//www.youtube.com/embed/8CHiR9yHvno?rel=0" data-mce-html="%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><iframe src="//www.youtube.com/embed/hID3mEjeuA8?rel=0" allowfullscreen="allowfullscreen" data-mce-src="//www.youtube.com/embed/8CHiR9yHvno?rel=0" height="360" frameborder="0" width="750" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"></iframe></span></div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_heading-twocolors-h3-module--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid sam_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto; background: #000; padding: 0;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<h3 style="text-align: center; margin-bottom: 0; line-height: 20px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; color: #fff; margin-top: 20px; font-size: 17px; margin: 10px 0px 10px 0px; padding-bottom: 8px;">&nbsp;</h3>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_heading-twocolors-h3-module--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid sam_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto; background: #000; padding: 0;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<h3 style="text-align: center; margin-bottom: 0; line-height: 20px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; color: #fff; margin-top: 20px; font-size: 17px; margin: 10px 0px 10px 0px; padding-bottom: 8px;">&nbsp;</h3>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-1" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Design</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_full-width-text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p class="sam_ed-0" style="position: relative; text-align: center; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">Ograniczając boczne krawędzie do minimum nadaliśmy smartfonom Galaxy S8 i S8+ innowacyjny i unikatowy design. <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Płynnie zakrzywione krawędzie ekranu</strong> i <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">zminimalizowane ramki</strong>&nbsp; definiują nową jakość ekranu smartfona. Urządzenie zamknięte w smukłej obudowie doskonale układa się w dłoni.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_KV--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_kolory-7fd7.jpg" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span></div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-s8_design_doskonale_lezy_w_dloni-def0.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Duży ekran, kt&oacute;ry wygodnie leży w dłoni" title="Samsung Duży ekran, kt&oacute;ry wygodnie leży w dłoni" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Duży ekran, kt&oacute;ry wygodnie leży w dłoni</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Smukły Galaxy S8+ posiada&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">ultrasmukłą&nbsp;ramkę</strong>&nbsp;oraz gładkie i zaokrąglone krawędzie (edge), kt&oacute;re nadają mu niesamowity i&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">przykuwający wzrok wygląd</strong>. Konstrukcja 6,2&nbsp;calowego smartfona&nbsp;sprawia, że Galaxy S8+ zawsze&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">wygodnie leży w dłoni</strong>.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-s8_zaokraglony_ekran-366f.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Zaokrąglone krawędzie ekranu (edge)" title="Samsung Zaokrąglone krawędzie ekranu (edge)" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Zaokrąglone krawędzie ekranu (edge)</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Galaxy S8+ ma&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">zaokrąglone krawędzie ekranu</strong>, kt&oacute;re łączą&nbsp;się płynnie z zakrzywionymi krawędziami obudowy, tworząc&nbsp;nie tylko sp&oacute;jny, ale także niezwykle efektowny wygląd. Nowa jakość ekranu to także wyjątkowa wygoda użytkowania urządzenia.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_design_02_wiekszy_ekran-c1c7.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Największy ekran jaki do tej pory stworzyliśmy w serii Galaxy S" title="Samsung Największy ekran jaki do tej pory stworzyliśmy w serii Galaxy S" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Największy ekran jaki do tej pory stworzyliśmy w serii Galaxy S</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Galaxy S8+ posiada r&oacute;wnież&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">największy wyświetlacz</strong>&nbsp;spośr&oacute;d wszystkich dotychczasowych smartfon&oacute;w z serii Galaxy S.&nbsp;Ekran o przekątnej 6,2&nbsp;cala zastosowany w Galaxy S8+ zapewni Ci o wiele&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">szersze pole widzenia</strong> oraz funkcjonalność na wyższym poziomie.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-2" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Wyjątkowy Wyświetlacz Infinity</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_full-width-text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p class="sam_ed-0 sam_text-center" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px; text-align: center;">Ekran&nbsp;Galaxy S8+ posiada niezwykle smukłą&nbsp;ramkę i wyświetla&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">obraz w formacie 18.5:9</strong>, co pozwoli Ci całkowicie <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">przenieść się w centrum wydarzeń</strong>. <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
Duży wyświetlacz Infinity sAMOLED ożywi każdy oglądany obraz <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">bogactwem kolor&oacute;w, dokładnością detali i oszałamiającą rozdzielczością.</strong></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_KV--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_infinity_display-83b6.jpg" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span></div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_mozesz_wiecej-30de.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Zrobisz o wiele więcej na dużym ekranie" title="Samsung Zrobisz o wiele więcej na dużym ekranie" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Zrobisz o wiele więcej na dużym ekranie</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Z Galaxy S8+ możesz więcej, niezależnie od sposobu jego trzymania. <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Filmy odtwarzane w trybie panoramicznym wypełnią wyświetlacz o proporcjach 18.5:9</strong>. <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
Dłuższy ekran umożliwia wyświetlenie<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">&nbsp;większej ilości&nbsp;informacji.</strong></div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_dyskretne_powiadomienia-3f8c.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Dyskretne powiadomienia" title="Samsung Dyskretne powiadomienia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Dyskretne powiadomienia</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Unikniesz dużych wyskakujących powiadomień</strong> w trakcie oglądania filmu lub grania dzięki funkcji Edge Lighting. W momencie otrzymania powiadomienia, krawędzie Galaxy S8+ podświetlą się, a na ekranie pojawi się mały wysuwany komunikat. Taka funkcja pozwoli Ci dalej <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">skupić się na filmie lub grze</strong>.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_skalowany_obraz-4144.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Ekran o idealnej ergonomii" title="Samsung Ekran o idealnej ergonomii" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Ekran o idealnej ergonomii</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Żaden obszar dużego ekranu Galaxy S8+ nie pozostanie poza Twoim zasięgiem.&nbsp;<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
Wystarczy włączyć&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">tryb obsługi jedną ręką</strong>, aby zmniejszyć rozmiar ekranu i mieć pewność, że wszystkie jego skrajne punkty będą w zasięgu Twojego kciuka.&nbsp;Możesz r&oacute;wnież&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">przewinąć ekran gł&oacute;wny w płaszczyźnie pionowej</strong>&nbsp;aby wywołać wszystkie aplikacje, a także zastosować rozwiązanie odwrotne aby je ukryć.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" id="sam_wydajnosc" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_wielozadaniowosc-d658.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Bezproblemowa wielozadaniowość" title="Samsung Bezproblemowa wielozadaniowość" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Bezproblemowa wielozadaniowość</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Większy&nbsp;ekran Galaxy S8+ jest&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">idealnie zaprojektowany do prowadzenia wielozadaniowych działań</strong>. Jeżeli chcesz poczatować z przyjaci&oacute;łmi w trakcie oglądania wideo, po prostu otw&oacute;rz sw&oacute;j ulubiony komunikator oraz odtwarzacz wideo przy użyciu funkcji<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">&nbsp;Wiele Okien</strong>&nbsp;i pisz wiadomości korzystając z pełnowymiarowej klawiatury bez konieczności minimalizowania odtwarzanego obrazu. <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Okno Opisu Ikony</strong>&nbsp;pozwala na wyświetlanie wyłącznie wybranego obszaru, jednocześnie aktualizując treści pojawiające się w tle.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_udogodnienia_nawigacji-53a2.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Udogodnienia wyświetlacza Infinity" title="Samsung Udogodnienia wyświetlacza Infinity" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Udogodnienia wyświetlacza Infinity</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Wyświetlacz Infinity stanowi kontynuację innowacyjnego dziedzictwa firmy Samsung, ale jednocześnie wyznacza nowe trendy. Posiada on&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">nowe przyciski Home</strong>&nbsp;(gł&oacute;wny przycisk),&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Back</strong>&nbsp;(powr&oacute;t) oraz&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Recents</strong>&nbsp;(ostatnio używane).</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-3" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Bezpieczeństwo</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_full-width-text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p class="sam_ed-0 sam_text-center" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px; text-align: center;">W trosce o Twoją prywatność wprowadziliśmy <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">wygodne i skuteczne mechanizmy zabezpieczające</strong> smartfon w postaci <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">skanera tęcz&oacute;wki</strong> oraz niezmiernie <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">szybkie rozpoznawanie twarzy</strong>.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_KV--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_bezpieczenstwo-8f2c.jpg" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span></div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_skanowanie_teczowki-c4a6.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Odblokuj telefon za pomocą skanowania tęcz&oacute;wki" title="Samsung Odblokuj telefon za pomocą skanowania tęcz&oacute;wki" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Odblokuj telefon za pomocą skanowania tęcz&oacute;wki</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Wz&oacute;r tęcz&oacute;wki jest <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">niepowtarzalny i praktycznie niemożliwy do skopiowania</strong>, co oznacza, że uwierzytelnianie za pomocą tęcz&oacute;wki jest jednym z najskuteczniejszych sposob&oacute;w, aby smartfon pozostał zablokowany bez narażania bezpieczeństwa jego zawartości.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_bezpieczenstwo_rozpoznawanie_twarzy-9e7b.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Rozpoznawanie twarzy" title="Samsung Rozpoznawanie twarzy" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Rozpoznawanie twarzy</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">To szybki i łatwy spos&oacute;b odblokowania Galaxy S8+.&nbsp;Podnieś sw&oacute;j smartfon jakbyś chciał zrobić selfie, aby umożliwić rozpoznanie twarzy i w rezultacie odblokować urządzenie.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_bezpieczenstwo_skaner-linii-papilarnych-eb96.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Skaner linii papilarnych" title="Samsung Skaner linii papilarnych" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Skaner linii papilarnych</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Przy podnoszeniu Galaxy S8+ palec naturalnie układa się na znajdującym się z tyłu czujniku linii papilarnych. Smartfon zostaje <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">odblokowany jednym płynnym ruchem</strong>, bez konieczności wybudzania urządzenia z trybu uśpienia.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_bezpieczenstwo_bezpieczny_folder-cc58.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Bezpieczny katalog" title="Samsung Bezpieczny katalog" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Bezpieczny katalog</strong><br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
</span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Twoje prywatne dane będą chronione</strong> w Bezpiecznym folderze opartym o platformę KNOX. System zabezpieczania danych Samsung KNOX zapewnia także nieprzerwaną <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">ochronę przed atakami hakerskimi i programami typu malware</strong>. Możesz nawet utworzyć <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">osobne konta dla każdej aplikacji</strong>, aby wrażliwe dane nie dostały się w niepowołane ręce.&nbsp;Teraz do oddzielnej, ukrytej przestrzeni możesz przenosić nawet aplikacje.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Doskonałe Aparaty</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_full-width-text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p class="sam_ed-0 sam_text-center" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px; text-align: center;">Robienie zdjęć <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">przy słabym oświetleniu</strong> dotychczas stanowiło problem. Dlatego wyposażyliśmy Galaxy S8+ w doskonały aparat, kt&oacute;ry <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">sprawdza się r&oacute;wnie dobrze w świetle dnia, jak i w nocy</strong>.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_KV--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-s8_aparat_soczewka-3a05.jpg" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span></div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-20--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 10px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_icon_heading_text_packshot--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; overflow-x: hidden;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-md-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<div class="sam_row sam_icon_heading_text_packshot--module-clone-row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; margin-bottom: 20px; position: relative;">
<div class="sam_col-md-3 sam_col-sm-2 sam_col-xs-3 sam_ed-0" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 16.66666667%;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_obszar-roboczy-10-a9cb.png" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Tylny aparat 12 Mpix F1.7 z matrycą Dual Pixel" title="Samsung Tylny aparat 12 Mpix F1.7 z matrycą Dual Pixel" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></div>
<div class="sam_col-md-9 sam_col-sm-10 sam_col-xs-9" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 83.33333333%;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 0px 0px 10px 0px;"><span style="color: #000000; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><strong class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Tylny aparat 12 Mpix F1.7 z matrycą Dual Pixel</strong></span></h3>
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">Za pomocą Galaxy S8+&nbsp;uwiecznisz ważne chwile nawet w czasie wieczornego spaceru czy podczas imprezy w przyciemnionych światłach klubu. Jasny obiektyw F1.7 i duże piksele 1,4&nbsp;&micro;m gwarantują&nbsp;więcej światła, nawet gdy na zewnątrz nie ma go pod dostatkiem.&nbsp;Matryca Dual Pixel w aparatach Galaxy S8+ ustawia ostrość tak samo szybko jak ludzkie oko.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
<div class="sam_row sam_icon_heading_text_packshot--module-clone-row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; margin-bottom: 20px; position: relative;">
<div class="sam_col-md-3 sam_col-sm-2 sam_col-xs-3 sam_ed-0" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 16.66666667%;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_prosty_interfejs-a9cb.png" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Prosty interfejs użytkownika" title="Samsung Prosty interfejs użytkownika" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></div>
<div class="sam_col-md-9 sam_col-sm-10 sam_col-xs-9" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 83.33333333%;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 0px 0px 10px 0px;"><span style="color: #000000; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><strong class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Prosty interfejs użytkownika</strong></span></h3>
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">W Galaxy S8+ sterujesz aparatem&nbsp;poprzez przesunięcie palcem po ekranie. Za pomocą jednej ręki możesz zmieniać kierunek aparatu, tryb jego pracy oraz przełączać&nbsp;filtry. Najważniejsze ustawienia masz zawsze pod ręką&nbsp;w prostym, szybkim i łatwym w obsłudze menu.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
<div class="sam_row sam_icon_heading_text_packshot--module-clone-row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; margin-bottom: 20px; position: relative;">
<div class="sam_col-md-3 sam_col-sm-2 sam_col-xs-3 sam_ed-0" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 16.66666667%;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_selfie-a9cb.png" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Dobrze zrobione selfie" title="Samsung Dobrze zrobione selfie" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></div>
<div class="sam_col-md-9 sam_col-sm-10 sam_col-xs-9" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 83.33333333%;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 0px 0px 10px 0px;"><span style="color: #000000; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><strong class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Dobrze zrobione selfie</strong></span></h3>
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">Przedni aparat Galaxy S8+&nbsp;z inteligentnym autofocusem umożliwia zrobienie selfie bardzo wysokiej jakości bez względu na panujące warunki. Dzięki udoskonalonemu przetwarzaniu obrazu wszystkie autoportrety są doskonale wyraźne i ostre.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
<div class="sam_row sam_icon_heading_text_packshot--module-clone-row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; margin-bottom: 20px; position: relative;">
<div class="sam_col-md-3 sam_col-sm-2 sam_col-xs-3 sam_ed-0" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 16.66666667%;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_filtry-a9cb.png" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Szybko dostępne filtry" title="Samsung Szybko dostępne filtry" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></div>
<div class="sam_col-md-9 sam_col-sm-10 sam_col-xs-9" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 83.33333333%;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 0px 0px 10px 0px;"><span style="color: #000000; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><strong class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Szybko dostępne filtry</strong></span></h3>
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">Dzięki&nbsp;predefiniowanym filtrom&nbsp;w Galaxy S8+ łatwo ustawisz&nbsp;parametry zdjęcia zanim je jeszcze zrobisz. Użyj dowolnych filtr&oacute;w Auto&nbsp;(podstawowy), Upiększanie Twarzy&nbsp;(uroda) lub trybu Jedzenie&nbsp;(przeznaczonego do fotografowania żywności) by&nbsp;wyświetlić od razu podgląd zdjęcia. Możesz także pobrać więcej filtr&oacute;w umożliwiających uzyskanie kreatywnych efekt&oacute;w.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
<div class="sam_row sam_icon_heading_text_packshot--module-clone-row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; margin-bottom: 20px; position: relative;">
<div class="sam_col-md-3 sam_col-sm-2 sam_col-xs-3 sam_ed-0" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 16.66666667%;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_pole_ostrosci-a9cb.png" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Wybi&oacute;rcze pole ostrości" title="Samsung Wybi&oacute;rcze pole ostrości" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></div>
<div class="sam_col-md-9 sam_col-sm-10 sam_col-xs-9" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 83.33333333%;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 0px 0px 10px 0px;"><span style="color: #000000; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><strong class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Wybi&oacute;rcze pole ostrości</strong></span></h3>
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">W trybie wybi&oacute;rczego pola ostrości możesz zdecydować o tym, jak ustawić głębię ostrości po wykonaniu zdjęcia. Po prostu wybierz to, co na fotografii będzie rozmyte: pierwszy plan, tło lub wszystko.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-5" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Wydajność</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_full-width-text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p class="sam_ed-0 sam_text-center" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px; text-align: center;">Z Galaxy S8+ masz jeszcze więcej możliwości. Smartfon jest&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">odporny na wodę i pył</strong>*&nbsp;oraz posiada&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">potężny procesor</strong>&nbsp;wykonany w technologii 10 nm, kt&oacute;ry gwarantuje&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">bezproblemową wielozadaniowość</strong>.&nbsp;Twoje ulubione piosenki zabrzmią lepiej dzięki nowym, dołączonym do zestawu&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">słuchawkom stworzonym we wsp&oacute;łpracy z firmą AKG</strong>, a także dzięki udoskonalonej technologii audio. Galaxy S8+ posiada r&oacute;wnież&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">rozszerzalną pamięć</strong>, kt&oacute;rą możesz zapełnić wszystkimi swoimi filmami, zdjęciami oraz muzyką.&nbsp;<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<span class="sam_text-star-content" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #999999; font-size: 11px; line-height: 15px; margin-top: 10px; display: inline-block;">*Odporny na wodę do głębokości 1,5 metra przez 30 minut.&nbsp;</span></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_KV--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_wydajnosc-85c0.jpg" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span></div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-20--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 10px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-04_wydajnosc_procesor_10nm-2b24.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Aplikacje ładują się szybciej i działają płynniej" title="Samsung Aplikacje ładują się szybciej i działają płynniej" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Aplikacje ładują się szybciej i działają płynniej</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Galaxy S8+ jest napędzany procesorem wykonanym w technologii 10 nm, co gwarantuje <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">bezproblemową wielozadaniowość przy jeszcze mniejszym zużyciu baterii</strong>. Możesz teraz czerpać przyjemność z płynnej, bezproblemowej rozrywki w trakcie grania w zaawansowane graficznie gry.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_04_wydajnosc_szybsze-pobieranie-63dc.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Jeszcze szybsze pobieranie" title="Samsung Jeszcze szybsze pobieranie" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Jeszcze szybsze pobieranie</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Prędkość pobierania jest <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">do 20% wyższa</strong> w por&oacute;wnaniu z poprzednim modelem*. Galaxy S8+ może obsługiwać <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Gigabit LTE</strong> i <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Gigabit Wi-Fi</strong>. Wi-Fi w Galaxy S8+ obsługuje 1024-QAM. Jeśli więc pobierasz duże pliki lub udostępniasz swoje dane, prędkość nie stanowi już problemu.</div>
<p class="sam_text-star-content sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #999; font-size: 10px; line-height: 14px; margin-top: 10px; display: inline-block;">*Galaxy S8 i S8+ są do 20% szybsze niż Galaxy S7 z Wi-Fi (802.11ac). Rzeczywista prędkość zależy od kraju, operatora i innych czynnik&oacute;w użytkowania. Wymagana jest obsługa routera 1024-QAM.</p>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_04_wydajnosc_granie-na-telefonie-63dc.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Graj na zupełnie nowym poziomie" title="Samsung Graj na zupełnie nowym poziomie" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Graj na zupełnie nowym poziomie</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Twoje ulubione gry wejdą na <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">najwyższy poziom</strong>. Grając na wyświetlaczu Infinity Galaxy S8+, dzięki możliwościom jakie daje Vulkan API, masz przed sobą <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">całe godziny wciągającej zabawy</strong>, nawet w przypadku gier z <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">bardzo wymagającą grafiką</strong>.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" id="sam_dzwiek" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_04_wydajnosc_dzwiek_akg2-2cb0.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Krystalicznie czysty dźwięk" title="Samsung Krystalicznie czysty dźwięk" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Krystalicznie czysty dźwięk</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Miłośnicy muzyki mogą cieszyć się&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">doskonałej jakości dźwiękiem</strong>. Galaxy S8+ wyposażono w obsługę 32-bitowego kodowania PCM i systemu zapisu dźwięku DSD64/128*.&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Nowe słuchawki opracowane przez AKG</strong>, oferują dźwięk w jakości premium. Udoskonaliliśmy też budowę słuchawek aby zapewnić większy komfort dla ucha, podczas gdy Ty delektujesz się muzycznymi doznaniami.</div>
<p class="sam_text-star-content sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #999; font-size: 10px; line-height: 14px; margin-top: 10px; display: inline-block;">*Odtwarzanie DSD64 i DSD128 może być ograniczone, w zależności od formatu pliku.</p>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" id="sam_odpornosc" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_04_wydajnosc_ip68-63dc.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Odporność na wodę i pył" title="Samsung Odporność na wodę i pył" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Odporność na wodę i pył</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Galaxy S8+ jest&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">odporny na wodę i pył</strong>&nbsp;(IP68*).&nbsp;Możesz zabrać go <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">na plażę czy na basen</strong>, nie martwiąc się o zalanie i uszkodzenie urządzenia.</div>
<p class="sam_text-star-content sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #999; font-size: 10px; line-height: 14px; margin-top: 10px; display: inline-block;">*Odporny na wodę do głębokości 1,5 metra przez 30 minut.</p>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_04_wydajnosc_tryby-wydajnosci-3c3e.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Tryb wydajności dostosowany do Twoich potrzeb" title="Samsung Tryb wydajności dostosowany do Twoich potrzeb" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Tryb wydajności dostosowany do Twoich potrzeb</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Galaxy S8+ oferuje <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">cztery tryby wydajności</strong>. Domyślnie ustawiony jest <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">tryb optymalizacji</strong>, kt&oacute;ry zapewnia dłuższy czas pracy baterii oraz bezproblemową wielozadaniowość. <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Tryb rozrywki</strong> jest idealny do oglądania film&oacute;w w wysokiej rozdzielczości, a <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">tryb gry</strong> stwarza perfekcyjne warunki do grania. Aby m&oacute;c prawdziwie docenić najwyższą jakość obrazu, włącz<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"> tryb wysokiej wydajności</strong>.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_4_kolumny--module" id="sam_pamiec" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-sm-3 sam_col-xs-6" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 25%;">
<h3 class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 18px; height: 40px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Pamięć z możliwością rozbudowy</strong></h3>
<span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_04_wydajnosc_pamiec-z-mozliwoscia-rozbud-63dc_2.jpg" class="sam_center-block sam_img-responsive" alt="Samsung Pamięć z możliwością rozbudowy" title="Samsung Pamięć z możliwością rozbudowy" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span>
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 20px 0;">
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">64 GB pamięci</strong> wewnętrznej spokojnie wystarczą na&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">codzienne potrzeby.</strong>&nbsp;Jeżeli chcesz rozszerzyć pamięć, możesz zastosować kartę pamięci&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">microSD o pojemności do 256 GB</strong>*.<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<span class="sam_text-star-content" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #999999; font-size: 11px; line-height: 15px; margin-top: 10px; display: inline-block;">* Karta nie jest dołączona do zestawu</span></p>
</div>
</div>
<div class="sam_col-sm-3 sam_col-xs-6" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 25%;">
<h3 class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 18px; height: 40px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Dłuższa praca na jednym ładowaniu</strong></h3>
<span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_04_wydajnosc_dluga-praca-na-jednym-ladow-63dc.jpg" class="sam_center-block sam_img-responsive" alt="Samsung Dłuższa praca na jednym ładowaniu" title="Samsung Dłuższa praca na jednym ładowaniu" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span>
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 20px 0;">
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">Wydajny procesor zapewnia bardziej efektywne wykorzystanie baterii. Dodatkowo wygodne <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">ładowanie bezprzewodowe i szybkie ładowanie baterii</strong> pozwoli Ci zaoszczędzić czas i wyeliminuje nerwowe chwile.</p>
</div>
</div>
<div class="sam_col-sm-3 sam_col-xs-6" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 25%;">
<h3 class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 18px; height: 40px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Ładowanie bezprzewodowe</strong></h3>
<span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_04_wydajnosc_szybkie-ladowanie-bezprzewo-27f0.jpg" class="sam_center-block sam_img-responsive" alt="Samsung Ładowanie bezprzewodowe" title="Samsung Ładowanie bezprzewodowe" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span>
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 20px 0;">
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">Bezprzewodowe<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"> ładowanie jest tak łatwe</strong>, jak odłożenie smartfona na stolik. <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Z szybkim ładowaniem w Galaxy S8+ Tw&oacute;j smartfon odzyskuje sprawność w kr&oacute;tkim czasie</strong>.<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<span class="sam_text-star-content" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #999999; font-size: 11px; line-height: 15px; margin-top: 10px; display: inline-block;">* Ładowarka nie jest dołączona do zestawu.</span></p>
</div>
</div>
<div class="sam_col-sm-3 sam_col-xs-6" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 25%;">
<h3 class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 18px; height: 40px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">USB typu C</strong></h3>
<span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/samsung-galaxy_s8_04_wydajnosc_dwustronne-usb-typu-c-63dc.jpg" class="sam_center-block sam_img-responsive" alt="Samsung USB typu C" title="Samsung USB typu C" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span>
<div class="sam_inner_padding" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 20px 0;">
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">W przypadku gniazda USB typu C nie ma znaczenia, kt&oacute;rą stroną włożysz wtyczkę do gniazda.</p>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" id="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-7" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Aplikacje Samsung</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_full-width-text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p class="sam_ed-0 sam_text-center" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px; text-align: center;">Korzystając z Galaxy S8+ <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">znajdziesz aplikację</strong>, kt&oacute;rej szukasz o wiele <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">łatwiej niż kiedykolwiek</strong>.&nbsp;Wszystkie <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">ikony aplikacji są oznaczone kolorami</strong>, dzięki czemu można je z łatwością zidentyfikować. Wygląd&nbsp;ikon został r&oacute;wnież poddany modyfikacji,&nbsp;&nbsp;w wyniku czego powstały ikony składające się z prostych, przerywanych linii.</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_KV--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-s8_karta_intro_aplikacje-39c8.jpg" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span></div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" id="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-8" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Smart Switch</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_smart_switch-0762.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Szybkie przenoszenie plik&oacute;w z aplikacją Smart Switch" title="Samsung Szybkie przenoszenie plik&oacute;w z aplikacją Smart Switch" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><strong class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Szybkie przenoszenie plik&oacute;w z aplikacją Smart Switch</strong></span></h3>
<p class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">Nie chcesz stracić treści ze starego telefonu lub ręcznie ich przenosić? Wraz z Samsungiem Galaxy otrzymujesz dostęp do najnowszej aplikacji Smart Switch. Dzięki niej <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">prześlesz ze starego urządzenia na Galaxy</strong>: muzykę, filmy, kontakty, notatki, zdjęcia, wiadomości, kalendarz a nawet ustawienia alarmu i Wi-Fi. Smart Switch działa na smartfonach z systemem Android, Symbian, iOS* oraz na BlackBerry.</p>
<p class="sam_text-star-content sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #999; font-size: 10px; line-height: 14px; margin-top: 10px; display: inline-block;">*&nbsp; System iOS pozwala na przenoszenie treści, kt&oacute;re są własnością użytkownika. Nie uwzględnia to muzyki, film&oacute;w, aplikacji oraz zdjęć pobranych z iTunes.</p>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">&nbsp;</div>
<div class="sam_icon_3" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="background: #fcfcfc; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
<div class="sam_col-sm-4 sam_col-xs-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-ico1-4331.png" class="sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 60px !important; margin-left: auto; margin-right: auto; margin: 0 auto;" /></span>
<p class="sam_ed-0  sam_text-center" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 10px auto; color: #616266; font-size: 13px; line-height: 20px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Przenieś treści ze starego </strong><br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">telefonu za pomocą kabla.</strong></p>
</div>
<div class="sam_col-sm-4  sam_col-xs-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-ico2-4331.png" class="sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 60px !important; margin-left: auto; margin-right: auto; margin: 0 auto;" /></span>
<p class="sam_ed-0 sam_text-center" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 10px auto; color: #616266; font-size: 13px; line-height: 20px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Przenieś bezprzewodowo </strong><br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">treści ze starego telefonu.</strong></p>
</div>
<div class="sam_col-sm-4 sam_col-xs-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-ico3-4331.png" class="sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 60px !important; margin-left: auto; margin-right: auto; margin: 0 auto;" /></span>
<p class="sam_ed-0  sam_text-center" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 10px auto; color: #616266; font-size: 13px; line-height: 20px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Przenieś treści z komputera </strong><br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">do nowego Galaxy.</strong></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-4" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">smartfony Galaxy S8 i S8+</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_tabelka_tabela_a5--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<div class="sam_table-gen sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<table class="sam_ed-0" style="width: 100%; position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-collapse: collapse; border-spacing: 0; table-layout: fixed;" data-mce-style="width: 100%;">
    <tbody style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
        <tr class="sam_border-none" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: none;">
            <td width="16.6667%" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">&nbsp;</td>
            <td width="16.6667%" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">&nbsp;</td>
            <td width="33.3333%" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-s8_tabelka-a9dd.png" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></td>
            <td class="sam_ac" width="33.3333%" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-s8_plus_tabelka-2ef5.png" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">&nbsp;</td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">&nbsp;</td>
            <td class="sam_nag" style="line-height: 25px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; text-align: center; color: #888;" data-mce-style="line-height: 25px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Galaxy&nbsp;S8</strong></td>
            <td class="sam_nag sam_ac" style="line-height: 25px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; background: #f1f1f1; text-align: center; color: #888;" data-mce-style="line-height: 25px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Galaxy&nbsp;S8+</strong></td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_design-a9cb.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">WYMIARY / WAGA</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">148.9 x 68.1 x&nbsp; 8.0 mm, 155g</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;">159.5 x 73.4 x 8.1 mm, 173g</td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_wyswietlacz-a9cb.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">EKRAN</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">Wyświetlacz Infinity QHD+ 5.8&quot; <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            (1440x2960 px) <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            Super AMOLED (18.5:9)</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;">Wyświetlacz Infinity QHD+ 6.2&quot; <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            (1440x2960 px) <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            Super AMOLED (18.5:9)</td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_pamiec-56e4.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">PAMIĘĆ</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">4 GB RAM<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            64 GB pamięci wewnętrznej na dane&nbsp;<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            + slot na kartę Micro SD do 256 GB</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;">4 GB RAM<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            64 GB pamięci wewnętrznej na dane <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            + slot na kartę Micro SD do 256 GB</td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_aparat-a9cb.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">APARATY</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Tył</strong>: Dual Pixel 12 MP OIS (F1.7)<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Prz&oacute;d</strong>: &nbsp; 8 MP F1.7 z AF (po raz pierwszy w telefonach Galaxy)</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Tył:</strong> Dual Pixel 12 MP OIS (F1.7)<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Prz&oacute;d:</strong> 8 MP F1.7 z AF (po raz pierwszy w telefonach Galaxy)</td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_wydajnosc-a9cb.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">PROCESOR</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">Procesor Octa core (4x2.3GHz + 4x1.7GHz) 10nm</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;">Procesor Octa core (4x2.3GHz + 4x1.7GHz) 10nm</td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_siec-1ecf.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">SIEĆ</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">LTE Cat.16</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;">LTE Cat.16</td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_lacznosc-35b7.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">ŁĄCZNOŚĆ</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">802.11 a/b/g/n/ac 2.4G+5GHz, VHT80 MU-MIMO<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            Bluetooth&reg; v 5.0 (do &nbsp;2Mbps) , ANT+ , USB Type C</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;">802.11 a/b/g/n/ac 2.4G+5GHz, VHT80 MU-MIMO<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            Bluetooth&reg; v 5.0 (do 2Mbps) , ANT+ , USB Type C</td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_odpornosc-a9cb.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">ODPOR&shy;NOŚĆ</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">Odporność na wodę i pył wg normy IP68*&nbsp;</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;">&nbsp;Odporność na wodę i pył wg normy IP68*</td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_security-a9cb.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">ZABEZPIECZE&shy;NIA</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">skaner tęcz&oacute;wki, czytnik linii papilarnych, rozpoznawanie twarzy&nbsp;</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;">&nbsp;skaner tęcz&oacute;wki, czytnik linii papilarnych, rozpoznawanie twarzy</td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_bateria-a9cb.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">BATERIA</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">3,000mAh<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            ładowanie bezprzewodowe, szybkie ładowanie</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">3,500mAh</strong><br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
            ładowanie bezprzewodowe, szybkie ładowanie</td>
        </tr>
        <tr style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #d4d4d4;">
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_sensory-f658.png" class="sam_img-responsive sam_icon sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: 50px; margin: 0 auto;" /></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">SENSORY</strong></td>
            <td style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px;">Skaner linii papilarnych, akcelerometr, zbliżeniowy, czujnik Halla, natężenia światła, kompas, barometr, pulsu, SpO2 (mierzenie saturacji krwi), Iris, czujnik nacisku.</td>
            <td class="sam_ac" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 6px; font-size: 12px; line-height: 15px; background: #f1f1f1;">Skaner linii papilarnych, akcelerometr, zbliżeniowy, czujnik Halla, natężenia światła, kompas, barometr, pulsu, SpO2 (mierzenie saturacji krwi), Iris, czujnik nacisku.</td>
        </tr>
    </tbody>
</table>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-10" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Akcesoria do Galaxy S8+</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_full-width-text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p class="sam_ed-0 sam_text-center" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px; text-align: center;"><span class="sam_text-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Personalizuj Galaxy S8+</strong> dzięki <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">kolorowym etui</strong>, utrzymuj smartfon <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">zawsze naładowany w podr&oacute;ży</strong> lub <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">zwiększ swoją wydajność</strong> za pomocą r&oacute;żnych narzędzi. Od ochrony po zasilanie &ndash; akcesoria przyjdą Ci z pomocą.<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
</span></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_KV--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_akcesoria-a728.jpg" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span></div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-20--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 10px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-push-6 sam_col-sm-push-6 sam_col-xs-push-0 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%; left: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_05_akcesoria_powerbank-ladowarka-63dc.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Ładowarka bezprzewodowa i powerbank" title="Samsung Ładowarka bezprzewodowa i powerbank" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_col-md-pull-6 sam_col-sm-pull-6 sam_col-xs-pull-0 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%; right: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Ładowarka bezprzewodowa i powerbank</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Nowa bezprzewodowa ładowarka sprawia, że <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">ładowanie smartfona jest tak łatwe, jak odłożenie go na stolik</strong>. Działa zar&oacute;wno jako ładowarka oraz <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">podstawka</strong> co sprawia, że zasilanie jest niezwykle ułatwione.&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Przenośna ładowarka</strong> o pojemności 5100mAh<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;"> ładuje się wyjątkowo szybko</strong> i gromadzi <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">wystarczająco dużo energii</strong> by w każdej chwili doładować baterię w smartfonie.</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_image_text-chess--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #616266; font-size: 11px; line-height: 18px;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row sam_vertical-center" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px; display: flex; -webkit-flex-direction: row; flex-direction: row; -webkit-align-items: center; align-items: center;">
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-right" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 0; float: left; width: 50%;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_05_akcesoria_etui-5beb.jpg" class="sam_img-responsive sam_img-responsive sam_visible-sm" alt="Samsung Ochrona i styl" title="Samsung Ochrona i styl" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto;" /></span></div>
<div class="sam_col-md-6 sam_col-sm-6 sam_col-xs-12 sam_no-padding-left" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 0; padding-right: 10px; float: left; width: 50%;">
<div class="sam_xyz-inner" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 20px; padding-right: 20px;">
<h3 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 19px; color: inherit; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 10px 0px 10px 0px;"><span style="color: rgb(0, 87, 184); position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" class="sam_ed-0"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Ochrona i styl</strong></span></h3>
<div class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Dla Galaxy S8+ została zaprojektowana dedykowana linia wysokiej jakości etui*, kt&oacute;re chronią Tw&oacute;j smartfon,&nbsp;wyświetlają dodatkowy&nbsp;interfejs i doskonale wyglądają.&nbsp;<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Clear View Standing Cover </strong>to także regulowana podstawka. <strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Etui LED View</strong>&nbsp;umożliwia sterowanie smartfonem bez otwierania etui. Możesz także wybrać spośr&oacute;d bogatej gamy kolorystycznej etui Clear Cover, Alcantra czy etui silikonowych.</div>
<p class="sam_text-star-content sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #999; font-size: 10px; line-height: 14px; margin-top: 10px; display: inline-block;">*Etui nie jest dołączane do zestawu</p>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-11" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Zawartość pudełka</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-20--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 10px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_KV--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><img src="/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/sm/samsung-galaxy_s8_in_the_box-8ca7.jpg" class="sam_img-responsive sam_center-block sam_img-responsive sam_visible-sm" alt="" title="" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 0; vertical-align: middle; display: block !important; max-width: 100%; height: auto; margin-left: auto; margin-right: auto;" /></span></div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-20--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 10px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_full-width-text--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p class="sam_ed-0 sam_text-center" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px; text-align: center;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Elementy w zestawie:&nbsp;</strong>Telefon,&nbsp;Słuchawki opracowane przez AKG,&nbsp;Kabel USB typu C,&nbsp;Ładowarka,&nbsp;Złącze USB (C-A),&nbsp;Złącze USB (C-B),&nbsp;Kluczyk do wyjmowania karty SIM,&nbsp;Wkładki douszne,&nbsp;Skr&oacute;cona instrukcja obsługi i ulotka Smart Switch</p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-7" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Co inni sądzą o Galaxy S8+?</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-20--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 10px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_recenzje_jasne--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_testimonials" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<div style="background-color: #e0e0e0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_inner-padding-both" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding-left: 4%; padding-right: 4%; padding-top: 4%; padding-bottom: 2%;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-lg-4 sam_col-sm-4 sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_inner-padding-horizontal sam_quote_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/quote.png&quot;) no-repeat scroll right top;">
<p class="sam_text-larger sam_text-white" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #202020; font-size: 15px; line-height: 20px;"><em class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Nowe Galaxy mimo ekran&oacute;w o większej przekątnej niż w konkurencyjnych konstrukcjach świetnie leżą w dłoni i nie sprawiają wrażenia tak nieporęcznych.<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
</em></p>
<p class="sam_text-bold sam_text-grey sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">Konstanty Młynarczyk, redaktor naczelny magazynu &bdquo;Chip&rdquo;.</p>
</div>
</div>
<div class="sam_col-lg-4 sam_col-sm-4 sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_inner-padding-horizontal sam_quote_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/quote.png&quot;) no-repeat scroll right top;">
<p class="sam_text-larger sam_text-white" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #202020; font-size: 15px; line-height: 20px;"><em class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Panie powinny być zachwycone, bo to chyba pierwszy tak zgrabny telefon z dużym wyświetlaczem <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
</em></p>
<p class="sam_text-bold sam_text-grey sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">Zbigniew Urbański, redaktor w serwisie tech.wp.pl</p>
</div>
</div>
<div class="sam_col-lg-4 sam_col-sm-4 sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; float: left; width: 33.33333333%;">
<div class="sam_inner-padding-horizontal sam_quote_bg" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: url(&quot;/si_upload/ASE/SAMSUNG/Galaxy_S8_plus/s/quote.png&quot;) no-repeat scroll right top;">
<p class="sam_text-larger sam_text-white" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #202020; font-size: 15px; line-height: 20px;"><em class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Staranny design, połączony z najwyższej jakości materiałami i, jeszcze raz, przepotężnymi ekranami dają wrażenie obcowania ze sprzętem futurystycznym, wyraźnie wyprzedzającym epokę.<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
</em></p>
<p class="sam_text-bold sam_text-grey sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;">Lech Okoń, telepolis.pl</p>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-20--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 10px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_separator-30--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; height: 15px; clear: both;">&nbsp;</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_naglowek_wysrodkowany_z_kreskami-2--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12 sam_naglowek_wysrodkowany_z_kreskami-2-wrap" id="sam_section-13" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px; text-align: center;">
<h2 style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: #06c; margin-top: 20px; margin-bottom: 10px; font-size: 17px; margin: 20px 0px 20px 0px; padding-bottom: 0px; text-align: center; position: relative; z-index: 2; text-transform: uppercase; display: inline-block; width: auto;"><span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; background: #fff; padding: 0 10px; display: inline-block; font-weight: bold;">Dodatkowe informacje</span></h2>
<div class="sam_linia" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border-bottom: 1px solid #ccc; position: absolute; top: 50%; left: 10px; right: 10px; z-index: 1;">&nbsp;</div>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_faq--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;"><span class="sam_text-blue sam_ed-0" style="position: relative; color: rgb(0, 87, 184); -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Czy zakrzywiony ekran przeszkadza i nie sprawia, że obszar roboczy wydaje się mniejszy?</strong></span>                                     <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Duży, podw&oacute;jnie zakrzywiony ekran w Galaxy S8+ polepsza wrażenia z używania smartfona. Nawet najzwyklejsza strona w internecie zdaje się zyskiwać nowy wymiar, nie wspominając o zdjęciach czy filmach. Zakrzywiony ekran nie wpływa też negatywnie na czytelność tekstu.</span></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_faq--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;"><span class="sam_text-blue sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #06c;"><strong style="color: #0057b8; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;" data-mce-style="color: #0057b8;">Co zmieniło się w Galaxy S8+ w por&oacute;wnaniu do Galaxy S7 edge?</strong></span>                                     <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Najwiekszą przewagą jest rewolucyjny wyświetlacz Infinity, kt&oacute;ry zajmuję o wiele więcej powierzchni smartfona, dzięki czemu możesz zobaczyć więcej przy zbliżonej wielkości obudowy. Nie posiada także fizycznych przycisk&oacute;w Home, Recent, Back, ale nie ma powodu do zmartwień, zastosowane sensory działają doskonale. Smartfon został dodatkowo zabezpieczony poprzez skaner tęcz&oacute;wki oka oraz rozpoznawanie twarzy. Galaxy S8+ posiada wydajniejszy, ośmiordzenowy procesor w por&oacute;wnaniu do modelu z 2016 roku, dzięki temu bateria starcza na dłużej. Ulepszony tylny i przedni aparat, dodatkowo przedni aparat posiada szybki autofocus. Posiada także gniazdo USB typu C. Wbudowana pamięć została powiększona do 64 GB, zmieścisz więcej swoich danych, a także możesz powiększyć pamięć o dodatkowe 256 GB.</span></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_faq--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;"><span class="sam_text-blue sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #06c;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Co oznacza IP68?</strong></span>                                     <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">IP<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">6</strong>X: Pierwsza cyfra oznacza poziom zabezpieczenia przed pyłem i kurzem. (Poziom 6: całkowita ochrona przed pyłem), IPX<strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">8</strong>: Druga cyfra oznacza poziom zabezpieczenia przed wodą. (Poziom 8: telefon wytrzymuje zanurzenie do głębokości 1,5&nbsp;m przez maks. 30 min)&nbsp;<br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
</span></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_faq--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;"><span class="sam_text-blue sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #06c;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Jak włączyć tryb pełnego ekranu w aplikacjach?</strong></span>                                     <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Tylko niekt&oacute;re aplikacje uruchamiają się domyślnie na pełnym ekranie. Obsługę trybu 18,5:9 można włączyć w ustawieniach.Niekt&oacute;re aplikacje mogą w takiej konfiguracji działać niepoprawnie.</span></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_faq--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;"><span class="sam_text-blue sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #06c;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Tryb Optymalny</strong></span>                                     <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Galaxy S8 działa w Trybie Optymalnym w niższej rozdzielczości - 2220x1080 px, co ma przede wszystkim na celu oszczędność energii. Natomiast r&oacute;żnica w normalnym użytkowaniu urządzenia nie jest dostrzegalna. Z poziomu ustawień można włączyć Tryb Wysokiej Wydajności, kt&oacute;ry najlepiej się sprawdza przy wykorzystaniu z Gear VR.</span></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
<div style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_faq--module" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">
<div class="sam_container-fluid" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-right: auto; margin-left: auto; padding-left: 10px; padding-right: 10px; max-width: 800px; margin: 0 auto;">
<div class="sam_row" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin-left: -10px; margin-right: -10px;">
<div class="sam_col-xs-12" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; position: relative; min-height: 1px; padding-left: 10px; padding-right: 10px;">
<p style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin: 0 0 10px; color: #616266; font-size: 13px; line-height: 20px;"><span class="sam_text-blue sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; color: #06c;"><strong style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; font-weight: bold;">Dodatkowe funkcje skanera linii papilarnych</strong></span>                                     <br style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;" />
<span class="sam_ed-0" style="position: relative; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;">Poza standardowym odblokowywaniem urządzenia można jeszcze używać gest&oacute;w, np. poprzez przesunięcie opuszkiem palca po skanerze linii papilarnych można zwijać i rozwijać panel powiadomień.</span></p>
</div>
<div class="sam_clearfix" style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; clear: both;">&nbsp;</div>
</div>
</div>
</div>
</div>
</div>
</div></div>
	</div>
<button id="expand-description" data-more="Rozwiń opis" data-less="Zwiń opis"></button>
<!-- Przeniesione z zakladaki dane techniczne -->
<div id="description-tech-content" class="has-basic-tech-details">
			<h2 data-tab="description-tech-content">
				Dane techniczne</h2>
			<h3>
	smartfon Samsung Galaxy S8 SM-G950 (Orchid Grey)</h3>
<div class="basic-tech-details">
		

<table class="description-tech-details js-tech-details">
	
	
		
			
				<tr class="group-name">
					<td>
						
							
							
								 Dane podstawowe&nbsp;
							
						
					</td>
			
			
		
		
			
				<td >&nbsp;</td></tr>
				
					<tr>
						<td>
							
								
									<a title="Wyświetlacz telefonu" class="js-dictionary" href="/slownik.bhtml?definitionId=14147900585">
										Wyświetlacz
									</a>
								
								
							
						</td>
						<td>
							
								
								
									5,8 cala 16 mln kolorów SUPER AMOLED Quad HD 2960 x 1440 pikseli
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="System operacyjny - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=320995948">
										System operacyjny
									</a>
								
								
							
						</td>
						<td>
							
								
								
									Android 7 Nougat
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="Pamięć wbudowana - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=2808454592">
										Pamięć wewnętrzna
									</a>
								
								
							
						</td>
						<td>
							
								
								
									64 GB
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="Czytnik kart pamięci - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=14138268062">
										Czytnik kart pamięci microSD
									</a>
								
								
							
						</td>
						<td>
							
								
								
									tak do 256 GB
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="Procesor - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=14154878697">
										Procesor
									</a>
								
								
							
						</td>
						<td>
							
								
								
									ośmiordzeniowy 4 x 2,3 GHz + 4 x 1,9 GHz
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="Pamięć RAM - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=14136441503">
										Pamięć RAM
									</a>
								
								
							
						</td>
						<td>
							
								
								
									4 GB
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Interfejs po polsku&nbsp;
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Złącza&nbsp;
								
							
						</td>
						<td>
							
								
									
										<a title="Złącze słuchawkowe lub mikrofonowe typu jack" class="fancyDict" href="/slownik.bhtml?definitionId=2215267472&productCode=1109569">słuchawkowe typ 3,5 mm jack</a>, 
									
										<a title="USB (Universal Serial Bus)" class="fancyDict" href="/slownik.bhtml?definitionId=320211948&productCode=1109569">USB typ C</a>
									
								
								
							
						</td>
					 
				</tr>
			
					
			
		
			
		
	
		
			
				<tr class="group-name">
					<td>
						
							
							
								 Transmisja danych&nbsp;
							
						
					</td>
			
			
		
		
			
				<td >&nbsp;</td></tr>
				
					<tr>
						<td>
							
								
									<a title="Wi-Fi - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=14146760217">
										Sieć Wi-Fi
									</a>
								
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="NFC (Near Field Communication)" class="js-dictionary" href="/slownik.bhtml?definitionId=2106029164">
										NFC
									</a>
								
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="Bluetooth - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=14146525561">
										Bluetooth
									</a>
								
								
							
						</td>
						<td>
							
								
								
									tak v5.0
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="LTE (Long Term Evolution)" class="js-dictionary" href="/slownik.bhtml?definitionId=2254278444">
										LTE
									</a>
								
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									HSDPA / HSUPA / HSPA+&nbsp;
								
							
						</td>
						<td>
							
								
								
									tak / tak / tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Zakres częstotliwości pracy UMTS/WCDMA&nbsp;
								
							
						</td>
						<td>
							
								
									
										850, 
									
										900, 
									
										1900, 
									
										2100
									
								
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									GPRS / EDGE&nbsp;
								
							
						</td>
						<td>
							
								
								
									tak / tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Zakresy częstotliwości pracy GSM&nbsp;
								
							
						</td>
						<td>
							
								
									
										850, 
									
										900, 
									
										1800, 
									
										1900
									
								
								
							
						</td>
					 
				</tr>
			
					
			
		
			
		
	
		
			
				<tr class="group-name">
					<td>
						
							
							
								 Funkcje multimedialne&nbsp;
							
						
					</td>
			
			
		
		
			
				<td >&nbsp;</td></tr>
				
					<tr>
						<td>
							
								
									<a title="Rozdzielczość aparatu" class="js-dictionary" href="/slownik.bhtml?definitionId=141355516">
										Aparat foto
									</a>
								
								
							
						</td>
						<td>
							
								
								
									12 Mpix
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Opcje aparatu&nbsp;
								
							
						</td>
						<td>
							
								
									
										<a title="Autofocus" class="fancyDict" href="/slownik.bhtml?definitionId=2053418582&productCode=1109569">autofocus</a>, 
									
										<a title="Geotagging" class="fancyDict" href="/slownik.bhtml?definitionId=2250971184&productCode=1109569">geotagging</a>, 
									
										jasność obiektywu f/1.7, 
									
										ledowa lampa błyskowa, 
									
										nagrywanie filmów Ultra HD 4K, 
									
										stabilizator obrazu
									
								
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Aparat przedni&nbsp;
								
							
						</td>
						<td>
							
								
								
									tak 8 mln pikseli
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="Odbiornik GPS - telefony" class="js-dictionary" href="/slownik.bhtml?definitionId=14137904601">
										Nawigacja: odbiornik GPS
									</a>
								
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="odtwarzacz MP3" class="js-dictionary" href="/slownik.bhtml?definitionId=2250980392">
										Odtwarzacz MP3
									</a>
								
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Gry&nbsp;
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
			
					
			
		
			
		
	
		
			
				<tr class="group-name">
					<td>
						
							
							
								 Funkcje telefonu&nbsp;
							
						
					</td>
			
			
		
		
			
				<td >&nbsp;</td></tr>
				
					<tr>
						<td>
							
								
								
									Standardy wysyłania/odbierania wiadomości&nbsp;
								
							
						</td>
						<td>
							
								
									
										e-mail, 
									
										MMS, 
									
										SMS
									
								
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Tryb głośnomówiący&nbsp;
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="Dyktafon w telefonie" class="js-dictionary" href="/slownik.bhtml?definitionId=14139105185">
										Dyktafon
									</a>
								
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Dzwonki MP3 / polifoniczne&nbsp;
								
							
						</td>
						<td>
							
								
								
									tak / tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Kalendarz&nbsp;
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Kalkulator&nbsp;
								
							
						</td>
						<td>
							
								
								
									tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Alarm (budzik) / wibracje&nbsp;
								
							
						</td>
						<td>
							
								
								
									tak / tak
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="Dual SIM" class="js-dictionary" href="/slownik.bhtml?definitionId=2250741248">
										Dual SIM
									</a>
								
								
							
						</td>
						<td>
							
								
								
									nie
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
									<a title="Rodzaj karty Sim" class="js-dictionary" href="/slownik.bhtml?definitionId=8520116867">
										Rodzaj karty SIM
									</a>
								
								
							
						</td>
						<td>
							
								
									
										nano SIM
									
								
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Czujniki&nbsp;
								
							
						</td>
						<td>
							
								
									
										<a title="Akcelerometr" class="fancyDict" href="/slownik.bhtml?definitionId=2053454642&productCode=1109569">akcelerometr</a>, 
									
										barometr, 
									
										czujnik geomagnetyczny, 
									
										<a title="Czujnik światła otoczenia" class="fancyDict" href="/slownik.bhtml?definitionId=2053458504&productCode=1109569">czujnik światła otoczenia</a>, 
									
										czujnik zbliżeniowy, 
									
										czytnik linii papilarnych, 
									
										czytnik tęczówki oka, 
									
										<a title="Cyfrowy kompas" class="fancyDict" href="/slownik.bhtml?definitionId=2542624358&productCode=1109569">kompas</a>, 
									
										pulsometr, 
									
										żyroskop
									
								
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Dodatkowe opcje&nbsp;
								
							
						</td>
						<td>
							
								
									
										<a title="3G" class="fancyDict" href="/slownik.bhtml?definitionId=2053449240&productCode=1109569">3G</a>, 
									
										4G LTE, 
									
										<a title="DLNA (Digital Living Network Alliance)" class="fancyDict" href="/slownik.bhtml?definitionId=264058182&productCode=1109569">DLNA</a>, 
									
										<a title="Dostęp do portali społecznościowych" class="fancyDict" href="/slownik.bhtml?definitionId=2053464982&productCode=1109569">dostęp do portali społecznościowych</a>, 
									
										ekran zakrzywiony na krawędź telefonu, 
									
										<a title="Google Play" class="fancyDict" href="/slownik.bhtml?definitionId=2250764542&productCode=1109569">Google Play</a>, 
									
										<a title="Gorilla Glass" class="fancyDict" href="/slownik.bhtml?definitionId=2257861300&productCode=1109569">Gorilla Glass</a>, 
									
										latarka, 
									
										ładowanie bezprzewodowe, 
									
										możliwość aktualizacji oprogramowania, 
									
										nawigacja A-GPS, 
									
										odporność na kurz i zachlapania, 
									
										S Health, 
									
										<a title="Stopień ochrony IP" class="fancyDict" href="/slownik.bhtml?definitionId=9901520912&productCode=1109569">standard IP68</a>, 
									
										S Translator, 
									
										S Voice Drive, 
									
										S Voice - zaawansowana obsługa głosowa, 
									
										szybkie ładowanie baterii, 
									
										<a title="NFC (Near Field Communication)" class="fancyDict" href="/slownik.bhtml?definitionId=2106029164&productCode=1109569">technologia NFC</a>, 
									
										<a title="Wi-Fi Direct" class="fancyDict" href="/slownik.bhtml?definitionId=2285843296&productCode=1109569">Wi-Fi Direct</a>, 
									
										<a title="Wi-Fi - zakresy" class="fancyDict" href="/slownik.bhtml?definitionId=14147022801&productCode=1109569">Wi-Fi dwuzakresowe (2.4G/5G)</a>
									
								
								
							
						</td>
					 
				</tr>
			
					
			
		
			
		
	
		
			
				<tr class="group-name">
					<td>
						
							
							
								 Parametry fizyczne&nbsp;
							
						
					</td>
			
			
		
		
			
				<td >&nbsp;</td></tr>
				
					<tr>
						<td>
							
								
									<a title="Rodzaj obudowy" class="js-dictionary" href="/slownik.bhtml?definitionId=383200236">
										Typ obudowy
									</a>
								
								
							
						</td>
						<td>
							
								
								
									klasyczna - ekran dotykowy
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Barwa obudowy&nbsp;
								
							
						</td>
						<td>
							
								
								
									szary
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Akumulator&nbsp;
								
							
						</td>
						<td>
							
								
								
									3000 mAh niewymienny
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Rozmiary (szer. x wys. x głęb.)&nbsp;
								
							
						</td>
						<td>
							
								
								
									148,9 x 68,1 x 8 mm
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Waga&nbsp;
								
							
						</td>
						<td>
							
								
								
									152 g
									
								
							
						</td>
					 
				</tr>
			
					
			
		
			
		
	
		
			
				<tr class="group-name">
					<td>
						
							
							
								 Wyposażenie&nbsp;
							
						
					</td>
			
			
		
		
			
				<td >&nbsp;</td></tr>
				
					<tr>
						<td>
							
								
								
									Wyposażenie&nbsp;
								
							
						</td>
						<td>
							
								
									
										adapter microUSB - USB C, 
									
										adapter USB - USB C, 
									
										instrukcja obsługi w języku polskim, 
									
										<a title="Przewód USB" class="fancyDict" href="/slownik.bhtml?definitionId=141355416&productCode=1109569">kabel USB</a>, 
									
										karta gwarancyjna, 
									
										ładowarka sieciowa, 
									
										słuchawki AKG
									
								
								
							
						</td>
					 
				</tr>
			
					
			
		
			
		
	
		
			
				<tr class="group-name">
					<td>
						
							
							
								 Gwarancja&nbsp;
							
						
					</td>
			
			
		
		
			
				<td >&nbsp;</td></tr>
				
					<tr>
						<td>
							
								
								
									Gwarancja&nbsp;
								
							
						</td>
						<td>
							
								
								
									24 miesiące
									
										<a href="#" class="js-warranty-scroll"><br/><span class="arrow-raquo">&nbsp;&nbsp;</span><strong> Rozszerz ochronę o przypadkowe uszkodzenia!</strong></a>
									
								
							
						</td>
					 
				</tr>
					<tr>
						<td>
							
								
								
									Szczegółowe warunki gwarancji&nbsp;
								
							
						</td>
						<td>
							
								
								
									<a href="//f00.osfr.pl/product_warranty_terms/13925125615/samsung-ogolny.pdf" class="f12" target="_blank">Pobierz <span class="arrow-raquo">&nbsp;&nbsp;</span></a>
									
								
							
						</td>
					 
				</tr>
			
					
			
		
			
		
	
		
			
				<tr class="group-name">
					<td>
						
							
							
								 Producent&nbsp;
							
						
					</td>
			
			
		
		
			
				<td >&nbsp;</td></tr>
				
					<tr>
						<td>
							
								
								
									Nazwa producenta / importera&nbsp;
								
							
						</td>
						<td>
							
								
								
									Samsung Electronics Polska Sp. z o.o.
									
								
							
						</td>
					 
				</tr>
			
					
			
		
			
		
	
		
			
				<tr class="group-name">
					<td>
						
							
							
								 Znak zgodności&nbsp;
							
						
					</td>
			
			
		
		
			
				<td >&nbsp;</td></tr>
				
					<tr>
						<td>
							
								
								
									Znak zgodności&nbsp;
								
							
						</td>
						<td>
							
								
								
									<div class="conformity-mark"><span class="mark-icon" style="background: url('//f01.osfr.pl/foto/conformity-mark-logos/8691544597.png') no-repeat center center;"></span><span class="mark-tip"></span></div>
									
								
							
						</td>
					 
				</tr>
			
					
			
		
			
		
	
	
	
		<tr class="wastes-management-cost">
			<td>Koszt gospodarowania odpadami</td>
			<td>nano Sim, żyroskop, Gorilla Glass (wliczony w cenę towaru)</td>
		</tr>
	
	
</table>
				</div>
</div>
	<a href="#" class="report-bug js-report-bug"  data-type="PRODUCT_CARD">
    Zgłoś błąd na tej stronie</a>
<div id="similar-products">
		<a id="find-similar-products" href="/telefony-komorkowe,_Samsung,od3000,g1.bhtml">
			Znajdź podobne produkty</a>
		<ul>
			<li>
					Marka: <span>Samsung</span>
				</li>
			<li>
					Cena: <span>od 3 000</span>
				</li>
			<li>
					Rodzaj: <span>smartfon</span>
				</li>
			</ul>
	</div>
</div>
					</div>
		</div>
	<div class="product-tab product-tab-opinie">
			<div class="product-tab-header" data-tab="opinie">
				Ocena klientów</div>
			<div class="product-tab-content">
				<div id="opinions">
							<div id="add-opinion-form" class="add-opinion-form-hidden">
	</div>
<div class="opinion-buttons">
					<button class="add-opinion js-add-opinion">Dodaj opinię</button>
				</div>
			<div class="opinion-statistics">
	<div class="opinion-statistics-grades">
	<div class="stars-rating">
		<div>
			<span style="width:100%"></span>
		</div>
	</div>
	<div class="opinion-summary-total">
		<span>5</span>
		<span>
			na podstawie 1 ocen</span>
	</div>

	<div class="grade-item js-load-grade-opinion" data-grade="831189955" data-value="5">
			<div class="attribute">
				5</div>
			<div class="grade">
				(1)
			</div>
			<div class="graph">
				<div style="width:100.0%"></div>
			</div>
		</div>
	<div class="grade-item js-load-grade-opinion" data-grade="831189953" data-value="4">
			<div class="attribute">
				4</div>
			<div class="grade">
				(0)
			</div>
			<div class="graph">
				<div style="width:0.0%"></div>
			</div>
		</div>
	<div class="grade-item js-load-grade-opinion" data-grade="831189951" data-value="3">
			<div class="attribute">
				3</div>
			<div class="grade">
				(0)
			</div>
			<div class="graph">
				<div style="width:0.0%"></div>
			</div>
		</div>
	<div class="grade-item js-load-grade-opinion" data-grade="831189949" data-value="2">
			<div class="attribute">
				2</div>
			<div class="grade">
				(0)
			</div>
			<div class="graph">
				<div style="width:0.0%"></div>
			</div>
		</div>
	<div class="grade-item js-load-grade-opinion" data-grade="831189947" data-value="1">
			<div class="attribute">
				1</div>
			<div class="grade">
				(0)
			</div>
			<div class="graph">
				<div style="width:0.0%"></div>
			</div>
		</div>
	</div><div class="opinion-best-attributes">
	<div>
					Najwyżej ocenione zalety:</div>
				<ul>
					<li class="attribute">Jakość wyświetlacza</li>
					<li class="attribute">Design</li>
					</ul>
			<div class="opinion-high-grades">
		<span class="percentage-value">100%</span>&nbsp;osób oceniło ten produkt wysoko</div>
</div></div><div id="opinion-banner">
			</div>

		<div id="opinion-list">
			<div id="opinion-filter">
	<div class="opinion-filter-sort">
		<label>Sortuj wg:</label>
		<div class="flat-select sort-select">
			<div class="select3-container theme-opinion-filter-sort">
	<div class="select3-selected-item">
		Wczytywanie...</div>
	<div class="select3-option-list select3-list-hidden">
		<div class="select3-option-list-wrap">
			<div data-value="DEFAULT" class="select3-option
	 select3-option-selected
">
	domyślne</div>
<div data-value="GRADE_DESC" class="select3-option
	
">
	oceny malejąco</div>
<div data-value="GRADE_ASC" class="select3-option
	
">
	oceny rosnąco</div>
<div data-value="DATE_ADDED_DESC" class="select3-option
	
">
	daty dodania malejąco</div>
<div data-value="DATE_ADDED_ASC" class="select3-option
	
">
	daty dodania rosnąco</div>
<div data-value="HELPFUL_PERCENTAGE_DESC" class="select3-option
	
">
	przydatności malejąco</div>
<div data-value="HELPFUL_PERCENTAGE_ASC" class="select3-option
	
">
	przydatności rosnąco</div>
</div>
	</div>
	<div class="select3-selection-icon">
	</div>
	<input
		data-autocomplete=""
		 id="sort-select"
		data-js="1"
		name="sortSelect"
		class="select3-input"
		
	/>
</div>
</div>
	</div>

	<div class="opinion-filter-stars js-opinion-filter-stars">
		<label>Pokaż opinie:</label>
		<div class="filter-stars-current filter-stars-current-visible">wszystkie</div>
		<div class="stars-rating" data-stars="831189955">
					<div>
						<span class="active" style="width:100%"></span>
					</div>
				</div>
			<div class="opinion-filter-stars-list">
			<div class="js-opinion-filter-stars-option opinion-filter-stars-option-all" data-stars="" data-value="0">
				wszystkie</div>
			<div class="js-opinion-filter-stars-option stars-rating" data-stars="831189955" data-value="5">
						<div>
							<span class="active" style="width:100%"></span>
						</div>
					</div>
				</div>
	</div>
</div><div class="opinion-item" id="opinion-16805878808">
				<div class="opinion-item-header-info">
	<div class="opinion-title">
			 S8</div>
	<div class="opinion-stars">
		<div class="stars-rating js-opinion-stars " title="rewelacyjny">
			<div>
				<span style="width:100%"></span>
			</div>
		</div>
		<div class="opinion-item-grades">
		<div class="grade-item">
				<span class="attribute">Jakość wyświetlacza:</span>
				<div class="stars-rating" title="rewelacyjny">
					<div>
						<span class="active" style="width:100%"></span>
					</div>
				</div>
			</div>
		<div class="grade-item">
				<span class="attribute">Design:</span>
				<div class="stars-rating" title="rewelacyjny">
					<div>
						<span class="active" style="width:100%"></span>
					</div>
				</div>
			</div>
		<div class="grade-item">
				<span class="attribute">Żywotność baterii:</span>
				<div class="stars-rating" title="rewelacyjny">
					<div>
						<span class="active" style="width:100%"></span>
					</div>
				</div>
			</div>
		<div class="grade-item">
				<span class="attribute">Bezawaryjność:</span>
				<div class="stars-rating" title="rewelacyjny">
					<div>
						<span class="active" style="width:100%"></span>
					</div>
				</div>
			</div>
		<div class="grade-item">
				<span class="attribute">Wytrzymałość:</span>
				<div class="stars-rating" title="rewelacyjny">
					<div>
						<span class="active" style="width:100%"></span>
					</div>
				</div>
			</div>
		<div class="grade-item">
				<span class="attribute">Łatwość użycia:</span>
				<div class="stars-rating" title="rewelacyjny">
					<div>
						<span class="active" style="width:100%"></span>
					</div>
				</div>
			</div>
		</div>
</div>
	
	<div class="opinion-additional-info">
	<div class="opinion-date">
		03-08-2017,
	</div>
	<div class="opinion-nick">
			siara</div>
	<div class="opinion-item-header-images">
		<div class="js-show-tooltip customer-confirmed" data-tooltip-id="hidden-customer-confirmed">Potwierdzony zakup</div>
			</div>
</div></div><div class="opinion-content">
	<div id="full-opinion16805878808" class="opinion-text js-opinion-text" data-max-length="253">
		<p> Polecam świetny telefon iphone się chowa;)</p>
		</div>
	<a href="#" class="js-toggle-content toggle-content" data-toggle="16805878808">Pokaż całość</a>
<div class="opinion-useful">
    <div id="rate-box16805878808" class="rate-box">
		<span class="opinion-helpful-question">
					Przydatna opinia?<a href="#" class="opinion-helpful-yes js-opinion-helpful" data-helpful="yes" data-id="16805878808"></a>
					<span id="helpful-quantity-16805878808" class="opinion-helpful-yes-number">(0)</span>
					<a href="#" class="opinion-helpful-no js-opinion-helpful" data-helpful="no" data-id="16805878808"></a>
					<span id="not-helpful-quantity-16805878808" class="opinion-helpful-no-number">(0)</span>
				</span>
			<span id="vote-yes-off-16805878808" class="vote-yes-off status">Według Ciebie ta opinia jest przydatna</span>
				<span id="vote-no-off-16805878808" class="vote-no-off status">Według Ciebie ta opinia jest nieprzydatna. Możesz&nbsp;<a href="#" class="js-report-abuse" data-opinion-id="16805878808" data-code="1109569">zgłosić nadużycie.</a></span>
				<span id="vote-added-16805878808" class="vote-added status">Już oddałeś głos na tę opinię</span>
			</div>
</div>
<div class="opinion-variants">
	Opinia dotyczy wariantu:<div class="variant">czarny</div>
	<a href="/telefony-komorkowe/samsung-galaxy-s8-sm-g950-czarny.bhtml#opinia-16805878808">Przejdź do produktu</a>
</div>
</div><div id="opinion-question16805878808" class="ask-question"></div></div>
			<div id="hidden-customer-confirmed" class="tooltip-description">Opinia została wydana na podstawie zakupionego towaru. Możemy zagwarantować, że autor zakupił opisywany sprzęt w sklepie OleOle! Wszystkie informacje zawarte w opinii są poparte prawdziwym doświadczeniem! Masz pytania? Możesz je zadać tutaj!</div>
		<div id="hidden-opinion-expert" class="tooltip-description">Jest to opinia niezależnego eksperta. Opinia powstała w wyniku testów recenzowanego produktu.</div>
	</div>
	</div>
					</div>
		</div>
	<div class="product-tab product-tab-uslugi">
			<div class="product-tab-header" data-tab="uslugi">
				Usługi</div>
			<div class="product-tab-content">
				<div id="services">
							<div class="group">
		<div class="title">Opcje</div>
		<div class="item service-item-3099258640" data-service-id="3099258640">
				<div class="product-photo js-service-info">
				    <img alt="Mam zu&#380;yty sprz&#281;t do oddania" class="lazy " src="/images/www/emptyImage.png" data-original="//f01.osfr.pl/img/desktop/ole/default-service.png" /></div>
				<div class="product-info">
					<div class="name js-service-info" id="service-name-3099258640">
						Mam zużyty sprzęt do oddania</div>
					<div class="short-desc js-service-info">
						<br /></div>
					<div class="actions">
						<span class="code">
							nr kat. 489</span>
						<a href="#" id="service-3099258640" class="js-service-info">
							Więcej</a>
						</div>

					<div class="product-sales-box">
						<div class="add-service-with-product">
								<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="services" class="js-add-service-checkbox" value="3099258640" data-service-price=""
/><i></i>
	<span class="toggle-text"
						 data-checked="Wybrane" data-unchecked="Wybieram"
				>
				</span>
			</span>
	</label>
</div>
						</div>
				</div>
			</div>
		</div>
</div>
					</div>
		</div>
	<div class="product-tab product-tab-gwarancje">
			<div class="product-tab-header" data-tab="gwarancje">
				Ekstra Ochrona</div>
			<div class="product-tab-content">
				<div id="warranties" class="warranties">
							<div class="intro">
		<p style="margin-left: 10px; margin-bottom: 10px;">Nie płać za naprawę lub wymianę sprzętu z własnej kieszeni. Z Ekstra Ochroną przez najbliższe 3 lub 5 lat będziesz mieć zapewniony spok&oacute;j i wygodę, aby w pełni cieszyć się nowym sprzętem.</p>
<p style="margin-left: 10px; margin-bottom: 10px;">Ekstra Ochrona to umowa ubezpieczenia (polisa) zawierana między Tobą, a ubezpieczycielem. Polisa nie stanowi gwarancji jakości w rozumieniu art. 577-581 k.c., ani też rękojmi za wady fizyczne lub prawne. Zakres ochrony i wyłączenia odpowiedzialności ubezpieczycieli opisują Warunki Ubezpieczenia.</p></div>

	<table class="columns-3">
	<colgroup>
		<col class="info-warranty"/>
		<col class="extended-warranty"/>
		<col class="basic-warranty"/>
		<col class="standard-warranty"/>
	</colgroup>
	<thead>
		<tr>
			<th>
				</th>
			<th class="warranty-icon warranty-extended"><div/></th>
			<th class="warranty-icon warranty-basic"><div/></th>
			<th class="warranty-icon warranty-standard"><div/></th>
		</tr>
		<tr>
			<th>
				<div>Zyskujesz</div>
			</th>
			<th class="warranty-icon">
					<div>Ochrona rozszerzona</div>
				</th>
			<th class="warranty-icon">
					<div>Ochrona podstawowa</div>
				</th>
			<th class="warranty-icon">
				<div>Standardowa gwarancja</div>
			</th>
		</tr>
	</thead>
	<tbody>
	<tr>
		<th>Od dnia zakupu<span class="mtuppgfix none"> przez 12 miesięcy</span></th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<td>Szybka naprawa w razie przypadkowego uszkodzenia</td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-unavailable" title="Niedostępna"></span></td>
		<td><span class="warranty-unavailable" title="Niedostępna"></span></td>
	</tr>
	<tr>
		<td>Szybka naprawa w razie przepięcia prądu</td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-unavailable" title="Niedostępna"></span></td>
		<td><span class="warranty-unavailable" title="Niedostępna"></span></td>
	</tr>
	<tr>
		<th>Po 2 latach od zakupu</th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<td>Szybki i profesjonalny serwis w razie awarii</td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-unavailable" title="Niedostępna"></span></td>
	</tr>
	<tr>
		<td>Wymiana na nowy, gdy naprawa jest niemożliwa lub nieopłacalna (pokrywasz 10% wartości nowego sprzętu)</td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-unavailable" title="Niedostępna"></span></td>
	</tr>
	<tr>
		<td>Brak limitu napraw i ukrytych kosztów</td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-unavailable" title="Niedostępna"></span></td>
	</tr>
	<tr>
		<td>Wygoda - wystarczy telefon, aby zgłosić awarię</td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-unavailable" title="Niedostępna"></span></td>
	</tr>
	<tr>
		<td>Oszczędność pieniędzy - niska cena w stosunku do kosztów napraw</td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-available" title="Dostępna"></span></td>
		<td><span class="warranty-unavailable" title="Niedostępna"></span></td>
	</tr>
<script>
function additionalTextToWarranties() {
    var groups = ['etui dedykowane', 'akumulator', 'helikopter', 'nawigacja samochodowa', 'nawigacja ręczna', 'nawigacja treningowa', 'nawigacja motocyklowa', 'lokalizator fotoradar', 'nawigacja ciężarowa G', 'nawigacja ciężarowa', 'nawigacja samochodowa', 'mapa GPS', 'echosonda', 'rejestrator jazdy', 'smartfon', 'telefon komórkowy', 'ciśnieniomierz nadgar', 'tablet iPad retina', 'tablet iPad new', 'tablet iPad', 'powerbank', 'tablet multimedialny', 'tablet iPad Air', 'tablet iPad 2', 'tablet iPad mini'];
    var searchText = ['Szybka naprawa w razie przypadkowego uszkodzenia', 'Szybka naprawa w razie przepięcia prądu'];
    var addText = '<span class="cred block">(przez 12 miesięcy)</span>';
    $('#warranties td, #warranty-form td').each(function() {
        var $that = $(this);
        var catName = ($that.parents('table.columns-3').find('.product-info .code').length)? $that.parents('table.columns-3').find('.product-info .code').text().split('|')[0].trim() : $('.productsName').val();

        if (groups.indexOf(catName) != -1) {
            for (var i = 0; i < searchText.length; i++) {
                if ($that.text().indexOf(searchText[i]) != -1) {
                    $that.html(function() {
                        var $that = $(this).html();
                        var html = $that;
                        if ($that.indexOf(addText) === -1) {
                            html = $(this).html() + addText
                        }
                        return html;
                    });
                }
            }
        }
    });
}
additionalTextToWarranties();
</script><tr class="summary">
			<td></td>
			<td class="warranty-conditions-price">
							<label class="years-price " for="waranty16675604416">do 3 lat&nbsp;<span>850&nbsp;zł</span></label>
		<div class="per-month">
			23,61&nbsp;zł zł miesięcznie</div>
	<a class="js-warranty-info" data-warranty-id="14493522" href="#">
	Warunki ubezpieczenia</a></td>
					<td class="warranty-conditions-price">
							<label class="years-price " for="waranty16675604424">do 3 lat&nbsp;<span>510&nbsp;zł</span></label>
		<div class="per-month">
			42,50&nbsp;zł zł miesięcznie</div>
	<a class="js-warranty-info" data-warranty-id="51202475" href="#">
	Warunki ubezpieczenia</a></td>
					<td class="warranty-conditions-price warranty-conditions-free">
				<label class="free-warranty">
						<span>bezpłatna</span>
					</label>
				</td>
		</tr>
		<tr class="summary buyable">
			<td></td>
			<td class="warranty-conditions-button">
							<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="warranty" class="js-add-warranty-checkbox" value="14493522" data-warranty-name="Ekstra Ochrona wariant ROZSZERZONY– 3 lata"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</td>
					<td class="warranty-conditions-button">
							<label class="checkbox-css theme2 checkbox-css-text button"><span class="text"><input
		type="checkbox"
		 name="warranty" class="js-add-warranty-checkbox" value="51202475" data-warranty-name="Ekstra Ochrona wariant PODSTAWOWY – 3 lata"
/><i></i>
	<span class="toggle-text"
						 data-checked="wybrane" data-unchecked="wybieram"
				>
				</span>
			</span>
	</label>
</td>
					<td class="warranty-conditions-button warranty-conditions-free">
				</td>
		</tr>
		</tbody>
</table>
</div>
					</div>
		</div>
	<div class="product-tab product-tab-raty">
			<div class="product-tab-header" data-tab="raty">
				Raty</div>
			<div class="product-tab-content">
				<div id="installments">
							<div class="instalment-tabs">
	<div class="tab js-tab" data-instalment="0" data-code="10x0%" data-duration="10">
			Raty 10x0%</div>
	<div class="tab js-tab" data-instalment="1" data-code="Male raty" data-duration="36">
			Małe Raty</div>
	</div>
<div class="instalment-options">
	<div class="instalment-option js-instalment-option" data-instalment="0">
			<div class="instalment-pricing selected-pricing">
						<div class="instalment-number">
							10</div>
						<div class="instalment-price">
							339,80&nbsp;zł</div>
					</div>
				<div class="instalment-desc">
				<div class="instalment-number">Liczba rat</div>
				<div class="instalment-price">Wysokość raty</div>
			</div>
			<a href="#" data-modal="0" class="instalment-popup js-instalment-popup">Zobacz więcej</a>
		</div>
	<div class="instalment-option js-instalment-option" data-instalment="1">
			<div class="instalment-customable js-instalment-customable">
						<div class="instalment-range js-instalment-range">
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							<div class="range-unit js-range-unit"></div>
							</div>
						<div class="instalment-thumb js-instalment-thumb"></div>
					</div>
					<div class="instalment-customable-desc">
						<div class="instalment-range-min">
							3 miesiące</div>
						<div class="instalment-range-max">
							36 miesięcy</div>
					</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="3">
							<div class="instalment-number">
								3</div>
							<div class="instalment-price">
								1 149,66&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="4">
							<div class="instalment-number">
								4</div>
							<div class="instalment-price">
								866,49&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="5">
							<div class="instalment-number">
								5</div>
							<div class="instalment-price">
								696,59&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="6">
							<div class="instalment-number">
								6</div>
							<div class="instalment-price">
								583,32&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="7">
							<div class="instalment-number">
								7</div>
							<div class="instalment-price">
								502,42&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="8">
							<div class="instalment-number">
								8</div>
							<div class="instalment-price">
								441,74&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="9">
							<div class="instalment-number">
								9</div>
							<div class="instalment-price">
								394,55&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="10">
							<div class="instalment-number">
								10</div>
							<div class="instalment-price">
								356,79&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="11">
							<div class="instalment-number">
								11</div>
							<div class="instalment-price">
								325,90&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="12">
							<div class="instalment-number">
								12</div>
							<div class="instalment-price">
								300,16&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="13">
							<div class="instalment-number">
								13</div>
							<div class="instalment-price">
								278,37&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="14">
							<div class="instalment-number">
								14</div>
							<div class="instalment-price">
								259,70&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="15">
							<div class="instalment-number">
								15</div>
							<div class="instalment-price">
								243,52&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="16">
							<div class="instalment-number">
								16</div>
							<div class="instalment-price">
								229,36&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="17">
							<div class="instalment-number">
								17</div>
							<div class="instalment-price">
								216,87&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="18">
							<div class="instalment-number">
								18</div>
							<div class="instalment-price">
								205,77&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="19">
							<div class="instalment-number">
								19</div>
							<div class="instalment-price">
								195,83&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="20">
							<div class="instalment-number">
								20</div>
							<div class="instalment-price">
								186,89&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="21">
							<div class="instalment-number">
								21</div>
							<div class="instalment-price">
								178,80&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="22">
							<div class="instalment-number">
								22</div>
							<div class="instalment-price">
								171,44&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="23">
							<div class="instalment-number">
								23</div>
							<div class="instalment-price">
								164,73&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="24">
							<div class="instalment-number">
								24</div>
							<div class="instalment-price">
								158,57&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="25">
							<div class="instalment-number">
								25</div>
							<div class="instalment-price">
								152,91&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="26">
							<div class="instalment-number">
								26</div>
							<div class="instalment-price">
								147,68&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="27">
							<div class="instalment-number">
								27</div>
							<div class="instalment-price">
								142,84&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="28">
							<div class="instalment-number">
								28</div>
							<div class="instalment-price">
								138,35&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="29">
							<div class="instalment-number">
								29</div>
							<div class="instalment-price">
								134,16&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="30">
							<div class="instalment-number">
								30</div>
							<div class="instalment-price">
								130,26&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="31">
							<div class="instalment-number">
								31</div>
							<div class="instalment-price">
								126,60&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="32">
							<div class="instalment-number">
								32</div>
							<div class="instalment-price">
								123,18&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="33">
							<div class="instalment-number">
								33</div>
							<div class="instalment-price">
								119,96&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="34">
							<div class="instalment-number">
								34</div>
							<div class="instalment-price">
								116,93&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="35">
							<div class="instalment-number">
								35</div>
							<div class="instalment-price">
								114,08&nbsp;zł</div>
						</div>
					<div class="instalment-pricing js-instalment-pricing" data-option="36">
							<div class="instalment-number">
								36</div>
							<div class="instalment-price">
								111,38&nbsp;zł</div>
						</div>
					<div class="instalment-desc">
				<div class="instalment-number">Liczba rat</div>
				<div class="instalment-price">Wysokość raty</div>
			</div>
			<a href="#" data-modal="1" class="instalment-popup js-instalment-popup">Zobacz więcej</a>
		</div>
	</div>
<div class="instalment-items">
	<div class="instalment-row">
		<div class="instalment-item">
				<button class="add-ca-instalment js-add-ca-instalment" data-payment="INSTALMENT_IN_CREDIT_AGRICOLE">
						Wybierz Raty przez Internet</button>
				</div>
		<div class="instalment-item">
				<button class="add-sygma-instalment js-add-sygma-instalment" data-payment="INSTALMENT_IN_SYGMA_BANK">
						Wybierz Raty z Kurierem</button>
				</div>
		</div>

	<div class="instalment-row">
		<div class="instalment-item short-desc">
				<ul style="font-size: 9pt; font-weight: bold; color: #04948f; margin: 20px">
    <li style="padding-bottom: 10px; padding-top: 0px; padding-left: 0px; padding-right: 0px">Podpisywanie (autoryzacja) umowy przez Internet</li>
    <li style="padding-bottom: 10px; padding-top: 0px; padding-left: 0px; padding-right: 0px">Potwierdzenie tożsamości przelewem online na 1zł</li>
    <li style="padding-bottom: 10px; padding-top: 0px; padding-left: 0px; padding-right: 0px">Minimalna kwota kredytu 300zł</li>
    <li style="padding-bottom: 10px; padding-top: 0px; padding-left: 0px; padding-right: 0px">Do 5000 zł kredytu</li>
</ul></div>
		<div class="instalment-item short-desc">
				<ul style="color:#bd3a97;margin:20px;font-weight:bold;font-size:9pt;">
    <li style="padding:0 0 10px 0;">Podpisanie umowy w obecności kuriera</li>
    <li style="padding:0 0 10px 0;">Potwierdzenie tożsamości dowodem osobistym</li>
    <li style="padding:0 0 10px 0;">Minimalna kwota kredytu 150 zł</li>
    <li style="padding:0 0 10px 0;">Do 20 000 zł kredytu</li>
</ul></div>
		</div>

	<div class="instalment-row">
		<div class="instalment-item">
				<a href="#" class="instalment-info js-ca-instalment-info">
					</a>
			</div>
		<div class="instalment-item">
				<a href="#" class="instalment-info js-sygma-instalment-info">
					</a>
			</div>
		</div>
</div>
<div id="modal-instalment-0" data-remodal-id="instalment-0" class="instalment-modal">
	<button class="remodal-close" data-remodal-action="close"></button>
	<div class="artContent">
<div>
<div>
<p style="font-size: 16px; padding-bottom: 10px;"><strong>Raty 10x0%</strong></p>
<div>Po co od razu płacić za zakupiony towar! Proponujemy Ci rozłożenie płatności na 10 nieoprocentowanych rat.</div>
<ul style="padding-bottom: 10px;">
    <li>stopa oprocentowania wynosi 0 %</li>
    <li>prowizja 0%</li>
    <li>bez uciążliwych formalności</li>
</ul>
<p class="MsoNormal"><i><span style="font-size: 8pt; font-family: Arial, sans-serif;">Rzeczywista Roczna Stopa Oprocentowania (RRSO) wynosi 0%, całkowita kwota kredytu 1550zł, całkowita kwota do zapłaty 1550 zł, oprocentowanie stałe 0%, całkowity koszt kredytu 0zł, 10 miesięcznych rat r&oacute;wnych wysokości 155zł.&nbsp; Kalkulacja została dokonana na dzień 27.06.2017r. na reprezentatywnym przykładzie.</span></i></p>
<p class="MsoNormal"><i><span style="font-size:8.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">W imieniu Credit Agricole Bank Polska S.A., Alior Bank S.A Euro-net umocowany jest do: zawierania, rozwiązywania um&oacute;w kredytowych, przyjmowania oświadczeń kredytobiorc&oacute;w składanych w wykonaniu um&oacute;w kredytowych, w tym o odstąpieniu od umowy kredytowej. Dostępność oferty ratalnej uzależniona jest od warunk&oacute;w formalnych określonych przez Banki.<span style="color:#1F497D"><o:p></o:p></span></span></i></p>
</div>
</div>
</div></div>
<div id="modal-instalment-1" data-remodal-id="instalment-1" class="instalment-modal">
	<button class="remodal-close" data-remodal-action="close"></button>
	<p>Raty na kt&oacute;re Cię stać już dostępne w OleOle.pl. Skorzystaj z &quot;Małych  Rat&quot;, a spłatę rozłożymy nawet na 36 miesięcy na korzystnych warunkach:</p>
<ol>
    <li>koszt kredytu - 0,5% miesięcznie,</li>
    <li>liczba miesięcznych rat - od 3 do 36,</li>
    <li>bez uciążliwych formalności</li>
</ol>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal">&nbsp;</p>
<p class="MsoNormal"><em><span style="font-size:8.0pt;color:black">Rzeczywista Roczna Stopa Oprocentowania (RRSO) dla kredytu ratalnego w ramach Umowy kredytu ratalnego, wynosi 11,5%, całkowita kwota kredytu (bez kredytowanych koszt&oacute;w kredytu) 1583,00 zł, całkowita kwota do zapłaty 1677,96 zł, oprocentowanie nominalne 5,0%, całkowity koszt kredytu 94,96 zł (w tym: prowizja 50,34 zł, odsetki  44,62 zł), 12 miesięcznych rat r&oacute;wnych w wysokości 139,84 zł. Kalkulacja została dokonana na dzień  29.06.2017 r. na reprezentatywnym przykładzie. W imieniu Credit Agricole Bank Polska S.A., Alior Bank S.A Euro-net umocowany jest do: zawierania, rozwiązywania um&oacute;w kredytowych, przyjmowania oświadczeń kredytobiorc&oacute;w składanych w wykonaniu um&oacute;w kredytowych, w tym o odstąpieniu od umowy kredytowej. Dostępność oferty ratalnej uzależniona jest od warunk&oacute;w formalnych określonych przez Banki.</span></em></p></div>
<div id="modal-instalment-ca" data-remodal-id="instalment-ca" class="instalment-modal-bank">
	<button class="remodal-close" data-remodal-action="close"></button>
	<div class="instalment-description">
			<br /></div>
		<div class="buttons">
			<button class="add-to-cart-instalment js-add-ca-instalment" data-payment="INSTALMENT_IN_CREDIT_AGRICOLE">
					Skorzystaj</button>
			<button class="check-other-instalment js-check-instalment-sygma">
					Zobacz Raty z Kurierem</button>
			</div></div>
<div id="modal-instalment-sygma" data-remodal-id="instalment-sygma" class="instalment-modal-bank">
	<button class="remodal-close" data-remodal-action="close"></button>
	<div class="instalment-description">
			<br /></div>
		<div class="buttons">
			<button class="add-to-cart-instalment js-add-sygma-instalment" data-payment="INSTALMENT_IN_SYGMA_BANK">
					Skorzystaj</button>
			<button class="check-other-instalment js-check-instalment-ca">
					Zobacz Raty przez Internet</button>
			</div></div>
</div>
					</div>
		</div>
	<div class="product-tab product-tab-promocje">
			<div class="product-tab-header" data-tab="promocje">
				Promocje dla tego produktu</div>
			<div class="product-tab-content">
				<div class="product-promotion">
	<div class="product-promotion-items">
		<div class="product-promotion-box">

    <a class="product-promotion-image" href="/cms/otrzymasz-za-zeta-na-powrot-do-szkoly.bhtml?zestaw=zestaw-2&amp;link=promo_kp">
            <img alt="Otrzymasz za zeta smartwatch" class="lazy " src="/images/www/emptyImage.png" data-original="//f00.osfr.pl/files/cms-promotion/75/02/1e/17098879968/6dd8abdecc9d0c259e9bc09826e242ea/17098879968_main.png" /></a>
    <a href="/cms/otrzymasz-za-zeta-na-powrot-do-szkoly.bhtml?zestaw=zestaw-2&amp;link=promo_kp" class="product-promotion-title">
		Otrzymasz za zeta smartwatch</a>
	<div class="product-promotion-description">
		przy zakupie wybranych smartfonów</div>
	<div class="product-promotion-date">
			Promocja obowiązuje do: 18.09.2017</div>
	</div></div>
</div></div>
		</div>
	<div id="see-recommended"></div>
	<div class="product-tab product-tab-print">
	<div class="product-tab-header">
		Informacje do wydruku</div>
	<div class="product-tab-content">
		Powyższe informacje pochodzą ze strony oleole.pl. W celu przeczytania opinii, pytań i odpowiedzi, promocji i innych informacji dotyczących Samsung Galaxy S8 SM-G950 (Orchid Grey), wpisz 1109569 w wyszukiwarce oleole.pl.</div>
</div>
</div>

		<!-- area: listing_footer, elements:0 , column: 874735525 -->
<!-- end area: listing_footer -->

		<!-- area: footer, elements:3 , column: 50337821 -->
<div class="banners banners-footer">
	<!-- banner id: 13454501371 -->
		<div data-id="13454501371-1" class="banner-item banner-text" >
			<script charset="UTF-8" type="text/javascript">
var nsl_exit_intent = {
    settings: {},
    init: function(options) {
        var cok = jQuery.cookie;
        this.settings = options;
        if (options != undefined && this.conditions(true)) {
            if (this.cookieName == undefined) this.cookieName = 'nsl_register';
            if (this.settings.type == 'exitIntent' && cok(this.cookieName) != 'exitIntent' && document.referrer.indexOf(document.location.hostname) != -1) {
                this.exitIntent();
            } else if (this.settings.type == 'onPageNumber') {
                this.onPageNumber();
            }
        }

    },
    openModal: function(disable) {
        if (disable == 'exit') {
            this.disableScrollEvent(false);
            $('#modalContainer').remove();
        } else {
            if ($('#modalContainer').length == 0) {
                set = this.settings.modalOption;
                set = (set == undefined) ? {} : set;
                if (set.backgroundColor == undefined) set.backgroundColor = '#000';
                if (set.opacity == undefined) set.opacity = '.8';
                if (set.modal == undefined) set.modal = false;
                if (set.id == undefined) set.id = 'modalBack';
                if (set.showExit == undefined) set.showExit = true;
                this.disableScrollEvent(true);
                $('<div id="modalContainer">').insertBefore('body');
                if ($('#' + set.id).length == 0) {
                    $('#modalContainer').css({
                        'position': 'fixed',
                        'width': '100%',
                        'height': window.outerHeight,
                        'top': '0',
                        'left': '0',
                        'z-index': '199'
                    }).html('<div id="' + set.id + '"></div>');
                    $('#' + set.id).css({
                        'background-color': set.backgroundColor,
                        'opacity': set.opacity,
                        'cursor': (set.modal) ? 'auto' : 'pointer',
                        'width': '100%',
                        'height': window.outerHeight,
                        'position': 'fixed',
                        'top': '0px',
                        'left': '0',
                        'z-index': 0
                    });
                    if ($('#m-content').length == 0) $('#modalContainer').prepend('<div id="m-content">');
                    $('#m-content').css({
                        'position': 'fixed',
                        'z-index': 5
                    }).html(this.settings.content);
                    if (set.showExit) $('#m-content').prepend('<div id="exit_modal"></div>');
                    setTimeout(function() { $('#m-content').addClass('show') }, 200);
                    this.events();
                    shopEventLogger({
                        id: nsl_exit_intent.cookieName,
                        f03: '',
                        f04: 'popup-show',
                        f05: nsl_exit_intent.settings.type
                    });
                }
            }
        }
    },
    disableScrollEvent: function(action) {
        if (action) {
            var scrollPosition = [
                self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
                self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop
            ];
            var html = jQuery('html');
            html.data('scroll-position', scrollPosition);
            html.data('previous-overflow', html.css('overflow'));
            html.css('overflow', 'hidden');
            window.scrollTo(scrollPosition[0], scrollPosition[1]);
            return true;
        } else {
            var html = jQuery('html');
            var scrollPosition = html.data('scroll-position');
            html.css('overflow', html.data('previous-overflow'));
            window.scrollTo(scrollPosition[0], scrollPosition[1]);
            return false;
        }
    },
    exitIntent: function() {
          function leaveFromTop(e) {
              if (e.clientY < 0) {
                  if ((jQuery.cookie(nsl_exit_intent.cookieName) == undefined) && !($('.remodal-is-opened').length || $('.show-loading').length)) {
                      jQuery.cookie(nsl_exit_intent.cookieName, nsl_exit_intent.settings.type, { path: '/' });
                      nsl_exit_intent.openModal();
                  }
              }
          }

          jQ(document).one('mouseleave', leaveFromTop);

    },
    onPageNumber: function() {
        if (www.pageType == 'productsList' || www.pageType == '') {
            page = document.location.pathname;
            page = (page.indexOf('strona') > -1) ? page.split(',')[page.split(',').length - 1].match(/\d+/)[0] : 0;

            var cookie = jQuery.cookie('unll');
            if (cookie == null) {
                jQuery.cookie('unll', '1|' + page, { path: '/' });
            } else if (cookie.split('|')[0] / 1 !== 0) {
                var splitedCookie = cookie.split('|');
                var cnt = splitedCookie[0] / 1;
                var cookiePageNr = splitedCookie[2] / 1;
                if (cookiePageNr !== page / 1) cnt++;
                jQuery.cookie('unll', cnt + '|' + page, { path: '/' });
            } else {

            }
            if (page != 0) {
                if (jQuery.cookie('unll').split('|')[0] / 1 == this.settings.onPageNumber) {
                    jQuery.cookie(this.cookieName, this.settings.type, { path: '/', expires: 2 });
                    this.openModal();
                }

            }

        }
    },
    events: function() {
        // logika wylaczania modala
        exit = (set.modal) ? $('#exit_modal, #exit_modal_link') : $('#exit_modal, #exit_modal_link, #' + set.id);
        if (exit) {
            exit.on('click', function() {
                if (jQuery.cookie(nsl_exit_intent.cookieName) != 'disabled') {
                    jQuery.cookie(nsl_exit_intent.cookieName, nsl_exit_intent.settings.type, { path: '/', expires: 2 });
                }
                nsl_exit_intent.openModal('exit');
                shopEventLogger({
                    id: nsl_exit_intent.cookieName,
                    f03: '',
                    f04: 'exit-' + exit.attr('id'),
                    f05: nsl_exit_intent.settings.type
                });
            });
        }

        var search = jQuery('#m-content').find('input[name=email]');
        var text = 'Wpisz swój adres e-mail';
        search.focus(function() {
            if (search.val() == text) search.val('');
        });
        search.blur(function() {
            if (search.val() == '') search.val(text);
        });

        jQuery(search.parents('form')).submit(function(e) {
            e.preventDefault();
            if (validateEmail(search.val())) {
                nsl_exit_intent.newsletterRegister(search.val());
            } else {
                if (!search.parent().find('.error-form').length)
                    search.parent().prepend('<div class="error-form">Wprowadź poprawny adres e-mail</div>');
            }

        });
    },
    newsletterRegister: function(email, pageTrackerName) {
        if (validateEmail(email)) {
            var surveys = true;
            var href = '/informail-newsletter.ltr';
            $.ajax({
                url: href,
                data: {
                    add: email,
                  	mailingList:237
                },
                success: function(html) {
                    if (html == 'true') {
                    	shopEventLogger({
                          id: 'newsletter',
                          f03: email,
                          f04: nsl_exit_intent.settings.type
                		});
                        shopEventLogger({
                            id: nsl_exit_intent.cookieName,
                            f03: email,
                            f04: 'register-success',
                            f05: nsl_exit_intent.settings.type
                        });
                        jQuery.cookie(nsl_exit_intent.cookieName, 'disabled', { path: '/', expires: 99999 });
                        $('#popup-new .nsl').html('<div id="form-ok"></div>').find('#form-ok').hide().html('<div class="form-ok">Dziękujemy za zapisanie na newsletter!<br> <br><b>Na Twój adres e-mail wysłaliśmy wiadomość z prośbą o <span>potwierdzenie subskrypcji</span><br> Na niektórych skrzynkach wiadomość ta może wpaść do innego folderu niż główny folder - sprawdź też zakładki Oferty, Inne lub Spam</b></div>').slideDown('slow');
                        if (pageTrackerName !== null && pageTrackerName !== undefined) {
                            UA.pageview(pageTrackerName);
                        } else {
                            UA.pageview('/akcje/zapisano-na-biuletyn/' + nsl_exit_intent.settings.type);
                        }
                    } else {
                        $('#popup-new .nsl').html('<div id="form-ok"></div>').find('#form-ok').hide().html('<div class="form-ok error">Wystąpił błąd przy rejestracji subskrypcji – prosimy o rejestrację w późniejszym terminie.</div>').slideDown('slow');
                    }
                },
                error: function() {
                    $('#popup-new .nsl').html('<div id="form-ok"></div>').find('#form-ok').hide().html('<div class="form-ok error">Wystąpił błąd przy rejestracji subskrypcji – prosimy o rejestrację w późniejszym terminie.</div>').slideDown('slow');
                    shopEventLogger({
                        id: nsl_exit_intent.cookieName,
                        f03: '',
                        f04: 'register-error',
                        f05: nsl_exit_intent.settings.type
                    });
                }
            });
        } else {
            $('#popup-new .nsl').html('<div id="form-ok"></div>').find('#form-ok').hide().html('<div class="form-ok error">Wystąpił błąd przy rejestracji subskrypcji – prosimy o rejestrację w późniejszym terminie.</div>').slideDown('slow');
        }

    },
    isDefined: function(x) {
        return typeof x !== 'undefined';
    },
    conditions: function(active) {
        if (active) {
            var url = document.location.href;
            if ((url.indexOf('utm_source=biuletyn') != -1) || (url.indexOf('from=newsletter') != -1)) {
                jQuery.cookie(nsl_exit_intent.cookieName, 'disabled', { path: '/', expires: 99999 });
                shopEventLogger({
                    id: nsl_exit_intent.cookieName,
                    f03: '',
                    f04: 'user-from-nsl',
                    f05: nsl_exit_intent.settings.type
                });
                return false;
            } else if (jQuery.cookie(nsl_exit_intent.cookieName) == 'disabled') {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }

    }
}

var opt = {
    'content': '<div id="popup-new"><div class="top_cont"> <img src="//f01.osfr.pl/si_upload/gfx/2016/08_10_newsletter/oleole.png"> <span>Zaczekaj, zapisz się na newsletter</span></div><div class="main_cont"><p class="head">NIE OMINIE CIĘ ŻADNA PROMOCJA</p><div class="nsl"><form action=""><div class="input-wrapper"><input type="text" name="email" id="pop_form_email2" value="Wpisz swój adres e-mail"/></div><input id="nllSubmit" type="submit" name="Zapisz się" value="Zapisz się"/></form></div><p class="head smaller">Dowiesz się przed innymi o:</p><ul><li>najlepszych promocjach w OleOle!</li><li>nowościach w naszej ofercie</li><li>konkursach i akcjach specjalnych dla naszych Klientów</li></ul><a href="/cms/polityka-prywatnosci.bhtml?link=exitIntent" class="polityka" target="_blank">Polityka prywatności</a></div></div>',
    'type': 'exitIntent',
    'onPageNumber': 2,
    'modal': true,
    'cookieName': 'nsl_register',
}
$(function() {
    setTimeout(function(){
      if(!$('html').hasClass('operator-logged')){
          nsl_exit_intent.init(opt);
      }
    }, 600);
});

</script></div>
	<!-- banner id: 16627071608 -->
		<div data-id="16627071608-2" class="banner-item banner-text" >
			<script>
  (function(){
    if(app.pageType() != "leaf") return false;
    var s = document.createElement('script');
    s.src = 'https://emisja.productstream.pl/emission?partner=oleole';
    document.getElementsByTagName('body')[0].appendChild(s);
  })();
</script></div>
	<!-- banner id: 14612659368 -->
		<div data-id="14612659368-3" class="banner-item banner-text" >
			<script>
  function hideButtonOnServices(){
      if($('.label-button.label-UNAVAILABLE_AT_THE_MOMENT').text().indexOf('Niedostępny') != -1){
        $('.remodal').find('.js-btn-add-service-with-product').hide();
      }
  }
  $(function(){
    if(app.pageType() == "product"){
		hideButtonOnServices();
      	$(document).on('opened','.remodal',function(){
			hideButtonOnServices();
        });
    }
  });
</script></div>
	</div>
<!-- end area: footer -->
</div>
	
	<div id="hiddenFancyLink"></div>
<div id="hiddenFancyBody"></div>

<footer id="main-footer">
	<div class="container">
		<!-- stopka_grafika -->
			<div id="footer-shops" class="footer-row">
			<div class="column">
				<p class="footer-heading">Zapisz się do newslettera</p>
				<p>
					Otrzymuj kupony <b>rabatowe</b>, informacje o <b>promocjach</b>, <br>
					informacje o <b>nowościach</b>, nową gazetkę promocyjną
				</p>
				<form id="newsletter-form" novalidate>
					<input class="js-newsletter" name="email" type="email" placeholder="Adres email">
					<input type="submit" value="ZAPISZ SIĘ">

				</form>
              <a class="see-more" href="/cms/biuletyny.bhtml?link=stopka">Dowiedz się więcej</a>
              	 <a class="privacy-policy" href="/cms/polityka-prywatnosci.bhtml?link=stopka_newsletter">Polityka prywatności</a>
			</div>
			<div class="column">
				<p class="footer-heading">Znajdź punkt odbioru</p>
				<p>
                  <b>OleOle!</b> to już <b>12</b> punktów odbioru <br>w <b>10</b> miastach<br>
					<a class="find-shop" href="/cms/punkty_odbioru_osobistego.bhtml?link=stopka">Znajdź najbliższy punkt odbioru</a>
				</p>
              <div class="find-shop-map"></div>
			</div>
		</div>
		<div id="footer-contact" class="footer-row">
			<div class="column js-go-to-contact-page">
				<p class="footer-heading">Skontaktuj się z nami</p>
				<p>
					Pracujemy w godzinach:<br>
					pon.-sob. <b>8:00 - 21:00</b>, niedz. <b>9:00 - 19:00</b>
				</p>
              <div class="footer-phone">812 812 812</div>
			</div>
			<div class="column">
				<p class="footer-heading">Napisz do nas</p>
				<a class="footer-contact" href="mailto:sklep@oleole.pl">sklep@oleole.pl</a>

					<!--<a class="footer-chat js-livechat" href="#">Czat z konsultantem</a>-->
			</div>
		</div>
		<style>
			#footer-menu{display:table;width:100%;table-layout:fixed;}
			#footer-menu div,#footer-menu div+div{display:table-cell;padding:0;color:#959595;}
            #footer-menu div b {color:#666}
			#footer-menu div a{display:block;float:none;padding:0;margin:8px 0 0;font-size:11px;}
          	#footer-menu div a.inline-block{display:inline-block}
		</style>
		<div id="footer-menu">
  			<div>
  				<b>Centrum informacji</b>
  				<a href="/cms/dostawa.bhtml?link=stopka" title="Dostawa - dostępne sposoby dostawy">Dostawa</a>
                <a class="inline-block" href="/cms/formy-platnosci.bhtml?link=stopka" title="Sposoby płatności- sprawdź jak możesz zapłacić">Sposoby płatności</a> |
  				<a class="inline-block" href="/cms/raty.bhtml?link=stopka" title="Raty - dowiedz się jak kupić na raty">Raty</a>
  				<a href="/cms/pomoc-odbior-zuzytego-sprzetu.bhtml?link=stopka" title="Odbiór zużytego sprzętu przez sklep OleOle.pl">Odbiór zużytego sprzętu</a>
  				<a class="inline-block" href="/cms/pomoc-reklamacja-towaru.bhtml?sp=e92&link=stopka" title="Reklamacje - sprawdź co zrobić, gdy sprzęt nie działa">Reklamacje</a> |
  				<a class="inline-block" href="/cms/pomoc-odstapienie.bhtml?sp=e93&link=stopka" title="Zwroty">Zwroty</a>
  			</div>
  			<div>
  				<b>Regulaminy</b>
  				<a href="/cms/regulamin.bhtml?link=stopka" title="Regulamin - zasady funkcjonowania sklepu internetowego">Regulamin</a>
  				<a href="/cms/polityka-prywatnosci.bhtml?link=stopka" title="Polityka prywatności sklepu OleOle.pl">Polityka prywatności</a>
  			</div>
          	<div>
  				<b>Reklama</b>
              	<a href="/cms/reklamuj_sie.bhtml?link=stopka" title="Reklama w OleOle!">Reklamuj się w OleOle!</a>
  				<a href="/cms/reklama_oleole.bhtml?link=stopka" title="Specyfikacja reklamy na OleOle!">Reklama - specyfikacja</a>
  			</div>
            <div>
				<b>Opinie o nas</b>
				<a href="/cms/opineo.bhtml?link=stopka" title="Opineo w OleOle!" rel="nofollow">Opineo</a>
              	<a href="/cms/ceneo.bhtml?link=stopka" title="Ceneo w OleOle!" rel="nofollow">Ceneo</a>
				<a href="//blog.oleole.pl" title="Blog OleOle!">Blog OleOle!</a>
			</div>
          	<div>
				<b>Moje konto</b>
                <a href="/account-login.bhtml?link=stopka" title="Zarejestruj się w sklepie OleOle.pl">Zarejestruj się</a>
              	<a href="/customer-order-list.bhtml?archival=false&link=stopka" title="Zamówienia - zobacz swoje zamówienia">Zamówienia</a>
				<a href="/customer-password.bhtml?link=stopka" title="Ustawienia konta - zmień ustawienia konta">Ustawienia konta</a>
			</div>
		</div><!-- stopka_bloczki -->
		<style>
/* MENU brands SIS */
.producers-list .sisBosch{background:url(//f00.osfr.pl/si_upload/upload_ole/kampanie/09_15/sis_bosch/bosch-57x14.png) top left no-repeat; height:14px; width:57px; display:inline-block;}
.producers-list .sisMiele{background:url(//f00.osfr.pl/si_upload/upload_ole/kampanie/04_2017/sis-miele/miele-logo-sis.png) top left no-repeat; height:14px; width:58px; display:inline-block;}
  
/* Footer logo */
#footer-logo-brands {text-align: center;padding: 12px 0 8px;}
#footer-logo-brands img {display: inline-block;vertical-align: middle;padding-left: 20px;}
#footer-logo-brands img:first-child{padding-right: 10px}
</style>
<div id="footer-logo-brands">
  <img src="//f00.osfr.pl/si_upload/unity/ole/stopka/footer-logo-brands.png">
  <img src="//f00.osfr.pl/si_upload/unity/ole/stopka/masterpass_black.png">
  <a href="/cms/visa-checkout.bhtml?link=stopka" target="_blank">
  	<img src="//f00.osfr.pl/si_upload/unity/ole/stopka/50x30-visa-checkout.png">
  </a>
</div>
  <div id="footer-owner">
    OleOle.pl 2017 Wszystkie prawa zastrzeżone, powielanie informacji zawartych na tej stronie zabronione.
  </div>
  <div id="footer-owner-address">
    Właściciel serwisu: Euro-net sp. z o.o., ul. Muszkieterów 15, 02-273 Warszawa
  </div></div>
</footer>

<script>
	UA.pageview();
	UA.pageCategory();
</script>
<!-- TODO: usunac po przeniesieniu do komunikatu -->

		<!-- area: scroll_footer, elements:0 , column: 16294710856 -->
<!-- end area: scroll_footer -->
</body>
</html>
HTML;

        return $html;
    }
}
