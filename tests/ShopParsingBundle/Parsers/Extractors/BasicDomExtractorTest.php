<?php

namespace Tests\ShopParsingBundle\Parsers\Extractors;

use PHPUnit\Framework\TestCase;
use ShopParsingBundle\Parsers\Extractors\BasicDomExtractor;
use Symfony\Component\DomCrawler\Crawler;
use Tests\ShopParsingBundle\PageFixtures\MoreleNet\iPhone5sSale;
use Tests\ShopParsingBundle\PageFixtures\MoreleNet\SamsungGalaxy;

class BasicDomExtractorTest extends TestCase
{
    /**
     * @dataProvider extractorTestDataProvider
     * @param $fixtureClassName
     * @param string $selector
     * @param string $expectedResult
     */
    public function testExtractor($fixtureClassName, $selector, $expectedResult)
    {
        $extractor = new BasicDomExtractor($selector);

        $crawler = new Crawler($fixtureClassName::getHtml());
        $calculatedResult = $extractor->extract($crawler);

        $this->assertEquals($expectedResult, $calculatedResult);
    }

    /**
     * @return array
     */
    public function extractorTestDataProvider()
    {
        return [
            [SamsungGalaxy::class, 'div[itemprop="name"] h1.page-title', 'Smartfon Samsung Galaxy S8 Orchid Grey (SM-G950F)'],
            [SamsungGalaxy::class, 'title', 'Samsung Galaxy S8 Orchid Grey (SM-G950F) w Morele.net'],
            [SamsungGalaxy::class, 'div[cantfindme="really"]', ''],
            [SamsungGalaxy::class, 'div.price div#product_price_brutto', '3398,00 zł'],
            [iPhone5sSale::class, 'div.price div.price-old', "\n1584,81 zł\n"],
        ];
    }
}
