<?php

namespace Tests\ShopParsingBundle\Parsers\Extractors;

use PHPUnit\Framework\TestCase;
use ShopParsingBundle\Parsers\Extractors\AttributeDomExtractor;
use Symfony\Component\DomCrawler\Crawler;
use Tests\ShopParsingBundle\PageFixtures\MoreleNet\SamsungGalaxy;

class AttributeDomExtractorTest extends TestCase
{
    /**
     * @var Crawler
     */
    private $crawler;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->crawler = new Crawler(SamsungGalaxy::getHtml());
    }

    /**
     * @dataProvider extractorTestDataProvider
     * @param string $selector
     * @param string $attribute
     * @param string $expectedResult
     */
    public function testExtractor($selector, $attribute, $expectedResult)
    {
        $extractor = new AttributeDomExtractor($selector, $attribute);

        $calculatedResult = $extractor->extract($this->crawler);

        $this->assertEquals($expectedResult, $calculatedResult);
    }

    /**
     * @return array
     */
    public function extractorTestDataProvider()
    {
        return [
            ['div[cantfindme="really"]', '', ''],
            ['', 'content', ''],
            ['div.price div#product_price_brutto', 'content', '3398.00'],
            ['div.price div#product_price_brutto', 'nosuchattr', ''],
        ];
    }
}
