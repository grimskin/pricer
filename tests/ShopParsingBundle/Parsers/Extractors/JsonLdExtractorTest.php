<?php

namespace Tests\ShopParsingBundle\Parsers\Extractors;

use PHPUnit\Framework\TestCase;
use ShopParsingBundle\Parsers\Extractors\JsonLdExtractor;
use Symfony\Component\DomCrawler\Crawler;
use Tests\ShopParsingBundle\PageFixtures\OleOlePl\SamsungGalaxy;

class JsonLdExtractorTest extends TestCase
{
    /**
     * @var Crawler
     */
    private $crawler;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->crawler = new Crawler(SamsungGalaxy::getHtml());
    }

    /**
     * @dataProvider extractorTestDataProvider
     * @param array $pathItems
     * @param string $expectedResult
     */
    public function testExtractor($pathItems, $expectedResult)
    {
        $extractor = new JsonLdExtractor(...$pathItems);

        $calculatedResult = $extractor->extract($this->crawler);

        $this->assertEquals($expectedResult, $calculatedResult);
    }

    /**
     * @return array
     */
    public function extractorTestDataProvider()
    {
        return [
            [['Product', 'name'], 'Samsung Galaxy S8 SM-G950 (Orchid Grey)'],
            [['Product', 'Name'], ''],
            [['Organization', 'contactPoint', '0', 'telephone'], '+48812812812'],
            [['Product', 'offers', 'price'], '3398.00'],
        ];
    }
}
