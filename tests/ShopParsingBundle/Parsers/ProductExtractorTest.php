<?php

namespace Tests\ShopParsingBundle\Parsers;

use PHPUnit\Framework\TestCase;
use ShopParsingBundle\Parsers\Interfaces\StringExtractorInterface;
use ShopParsingBundle\Parsers\ProductExtractor;
use Symfony\Component\DomCrawler\Crawler;

class ProductExtractorTest extends TestCase
{
    /**
     * @var Crawler|\PHPUnit_Framework_MockObject_MockObject
     */
    private $crawler;

    /**
     * @var StringExtractorInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $priceExtractor;

    /**
     * @var StringExtractorInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $originalPriceExtractor;

    /**
     * @var ProductExtractor|\PHPUnit_Framework_MockObject_MockObject
     */
    private $productExtractor;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        $this->originalPriceExtractor = $this->createMock(StringExtractorInterface::class);
        $this->priceExtractor = $this->createMock(StringExtractorInterface::class);
        $this->crawler = $this->createMock(Crawler::class);

        $this->productExtractor = new ProductExtractor();
        $this->productExtractor->setPriceExtractor($this->priceExtractor);
        $this->productExtractor->setOriginalPriceExtractor($this->originalPriceExtractor);
        $this->productExtractor->setCrawler($this->crawler);
    }

    /**
     * @dataProvider dealLogicDataProvider
     * @param $originalPrice
     * @param $price
     * @param $expectedResult
     */
    public function testIsDealLogic($originalPrice, $price, $expectedResult)
    {
        $this->originalPriceExtractor
            ->expects($this->any())
            ->method('extract')
            ->willReturn($originalPrice);

        $this->priceExtractor
            ->expects($this->any())
            ->method('extract')
            ->willReturn($price);

        $calculatedResult = $this->productExtractor->isDeal();

        $this->assertEquals($expectedResult, $calculatedResult);
    }

    /**
     * @return array
     */
    public function dealLogicDataProvider()
    {
        return [
            [0, 150, false],
            [150, 150, false],
            [151, 150, true],
        ];
    }
}
