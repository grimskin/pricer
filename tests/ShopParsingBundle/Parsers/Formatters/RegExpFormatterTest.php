<?php

namespace Tests\ShopParsingBundle\Parsers\Formatters;

use PHPUnit\Framework\TestCase;
use ShopParsingBundle\Parsers\Formatters\RegExpFormatter;

class RegExpFormatterTest extends TestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @dataProvider extractorTestDataProvider
     * @param string $regexp
     * @param string $string
     * @param string $expectedResult
     */
    public function testExtractor($regexp, $string, $expectedResult)
    {
        $extractor = new RegExpFormatter($regexp);

        $calculatedResult = $extractor->format($string);

        $this->assertEquals($expectedResult, $calculatedResult);
    }

    /**
     * @return array
     */
    public function extractorTestDataProvider()
    {
        return [
            [
                "'^(.*) w Morele\.net$'isU",
                'Samsung Galaxy S8 Orchid Grey (SM-G950F) w Morele.net',
                'Samsung Galaxy S8 Orchid Grey (SM-G950F)',
            ],
            [
                "'^(.*) v Morele\.net$'isU",
                'Samsung Galaxy S8 Orchid Grey (SM-G950F) w Morele.net',
                '',
            ],
        ];
    }
}
