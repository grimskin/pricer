<?php

namespace Tests\ShopParsingBundle\Parsers\Formatters;

use PHPUnit\Framework\TestCase;
use ShopParsingBundle\Parsers\Formatters\BluntFloatToIntFormatter;

class BluntFloatToIntFormatterTest extends TestCase
{
    public function testFormatting()
    {
        $formatter = new BluntFloatToIntFormatter();

        $pattern = '3398,00 zł';

        $formatResult = $formatter->format($pattern);

        $this->assertEquals('339800', $formatResult);
    }
}
