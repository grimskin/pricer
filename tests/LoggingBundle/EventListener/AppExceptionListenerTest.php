<?php

namespace Tests\LoggingBundle\EventListener;

use AppBundle\Events\AppExceptionEvent;
use LoggingBundle\EventListener\AppExceptionListener;
use LoggingBundle\Interfaces\ChanneledLoggerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class AppExceptionListenerTest extends TestCase
{
    /**
     * @var ChanneledLoggerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var AppExceptionListener
     */
    private $listener;

    protected function setUp()
    {
        parent::setUp();

        $this->loggerMock = $this->createMock(ChanneledLoggerInterface::class);

        $this->listener = new AppExceptionListener($this->loggerMock);
    }

    public function testAppExceptionHandling()
    {
        $exception = new \Exception('testAppExceptionHandling');
        $channel = 'some channel';
        $context = [
            'some key' => 'some value'
        ];
        $event = new AppExceptionEvent($exception, $channel, $context);

        $this->loggerMock
            ->expects($this->once())
            ->method('onChannel')
            ->with($channel)
            ->willReturn($this->loggerMock);

        $this->loggerMock
            ->expects($this->once())
            ->method('error')
            ->with($exception->getMessage(), $context);

        $this->listener->onAppException($event);
    }

    public function testKernelExceptionHandling()
    {
        $exception = new \Exception('testKernelExceptionHandling');
        $event = $this->createMock(GetResponseForExceptionEvent::class);
        $event
            ->expects($this->any())
            ->method('getException')
            ->willReturn($exception);

        $this->loggerMock
            ->expects($this->once())
            ->method('onChannel')
            ->with('general')
            ->willReturn($this->loggerMock);

        $this->loggerMock
            ->expects($this->once())
            ->method('error')
            ->with($exception->getMessage(), []);

        $this->listener->onKernelException($event);
    }
}
