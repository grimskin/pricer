<?php

namespace Tests\ShopCrawlingBundle\Service;

use PHPUnit\Framework\TestCase;
use ShopCrawlingBundle\Service\BaseSource;
use ShopCrawlingBundle\Service\SourceDetector;

class SourceDetectorTest extends TestCase
{
    /**
     * @var SourceDetector
     */
    private $detector;

    /**
     * @var BaseSource|\PHPUnit_Framework_MockObject_MockObject
     */
    private $source1;

    /**
     * @var BaseSource|\PHPUnit_Framework_MockObject_MockObject
     */
    private $source2;

    protected function setUp()
    {
        parent::setUp();

        $this->source1 = $this->createMock(BaseSource::class);
        $this->source2 = $this->createMock(BaseSource::class);

        $this->detector = new SourceDetector();
        $this->detector->addSource($this->source1);
        $this->detector->addSource($this->source2);
    }

    public function testUnknownUrl()
    {
        $url = 'definitely unknown URL';

        try {
            $this->detector->detectSource($url);
        } catch (\Exception $e) {
            $this->assertContains($url, $e->getMessage());
        }

        $this->assertTrue(isset($e));
    }

    public function testSourceDetection()
    {
        $this->source1
            ->expects($this->any())
            ->method('isSourceOf')
            ->willReturn(false);

        $this->source2
            ->expects($this->any())
            ->method('isSourceOf')
            ->willReturn(true);

        $foundSource = $this->detector->detectSource('');

        $this->assertSame($foundSource, $this->source2);
    }
}
