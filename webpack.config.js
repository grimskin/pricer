// webpack.config.js
var Encore = require('@symfony/webpack-encore');

Encore
    // directory where all compiled assets will be stored
    .setOutputPath('web/build/')

    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/build')

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // will output as web/build/app.js
    .addEntry('Admin/main', './assets/js/Admin/main.jsx')
    .addEntry('Admin/Admin', './assets/js/Admin/Admin.jsx')
    .addEntry('Admin/AdminNavBar', './assets/js/Admin/AdminNavBar.jsx')

    .addEntry('Logging/main', './assets/js/Logging/main.jsx')
    .addEntry('Logging/App', './assets/js/Logging/App.jsx')
    .addEntry('Logging/LogList', './assets/js/Logging/LogList.jsx')
    .addEntry('Logging/registerServiceWorker', './assets/js/Logging/registerServiceWorker.jsx')

    // will output as web/build/global.css
    .addStyleEntry('global', './assets/css/Admin/main.css')
    // .addStyleEntry('global', './assets/css/global.scss')

    // allow sass/scss files to be processed
    .enableSassLoader()

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())

    .enableReactPreset()

    // create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();
