#!/bin/sh
set -ex

echo "http://php.codecasts.rocks/v3.6/php-7.1" >> /etc/apk/repositories

echo "---> Installing PHP"

apk add --update \
    curl

apk --no-cache add \
    php7 \
    php7-ctype \
    php7-curl \
    php7-dom \
    php7-fileinfo \
    php7-fpm \
    php7-iconv \
    php7-json \
    php7-mbstring \
    php7-mongodb \
    php7-opcache \
    php7-openssl \
    php7-pdo \
    php7-pear \
    php7-phar \
    php7-posix \
    php7-redis \
    php7-session \
    php7-simplexml \
    php7-tokenizer \
    php7-xml \
    php7-xmlreader \
    php7-xmlwriter \
    php7-zlib && \
    echo "---> Installing Composer" && \
    curl -sS https://getcomposer.org/installer | /usr/bin/php7 -- --install-dir=/usr/local/bin --filename=composer && \
    echo "---> Cleaning up" && \
    rm -rf /tmp/*
