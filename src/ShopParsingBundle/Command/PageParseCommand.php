<?php

namespace ShopParsingBundle\Command;

use LoggingBundle\Interfaces\ChanneledLoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PageParseCommand extends Command
{
    private $logger;

    /**
     * PageParseCommand constructor.
     * @param ChanneledLoggerInterface $logger
     */
    public function __construct(ChanneledLoggerInterface $logger)
    {
        parent::__construct();

        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('shop-parser:page')
            ->setDescription('dev command for page parsing testing')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->onChannel('command')->debug('another message in the log');
    }
}
