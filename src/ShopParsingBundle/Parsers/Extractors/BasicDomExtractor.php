<?php

namespace ShopParsingBundle\Parsers\Extractors;

use ShopParsingBundle\Parsers\Interfaces\StringExtractorInterface;
use Symfony\Component\DomCrawler\Crawler;

class BasicDomExtractor implements StringExtractorInterface
{
    /**
     * @var string
     */
    private $selector;

    /**
     * @param string $selector
     */
    public function __construct(string $selector)
    {
        $this->selector = $selector;
    }

    /**
     * {@inheritdoc}
     */
    public function extract(Crawler $crawler): string
    {
        $matches = $crawler->filter($this->selector);

        return $matches->count() ? $matches->first()->html() : '';
    }
}
