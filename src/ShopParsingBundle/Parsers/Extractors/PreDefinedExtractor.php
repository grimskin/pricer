<?php

namespace ShopParsingBundle\Parsers\Extractors;

use ShopParsingBundle\Parsers\Interfaces\StringExtractorInterface;
use Symfony\Component\DomCrawler\Crawler;

class PreDefinedExtractor implements StringExtractorInterface
{
    /**
     * @var string
     */
    private $result;

    /**
     * PreDefinedExtractor constructor.
     * @param string $result
     */
    public function __construct(string $result)
    {
        $this->result = $result;
    }

    /**
     * {@inheritdoc}
     */
    public function extract(Crawler $crawler): string
    {
        return $this->result;
    }
}
