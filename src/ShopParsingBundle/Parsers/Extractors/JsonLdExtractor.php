<?php

namespace ShopParsingBundle\Parsers\Extractors;

use ShopParsingBundle\Parsers\Interfaces\StringExtractorInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 * @see https://en.wikipedia.org/wiki/JSON-LD
 */
class JsonLdExtractor implements StringExtractorInterface
{
    /**
     * @var string[]
     */
    private $pathItems = [];

    /**
     * JsonLdExtractor constructor.
     * @param string[] ...$pathItems
     */
    public function __construct(...$pathItems)
    {
        $this->pathItems = $pathItems;
    }

    /**
     * @param Crawler $crawler
     * @return array
     */
    private function parse(Crawler $crawler)
    {
        $ldNodes = $crawler->filter('script[type="application/ld+json"]');

        $decodedLD = $ldNodes->each(function (Crawler $crawler) {
            return json_decode($crawler->html(), true);
        });

        $data = [];

        foreach ($decodedLD as $markupItem) {
            if (isset($markupItem['@type'])) {
                $data[$markupItem['@type']] = $markupItem;
            }
        }

        return $data;
    }

    /**
     * @param Crawler $crawler
     * @return string
     */
    public function extract(Crawler $crawler): string
    {
        $result = $this->parse($crawler);

        foreach ($this->pathItems as $pathItem) {
            if (isset($result[$pathItem])) {
                $result = $result[$pathItem];
            } else {
                $result = '';

                break;
            }
        }

        return $result;
    }
}
