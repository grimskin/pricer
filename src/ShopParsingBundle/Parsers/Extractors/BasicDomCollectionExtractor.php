<?php

namespace ShopParsingBundle\Parsers\Extractors;

use ShopParsingBundle\DTO\FormatterDTO;
use ShopParsingBundle\Parsers\Interfaces\CollectionExtractorInterface;
use ShopParsingBundle\Parsers\Interfaces\StringExtractorInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 * @todo add tests
 */
class BasicDomCollectionExtractor implements CollectionExtractorInterface
{
    /**
     * @var string
     */
    private $selector;

    /**
     * @var StringExtractorInterface[]
     */
    private $fieldExtractors = [];

    /**
     * @var FormatterDTO[]
     */
    private $formatters = [];

    /**
     * @param string $selector
     */
    public function __construct(string $selector)
    {
        $this->selector = $selector;
    }

    /**
     * {@inheritdoc}
     */
    public function addFieldExtractor(string $fieldName, StringExtractorInterface $extractor)
    {
        $this->fieldExtractors[$fieldName] = $extractor;
    }

    /**
     * {@inheritdoc}
     */
    public function addFormatter(FormatterDTO $formatterDTO)
    {
        $this->formatters[] = $formatterDTO;
    }

    /**
     * {@inheritdoc}
     */
    public function extract(Crawler $crawler): array
    {
        $matches = $crawler->filter($this->selector);
        $extractors = $this->fieldExtractors;

        return $matches->count()
            ? $matches->each(function (Crawler $node, $i) use ($extractors) {
                $result = [];

                /**
                 * @var string $fieldName
                 * @var StringExtractorInterface $extractor
                 */
                foreach ($extractors as $fieldName=>$extractor) {
                    $result[$fieldName] = $this->formatValue($extractor->extract($node) , $fieldName);
                }

                return $result;
            })
            : [];
    }

    /**
     * @param string $value
     * @param string $key
     * @return string
     */
    private function formatValue(string $value, string $key)
    {
        /** @var FormatterDTO $formatterDTO */
        foreach ($this->formatters as $formatterDTO) {
            if ($formatterDTO->isApplicableTo($key)) {
                $value = $formatterDTO->getFormatter()->format($value);
            }
        }

        return $value;
    }
}
