<?php

namespace ShopParsingBundle\Parsers\Extractors;

use ShopParsingBundle\Parsers\Interfaces\StringExtractorInterface;
use Symfony\Component\DomCrawler\Crawler;

class AttributeDomExtractor implements StringExtractorInterface
{
    /**
     * @var string
     */
    private $selector;

    /**
     * @var string
     */
    private $attribute;

    /**
     * @param string $selector
     * @param string $attribute
     */
    public function __construct(string $selector, string $attribute)
    {
        $this->selector = $selector;
        $this->attribute = $attribute;
    }

    /**
     * {@inheritdoc}
     */
    public function extract(Crawler $crawler): string
    {
        $matches = $crawler->filter($this->selector);

        $result = $matches->count() ? $matches->first()->attr($this->attribute) : '';
        $result = ($result === null) ? '' : $result;

        return $result;
    }
}
