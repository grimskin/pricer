<?php

namespace ShopParsingBundle\Parsers\Interfaces;

use ShopParsingBundle\DTO\FormatterDTO;
use Symfony\Component\DomCrawler\Crawler;

interface CollectionExtractorInterface
{
    /**
     * @param Crawler $crawler
     * @return array
     */
    public function extract(Crawler $crawler): array;

    /**
     * @param string $fieldName
     * @param StringExtractorInterface $extractor
     * @return void
     */
    public function addFieldExtractor(string $fieldName, StringExtractorInterface $extractor);

    /**
     * @param FormatterDTO $formatterDTO
     * @return void
     */
    public function addFormatter(FormatterDTO $formatterDTO);
}
