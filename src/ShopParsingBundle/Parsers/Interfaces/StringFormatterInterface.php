<?php

namespace ShopParsingBundle\Parsers\Interfaces;

interface StringFormatterInterface
{
    /**
     * @param string $string
     * @return string
     */
    public function format(string $string): string;
}
