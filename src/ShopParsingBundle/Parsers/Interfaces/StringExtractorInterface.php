<?php

namespace ShopParsingBundle\Parsers\Interfaces;

use Symfony\Component\DomCrawler\Crawler;

interface StringExtractorInterface
{
    /**
     * @param Crawler $crawler
     * @return string
     */
    public function extract(Crawler $crawler): string;
}
