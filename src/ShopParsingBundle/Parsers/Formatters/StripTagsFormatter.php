<?php

namespace ShopParsingBundle\Parsers\Formatters;

use ShopParsingBundle\Parsers\Interfaces\StringFormatterInterface;

/**
 * @todo add tests
 */
class StripTagsFormatter implements StringFormatterInterface
{
    /**
     * {@inheritdoc}
     */
    public function format(string $string): string
    {
        $result = strip_tags($string);

        return $result;
    }
}
