<?php

namespace ShopParsingBundle\Parsers\Formatters;

use ShopParsingBundle\Parsers\Interfaces\StringFormatterInterface;

class RegExpFormatter implements StringFormatterInterface
{
    /**
     * @var string
     */
    private $regexp;

    /**
     * RegExpFormatter constructor.
     * @param string $regexp
     */
    public function __construct(string $regexp)
    {
        $this->regexp = $regexp;
    }

    /**
     * {@inheritdoc}
     */
    public function format(string $string): string
    {
        preg_match($this->regexp, $string, $matches);

        $result = isset($matches[1]) ? $matches[1] : '';

        return $result;
    }
}
