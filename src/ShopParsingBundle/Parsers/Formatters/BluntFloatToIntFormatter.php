<?php

namespace ShopParsingBundle\Parsers\Formatters;

use ShopParsingBundle\Parsers\Interfaces\StringFormatterInterface;

class BluntFloatToIntFormatter implements StringFormatterInterface
{
    /**
     * {@inheritdoc}
     */
    public function format(string $string): string
    {
        $preg = "'([0-9]*)'isu";

        preg_match_all($preg, $string, $matches);

        $result = isset($matches[1]) ? implode('', $matches[1]) : '';

        return $result;
    }
}
