<?php

namespace ShopParsingBundle\Parsers;

use ShopParsingBundle\DTO\CategoryDTO;
use ShopParsingBundle\DTO\ProductDTO;
use ShopParsingBundle\Parsers\Interfaces\CollectionExtractorInterface;
use ShopParsingBundle\Parsers\Interfaces\StringExtractorInterface;
use ShopParsingBundle\Parsers\Interfaces\StringFormatterInterface;
use Symfony\Component\DomCrawler\Crawler;

class ProductExtractor
{
    /**
     * @var Crawler
     */
    private $crawler;

    /**
     * @var StringExtractorInterface
     */
    private $nameExtractor;

    /**
     * @var StringFormatterInterface
     */
    private $nameFormatter;

    /**
     * @var StringExtractorInterface
     */
    private $priceExtractor;

    /**
     * @var StringExtractorInterface
     */
    private $originalPriceExtractor;

    /**
     * @var StringFormatterInterface
     */
    private $priceFormatter;

    /**
     * @var StringExtractorInterface
     */
    private $currencyExtractor;

    /**
     * @var StringFormatterInterface
     */
    private $currencyFormatter;

    /**
     * @var CollectionExtractorInterface
     */
    private $categoriesExtractor;

    /**
     * @param StringExtractorInterface $extractor
     */
    public function setNameExtractor(StringExtractorInterface $extractor)
    {
        $this->nameExtractor = $extractor;
    }

    /**
     * @param StringFormatterInterface $formatter
     */
    public function setNameFormatter(StringFormatterInterface $formatter)
    {
        $this->nameFormatter = $formatter;
    }

    /**
     * @param StringExtractorInterface $extractor
     */
    public function setPriceExtractor(StringExtractorInterface $extractor)
    {
        $this->priceExtractor = $extractor;
    }

    /**
     * @param StringFormatterInterface $formatter
     */
    public function setPriceFormatter(StringFormatterInterface $formatter)
    {
        $this->priceFormatter = $formatter;
    }

    /**
     * @param StringExtractorInterface $extractor
     */
    public function setOriginalPriceExtractor(StringExtractorInterface $extractor)
    {
        $this->originalPriceExtractor = $extractor;
    }

    /**
     * @param StringExtractorInterface $extractor
     */
    public function setCurrencyExtractor(StringExtractorInterface $extractor)
    {
        $this->currencyExtractor = $extractor;
    }

    /**
     * @param StringFormatterInterface $formatter
     */
    public function setCurrencyFormatter(StringFormatterInterface $formatter)
    {
        $this->currencyFormatter = $formatter;
    }

    /**
     * @param CollectionExtractorInterface $extractor
     */
    public function setCategoriesExtractor(CollectionExtractorInterface $extractor)
    {
        $this->categoriesExtractor = $extractor;
    }

    /**
     * @param Crawler $crawler
     */
    public function setCrawler(Crawler $crawler)
    {
        $this->crawler = $crawler;
    }

    /**
     * @return string
     */
    public function getName()
    {
        $result = $this->nameExtractor->extract($this->crawler);

        if ($this->nameFormatter) {
            $result = $this->nameFormatter->format($result);
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        $result = $this->priceExtractor->extract($this->crawler);

        if ($this->priceFormatter) {
            $result = $this->priceFormatter->format($result);
        }

        return (int)$result;
    }

    /**
     * @return int
     */
    public function getOriginalPrice()
    {
        $result = $this->originalPriceExtractor->extract($this->crawler);

        if ($this->priceFormatter) {
            $result = $this->priceFormatter->format($result);
        }

        return (int)$result;
    }

    /**
     * @return CategoryDTO[]
     */
    public function getCategories()
    {
        $result = $this->categoriesExtractor->extract($this->crawler);

        return $result;
    }

    /**
     * @return bool
     */
    public function isDeal(): bool
    {
        return ($this->getOriginalPrice() && ($this->getOriginalPrice() > $this->getPrice()));
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        $result = $this->currencyExtractor->extract($this->crawler);

        if ($this->currencyFormatter) {
            $result = $this->currencyFormatter->format($result);
        }

        return $result;
    }

    /**
     * @todo add attributes crawling
     * @todo test categories filtering
     *
     * @return ProductDTO
     */
    public function extractProductData(): ProductDTO
    {
        $result = new ProductDTO();

        $result->setName($this->getName());
        $result->setActualPrice($this->getPrice());
        $result->setFullPrice($this->getOriginalPrice() ?: $this->getPrice());
        $result->setCurrency($this->getCurrency());
        $result->setHasDeal($this->isDeal());

        $categoriesList = array_map(function($item) {
            $category = new CategoryDTO();
            $category->setName($item['name']);
            $category->setUrl($item['url']);

            return $category;
        }, $this->getCategories());

        $categoriesList = array_filter($categoriesList, function (CategoryDTO $categoryDTO) {
            return $categoryDTO->getName() && $categoryDTO->getUrl();
        });

        $result->setCategories($categoriesList);


        return $result;
    }
}
