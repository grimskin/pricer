<?php


namespace ShopParsingBundle\DTO;


use ShopParsingBundle\Parsers\Interfaces\StringFormatterInterface;

class FormatterDTO
{
    /**
     * @var StringFormatterInterface
     */
    private $formatter;

    /**
     * @var string[]
     */
    private $itemsToApply;

    /**
     * FormatterDTO constructor.
     * @param StringFormatterInterface $formatter
     * @param array $itemsToApply
     */
    public function __construct(StringFormatterInterface $formatter, array $itemsToApply = [])
    {
        $this->formatter = $formatter;
        $this->itemsToApply = $itemsToApply;
    }

    /**
     * @return StringFormatterInterface
     */
    public function getFormatter(): StringFormatterInterface
    {
        return $this->formatter;
    }

    /**
     * @return mixed
     */
    public function getItemsToApply()
    {
        return $this->itemsToApply;
    }

    public function isApplicableTo(string $key)
    {
        return !count($this->itemsToApply) || in_array($key, $this->itemsToApply);
    }
}
