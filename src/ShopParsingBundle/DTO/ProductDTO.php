<?php


namespace ShopParsingBundle\DTO;

class ProductDTO
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $actualPrice;

    /**
     * @var int
     */
    public $fullPrice;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var bool
     */
    public $hasDeal;

    /**
     * @var CategoryDTO[]
     */
    public $categories;

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param int $actualPrice
     */
    public function setActualPrice(int $actualPrice)
    {
        $this->actualPrice = $actualPrice;
    }

    /**
     * @param int $fullPrice
     */
    public function setFullPrice(int $fullPrice)
    {
        $this->fullPrice = $fullPrice;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @param bool $hasDeal
     */
    public function setHasDeal(bool $hasDeal)
    {
        $this->hasDeal = $hasDeal;
    }

    /**
     * @param CategoryDTO[] $categories
     */
    public function setCategories(array $categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getActualPrice(): int
    {
        return $this->actualPrice;
    }

    /**
     * @return int
     */
    public function getFullPrice(): int
    {
        return $this->fullPrice;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function isHasDeal(): bool
    {
        return $this->hasDeal;
    }

    /**
     * @return CategoryDTO[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }
}
