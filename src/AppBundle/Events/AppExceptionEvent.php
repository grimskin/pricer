<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;

class AppExceptionEvent extends Event
{
    /**
     * @var \Exception
     */
    private $exception;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var array
     */
    private $context;

    /**
     * AppExceptionEvent constructor.
     * @param \Exception $exception
     * @param string $channel
     * @param array $context
     */
    public function __construct(\Exception $exception, string $channel = '', $context = [])
    {
        $this->exception = $exception;
        $this->channel = $channel;
        $this->context = $context;
    }

    /**
     * @return \Exception
     */
    public function getException(): \Exception
    {
        return $this->exception;
    }

    /**
     * @return string
     */
    public function getChannel(): string
    {
        return $this->channel;
    }

    /**
     * @return string
     */
    public static function getId()
    {
        return 'custom_event.app_exception';
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }
}
