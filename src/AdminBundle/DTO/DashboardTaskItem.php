<?php

namespace AdminBundle\DTO;

class DashboardTaskItem
{
    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $caption;

    /**
     * @var int
     */
    private $count;

    /**
     * @var string
     */
    private $url;

    /**
     * DashboardTaskItem constructor.
     * @param string $alias
     * @param string $caption
     * @param int    $count
     * @param string $url
     */
    private function __construct(string $alias, string $caption, int $count, string $url)
    {
        $this->alias = $alias;
        $this->caption = $caption;
        $this->count = $count;
        $this->url = $url;

    }

    /**
     * @param string $alias
     * @param string $caption
     * @param int    $count
     * @param string $url
     * @return DashboardTaskItem
     */
    public static function create(string $alias, string $caption, int $count, string $url): DashboardTaskItem
    {
        return new self($alias, $caption, $count, $url);
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @return string
     */
    public function getCaption(): string
    {
        return $this->caption;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
