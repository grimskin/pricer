<?php

namespace AdminBundle\Service;

use AdminBundle\DTO\DashboardTaskItem;
use DataCollectingBundle\Document\Repository\ShopCategoryRepository;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class Dashboard
{
    /**
     * @var ShopCategoryRepository
     */
    private $shopCategoryRepository;

    /**
     * @var Router
     */
    private $router;

    /**
     * Dashboard constructor.
     * @param ShopCategoryRepository $shopCategoryRepository
     * @param Router                 $router
     */
    public function __construct(
        ShopCategoryRepository $shopCategoryRepository,
        Router $router
    ) {
        $this->shopCategoryRepository = $shopCategoryRepository;
        $this->router = $router;
    }

    /**
     * @return DashboardTaskItem[]
     */
    public function tasksInfo()
    {
        $result = [];

        $result[] = DashboardTaskItem::create(
            'task_unlinked_categories',
            'Unlinked Shop Categories',
            $this->shopCategoryRepository->getUnlinkedCount(),
            $this->getUrlByRoute('link_shop_categories')
        );

        return $result;
    }

    /**
     * @param string $route
     * @return string
     */
    private function getUrlByRoute(string $route)
    {
        return $this->router->generate($route);
    }
}
