<?php

namespace AdminBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

class ApiController extends FOSRestController
{
    /**
     * @Rest\Get("/dashboard")
     */
    public function dashboardAction()
    {
        $dashboard = $this->get('admin.service.dashboard');
        $result = $dashboard->tasksInfo();

        return ['tasks' => $result];
    }
}
