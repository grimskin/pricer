<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('AdminBundle:Default:index.html.twig');
    }

    /**
     * @Route("/link-shop-categories", name="link_shop_categories")
     */
    public function linkShopCategoriesAction()
    {
        return $this->render('AdminBundle:Default:index.html.twig');
    }
}
