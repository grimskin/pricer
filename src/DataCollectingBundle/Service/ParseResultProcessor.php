<?php


namespace DataCollectingBundle\Service;

use DataCollectingBundle\Document\ShopCategory;
use Doctrine\ODM\MongoDB\DocumentManager;
use ShopCrawlingBundle\DTO\CrawlResult;
use ShopCrawlingBundle\Service\BaseSource;
use ShopCrawlingBundle\Service\SourceDetector;
use ShopParsingBundle\DTO\CategoryDTO;
use ShopParsingBundle\DTO\ProductDTO;

class ParseResultProcessor
{
    /**
     * @var SourceDetector
     */
    private $sourceDetector;

    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * ParseResultProcessor constructor.
     *
     * @param SourceDetector $sourceDetector
     * @param DocumentManager $documentManager
     */
    public function __construct(
        SourceDetector $sourceDetector,
        DocumentManager $documentManager
    ) {
        $this->sourceDetector = $sourceDetector;
        $this->documentManager = $documentManager;
    }

    /**
     * @todo tests
     * @param CrawlResult $crawlResult
     */
    public function processParseResults(CrawlResult $crawlResult)
    {
        $source = $this->sourceDetector->getSourceByName($crawlResult->sourceName);

        if (!$source) {
            throw new \DomainException(sprintf('Unable to find source %s', $crawlResult->sourceName));
        }

        $this->saveShopProduct($crawlResult->getProductData());

        $categories = $crawlResult->getProductData()->getCategories();

        usort($categories, function(CategoryDTO $a, CategoryDTO $b) {
            return strlen($a->url) < strlen($b->url) ? -1 : 1;
        });

        $previousCategory = null;
        foreach ($categories as $categoryDTO) {
            $previousCategory = $this->saveShopCategory($categoryDTO, $source, $previousCategory);
        }

        $this->documentManager->flush();
    }

    private function saveShopProduct(ProductDTO $productDTO)
    {

    }

    /**
     * @todo make sure relative URL is relative
     * @todo tests and extract
     * @todo index?
     *
     * @param CategoryDTO $categoryDTO
     * @param BaseSource $source
     * @param ShopCategory|null $parentCategory
     *
     * @return ShopCategory
     */
    private function saveShopCategory(
        CategoryDTO $categoryDTO,
        BaseSource $source,
        ?ShopCategory $parentCategory = null
    ): ShopCategory {
        $repo = $this->documentManager->getRepository(ShopCategory::class);

        $category = $repo->findOneBy([
            'relativePageUrl' => $categoryDTO->getUrl(),
            'sourceName' => $source->getName()
        ]);

        if (!$category) {
            $category = new ShopCategory();
            $category->setSourceName($source->getName());
            $category->setName($categoryDTO->getName());
            $category->setRelativePageUrl($categoryDTO->getUrl());
            $category->setParentCategory($parentCategory);

            $this->documentManager->persist($category);
        }

        return $category;
    }
}
