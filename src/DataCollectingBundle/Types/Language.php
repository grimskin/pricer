<?php


namespace DataCollectingBundle\Types;


abstract class Language
{
    const EN_GB = 'EN_GB';
    const PL_PL = 'PL_PL';
}
