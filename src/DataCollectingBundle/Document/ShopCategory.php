<?php


namespace DataCollectingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(
 *     repositoryClass="DataCollectingBundle\Document\Repository\ShopCategoryRepository"
 * )
 */
class ShopCategory
{
    /**
     * @Mongo\Id()
     * @var string
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     * @var string
     */
    private $sourceName;

    /**
     * @Mongo\Field(type="string")
     * @var string
     */
    private $name;

    /**
     * @Mongo\Field(type="string")
     * @var string
     */
    private $relativePageUrl;

    /**
     * @Mongo\ReferenceOne(targetDocument="ShopCategory")
     * @var ?ShopCategory
     */
    private $parentCategory = null;

    /**
     * @todo index for ShopCategoryRepository::getUnlinkedCount ?
     * @Mongo\ReferenceOne(targetDocument="Category")
     * @var ?Category
     */
    private $linkedTo = null;

    /**
     * @Mongo\Field(type="int")
     * @var int
     */
    private $level = 0;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSourceName(): string
    {
        return $this->sourceName;
    }

    /**
     * @param string $sourceName
     */
    public function setSourceName(string $sourceName)
    {
        $this->sourceName = $sourceName;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getRelativePageUrl(): string
    {
        return $this->relativePageUrl;
    }

    /**
     * @param string $relativePageUrl
     */
    public function setRelativePageUrl(string $relativePageUrl)
    {
        $this->relativePageUrl = $relativePageUrl;

        $this->calculateLevel();
    }

    /**
     * @return mixed
     */
    public function getLinkedTo()
    {
        return $this->linkedTo;
    }

    /**
     * @return null|ShopCategory
     */
    public function getParentCategory(): ?ShopCategory
    {
        return $this->parentCategory;
    }

    /**
     * @param ShopCategory $parentCategory
     */
    public function setParentCategory(?ShopCategory $parentCategory)
    {
        $this->parentCategory = $parentCategory;
    }

    /**
     * @param mixed $linkedTo
     */
    public function setLinkedTo($linkedTo)
    {
        $this->linkedTo = $linkedTo;
    }

    /**
     * @return bool
     */
    public function isLinkedTo(): bool
    {
        return $this->linkedTo ? true : false;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     */
    public function setLevel(int $level)
    {
        $this->level = $level;
    }

    private function calculateLevel()
    {
        $this->level = array_reduce(explode('/', $this->relativePageUrl), function($carry, $item) {
            return $item ? ++$carry : $carry;
        }, 0);
    }
}
