<?php

namespace DataCollectingBundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class ShopCategoryRepository extends DocumentRepository
{
    /**
     * @return int
     */
    public function getUnlinkedCount(): int
    {
        $result = $this->createQueryBuilder()
            ->field('linkedTo')
            ->equals(null)
            ->count()
            ->getQuery()
            ->execute();

        return $result;
    }
}
