<?php


namespace DataCollectingBundle\Document;

use DataCollectingBundle\Types\Language;
use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document()
 */
class Category
{
    /**
     * @Mongo\Id()
     * @var string
     */
    private $id;

    /**
     * @Mongo\ReferenceOne(targetDocument="Category")
     * @var Category|null
     */
    private $parentCategory;

    /**
     * @Mongo\EmbedMany(targetDocument="Translation")
     * @var Translation[]
     */
    private $names = [];

    /**
     * @Mongo\Field(type="string")
     * @var string
     */
    private $slug;

    /**
     * @Mongo\Field(type="string")
     * @var string
     */
    private $path;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return Category|null
     */
    public function getParentCategory()
    {
        return $this->parentCategory;
    }

    /**
     * @param Category|null $parentCategory
     */
    public function setParentCategory($parentCategory)
    {
        $this->parentCategory = $parentCategory;
    }

    /**
     * @return string
     */
    public function getNames(): string
    {
        return $this->names;
    }

    /**
     * @param string $names
     */
    public function setNames(string $names)
    {
        $this->names = $names;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->path = $path;
    }

    /**
     * @todo test
     * @param string $language
     * @param string $fallbackLanguage
     *
     * @return string
     */
    public function getName(string $language, string $fallbackLanguage = Language::EN_GB): string
    {
        $translation = $this->getNameTranslationByLanguage($language);
        $translation = $translation ?? $this->getNameTranslationByLanguage($fallbackLanguage);

        if (!$translation) {
            throw new \DomainException(
                sprintf('No proper translation for category %s for languages %s or %s',
                    $this->id,
                    $language,
                    $fallbackLanguage
                )
            );
        }

        return $translation->getValue();
    }

    /**
     * @param string $language
     *
     * @return Translation|null
     */
    private function getNameTranslationByLanguage(string $language): ?Translation
    {
        return array_reduce($this->names, function (?Translation $carry, Translation $item) use ($language) {
            $carry = ($item->getLanguage() == $language) ? $item : null;

            return $carry;
        });
    }
}
