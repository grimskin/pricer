<?php


namespace DataCollectingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document()
 */
class Translation
{
    /**
     * @Mongo\Id()
     * @var string
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     * @var string
     */
    private $language;

    /**
     * @Mongo\Field(type="string")
     * @var string
     */
    private $value;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }
}
