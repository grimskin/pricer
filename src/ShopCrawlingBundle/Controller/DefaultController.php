<?php

namespace ShopCrawlingBundle\Controller;

use AppBundle\Events\AppExceptionEvent;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ShopCrawlingBundle\DTO\CrawlResult;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends FOSRestController
{
    /**
     * @Rest\Post("/", name="crawl_item")
     * @param Request $request
     * @return JsonResponse
     */
    public function crawlAction(Request $request)
    {
        $url = $request->request->get('url');

        try {
            $api = $this->get('shop_crawling.service.api');
            $result = $api->getDataFromUrl($url);

            $processor = $this->get('data_collecting.parse_result_processor');
            $processor->processParseResults($result);
        } catch (\Exception $e) {
            /** @todo make global exception handler */
            $eventDispatcher = $this->get('event_dispatcher');
            $eventDispatcher->dispatch(
                AppExceptionEvent::getId(),
                new AppExceptionEvent($e, 'crawling')
            );

            return new JsonResponse($e->getMessage(), 500);
        }

        return new JsonResponse(['crawl_result' => $result]);
    }
}
