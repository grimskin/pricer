<?php

namespace ShopCrawlingBundle\Service;

class SourceDetector
{
    /**
     * @var BaseSource[]
     */
    private $sources = [];

    /**
     * @param BaseSource $source
     */
    public function addSource(BaseSource $source)
    {
        $this->sources[] = $source;
    }

    /**
     * @todo test
     * @param string $sourceName
     *
     * @return null|BaseSource
     */
    public function getSourceByName(string $sourceName): ?BaseSource
    {
        foreach ($this->sources as $source) {
            if ($sourceName == $source->getName()) {
                $result = $source;

                break;
            }
        }

        return $result ?? null;
    }

    /**
     * @param string $url
     * @return BaseSource
     * @throws \Exception
     */
    public function detectSource(string $url)
    {
        foreach ($this->sources as $source) {
            if ($source->isSourceOf($url)) {
                return $source;
            }
        }

        throw new \Exception('No source found for URL: '.$url);
    }
}
