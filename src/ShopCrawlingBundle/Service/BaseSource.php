<?php

namespace ShopCrawlingBundle\Service;

use ScrapingBundle\Document\ShopPage;
use ShopCrawlingBundle\DTO\CrawlResult;
use ShopParsingBundle\Parsers\Interfaces\CollectionExtractorInterface;
use ShopParsingBundle\Parsers\ProductExtractor;
use Symfony\Component\DomCrawler\Crawler;

class BaseSource
{
    /**
     * @var string[]
     */
    private $urlPatterns = [];

    /**
     * @var ProductExtractor
     */
    private $productExtractor;

    /**
     * @var CollectionExtractorInterface
     */
    private $categoriesExtractors = [];

    /**
     * @var string
     */
    private $name = '';

    /**
     * @param string $url
     * @return bool
     */
    public function isSourceOf(string $url): bool
    {
        foreach ($this->urlPatterns as $pattern) {
            if (preg_match($pattern, $url)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $pattern
     */
    public function addUrlPattern(string $pattern)
    {
        $this->urlPatterns[] = $pattern;
    }

    /**
     * @return ProductExtractor
     */
    public function getProductExtractor(): ?ProductExtractor
    {
        return $this->productExtractor;
    }

    /**
     * @param ProductExtractor $productExtractor
     */
    public function setProductExtractor(ProductExtractor $productExtractor)
    {
        $this->productExtractor = $productExtractor;
    }

    /**
     * @param CollectionExtractorInterface $extractor
     */
    public function addCategoriesExtractor(CollectionExtractorInterface $extractor)
    {
        $this->categoriesExtractors[] = $extractor;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param ShopPage $shopPage
     * @return CrawlResult
     */
    public function crawlPage(ShopPage $shopPage)
    {
        $crawler = new Crawler($shopPage->getContent()->getHtml());
        $this->productExtractor->setCrawler($crawler);

        $result = new CrawlResult();
        $result->setSourceName($this->getName());
        $result->setUrl($shopPage->getUrl());
        $result->setProductData($this->productExtractor->extractProductData());

        /** @var CollectionExtractorInterface $extractor */
        foreach ($this->categoriesExtractors as $extractor) {
            $categories = $extractor->extract($crawler);
            foreach ($categories as $category) {
                $result->addCategory($category);
            }
        }

        return $result;
    }
}
