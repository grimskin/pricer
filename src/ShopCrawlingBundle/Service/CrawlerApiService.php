<?php


namespace ShopCrawlingBundle\Service;


use ScrapingBundle\Service\PageRetriever;
use ShopCrawlingBundle\DTO\CrawlResult;

class CrawlerApiService
{
    /**
     * @var SourceDetector
     */
    private $sourceDetector;
    /**
     * @var PageRetriever
     */
    private $pageRetriever;

    /**
     * CrawlerApiService constructor.
     * @param SourceDetector $sourceDetector
     * @param PageRetriever $pageRetriever
     */
    public function __construct(
        SourceDetector $sourceDetector,
        PageRetriever $pageRetriever
    ) {
        $this->sourceDetector = $sourceDetector;
        $this->pageRetriever = $pageRetriever;
    }

    /**
     * @param string $url
     * @return CrawlResult|null
     */
    function getDataFromUrl(string $url): ?CrawlResult
    {
        $result = null;

        $source = $this->sourceDetector->detectSource($url);

        $shopPage = $this->pageRetriever->retrieveUnknownPage($url);

        if ($shopPage) {
            $result = $source->crawlPage($shopPage);
        }

        return $result;
    }
}
