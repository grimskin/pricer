<?php

namespace ShopCrawlingBundle\DTO;


use ShopParsingBundle\DTO\ProductDTO;

class CrawlResult
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $sourceName;

    /**
     * @var ProductDTO
     */
    public $productData;

    /**
     * @var array
     */
    public $categories = [];

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @param string $sourceName
     */
    public function setSourceName(string $sourceName)
    {
        $this->sourceName = $sourceName;
    }

    /**
     * @param ProductDTO $productData
     */
    public function setProductData(ProductDTO $productData)
    {
        $this->productData = $productData;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getSourceName(): string
    {
        return $this->sourceName;
    }

    /**
     * @return ProductDTO
     */
    public function getProductData(): ProductDTO
    {
        return $this->productData;
    }

    /**
     * @param array $category
     */
    public function addCategory($category)
    {
        $this->categories[] = $category;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }
}
