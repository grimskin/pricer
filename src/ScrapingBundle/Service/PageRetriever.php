<?php


namespace ScrapingBundle\Service;

use ScrapingBundle\Document\Repository\ShopPageRepository;
use ScrapingBundle\Document\ShopPage;

class PageRetriever
{
    const CACHE_AGE = 'P3D';

    /**
     * @var DomainRequestParamsManager
     */
    private $paramsManager;

    /**
     * @var ShopPageRepository
     */
    private $shopPageRepository;

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @param DomainRequestParamsManager $paramsManager
     * @param ShopPageRepository $shopPageRepository
     * @param HttpClient $httpClient
     */
    public function __construct(
        DomainRequestParamsManager $paramsManager,
        ShopPageRepository $shopPageRepository,
        HttpClient $httpClient
    ) {
        $this->paramsManager = $paramsManager;
        $this->shopPageRepository = $shopPageRepository;
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $url
     * @param string $referer
     * @param bool $respectCache
     * @return null|ShopPage
     */
    public function retrieveUnknownPage(string $url, string $referer = '', $respectCache = true): ?ShopPage
    {
        $shopPage = $this->shopPageRepository->findEntryForUrl($url);

        if (!$shopPage) {
            $shopPage = $this->shopPageRepository->createNewEntry($url, $referer);
        }

        $oldestValid = new \DateTime();
        $oldestValid = $oldestValid->sub(new \DateInterval(self::CACHE_AGE));

        if ($respectCache && ($shopPage->getLastScrapedAt() < $oldestValid)) {
            $resultPage = $this->retrieveKnownPage($shopPage);

            if ($resultPage) {
                $resultPage->setLastScrapedAt(new \DateTime());
                $this->shopPageRepository->save($resultPage);
            } else {
                return null;
            }
        } else {
            $resultPage = $shopPage;
        }

        return $resultPage;
    }

    /**
     * @param ShopPage $shopPage
     * @return null|ShopPage
     */
    public function retrieveKnownPage(ShopPage $shopPage): ?ShopPage
    {
        $params = $this->paramsManager->getParamsForUrl($shopPage->getUrl());
        $content = $shopPage->getContent() ?? null;

        $resultPage = $this->httpClient->get($shopPage, $params, $content);

        return $resultPage;
    }
}
