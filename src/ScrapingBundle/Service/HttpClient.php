<?php


namespace ScrapingBundle\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use LoggingBundle\Interfaces\ChanneledLoggerInterface;
use ScrapingBundle\Document\DomainRequestParams;
use ScrapingBundle\Document\PageContent;
use ScrapingBundle\Document\ShopPage;

class HttpClient
{
    /**
     * @var ChanneledLoggerInterface
     */
    private $logger;

    /**
     * HttpClient constructor.
     * @param ChanneledLoggerInterface $logger
     */
    public function __construct(ChanneledLoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @todo redirect history?
     * @param ShopPage $page
     * @param DomainRequestParams $params
     * @param PageContent|null $pageContent
     * @return null|ShopPage
     */
    public function get(ShopPage $page, DomainRequestParams $params, PageContent $pageContent = null): ?ShopPage
    {
        $pageContent = $pageContent ?? new PageContent();
        $page->setContent($pageContent);

        $cookieJar = new CookieJar();
        if ($cookiesString = $params->getCookies()) {
            $cookies = \GuzzleHttp\json_decode($cookiesString, true);
            foreach ($cookies as $cookie) {
                $cookieJar->setCookie(SetCookie::fromString(implode(';', $cookie)));
            }
        }

        $requestOptions = [
            'headers' => [
                'Accept' => $params->getAccept(),
                'User-Agent' => $params->getUserAgent(),
                'Accept-Encoding' => $params->getAcceptEncoding(),
                'Accept-Language' => $params->getAcceptLanguage(),
                'Cache-Control' => $params->getCacheControl(),
                'Referer' => $page->getReferer(),
            ],
            'cookies' => $cookieJar,
        ];

        $client = new Client();
        $response = $client->request(
            'GET',
            $page->getUrl(),
            $requestOptions
        );

        if ($response->getStatusCode() == '200') {
            $pageContent->setHeaders(\GuzzleHttp\json_encode($response->getHeaders()));
            $pageContent->setHtml($response->getBody()->getContents());

            $params->setCookies(\GuzzleHttp\json_encode($cookieJar->toArray()));
        } else {
            $this->logger->onChannel('scraper')->error(
                sprintf(
                    'Failed to fetch %s with status code %s',
                    $page->getUrl(),
                    $response->getStatusCode()
                )
            );

            return null;
        }

        return $page;
    }
}
