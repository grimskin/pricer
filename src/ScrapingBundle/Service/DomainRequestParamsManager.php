<?php

namespace ScrapingBundle\Service;

use ScrapingBundle\Document\DomainRequestParams;
use ScrapingBundle\Document\Repository\DomainRequestParamsRepository;
use ScrapingBundle\Producers\AcceptEncodingProducer;
use ScrapingBundle\Producers\AcceptLanguageProducer;
use ScrapingBundle\Producers\AcceptProducer;
use ScrapingBundle\Producers\CacheControlProducer;
use ScrapingBundle\Producers\UserAgentProducer;

class DomainRequestParamsManager
{
    /**
     * @var DomainRequestParamsRepository
     */
    private $repository;

    /**
     * @param DomainRequestParamsRepository $repository
     */
    public function __construct(
        DomainRequestParamsRepository $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * @param string $url
     *
     * @return DomainRequestParams
     */
    public function getParamsForUrl(string $url)
    {
        $domain = parse_url($url, PHP_URL_HOST);

        return $this->getParamsForDomain($domain);
    }

    /**
     * @param string $domain
     *
     * @return DomainRequestParams
     */
    public function getParamsForDomain(string $domain)
    {
        $params = $this->repository->findOneBy(['domain' => $domain]);

        if (!$params) {
            $params = new DomainRequestParams();
            $params->setAccept(AcceptProducer::get());
            $params->setAcceptEncoding(AcceptEncodingProducer::get());
            $params->setAcceptLanguage(AcceptLanguageProducer::get());
            $params->setCacheControl(CacheControlProducer::get());
            $params->setUserAgent(UserAgentProducer::get());
            $params->setCookies('');
            $params->setDomain($domain);

            $this->save($params);
        }

        return $params;
    }

    /**
     * @param DomainRequestParams $entry
     */
    public function save(DomainRequestParams $entry)
    {
        $dm = $this->repository->getDocumentManager();

        $dm->persist($entry);
        $dm->flush();
    }
}
