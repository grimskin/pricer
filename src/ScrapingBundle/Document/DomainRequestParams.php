<?php

namespace ScrapingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(
 *     repositoryClass="ScrapingBundle\Document\Repository\DomainRequestParamsRepository"
 * )
 */
class DomainRequestParams
{
    /**
     * @Mongo\Id()
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     */
    private $domain;

    /**
     * @Mongo\Field(type="string")
     */
    private $userAgent;

    /**
     * @Mongo\Field(type="string")
     */
    private $cookies;

    /**
     * @Mongo\Field(type="string")
     */
    private $accept;

    /**
     * @Mongo\Field(type="string")
     */
    private $acceptEncoding;

    /**
     * @Mongo\Field(type="string")
     */
    private $acceptLanguage;

    /**
     * @Mongo\Field(type="string")
     */
    private $cacheControl;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param mixed $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * @return mixed
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @param mixed $userAgent
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    }

    /**
     * @return mixed
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * @param mixed $cookies
     */
    public function setCookies($cookies)
    {
        $this->cookies = $cookies;
    }

    /**
     * @return mixed
     */
    public function getAccept()
    {
        return $this->accept;
    }

    /**
     * @param mixed $accept
     */
    public function setAccept($accept)
    {
        $this->accept = $accept;
    }

    /**
     * @return mixed
     */
    public function getAcceptEncoding()
    {
        return $this->acceptEncoding;
    }

    /**
     * @param mixed $acceptEncoding
     */
    public function setAcceptEncoding($acceptEncoding)
    {
        $this->acceptEncoding = $acceptEncoding;
    }

    /**
     * @return mixed
     */
    public function getAcceptLanguage()
    {
        return $this->acceptLanguage;
    }

    /**
     * @param mixed $acceptLanguage
     */
    public function setAcceptLanguage($acceptLanguage)
    {
        $this->acceptLanguage = $acceptLanguage;
    }

    /**
     * @return mixed
     */
    public function getCacheControl()
    {
        return $this->cacheControl;
    }

    /**
     * @param mixed $cacheControl
     */
    public function setCacheControl($cacheControl)
    {
        $this->cacheControl = $cacheControl;
    }
}
