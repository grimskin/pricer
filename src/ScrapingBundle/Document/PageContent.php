<?php


namespace ScrapingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document()
 */
class PageContent
{
    /**
     * @Mongo\Id()
     * @var string
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     * @var string
     */
    private $headers;

    /**
     * @Mongo\Field(type="string")
     * @var string
     */
    private $html;

    /**
     * @Mongo\Field(type="date")
     * @var \DateTime
     */
    private $scrapedAt;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHeaders(): string
    {
        return $this->headers;
    }

    /**
     * @param string $headers
     */
    public function setHeaders(string $headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @param string $html
     */
    public function setHtml($html)
    {
        $this->html = $html;
    }

    /**
     * @return \DateTime
     */
    public function getScrapedAt()
    {
        return $this->scrapedAt;
    }

    /**
     * @param \DateTime $scrapedAt
     */
    public function setScrapedAt(\DateTime $scrapedAt)
    {
        $this->scrapedAt = $scrapedAt;
    }
}
