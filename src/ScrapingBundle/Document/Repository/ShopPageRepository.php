<?php

namespace ScrapingBundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use ScrapingBundle\Document\ShopPage;

class ShopPageRepository extends DocumentRepository
{
    /**
     * @param string $url
     * @return null|ShopPage
     */
    public function findEntryForUrl(string $url): ?ShopPage
    {
        /** @var ShopPage|null $entry */
        $entry = $this->findOneBy(['url' => $url]);

        return $entry;
    }

    /**
     * @param string $url
     * @param string $referer
     * @return ShopPage
     */
    public function createNewEntry(string $url, string $referer = ''): ShopPage
    {
        $entry = new ShopPage();
        $entry->setUrl($url);
        $entry->setReferer($referer);
        $entry->setLastScrapedAt(new \DateTime('2000-01-01 00:00:00'));

        $this->getDocumentManager()->persist($entry);
        $this->getDocumentManager()->flush();

        return $entry;
    }

    /**
     * @param ShopPage $page
     */
    public function save(ShopPage $page)
    {
        $dm = $this->getDocumentManager();

        $dm->persist($page);
        $dm->flush();
    }
}
