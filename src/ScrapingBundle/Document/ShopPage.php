<?php


namespace ScrapingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(
 *     repositoryClass="ScrapingBundle\Document\Repository\ShopPageRepository"
 * )
 * @Mongo\Indexes({
 *     @Mongo\Index(keys={"url"}, options={"unique"=true})
 * })
 * @todo check, wtf is wrong with indexes
 */
class ShopPage
{
    /**
     * @Mongo\Id()
     * @var string
     */
    private $id;

    /**
     * @Mongo\Field(type="string")
     * @Mongo\Index(name="idx_url", unique=true)
     * @var string
     */
    private $url;

    /**
     * @Mongo\Field(type="string")
     * @var string
     */
    private $referer;

    /**
     * @Mongo\Field(type="date")
     * @var \DateTime
     */
    private $lastScrapedAt;

    /**
     * @Mongo\ReferenceOne(targetDocument="PageContent", cascade={"all"})
     * @var PageContent
     */
    private $content;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getReferer(): string
    {
        return $this->referer;
    }

    /**
     * @param string $referer
     */
    public function setReferer(string $referer)
    {
        $this->referer = $referer;
    }

    /**
     * @return \DateTime
     */
    public function getLastScrapedAt()
    {
        return $this->lastScrapedAt;
    }

    /**
     * @param \DateTime $lastScrapedAt
     */
    public function setLastScrapedAt($lastScrapedAt)
    {
        $this->lastScrapedAt = $lastScrapedAt;
    }

    /**
     * @return PageContent
     */
    public function getContent(): ?PageContent
    {
        return $this->content;
    }

    /**
     * @param PageContent $content
     */
    public function setContent(?PageContent $content)
    {
        $this->content = $content;
    }
}
