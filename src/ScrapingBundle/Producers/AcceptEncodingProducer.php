<?php

namespace ScrapingBundle\Producers;

class AcceptEncodingProducer extends AbstractProducer
{
    /**
     * {@inheritdoc}
     */
    protected static function getItems(): array
    {
        return [
            'gzip, deflate, br'
        ];
    }
}
