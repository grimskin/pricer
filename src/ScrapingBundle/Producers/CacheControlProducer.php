<?php

namespace ScrapingBundle\Producers;

class CacheControlProducer extends AbstractProducer
{
    /**
     * {@inheritdoc}
     */
    protected static function getItems(): array
    {
        return [
            'max-age=0'
        ];
    }
}
