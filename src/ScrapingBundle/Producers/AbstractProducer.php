<?php


namespace ScrapingBundle\Producers;

abstract class AbstractProducer
{
    /**
     * @return string[]
     */
    abstract protected static function getItems(): array;

    /**
     * @return string
     */
    public static function get(): string
    {
        $items = static::getItems();

        return $items[mt_rand(0, count($items)-1)];
    }

}
