<?php


namespace ScrapingBundle\Producers;


class AcceptProducer extends AbstractProducer
{
    /**
     * {@inheritdoc}
     */
    protected static function getItems(): array
    {
        return [
            'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
        ];
    }
}
