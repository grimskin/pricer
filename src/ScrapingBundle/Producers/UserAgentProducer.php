<?php


namespace ScrapingBundle\Producers;

class UserAgentProducer extends AbstractProducer
{
    /**
     * {@inheritdoc}
     */
    protected static function getItems(): array
    {
        return [
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'
        ];
    }
}
