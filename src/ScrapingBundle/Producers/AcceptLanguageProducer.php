<?php

namespace ScrapingBundle\Producers;

class AcceptLanguageProducer extends AbstractProducer
{
    /**
     * {@inheritdoc}
     */
    protected static function getItems(): array
    {
        return [
            'en-US,en;q=0.8,uk;q=0.6'
        ];
    }
}
