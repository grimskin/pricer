<?php

namespace LoggingBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use LoggingBundle\Document\LogEntry;
use LoggingBundle\Interfaces\ChanneledLoggerInterface;
use Symfony\Bridge\Doctrine\ManagerRegistry;

class MongoLogger implements ChanneledLoggerInterface
{
    /**
     * @var string
     */
    private $channel = '';

    /**
     * @var ObjectManager
     */
    private $dm;

    /**
     * MongoLogger constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->dm = $managerRegistry->getManager();
    }

    /**
     * {@inheritdoc}
     */
    public function emergency($message, array $context = [])
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function alert($message, array $context = [])
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function critical($message, array $context = [])
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function error($message, array $context = [])
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function warning($message, array $context = [])
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function notice($message, array $context = [])
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function info($message, array $context = [])
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function debug($message, array $context = [])
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function onChannel(string $channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function log($level, $message, array $context = [])
    {
        $logEntry = new LogEntry();
        $logEntry->setChannel($this->channel);
        $logEntry->setLevel($level);
        $logEntry->setMessage($message);
        $logEntry->setContext($context);

        $this->dm->persist($logEntry);
        $this->dm->flush();

        $this->channel = '';
    }
}
