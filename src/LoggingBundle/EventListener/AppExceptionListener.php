<?php

namespace LoggingBundle\EventListener;

use AppBundle\Events\AppExceptionEvent;
use LoggingBundle\Interfaces\ChanneledLoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class AppExceptionListener
{
    /**
     * @var ChanneledLoggerInterface
     */
    private $logger;

    /**
     * AppExceptionListener constructor.
     * @param ChanneledLoggerInterface $logger
     */
    public function __construct(ChanneledLoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param AppExceptionEvent $event
     */
    public function onAppException(AppExceptionEvent $event)
    {
        $this->logException($event->getException(), $event->getChannel(), $event->getContext());
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $this->logException($event->getException(), 'general', []);
    }

    /**
     * @param \Exception $exception
     * @param string $channel
     * @param $context
     */
    private function logException(\Exception $exception, $channel, $context)
    {
        $this->logger->onChannel($channel)->error($exception->getMessage(), $context);
    }
}
