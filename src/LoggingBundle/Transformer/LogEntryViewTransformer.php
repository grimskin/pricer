<?php

namespace LoggingBundle\Transformer;

use LoggingBundle\Document\LogEntry;

class LogEntryViewTransformer
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $level;

    /**
     * @var string
     */
    public $channel;

    /**
     * @var string
     */
    public $message;

    /**
     * @var string
     */
    public $createdAt;

    /**
     * @param LogEntry $entry
     * @return LogEntryViewTransformer
     */
    public static function fromDocument(LogEntry $entry)
    {
        $result = new self();

        $result->id = $entry->getId();
        $result->level = $entry->getLevel();
        $result->channel = $entry->getChannel();
        $result->message = $entry->getMessage();
        $result->createdAt = $entry->getCreatedAt()->format('Y-m-d H:i:s');

        return $result;
    }
}
