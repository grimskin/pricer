<?php

namespace LoggingBundle\Interfaces;

use Psr\Log\LoggerInterface;

interface ChanneledLoggerInterface extends LoggerInterface
{
    /**
     * @param string $channel
     * @return ChanneledLoggerInterface
     */
    public function onChannel(string $channel);
}