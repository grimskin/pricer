<?php

namespace LoggingBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use LoggingBundle\Transformer\LogEntryViewTransformer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends FOSRestController
{
    /**
     * @Route("/")
     * @todo make it REST
     */
    public function indexAction()
    {
        $repo = $this->get('logging.repository.log_entry');
        $entries = $repo->last100();

        $result = [];
        foreach ($entries as $entry) {
            $result[] = LogEntryViewTransformer::fromDocument($entry);
        }

        return new JsonResponse(['logs' => $result]);
    }
}
