<?php

namespace LoggingBundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use LoggingBundle\Document\LogEntry;

class LogEntryRepository extends DocumentRepository
{
    /**
     * @return LogEntry[]
     */
    public function last100()
    {
        $result = $this->createQueryBuilder()
            ->sort('createdAt', 'DESC')
            ->limit(100)
            ->getQuery()
            ->execute()
            ->toArray();

        return $result;
    }
}
