<?php

namespace LoggingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as Mongo;

/**
 * @Mongo\Document(
 *     repositoryClass="LoggingBundle\Document\Repository\LogEntryRepository"
 * )
 */
class LogEntry
{
    /**
     * @Mongo\Id()
     */
    private $id;
    /**
     * @Mongo\Field(type="string")
     */
    private $level;
    /**
     * @Mongo\Field(type="string")
     */
    private $channel;
    /**
     * @Mongo\Field(type="string")
     */
    private $message;
    /**
     * @Mongo\Field(type="collection")
     */
    private $context;
    /**
     * @Mongo\Field(type="date")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param mixed $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param mixed $context
     */
    public function setContext($context)
    {
        $this->context = $context;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt ?? new \DateTime('1980-01-01 00:00:00');
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}